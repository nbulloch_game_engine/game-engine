package audioEngine.audio;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL11.*;

public class AudioTest {

	public static void main(String[] args) throws InterruptedException{
		AudioMaster.init();
		AudioMaster.setListenerPosition(0, 0, 0);
		alDistanceModel(AL_EXPONENT_DISTANCE_CLAMPED);
		
		int buffer = AudioMaster.loadSound("audioEngine/audio/bounce.wav");
		Source source = new Source();
		source.setLooping(true);
		source.play(buffer);
		source.setPosition(20, 4, 0);
		float xPos = 20;
		
		char c = ' ';
		while(c != 'q'){
			xPos -= 0.02f;
			source.setPosition(xPos, 4, 0);
			Thread.sleep(10);
		}
	}
	
}
