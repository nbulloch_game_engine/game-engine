package audioEngine.audio;

import static org.lwjgl.openal.AL10.*;

import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.util.WaveData;


public class AudioMaster {

	private static ArrayList<Integer> buffers = new ArrayList<Integer>();
	
	public static void init(){
		try {
			AL.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	public static void setListenerPosition(float x, float y, float z){
		alListener3f(AL_POSITION,x,y,z);
	}
	
	public static void setListenerVelocity(float x, float y, float z){
		alListener3f(AL_VELOCITY,x,y,z);	
	}
	
	public static int loadSound(String file){
		int buffer = alGenBuffers();
		buffers.add(buffer);
		WaveData waveFile = WaveData.create(file);
		alBufferData(buffer, waveFile.format, waveFile.data, waveFile.samplerate);
		waveFile.dispose();
		return buffer;
	}
	
	public static void clean(){
		for(int buffer : buffers){
			alDeleteBuffers(buffer);
		}
		AL.destroy();
	}
}
