package physicsEngine.physicsEngine;

import java.util.Set;

import org.lwjgl.util.vector.Vector3f;

public class SIEuler extends Integrator{
	
	boolean alternating = true;
	boolean forward = true;
	
	@Override
	public void integrate(Set<PhysicsEntity> entities, float delta) {
		for(PhysicsEntity entity : entities){
			if(forward) {
				Vector3f delta_v = (Vector3f) new Vector3f(entity.getAcceleration(0)).scale(delta);
				Vector3f.add(entity.getVelocity(), delta_v, entity.getVelocity());
				Vector3f delta_p = (Vector3f) new Vector3f(entity.getVelocity()).scale(delta);
				Vector3f.add(entity.getPostition(), delta_p, entity.getPostition());
			}else {
				Vector3f delta_p = (Vector3f) new Vector3f(entity.getVelocity()).scale(delta);
				Vector3f.add(entity.getPostition(), delta_p, entity.getPostition());
				Vector3f delta_v = (Vector3f) new Vector3f(entity.getAcceleration(0)).scale(delta);
				Vector3f.add(entity.getVelocity(), delta_v, entity.getVelocity());
			}if(alternating)
				forward = !forward;
		}
	}
	
	public void alternate(boolean alternating) { 
		this.alternating = alternating;
	}
	
}
