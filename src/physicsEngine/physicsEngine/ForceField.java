package physicsEngine.physicsEngine;

import org.lwjgl.util.vector.Vector3f;

public class ForceField extends Force {
	
	private static final float RADIUS = 3;
	private static final float R_R = RADIUS * RADIUS;
	
	@Override
	public void apply(PhysicsEntity entity, double delta) {
		Vector3f center = new Vector3f(10,10,10);
		Vector3f df = Vector3f.sub(center, entity.getPostition(), null);
		if(df.lengthSquared()>R_R) {
			df.scale(entity.getVelocity().lengthSquared());
			entity.addForce(df);
		}
	}

}
