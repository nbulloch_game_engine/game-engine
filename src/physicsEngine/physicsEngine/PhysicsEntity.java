package physicsEngine.physicsEngine;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector3f;

import physicsEngine.collisionDetection.primitives.AABB;

public class PhysicsEntity {

	private AABB aabb;//Bounding Volume
	
	private Vector3f velocity;
	private Vector3f position;
	
	private Vector3f angularVelocity;
	private Vector3f angularPosition;
	
	private float inverseMass;
	private Vector3f centerOfMass;
	private Matrix3f inverseInertiaTensor;
	
	private Vector3f netTorque;
	private Vector3f netForce;
	private ArrayList<Force> forces;
	
	public PhysicsEntity(Vector3f centerOfMass, Matrix3f inertiaTensor, float mass){
		netForce = new Vector3f();
		netTorque = new Vector3f();
		
		forces = new ArrayList<Force>();
		inverseMass = 1.0f / mass;
	}
	
	/**
	 * Sets all null vectors to zero
	 */
	public void initialize() {
		if(velocity==null)
			velocity = new Vector3f();
		if(position==null)
			position = new Vector3f();
	}
	
	public void setPostion(Vector3f postion) {
		this.position = postion;
	}
	
	public Vector3f getPostition() {
		return position;
	}
	
	/**
	 * Physically, setting the velocity should only occur during collision resolution.
	 * @param velocity The entity's new velocity
	 */
	protected void setVelocity(Vector3f velocity) {
		this.velocity = velocity;
	}
	
	public Vector3f getVelocity() {
		return velocity;
	}
	
	public void setInfiniteMass() {
		inverseMass = 0;
	}
	
	public static final Vector3f G = new Vector3f(0, -9.81f, 0);
	
	public void setZeroMass() {
		inverseMass = Float.POSITIVE_INFINITY;
	}
	
	/**
	 * Sets the inverseMass by calculating 1 / mass. No zero masses.
	 * @param mass The new mass of this entity
	 */
	public void setMass(float mass) {
		inverseMass = 1.0f / mass;
	}
	
	public float getInverseMass() { 
		return inverseMass;
	}
	
	/**
	 * Gets the entity's acceleration at time = current time + delta
	 * @param delta The time since the last physics loop (used for multi-step integrators)
	 * @return The entity's acceleration
	 */
	public Vector3f getAcceleration(double delta) {
		Vector3f acceleration = netForce;
		acceleration.scale(inverseMass);
		Vector3f.add(acceleration, Gravity.G, acceleration);
		netForce = new Vector3f();
		return acceleration;
	}
	
	/**
	 * @param force The force to be applied to the center of mass this frame
	 */
	public void addForce(Vector3f force) {
		Vector3f.add(force, netForce, netForce);
	}
	
	/**
	 * @param force The force to apply at point
	 * @param point The point of application used to determine the torque generated
	 */
	public void addForce(Vector3f force, Vector3f point) {
		addForce(force);
		Vector3f r = Vector3f.sub(point, centerOfMass, null);
		Vector3f torque = Vector3f.cross(r, force, null);
		Vector3f.add(netTorque, torque, netTorque);
	}
}
