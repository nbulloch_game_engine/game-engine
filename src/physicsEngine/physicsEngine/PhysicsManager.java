package physicsEngine.physicsEngine;

import java.util.ArrayList;
import java.util.HashMap;
import renderEngine.renderEngine.DisplayManager;

public class PhysicsManager {

	private static Integrator integrator;
	private static HashMap<PhysicsEntity, ArrayList<Force>> registry;
	
	public static void init() {
		integrator = new SIEuler();
	}
	
	public static void update() {
		integrator.integrate(registry.keySet(), DisplayManager.getFrameTimeSeconds());
	}
	
	public static void evaluateForces(float delta) {
		for(PhysicsEntity entity : registry.keySet()) {
			for(Force force : registry.get(entity)) {
				force.apply(entity, delta);
			}
		}
	}
}
