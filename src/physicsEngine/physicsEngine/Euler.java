package physicsEngine.physicsEngine;

import java.util.Set;

import org.lwjgl.util.vector.Vector3f;

public class Euler extends Integrator{

	@Override
	public void integrate(Set<PhysicsEntity> entities, float delta) {
		for(PhysicsEntity entity : entities){
			Vector3f delta_v = (Vector3f) entity.getAcceleration(0).scale(delta);
			Vector3f delta_p = (Vector3f) entity.getVelocity().scale(delta);
			Vector3f.add(entity.getVelocity(), delta_v, entity.getVelocity());
			Vector3f.add(entity.getPostition(), delta_p, entity.getPostition());
		}
		
	}
	
}
