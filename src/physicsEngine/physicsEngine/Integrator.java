package physicsEngine.physicsEngine;

import java.util.Set;

public abstract class Integrator {

	public abstract void integrate(Set<PhysicsEntity> entities, float delta);
	
}
