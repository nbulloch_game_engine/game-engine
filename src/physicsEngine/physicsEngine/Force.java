package physicsEngine.physicsEngine;

public abstract class Force{

	public abstract void apply(PhysicsEntity entity, double delta);
	
}
