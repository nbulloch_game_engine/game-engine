package physicsEngine.physicsEngine;

import org.lwjgl.util.vector.Vector3f;

public class Gravity extends Force{

	public static final Vector3f G = new Vector3f(0, -9.81f, 0);
	
	@Override
	public void apply(PhysicsEntity entity, double delta) {
		Vector3f force = new Vector3f(G);
		force.scale(1.0f / entity.getInverseMass());
		entity.addForce(force);
	}

}
