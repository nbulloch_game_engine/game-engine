package physicsEngine.collisionDetection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import physicsEngine.collisionDetection.primitives.AABB;
import physicsEngine.collisionDetection.primitives.ConvexHull;
import physicsEngine.collisionDetection.primitives.Plane;
import physicsEngine.collisionDetection.primitives.Ray;
import physicsEngine.collisionDetection.primitives.Triangle;
import renderEngine.objConverter.OBJWriter;

public class CollisionDetection {
	
//	public static Vector3f rayPlaneI(Plane P, Ray L){
//		Vector3f ray = Vector3f.sub(L.v1, L.v0, null);
//		Vector3f toPointOnPlane = Vector3f.sub(P.v0, L.v0, null);
//		float toPointOnPlanePrime = Vector3f.dot(toPointOnPlane, P.n);
//		float rayPrime = Vector3f.dot(ray, P.n);
//		Vector3f rayOriginToPlane = (Vector3f)ray.scale(toPointOnPlanePrime/rayPrime);
//		Vector3f intersectionPoint = Vector3f.add(rayOriginToPlane, L.v0, null);
//		return intersectionPoint;
//	}
	
	public static Vector3f aabbRayI(AABB aabb, Ray ray){
		Vector3f max = Vector3f.sub(aabb.getMax(), ray.v0, null);
		Vector3f min = Vector3f.sub(aabb.getMin(), ray.v0, null);
		Vector3f direction = Vector3f.sub(ray.v1, ray.v0, null);
		
		float xScale1 = min.x/direction.x;
		float xScale2 = max.x/direction.x;
		float xScaleMin = xScale1 < xScale2 ? xScale1 : xScale2;
		float xScaleMax = xScale1 > xScale2 ? xScale1 : xScale2;
		
		float yScale1 = min.y/direction.y;
		float yScale2 = max.y/direction.y;
		float yScaleMin = yScale1 < yScale2 ? yScale1 : yScale2;
		float yScaleMax = yScale1 > yScale2 ? yScale1 : yScale2;
		
		float zScale1 = min.z/direction.z;
		float zScale2 = max.z/direction.z;
		float zScaleMin = zScale1 < zScale2 ? zScale1 : zScale2;
		float zScaleMax = zScale1 > zScale2 ? zScale1 : zScale2;
		
		float largestMin = xScaleMin;
		float smallestMax = xScaleMax;
		largestMin = largestMin<yScaleMin ? yScaleMin : largestMin;
		largestMin = largestMin<zScaleMin ? zScaleMin : largestMin;
		smallestMax = smallestMax>yScaleMax ? yScaleMax : smallestMax;
		smallestMax = smallestMax>zScaleMax ? zScaleMax : smallestMax;
		if(largestMin<=smallestMax){
			direction.scale(largestMin);
			Vector3f intersection = direction;
			Vector3f.add(intersection, ray.v0, intersection);
			return intersection;
		}
		return null;
	}
	
	public static Vector3f toNearestSide(AABB aabb, Vector3f point){
		Vector3f min = Vector3f.sub(aabb.getCenter(), aabb.getRadii(), null);
		Vector3f max = Vector3f.add(aabb.getCenter(), aabb.getRadii(), null);
		Vector3f distanceX = point.x > aabb.getCenter().x ? new Vector3f(max.x - point.x, 0, 0) : new Vector3f(min.x-point.x, 0 ,0);
		Vector3f distanceY = point.y > aabb.getCenter().y ? new Vector3f(0, max.y - point.y, 0) : new Vector3f(0, min.y-point.y, 0);
		Vector3f distanceZ = point.z > aabb.getCenter().z ? new Vector3f(0, 0, max.z - point.z) : new Vector3f(0, 0, min.z-point.z);
		Vector3f nearest = distanceX;
		if(nearest.length() > distanceY.length())
			nearest = distanceY;
		if(nearest.length() > distanceZ.length())
			nearest = distanceZ;
		return nearest;
	}
	
	//intersection = v0 + su + tv
	//u = T.v1 - T.v0
	//v = T.v2 - T.v0
	public static Vector3f getRayTriangleI(Triangle T, Ray ray){
		Vector3f planeIntersection = rayPlaneI(T.getP(), ray);
		if(planeIntersection != null){
			Vector2f coords = getParametricCoords(T, planeIntersection);//returns (s, t)
			if(coords.x >= 0 && coords.y >= 0 && (coords.x+coords.y) <= 1){
				return planeIntersection;
			}
		}
		return null;
	}
	
	//possibly unused
	public static boolean intersectsTriangle(Triangle T, Ray ray){
		Vector3f planeIntersection = rayPlaneI(T.getP(), ray);
		if(planeIntersection != null){
			Vector2f coords = getParametricCoords(T, planeIntersection);
			if((coords.x >= 0 || coords.y >= 0) && coords.length() <= 1){
				return true;
			}
		}
		return false;
	}
	
	//returns vec2(s, t) where V(s, t) = v0 + su + tv
	//s and t are scalars
	//u = v1 - v0 and v = v2 - v0 in triangle T: v0, v1, and v2
	//T is in Plane P
	//w = p1 - v0 where p1 is in P
	//w = su + tv
	//v0 = V(0, 0), v1 = V(1, 0), and v2 = V(0, 1)
	//the coordinate is inside T when: s >= 0 || t >= 0 || s + t <= 1
	
	//		(u�v)(w�v) - (v�v)(w�u)
	// sI = -----------------------
	//		 (u�v)^2 - (u�u)(v�v)
	
	//		(u�v)(w�u) - (u�u)(w�v)
	// tI = -----------------------
	//		 (u�v)^2 - (u�u)(v�v)
	
	//http://geomalgorithms.com/a06-_intersect-2.html
	public static Vector2f getParametricCoords(Triangle T, Vector3f p1){
		Vector3f u = Vector3f.sub(T.v1, T.v0, null);
		Vector3f v = Vector3f.sub(T.v2, T.v0, null);
		Vector3f w = Vector3f.sub(p1, T.v0, null);
		float u_v = Vector3f.dot(u, v);
		float w_v = Vector3f.dot(w, v);
		float v_v = Vector3f.dot(v, v);
		float w_u = Vector3f.dot(w, u);
		float u_u = Vector3f.dot(u, u);
		double denominator = u_v*u_v - u_u*v_v;
		float s = (float) ((u_v*w_v - v_v*w_u)/denominator);
		float t = (float) ((u_v*w_u - u_u*w_v)/denominator);
		return new Vector2f(s, t);
	}
	
//	public static boolean intersectsOOB(OBB oobA, OBB oobB){
//		Vector3f[] an = oobA.getNormals();
//		Vector3f[] bn = oobB.getNormals();
//		Vector3f[] ap = oobA.getPoints();
//		Vector3f[] bp = oobB.getPoints();
//		for(Vector3f anx : an){
//			if(!intersectsOnAxis(ap, bp, anx)){
//				return false;
//			}
//		}for(Vector3f bnx : bn){
//			if(!intersectsOnAxis(ap, bp, bnx)){
//				return false;
//			}
//		}for(Vector3f anx : an){
//			for(Vector3f bnx : bn){
//				if(!intersectsOnAxis(ap, bp, Vector3f.cross(anx, bnx, null))){
//					return false;
//				}
//			}
//		}
//		return true;
//	}
//	
//	private static boolean intersectsOnAxis(Vector3f[] a, Vector3f[] b, Vector3f axis){
//		float aNear = Float.MAX_VALUE;
//		float aFar = Float.MIN_NORMAL;
//		for(Vector3f ap : a){
//			float factor = Maths.projectionFactor(axis, ap);
//			if(factor < aNear)
//				aNear = factor;
//			if(factor > aFar)
//				aFar = factor;
//		}
//		float bNear = Float.MAX_VALUE;
//		float bFar = Float.MIN_NORMAL;
//		for(Vector3f bp : b){
//			float factor = Maths.projectionFactor(axis, bp);
//			if(factor < bNear)
//				bNear = factor;
//			if(factor > bFar)
//				bFar = factor;
//		}
//		return (aNear < bFar && aNear > bNear) || (bNear < aFar && bNear > aNear);		
//	}
	
	// � = alt + 775

	// line L from p0 to p1: P(r) = p0 + r(p1 - p0)
	// plane P through v0 with normal n
	// intersection of L and P at P(rI)
	
	//		n�(v0 - p0)
	// rI = -----------
	//		n�(p1 - p0)
	
	// if n�(p1 - p0) = 0, they are parallel
	// if rI >= 0, they intersect
	//http://geomalgorithms.com/a06-_intersect-2.html
	public static Vector3f rayPlaneI(Plane P, Ray L){
		float denominator = Vector3f.dot(P.n, Vector3f.sub(L.v1, L.v0, null));
		if(denominator==0)
			return null;
		float rI = Vector3f.dot(P.n, Vector3f.sub(P.v0, L.v0, null))/denominator;
		if(rI>=0){
			return Vector3f.add(L.v0 , (Vector3f) Vector3f.sub(L.v1, L.v0, null).scale(rI), null);
		}
		return null;
	}
	
	public static float rayPlaneScalar(Plane P, Ray L){
		float denominator = Vector3f.dot(P.n, Vector3f.sub(L.v1, L.v0, null));
		if(denominator==0)
			return Float.NaN;//parallel or coplanar
		float rI = Vector3f.dot(P.n, Vector3f.sub(P.v0, L.v0, null));
		return rI;
		//	  (p0 - l0) � pN
		//d = -------------- = intersection scalar
		//	  (p1 - p0) � pN
//		Vector3f l_d = Vector3f.sub(l.v1, l.v0, null);
//		if(Vector3f.dot(l_d, p.n)!=0){
//			float d = Vector3f.dot(Vector3f.sub(p.v0, l.v0, null), p.n)/(Vector3f.dot(l_d, p.n));
//			return d;
//		}
//		return Float.NaN;//parallel or coplanar
	}
	
	public static Vector3f linePlaneI(Plane p, Ray r){
		float d = linePlaneScalar(p, r);
		if(d!=Float.NaN){
			return Vector3f.add((Vector3f) r.getN().scale(d), r.v0, null);
		}
		return null;//parallel or coplanar
	}
	
	public static float linePlaneScalar(Plane p, Ray r){
		//	  (v0 - p0) � pN
		//d = -------------- = intersection scalar
		//	  (p1 - p0) � pN
		Vector3f r_d = Vector3f.sub(r.v1, r.v0, null);
		float denom = Vector3f.dot(r_d, p.n);
		if(denom!=0){
			float d = Vector3f.dot(Vector3f.sub(p.v0, r.v0, null), p.n)/denom;
			return d;
		}
		return Float.NaN;//parallel or coplanar
	}
	
	public static boolean GJKIntersection(ConvexHull hullA, ConvexHull hullB) {
		HashSet<Vector3f> a = hullA.getPoints();
		HashSet<Vector3f> b = hullB.getPoints();
		Stack<Vector3f> simplex = new Stack<Vector3f>();
		//d = <0,1,0>
		
		Vector3f a_max = null;
		float extreme = Float.MIN_VALUE;
		for(Vector3f point : a) {
			if(point.y > extreme) {
				extreme = point.y;
				a_max = point;
			}
		}
		Vector3f b_min = null;
		extreme = Float.MAX_VALUE;
		for(Vector3f point : b) {
			if(point.y < extreme) {
				extreme = point.y;
				b_min = point;
			}
		}
		Vector3f first = Vector3f.sub(a_max, b_min, null);
		simplex.add(first);

		return GJKIntersection(hullA, hullB, simplex, first.negate(null));
	}
	
	public static boolean GJKIntersection(ConvexHull hullA, ConvexHull hullB, Stack<Vector3f> simplex, Vector3f d) {
		HashSet<Vector3f> a = hullA.getPoints();
		HashSet<Vector3f> b = hullB.getPoints();
		
		while(true) {
			Vector3f next = Vector3f.sub(support(a, d), support(b, d.negate(null)), null);
			
			if(Vector3f.dot(next, d)<0)
				return false;
			simplex.add(next);
			
			if(doSimplex(simplex, d))
				return true;
		}
	}
	
	/**
	 * Shape must have a length >= 1 and no null values
	 * @param shape - set of points that represents a shape
	 * @param d - direction to search
	 * @return the point in shape that is the furthest along d
	 */
	public static Vector3f support(HashSet<Vector3f> shape, Vector3f d) {
		Vector3f max = null;
		float maxDot = Float.MIN_VALUE;
		for(Vector3f point : shape) {
			float dot = Vector3f.dot(point, d);
			if(dot > maxDot) {
				maxDot = dot;
				max = point;
			}
		}
		return max;
	}
	
	/**
	 * @param simplex - (In) The current simplex (Out) The nearest sub-simplex
	 * @param dir - (out) The next direction for support()
	 * @param dir_neg - (out) negated direction
	 * @return Whether the origin is contained in a tetrahedron-simplex
	 */
	public static boolean doSimplex(Stack<Vector3f> simplex, Vector3f dir) {		
		if(simplex.size()==4) {//tetrahedron
			Vector3f d = simplex.get(0);
			Vector3f c = simplex.get(1);
			Vector3f b = simplex.get(2);
			Vector3f a = simplex.get(3);
			Vector3f ab = Vector3f.sub(b, a, null);
			Vector3f ac = Vector3f.sub(c, a, null);
			Vector3f ad = Vector3f.sub(d, a, null);
			Vector3f acb = Vector3f.cross(ac, ab, null);
			Vector3f adc = Vector3f.cross(ad, ac, null);
			Vector3f abd = Vector3f.cross(ab, ad, null);
			if(Vector3f.dot(acb, a)<0) {
				if(Vector3f.dot(Vector3f.cross(acb, ab, null), a)<0) {
					if(Vector3f.dot(Vector3f.cross(ab, abd, null), a)<0) {
						simplex.remove(1);
						simplex.remove(0);
						Vector3f.cross(Vector3f.cross(a, ab, null), ab, dir);
					}else {
						simplex.remove(1);
						simplex.set(0, b);
						simplex.set(1, d);
						dir.set(abd);
					}
				}else if(Vector3f.dot(Vector3f.cross(ac, acb, null), a)<0) {
					if(Vector3f.dot(Vector3f.cross(adc, ac, null), a)<0) {
						simplex.remove(2);
						simplex.remove(0);
						Vector3f.cross(Vector3f.cross(a, ac, null), ac, dir);
					}else {
						simplex.remove(2);
						simplex.set(0, d);
						simplex.set(1, c);
						dir.set(adc);
					}
				}else {
					simplex.remove(0);
					simplex.set(0, c);
					simplex.set(1, b);
					dir.set(acb);
				}
			}else if(Vector3f.dot(adc, a)<0) {
				if(Vector3f.dot(Vector3f.cross(ad, adc, null), a)<0) {
					if(Vector3f.dot(Vector3f.cross(abd, ad, null), a)<0) {
						simplex.remove(2);
						simplex.remove(1);
						Vector3f.cross(Vector3f.cross(a, ad, null), ad, dir);
					}else {
						simplex.remove(1);
						simplex.set(0, b);
						simplex.set(1, d);
						dir.set(abd);
					}
				}else {
					simplex.remove(2);
					simplex.set(0, d);
					simplex.set(1, c);
					dir.set(adc);
				}
			}else if(Vector3f.dot(abd, a)<0) {
				simplex.remove(1);
				simplex.set(0, b);
				simplex.set(1, d);
				dir.set(abd);
			}else
				return true;
		}else if(simplex.size()==3) {//triangle
			Vector3f c = simplex.get(0);
			Vector3f b = simplex.get(1);
			Vector3f a = simplex.get(2);
			Vector3f ab = Vector3f.sub(b, a, null);
			Vector3f ac = Vector3f.sub(c, a, null);
			Vector3f abc = Vector3f.cross(ab, ac, null);//triangle's normal
			if(Vector3f.dot(Vector3f.cross(abc, ac, null), a) < 0) {//ao = -a
				simplex.set(0, simplex.remove(0));
				Vector3f.cross(Vector3f.cross(a, ac, null), ac, dir);
			}else if(Vector3f.dot(Vector3f.cross(ab, abc, null), a) < 0) {
				simplex.set(0, simplex.remove(1));
				Vector3f.cross(Vector3f.cross(a, ab, null), ab, dir);
			}else if(Vector3f.dot(abc, a)<0){
				simplex.set(1, c);
				simplex.set(0, b);//rewind the triangle to preserve -> normal = ab x ac, where the normal points away from the origin
				dir.set(abc);
			}else {
				abc.negate(dir);
			}
		}else {//line
			Vector3f b = simplex.get(0);
			Vector3f a = simplex.get(1);
			Vector3f ab = Vector3f.sub(b, a, null);
			Vector3f.cross(Vector3f.cross(a, ab, null), ab, dir);
		}
		return false;
	}
	
	public static ConvexHull quickHull(HashSet<Vector3f> points) {
		//build starting simplex
		HashSet<Vector3f> unprocessed = new HashSet<Vector3f>(points);
		Vector3f a = null;
		float max = Float.MIN_VALUE;
		for(Vector3f point : unprocessed) {
			if(point.y > max) {
				max = point.y;
				a = point;
			}
		}
		unprocessed.remove(a);
		
		Vector3f b = null;
		Vector3f ab = null;
		max = 0;
		for(Vector3f point : unprocessed) {
			Vector3f diff = Vector3f.sub(point, a, null);
			float dist = diff.lengthSquared();
			if(dist>max) {
				max = dist;
				b = point;
				ab = diff;
			}
		}
		unprocessed.remove(b);
		
		Vector3f c = support(unprocessed, new Vector3f(0,ab.z,-ab.y));//ab x <1,0,0> = <0,z,-y>
		unprocessed.remove(c);
		
		Vector3f ac = Vector3f.sub(c, a, null);
		Vector3f n = Vector3f.cross(ab, ac, null);
		Set<Vector3f> inside = new HashSet<Vector3f>();
		Set<Vector3f> outside = new HashSet<Vector3f>();
		Vector3f minVec = null;
		Vector3f maxVec = null;
		float min = Float.MAX_VALUE;
		max = Float.MIN_VALUE;
		for(Vector3f point : unprocessed) {
			Vector3f ray = Vector3f.sub(point, a, null);
			float dot = Vector3f.dot(n, ray);
			if(Vector3f.dot(n, ray)<0) {
				inside.add(point);
				if(dot<min) {
					min = dot;
					minVec = point;
				}
			}else {
				outside.add(point);
				if(dot>max){
					max = dot;
					maxVec = point;
				}
			}
		}
		Facet abc = null;
		Vector3f d = null;
		if(outside.size()<inside.size()) {
			abc = new Facet(a, b, c, n);
			d = minVec;
		}else {
			n.negate();
			Vector3f swap = c;
			c = b;
			b = swap;
			abc = new Facet(a, b, c, n);
			d = maxVec;
			Set<Vector3f> temp = inside;
			inside = outside;
			outside = temp;
		}
		
		Facet dba = new Facet(d, b, a);
		Facet dcb = new Facet(d, c, b);
		Facet dac = new Facet(d, a, c);
		new Ridge(0, 1, abc, dba);
		new Ridge(1, 1, abc, dcb);
		new Ridge(2, 1, abc, dac);
		new Ridge(0, 2, dba, dcb);
		new Ridge(2, 0, dba, dac);
		new Ridge(0, 2, dcb, dac);
		
		unprocessed.removeAll(outside);
		dba.cullInside(unprocessed, inside);
		dcb.cullInside(unprocessed, inside);
		dac.cullInside(unprocessed, inside);
		unprocessed.removeAll(inside);
		unprocessed.addAll(outside);
		
		ArrayList<Facet> hull = new ArrayList<Facet>();
		hull.add(abc);
		hull.add(dba);
		hull.add(dcb);
		hull.add(dac);
		
		//expand simplex
		for(int j = 0; j < hull.size();) {
			Facet facet = hull.get(j);
			Set<Vector3f> outsideAll = new HashSet<Vector3f>();
			Vector3f p = facet.findFurthest(unprocessed, outsideAll);
			if(p!=null) {
				Stack<Facet> visible = new Stack<Facet>();
				visible.add(facet);
				ArrayList<Ridge> loop = new ArrayList<Ridge>();
				loop.add(facet.getAdjacent()[0]);
				loop.add(facet.getAdjacent()[1]);
				loop.add(facet.getAdjacent()[2]);
				visible(visible, loop, outsideAll, unprocessed, facet, p);
				
				Stack<Facet> newFacets = new Stack<Facet>();
				Facet first = loop.get(0).createFacet(visible.pop(), p);
				newFacets.add(first);
				int deltaFacets = 0;
				for(int i = 1; i < loop.size(); i++) {
					Facet recycle = null;
					boolean add = (visible.size()==0);
					if(!add) {
						recycle = visible.pop();
					}
					Ridge ridge = loop.get(i);
					Facet old = newFacets.peek();
					Facet next = ridge.createFacet(recycle, p);
					new Ridge(2,1,next,old);
					newFacets.add(next);
					if(add)
						hull.add(j+(++deltaFacets), next);
				}
				new Ridge(2,1,first,newFacets.peek());
				while(visible.size()!=0) {
					hull.remove(visible.pop());
				}
				Set<Vector3f> insideAll = first.getInsideSet(outsideAll);
				while(insideAll.size()>0 && !newFacets.isEmpty()) {
					Facet newFacet = newFacets.pop();
					newFacet.cullInside(outsideAll, insideAll);
				}
				outsideAll.removeAll(insideAll);
				unprocessed.addAll(outsideAll);
				unprocessed.remove(p);
			}else {
				j++;
			}
		}
		ArrayList<Triangle> tris = new ArrayList<Triangle>();
		HashSet<Vector3f> uniquePoints = new HashSet<Vector3f>();
		for(Facet facet : hull) {
			Triangle tri = facet.getTriangle();
			tris.add(tri);
			uniquePoints.add(tri.v0);
			uniquePoints.add(tri.v1);
			uniquePoints.add(tri.v2);
		}
		
		Triangle[] arrayT = new Triangle[tris.size()];
		tris.toArray(arrayT);
		OBJWriter.write(arrayT, "hull");
		
		return new ConvexHull(uniquePoints);
	}
	
	private static Stack<Facet> visible(Stack<Facet> visible, ArrayList<Ridge> ccwLoop, Set<Vector3f> outsideAll, Set<Vector3f> unprocessed, Facet facet, Vector3f p){
		Ridge[] ridges = facet.getAdjacent();
		for(int i = 0; i < 3; i++) {
			Facet neighbour = ridges[i].jumpRidge();
			if(!visible.contains(neighbour)) {
				Vector3f ap = Vector3f.sub(p, neighbour.getPoint(0), null);
				if(Vector3f.dot(ap, neighbour.getNormal())>0) {
					visible.add(neighbour);
					
					int index = ccwLoop.indexOf(ridges[i]);
					Ridge[] insertRidges = facet.getNextRidges(ridges[i]);
					ccwLoop.set(index, insertRidges[0]);
					ccwLoop.add(index+1, insertRidges[1]);
					neighbour.addOutside(unprocessed, outsideAll);
					
					visible(visible, ccwLoop, outsideAll, unprocessed, neighbour, p);
				}
			}
		}
		return visible;
	}
}
