package physicsEngine.collisionDetection;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.util.vector.Vector3f;

import physicsEngine.collisionDetection.primitives.Triangle;

public class Facet {

	
	private Vector3f[] points = new Vector3f[3]; //{a,b,c} ccw
	private Ridge[] adjacent = new Ridge[3]; //{ab,bc,ca}
	private Vector3f normal;
	
	public Facet(Vector3f a, Vector3f b, Vector3f c) {
		points[0] = a;
		points[1] = b;
		points[2] = c;
		Vector3f ab = Vector3f.sub(b, a, null);
		Vector3f ac = Vector3f.sub(c, a, null);
		normal = Vector3f.cross(ab, ac, null);
	}
	
	public Facet(Vector3f a, Vector3f b, Vector3f c, Vector3f normal) {
		points[0] = a;
		points[1] = b;
		points[2] = c;
		this.normal = normal;
	}
	
	public Facet recycle(Vector3f a, Vector3f b, Vector3f c) {
		points[0] = a;
		points[1] = b;
		points[2] = c;
		Vector3f ab = Vector3f.sub(b, a, null);
		Vector3f ac = Vector3f.sub(c, a, null);
		normal = Vector3f.cross(ab, ac, null);
		return this;
	}
	
	public void addFacet(Ridge ridge, int index) {
		adjacent[index] = ridge;
	}
	
	public void addOutside(Set<Vector3f> unprocessed, Set<Vector3f> outsideAll) {
		Set<Vector3f> remove = new HashSet<Vector3f>();
		for(Vector3f point : unprocessed) {
			Vector3f ray = Vector3f.sub(point, points[0], null);
			if(Vector3f.dot(ray, normal)>0) {
				outsideAll.add(point);
				remove.add(point);
			}
		}
		unprocessed.removeAll(remove);
	}
	
	public void cullInside(Set<Vector3f> outsideAll, Set<Vector3f> insideAll) {
		for(Vector3f point : outsideAll) {
			Vector3f ray = Vector3f.sub(point, points[0], null);
			if(Vector3f.dot(ray, normal)>0) {
				insideAll.remove(point);
			}
		}
	}
	
	public Set<Vector3f> getInsideSet(Set<Vector3f> outsideAll) {
		Set<Vector3f> inside = new HashSet<Vector3f>();
		for(Vector3f point : outsideAll) {
			Vector3f ray = Vector3f.sub(point, points[0], null);
			if(Vector3f.dot(ray, normal)<=0) {
				inside.add(point);
			}
		}
		return inside;
	}
	
	public Vector3f findFurthest(Set<Vector3f> unprocessed, Set<Vector3f> outsideAll) {
		float max = 0;
		Vector3f furthest = null;
		for(Vector3f point : unprocessed) {
			Vector3f ray = Vector3f.sub(point, points[0], null);
			float dot = Vector3f.dot(ray, normal);
			if(dot>0) {
				outsideAll.add(point);
				if(dot>max) {
					max = dot;
					furthest = point;
				}
			}
		}
		unprocessed.removeAll(outsideAll);
		return furthest;
	}
	
	public Ridge[] getNextRidges(Ridge ab) {
		Ridge[] nextRidges = new Ridge[2];
		Ridge[] adjacent = ab.jumpRidge().getAdjacent();
		for(int i = 0; i < adjacent.length; i++) {
			if(adjacent[i].equals(ab.getBackReference())) {
				nextRidges[0] = adjacent[(++i)%3];
				nextRidges[1] = adjacent[(++i)%3];
			}
		}
		return nextRidges;
	}
	
	public Vector3f getNormal() {
		return normal;
	}
	
	public Ridge[] getAdjacent() {
		return adjacent;
	}
	
	public Triangle getTriangle() {
		Triangle triangle = new Triangle(points[0], points[1], points[2], normal);
		return triangle;
	}
	
	public Vector3f getPoint(int index) {
		return points[index];
	}
}
