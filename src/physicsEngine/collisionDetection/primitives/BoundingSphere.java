package physicsEngine.collisionDetection.primitives;

import java.util.Random;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.utils.Maths;

public class BoundingSphere {

	private static final float ITER_SCALE = 0.95f;
	private static final int MAX_ITER = 8;
	
	private Vector3f center;
	private float radius;
	
	public BoundingSphere(Vector3f center, float radius) {
		setCenter(center);
		this.radius = radius;
	}
	
	public BoundingSphere(BoundingSphere copy) {
		setCenter(copy.center);
		radius = copy.radius;
	}
	
	public void setCenter(Vector3f center){
		this.center = center;
	}
	
	public Vector3f getCenter(){
		return center;
	}
	
	public float getRadius(){
		return radius;
	}
	
	/**
	 * Calculates the smallest sphere enclosing this ∪ o
	 * @param o The other BoundingSphere
	 * @return The enclosing sphere
	 */
	public BoundingSphere merge(BoundingSphere o) {
		Vector3f d = Vector3f.add(center, o.center, null);
		float distSq = d.lengthSquared();
		if(distSq<Math.abs(radius*radius-o.radius*o.radius)) {
			if(radius>o.radius)
				return new BoundingSphere(this);
			return new BoundingSphere(o);
		}
		float dist = (float)Math.sqrt(distSq);
		float radius = dist + this.radius + o.radius;
		radius/=2;
		d.scale((this.radius-radius)/dist);
		Vector3f center = Vector3f.add(this.center, d, null);
		return new BoundingSphere(center, radius);
	}
	
	/**
	 * Calculates the tightest bounding sphere around this ∩ o
	 * @param o The other BoundingSphere
	 * @return The sphere containing the intersection volume of this and o or null if ¬∃(this ∩ o)
	 */
	public BoundingSphere intersect(BoundingSphere o) {
		Vector3f d = Vector3f.sub(o.center, center, null);
		float distSq = d.lengthSquared();
		float r_r = radius*radius;
		float or_r = o.radius*o.radius;
		if(distSq<Math.abs(r_r-or_r)) {
			if(radius<o.radius)
				return new BoundingSphere(this);
			return new BoundingSphere(o);
		}
		if(distSq<r_r+or_r) {
			float dist = (float)Math.sqrt(distSq);
			float depth = radius+o.radius-dist;
			float halfDepth = depth/2;
			d.scale((o.radius-halfDepth)/dist);
			Vector3f center = Vector3f.add(o.center, d, null);
			float radius = (float) Math.sqrt(depth*this.radius-halfDepth*halfDepth);
			return new BoundingSphere(center, radius);
		}
		return null;
	}
	
	/**
	 * Expands the BoundingSphere to the smallest sphere enclosing this ∪ p
	 * @param p The point to include
	 */
	public void includePoint(Vector3f p) {
		Vector3f d = Vector3f.sub(center, p, null);
		float lenSq = d.lengthSquared();
		if(lenSq>radius*radius) {
			float len = (float)Math.sqrt(lenSq);
			float dRad = (len-radius)/2;
			d.scale(dRad/len);
			Vector3f.add(center, d, center);
			radius+=dRad;
		}
	}
	
	public void copy(BoundingSphere copy) {
		center = copy.center;
		radius = copy.radius;
	}
	
	/**
	 * Fastest sphere approximation. Easily skewed by clustered verticies.
	 * @param points The points to bound
	 * @return The approximate bounding sphere
	 */
	public static BoundingSphere averageCenter(Vector3f[] points) {
		Vector3f center = new Vector3f();
		for(Vector3f point : points) {
			Vector3f.add(center, point, center);
		}
		center.scale(1.0f/(float)points.length);
		
		return centeredSphere(points, center);
	}
	
	/**
	 * Quickly generates a rough approximation of a bounding sphere around points with a center at
	 * the center of the AABB of points.
	 * @param points The points to bound with a sphere
	 * @return A loose bounding sphere for points
	 */
	public static BoundingSphere aabbSphere(Vector3f[] points) {
		AABB temp = new AABB(points);
		Vector3f center = temp.getCenter();
		
		return centeredSphere(points, center);
	}
	
	/**
	 * Approximates the optimal bounding sphere around points by finding the farthest points
	 * defining the AABB of points, then expanding to include any outliers.
	 * @param points The points to bound with a sphere
	 * @return A bounding sphere with a nearly tight fit to points
	 */
	public static BoundingSphere ritterSphere(Vector3f[] points) {
		int[] indices = AABB.getExtremePoints(points);
		float xSq = Vector3f.sub(points[indices[1]], points[indices[0]], null).lengthSquared();
		float ySq = Vector3f.sub(points[indices[3]], points[indices[2]], null).lengthSquared();
		float zSq = Vector3f.sub(points[indices[5]], points[indices[4]], null).lengthSquared();
		
		int min = indices[0];
		int max = indices[1];
		float radius = xSq;
		
		if(ySq > xSq && ySq > zSq) {
			min = indices[2];
			max = indices[3];
			radius = ySq;
		}if(zSq > xSq && zSq > ySq) {
			min = indices[4];
			max = indices[5];
			radius = zSq;
		}
		
		radius = (float) Math.sqrt(radius)/2;
		Vector3f center = Vector3f.add(points[min], points[max], null);
		center.scale(0.5f);
		
		BoundingSphere sphere = new BoundingSphere(center, radius);
		
		for(Vector3f point : points)
			sphere.includePoint(point);
		
		return sphere;
	}
	
	/**
	 * Uses PCA to find the axis with the greatest spread, builds approximate sphere from the 
	 * furthest points on the axis, then expands to include all points.
	 * @param points The point set to bound
	 * @return A bounding sphere for points
	 */
	public static BoundingSphere ritterEigenSphere(Vector3f[] points) {
		Matrix3f covariance = Maths.covarianceMatrix(points);
		
		//extract eigenvectors and eigenvalues
		
		//create starting sphere with furthest max-spread-axis points
		
		//loop points and include
		
		return null;
	}
	
	/**
	 * Iteratively calculates the Ritter sphere, shrinks the result and recalculates,
	 * keeping any smaller spheres. Often near-optimal, but can get stuck in local minima.
	 * @param points The point set to bound with a sphere
	 * @return A tight bounding sphere for points
	 */
	public static BoundingSphere iterativeRitter(Vector3f[] points) {
		return iterativeRitter(points, MAX_ITER, ITER_SCALE);
	}
	
	/**
	 * Iteratively calculates the Ritter sphere, shrinks the result and recalculates,
	 * keeping any smaller spheres. Often near-optimal, but can get stuck in local minima.
	 * @param points The point set to bound with a sphere
	 * @param iterations The number of times the sphere is shrunk
	 * @param scale The scale to apply to radius each iteration
	 * @return A tight bounding sphere for points
	 */
	public static BoundingSphere iterativeRitter(Vector3f[] points, int iterations, float scale) {
		BoundingSphere s = ritterSphere(points);
		Random rand = new Random();
		Vector3f[] temp  = points.clone();
		BoundingSphere s2 = new BoundingSphere(s);
		for(int i = 0; i < iterations; i++) {
			s2.radius*=scale;
			
			for(int j = 0; j < temp.length; j++) {
				int remaining = temp.length - j;
				int k = j + (int)(rand.nextFloat()*remaining);
				Vector3f swap = temp[j];
				temp[j] = temp[k];
				temp[k] = swap;			
				s2.includePoint(temp[j]);
			}
			
			if(s2.radius < s.radius)
				s.copy(s2);
		}
		
		return s;
	}
	
	/**
	 * Calculates the optimal bounding sphere for the point set in points.
	 * @param points The points to bound
	 * @return The optimal bounding sphere
	 */
	public static BoundingSphere welzlSphere(Vector3f[] points) {
		
		return null;
	}
	
	/**
	 * Calculates the smallest distance between center and all points. Useful to set center to the center of rotation to maintain rotational invariance.
	 * @param points The points to bound
	 * @param center The sphere's center
	 * @return
	 */
	public static BoundingSphere centeredSphere(Vector3f[] points, Vector3f center) {
		float maxRadSq = 0;
		for(Vector3f point : points) {
			float radSq = Vector3f.sub(center, point, null).lengthSquared();
			if(radSq > maxRadSq)
				maxRadSq = radSq;
		}
		float radius = (float) Math.sqrt(maxRadSq);
		
		return new BoundingSphere(center, radius);
	}
	
	@Override
	public String toString() {
		return "BoundingSphere [" + center + "\n" + radius + "]";
	}
}
