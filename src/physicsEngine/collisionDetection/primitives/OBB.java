package physicsEngine.collisionDetection.primitives;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.utils.Maths;

public class OBB {

	private Matrix3f rotationMatrix;
	private AABB box; //convenient way to store center and extents
	
	/**
	 * OBBs can rotate with the object, but have expensive intersection tests
	 * @param aabb The AABB that sets this OBB's center and extents
	 * @param rotation The OBB's Euler angle rotation in radians
	 */
	public OBB(AABB aabb, Vector3f rotation){
		box = aabb;
		rotationMatrix = Maths.createRotationMatrixRadians(rotation);
	}
	
	public OBB(Vector3f center, Vector3f extents, Vector3f rotation) {
		box = new AABB(center, extents);
		rotationMatrix = Maths.createRotationMatrixRadians(rotation);
	}
	
	/**
	 * An intersection algorithm that is usually right, but may report false-positives
	 * near edge-edge intersections. Faster than full OBB vs OBB test (could be pretest).
	 * @param o The other OBB
	 * @return Whether the OBBs intersect (approximate)
	 */
	public boolean conservativeIntersect(OBB o) {
		//TODO: Optimize for least transformation / minimize calculations
		Vector3f localx = new Vector3f(rotationMatrix.m00, rotationMatrix.m01, rotationMatrix.m02);
		Vector3f localy = new Vector3f(rotationMatrix.m10, rotationMatrix.m11, rotationMatrix.m12);
		Vector3f localz = new Vector3f(rotationMatrix.m20, rotationMatrix.m21, rotationMatrix.m22);
		for(Vector3f point : o.getPoints()) {
			Vector3f.sub(point, box.getCenter(), point);
			float x = Vector3f.dot(localx, point);
			if(Math.abs(x)<box.getRadii().x) {
				return true;
			}
			float y = Vector3f.dot(localy, point);
			if(Math.abs(y)<box.getRadii().y) {
				return true;
			}
			float z = Vector3f.dot(localz, point);
			if(Math.abs(z)<box.getRadii().z) {
				return true;
			}
		}
		
		Vector3f olocalx = new Vector3f(o.rotationMatrix.m00, o.rotationMatrix.m01, o.rotationMatrix.m02);
		Vector3f olocaly = new Vector3f(o.rotationMatrix.m10, o.rotationMatrix.m11, o.rotationMatrix.m12);
		Vector3f olocalz = new Vector3f(o.rotationMatrix.m20, o.rotationMatrix.m21, o.rotationMatrix.m22);
		for(Vector3f point : getPoints()) {
			Vector3f.sub(point, o.box.getCenter(), point);
			float x = Vector3f.dot(olocalx, point);
			if(Math.abs(x)<o.box.getRadii().x) {
				return true;
			}
			float y = Vector3f.dot(olocaly, point);
			if(Math.abs(y)<o.box.getRadii().y) {
				return true;
			}
			float z = Vector3f.dot(olocalz, point);
			if(Math.abs(z)<o.box.getRadii().z) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Uses SAT for exact intersect test (slow).
	 * @param o The other OBB
	 * @return Whether the OBBs are intersecting
	 */
	public boolean intersection(OBB o) {
		//TODO: implement SAT (and 6-axis conservative test?)
		return false;
	}
	
	public Vector3f[] getPoints() {
		Vector3f[] points = box.getPoints();
		for(Vector3f point : points) {
			Matrix3f.transform(rotationMatrix, point, point);
		}
		return points;
	}
	
	public Vector3f getCenter() {
		return box.getCenter();
	}
	
	public void setCenter(Vector3f center) {
		box.setCenter(center);
	}
	
	public Vector3f getExtents() {
		return box.getRadii();
	}
	
	public Matrix3f getRotationMatrix(){
		return rotationMatrix;
	}
	
	/**
	 * @param rotation Rotation as Euler angles (x,y,z) in Radians
	 */
	public void updateRotation(Vector3f rotation){
		rotationMatrix = Maths.createRotationMatrixRadians(rotation);
	}
	
	public static OBB surfaceCovarianceOBB(ConvexHull hull) {
		
		return null;
	}
}
