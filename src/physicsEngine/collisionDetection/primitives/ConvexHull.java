package physicsEngine.collisionDetection.primitives;

import java.util.HashSet;

import org.lwjgl.util.vector.Vector3f;

public class ConvexHull {

	private HashSet<Vector3f> points;
	//TODO: neighbors for hill climbing and triangles for OBB PCA calculations (and maybe inertia tensor)
	
	public ConvexHull(HashSet<Vector3f> points) {
		this.points = points;
	}
	
	public HashSet<Vector3f> getPoints() {
		return points;
	}
}
