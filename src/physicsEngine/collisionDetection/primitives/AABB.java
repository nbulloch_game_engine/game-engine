package physicsEngine.collisionDetection.primitives;

import java.util.HashSet;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.utils.Maths;

public class AABB {

	private Vector3f center;
	private Vector3f radii;
	
	public AABB(Vector3f center, Vector3f radii){
		this.center = center;
		this.radii = radii;
	}
	
	/**
	 * Calculates the tightest AABB around the points
	 * @param points The points used to calculate the AABB
	 */
	public AABB(HashSet<Vector3f> points) {
		Vector3f min = new Vector3f(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE);
		Vector3f max = new Vector3f(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE);
		for(Vector3f point : points) {
			Maths.getMaxComponents(point, max, max);
			Maths.getMinComponents(point, min, min);
		}
		Vector3f.sub(max, min, radii);
		radii.scale(0.5f);
		Vector3f.add(min, radii, center);
	}
	
	/**
	 * Calculates the tightest AABB around the points
	 * @param points The points used to calculate the AABB
	 */
	public AABB(Vector3f[] points) {
		Vector3f min = new Vector3f(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE);
		Vector3f max = new Vector3f(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE);
		for(Vector3f point : points) {
			Maths.getMaxComponents(point, max, max);
			Maths.getMinComponents(point, min, min);
		}
		Vector3f.sub(max, min, radii);
		radii.scale(0.5f);
		Vector3f.add(min, radii, center);
	}
	
	public Vector3f getCenter(){
		return center;
	}
	
	/**
	 * @return The "radii" or half-widths of the AABB
	 */
	public Vector3f getRadii(){
		return radii;
	}
	
	public Vector3f getMin() {
		return Vector3f.sub(center, radii, null);
	}

	public Vector3f getMax() {
		return Vector3f.add(center, radii, null);
	}
	
	public Vector3f[] getPoints(){
		Vector3f min = getMin();
		Vector3f max = getMax();
		Vector3f[] points = {
				min,
				new Vector3f(min.x, min.y, max.z),
				new Vector3f(min.x, max.y, min.z),
				new Vector3f(min.x, max.y, max.z),
				new Vector3f(max.x, min.y, min.z),
				new Vector3f(max.x, min.y, max.z),
				new Vector3f(max.x, max.y, min.z),
				max
		};
		return points;
	}
	
	public Plane[] getPlanes(){
		Vector3f min = getMin();
		Vector3f max = getMax();
		Plane[] planes = new Plane[6];
		planes[0] = new Plane(new Vector3f(0,0,-1), min);
		planes[1] = new Plane(new Vector3f(0,0,1), max);
		planes[2] = new Plane(new Vector3f(0,-1,0), min);
		planes[3] = new Plane(new Vector3f(0,1,0), max);
		planes[4] = new Plane(new Vector3f(-1,0,0), min);
		planes[5] = new Plane(new Vector3f(1,0,0), max);
		return planes;
	}
	
	public void setMax(Vector3f max){
		Vector3f min = Vector3f.sub(center, radii, null);
		setDimensions(min, max);
	}
	
	public void setMin(Vector3f min){
		Vector3f max = Vector3f.add(center, radii, null);
		setDimensions(min, max);
	}
	
	/**
	 * Sets the center and radii from min and max. (Much) faster to calculate both at once.
	 * @param min The new minimum extent of the AABB
	 * @param max The new maximum extent of the AABB
	 */
	public void setDimensions(Vector3f min, Vector3f max) {
		Vector3f.sub(max, min, radii);
		radii.scale(0.5f);
		Vector3f.add(min, radii, center);
	}
	
	public void setRadii(Vector3f radii) {
		this.radii = radii;
	}
	
	public void setCenter(Vector3f center) {
		this.center = center;
	}
	
	public boolean contains(Vector3f p){
		Vector3f d = Vector3f.sub(center, p, null);
		return (Math.abs(d.x)<=radii.x)&&(Math.abs(d.y)<=radii.y)&&(Math.abs(d.z)<=radii.z);
	}
	
	public boolean intersects(AABB obj) {
		Vector3f d = Vector3f.sub(center, obj.center, null);
		Vector3f radii = Vector3f.add(this.radii, obj.radii, null);
		return  Math.abs(d.x)<=radii.x&&
				Math.abs(d.y)<=radii.y&&
				Math.abs(d.z)<=radii.z;
	}
	
	/**
	 * Finds the intersection volume defined by this ∩ obj
	 * @param obj The AABB to intersect with this AABB
	 * @return The intersection AABB of this and obj or null if no volume exists
	 */
	public AABB getIntersection(AABB obj) {
		Vector3f max = getMax();
		Vector3f min = getMin();
		Vector3f maxO = obj.getMax();
		Vector3f minO = obj.getMin();
		Maths.getMinComponents(max, maxO, max);
		Maths.getMaxComponents(min, minO, min);
		if(max.x<=min.x||max.y<=min.y||max.z<=min.z) {
			return null;
		}
		Vector3f radii = Vector3f.sub(max, min, null);
		radii.scale(0.5f);
		Vector3f center = Vector3f.add(min, radii, null);
		return new AABB(center, radii);
	}
	
	/**
	 * Finds the smallest AABB that encloses this ∪  obj 
	 * @param obj The AABB to merge with this
	 * @return The AABB that represents the union of this and obj
	 */
	public AABB merge(AABB obj) {
		Vector3f max = getMax();
		Vector3f min = getMin();
		Vector3f maxO = obj.getMax();
		Vector3f minO = obj.getMin();
		Maths.getMaxComponents(max, maxO, max);
		Maths.getMinComponents(min, minO, min);
		Vector3f radii = Vector3f.sub(max, min, null);
		radii.scale(0.5f);
		Vector3f center = Vector3f.add(min, radii, null);
		return new AABB(center, radii);
	}

	/**
	 * Finds the nearest point on or within the AABB
	 * @param p The original point
	 * @param out The vector to store the nearest point in. Creates a new vector if null.
	 */
	public Vector3f nearestPoint(Vector3f p, Vector3f out) {
		if(out==null)
			out = new Vector3f();
		Maths.getMinComponents(p, getMax(), out);
		Maths.getMaxComponents(out, getMin(), out);
		return out;
	}
	
	/**
	 * @param points An arbitrary point set
	 * @return The indices of the (up to six unique) points that define each AABB extent. {xmin, xmax, ymin, ymax, zmin, zmax}
	 */
	public static int[] getExtremePoints(Vector3f[] points) {
		int[] indices = new int[6];
		for(int i = 1; i < points.length; i++) {
			Vector3f p = points[i];
			if(p.x<points[indices[0]].x)
				indices[0]=i;
			if(p.x>points[indices[1]].x)
				indices[1]=i;
			if(p.y<points[indices[2]].y)
				indices[2]=i;
			if(p.y>points[indices[3]].y)
				indices[3]=i;
			if(p.z<points[indices[4]].z)
				indices[4]=i;
			if(p.z>points[indices[5]].z)
				indices[5]=i;
		}
		return indices;
	}
	
	@Override
	public String toString() {
		return "AABB [" + getMin() + "\n" + getMax() + "]";
	}

}
