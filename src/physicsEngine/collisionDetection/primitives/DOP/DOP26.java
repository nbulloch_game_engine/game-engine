package physicsEngine.collisionDetection.primitives.DOP;

import java.util.HashSet;

import org.lwjgl.util.vector.Vector3f;

public class DOP26 extends KDOP{
	
	//26 axis: (�1,0,0) U (0,�1,0) U (0,0,�1) U (�1,�1,0) U (�1,0,�1) U (0,�1,�1) U (�1,�1,�1)

	public DOP26(HashSet<Vector3f> points) {
		min = new float[] {Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE,
						   Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE,
						   Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE};
		max = new float[] {Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE,
						   Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE,
						   Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE};
		for(Vector3f p : points) {
			//axis(1,0,0)
			float value = p.x;
			includeValue(value, 0);
			
			//axis (0,1,0)
			value = p.y;
			includeValue(value, 1);
			
			//axis (0,0,1)
			value = p.z;
			includeValue(value, 2);
			
			float xy = p.x + p.y;
			//axis (1,1,0)
			value = xy;
			includeValue(value, 3);
			
			//axis (-1,1,0)
			value = -p.x + p.y;
			includeValue(value, 4);
			
			float xz = p.x + p.z;
			//axis (1,0,1)
			value = xz;
			includeValue(value, 5);
			
			//axis (-1,0,1)
			value = -p.x + p.z;
			includeValue(value, 6);
			
			float yz = p.y + p.z;
			//axis (0,1,1)
			value = yz;
			includeValue(value, 7);
			
			//axis (0,-1,1)
			value = -p.y + p.z;
			includeValue(value, 8);
			
			//axis (1,1,1)
			value = p.x + yz;
			includeValue(value, 9);
			
			//axis (-1,1,1)
			value = -p.x + yz;
			includeValue(value, 10);
			
			//axis (1,-1,1)
			value = xz - p.y;
			includeValue(value, 11);
			
			//axis (1,1,-1)
			value = xy - p.z;
			includeValue(value, 12);
		}
	}
	
	/**
	 * Creates a new 26-DOP from precomputed values.
	 * @param min The minimum bounds on each axis (min.length must be 13)
	 * @param max The maximum bounds on each axis (max.length must be 13)
	 */
	public DOP26(float[] min, float[] max) {
		super.min = min;
		super.max = max;
	}

	@Override
	protected KDOP createKDOP(float[] min, float[] max) {
		return new DOP26(min, max);
	}

	@Override
	public boolean contains(Vector3f p) {
		//axis(1,0,0)
		float value = p.x;
		if(axisExcludes(value, 0))
			return false;
		
		//axis (0,1,0)
		value = p.y;
		if(axisExcludes(value, 1))
			return false;
		
		//axis (0,0,1)
		value = p.z;
		if(axisExcludes(value, 2))
			return false;
		
		float xy = p.x + p.y;
		//axis (1,1,0)
		value = xy;
		if(axisExcludes(value, 3))
			return false;
		
		//axis (-1,1,0)
		value = -p.x + p.y;
		if(axisExcludes(value, 4))
			return false;
		
		float xz = p.x + p.z;
		//axis (1,0,1)
		value = xz;
		if(axisExcludes(value, 5))
			return false;
		
		//axis (-1,0,1)
		value = -p.x + p.z;
		if(axisExcludes(value, 6))
			return false;
		
		float yz = p.y + p.z;
		//axis (0,1,1)
		value = yz;
		if(axisExcludes(value, 7))
			return false;
		
		//axis (0,-1,1)
		value = -p.y + p.z;
		if(axisExcludes(value, 8))
			return false;
		
		//axis (1,1,1)
		value = p.x + yz;
		if(axisExcludes(value, 9))
			return false;
		
		//axis (-1,1,1)
		value = -p.x + yz;
		if(axisExcludes(value, 10))
			return false;
		
		//axis (1,-1,1)
		value = xz - p.y;
		if(axisExcludes(value, 11))
			return false;
		
		//axis (1,1,-1)
		value = xy - p.z;
		if(axisExcludes(value, 12))
			return false;
		
		return true;
	}
}
