package physicsEngine.collisionDetection.primitives.DOP;

import java.util.HashSet;

import org.lwjgl.util.vector.Vector3f;

public class DOP8 extends KDOP{

	//8 axis: (�1,�1,�1)
	
	public DOP8(HashSet<Vector3f> points) {
		min = new float[] {Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE};
		max = new float[] {Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE};
		for(Vector3f p : points) {
			float yz = p.y + p.z;
			//axis (1,1,1)
			float value = p.x + yz;
			includeValue(value, 0);
			
			//axis (-1,1,1)
			value = -p.x + yz;
			includeValue(value, 1);
			
			//axis (1,-1,1)
			value = p.x - p.y + p.z;
			includeValue(value, 2);
			
			//axis (1,1,-1)
			value = p.x + p.y - p.z;
			includeValue(value, 3);
		}
	}
	
	/**
	 * Creates a new 8-DOP from precomputed values.
	 * @param min The minimum bounds on each axis (min.length must be 4)
	 * @param max The maximum bounds on each axis (max.length must be 4)
	 */
	public DOP8(float[] min, float[] max) {
		super.min = min;
		super.max = max;
	}

	@Override
	protected KDOP createKDOP(float[] min, float[] max) {
		return new DOP8(min, max);
	}
	
	@Override
	public boolean contains(Vector3f p) {
		//axis (1,1,1)
		float yz = p.y + p.z;
		float value = p.x + yz;
		if(axisExcludes(value, 0))
			return false;
		
		//axis (-1,1,1)
		value = -p.x + yz;
		if(axisExcludes(value, 1))
			return false;
		
		//axis (1,-1,1)
		value = p.x - p.y + p.z;
		if(axisExcludes(value, 2))
			return false;
		
		//axis (1,1,-1)
		value = p.x + p.y - p.z;
		if(axisExcludes(value, 3))
			return false;
		
		return true;
	}
}
