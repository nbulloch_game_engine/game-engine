package physicsEngine.collisionDetection.primitives.DOP;

import java.util.HashSet;

import org.lwjgl.util.vector.Vector3f;

public class DOP20 extends KDOP{

	//20 axis: (�1,�1,0) U (�1,0,�1) U (0,�1,�1) U (�1,�1,�1)
	
	public DOP20(HashSet<Vector3f> points) {
		min = new float[] {Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE,
						   Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE};
		max = new float[] {Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE,
						   Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE,};
		for(Vector3f p : points) {
			float xy = p.x + p.y;
			//axis (1,1,0)
			float value = xy;
			includeValue(value, 0);
			
			//axis (-1,1,0)
			value = -p.x + p.y;
			includeValue(value, 1);
			
			float xz = p.x + p.z;
			//axis (1,0,1)
			value = xz;
			includeValue(value, 2);
			
			//axis (-1,0,1)
			value = -p.x + p.z;
			includeValue(value, 3);
			
			float yz = p.y + p.z;
			//axis (0,1,1)
			value = yz;
			includeValue(value, 4);
			
			//axis (0,-1,1)
			value = -p.y + p.z;
			includeValue(value, 5);
			
			//axis (1,1,1)
			value = p.x + yz;
			includeValue(value, 6);
			
			//axis (-1,1,1)
			value = -p.x + yz;
			includeValue(value, 7);
			
			//axis (1,-1,1)
			value = xz - p.y;
			includeValue(value, 8);
			
			//axis (1,1,-1)
			value = xy - p.z;
			includeValue(value, 9);
		}
	}
	
	/**
	 * Creates a new 20-DOP from precomputed values.
	 * @param min The minimum bounds on each axis (min.length must be 10)
	 * @param max The maximum bounds on each axis (max.length must be 10)
	 */
	public DOP20(float[] min, float[] max) {
		super.min = min;
		super.max = max;
	}

	@Override
	protected KDOP createKDOP(float[] min, float[] max) {
		return new DOP20(min, max);
	}

	@Override
	public boolean contains(Vector3f p) {
		float xy = p.x + p.y;
		//axis (1,1,0)
		float value = xy;
		if(axisExcludes(value, 0))
			return false;
		
		//axis (-1,1,0)
		value = -p.x + p.y;
		if(axisExcludes(value, 1))
			return false;
		
		float xz = p.x + p.z;
		//axis (1,0,1)
		value = xz;
		if(axisExcludes(value, 2))
			return false;
		
		//axis (-1,0,1)
		value = -p.x + p.z;
		if(axisExcludes(value, 3))
			return false;
		
		float yz = p.y + p.z;
		//axis (0,1,1)
		value = yz;
		if(axisExcludes(value, 4))
			return false;
		
		//axis (0,-1,1)
		value = -p.y + p.z;
		if(axisExcludes(value, 5))
			return false;
		
		//axis (1,1,1)
		value = p.x + yz;
		if(axisExcludes(value, 6))
			return false;
		
		//axis (-1,1,1)
		value = -p.x + yz;
		if(axisExcludes(value, 7))
			return false;
		
		//axis (1,-1,1)
		value = xz - p.y;
		if(axisExcludes(value, 8))
			return false;
		
		//axis (1,1,-1)
		value = xy - p.z;
		if(axisExcludes(value, 9))
			return false;
		
		return true;
	}
}
