package physicsEngine.collisionDetection.primitives.DOP;

import java.util.HashSet;

import org.lwjgl.util.vector.Vector3f;

public class DOP18 extends KDOP{
	
	//18 axis: (�1,0,0) U (0,�1,0) U (0,0,�1) U (�1,�1,0) U (�1,0,�1) U (0,�1,�1)
	
	public DOP18(HashSet<Vector3f> points) {
		min = new float[] {Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE,
						   Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE};
		max = new float[] {Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE,
						   Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE};
		for(Vector3f p : points) {
			//axis(1,0,0)
			float value = p.x;
			includeValue(value, 0);
			
			//axis (0,1,0)
			value = p.y;
			includeValue(value, 1);
			
			//axis (0,0,1)
			value = p.z;
			includeValue(value, 2);
			
			//axis (1,1,0)
			value = p.x + p.y;
			includeValue(value, 3);
			
			//axis (-1,1,0)
			value = -p.x + p.y;
			includeValue(value, 4);
			
			//axis (1,0,1)
			value = p.x + p.z;
			includeValue(value, 5);
			
			//axis (-1,0,1)
			value = -p.x + p.z;
			includeValue(value, 6);
			
			//axis (0,1,1)
			value = p.y + p.z;
			includeValue(value, 7);
			
			//axis (0,-1,1)
			value = -p.y + p.z;
			includeValue(value, 8);
		}
	}
	
	/**
	 * Creates a new 18-DOP from precomputed values.
	 * @param min The minimum bounds on each axis (min.length must be 9)
	 * @param max The maximum bounds on each axis (max.length must be 9)
	 */
	public DOP18(float[] min, float[] max) {
		super.min = min;
		super.max = max;
	}
	
	@Override
	protected KDOP createKDOP(float[] min, float[] max) {
		return new DOP18(min, max);
	}

	@Override
	public boolean contains(Vector3f p) {
		//axis(1,0,0)
		float value = p.x;
		if(axisExcludes(value, 0))
			return false;
		
		//axis (0,1,0)
		value = p.y;
		if(axisExcludes(value, 1))
			return false;
		
		//axis (0,0,1)
		value = p.z;
		if(axisExcludes(value, 2))
			return false;
		
		//axis (1,1,0)
		value = p.x + p.y;
		if(axisExcludes(value, 3))
			return false;
		
		//axis (-1,1,0)
		value = -p.x + p.y;
		if(axisExcludes(value, 4))
			return false;
		
		//axis (1,0,1)
		value = p.x + p.z;
		if(axisExcludes(value, 5))
			return false;
		
		//axis (-1,0,1)
		value = -p.x + p.z;
		if(axisExcludes(value, 6))
			return false;
		
		//axis (0,1,1)
		value = p.y + p.z;
		if(axisExcludes(value, 7))
			return false;
		
		//axis (0,-1,1)
		value = -p.y + p.z;
		if(axisExcludes(value, 8))
			return false;
		
		return true;
	}
}
