package physicsEngine.collisionDetection.primitives.DOP;

import org.lwjgl.util.vector.Vector3f;

public abstract class KDOP{

	protected float[] min;
	protected float[] max;
	
	/**
	 * Determines whether this KDOP intersects with obj
	 * @param obj The KDOP to test for intersection with this
	 * @return True if this intersects obj
	 */
	public boolean intersects(KDOP obj) {
		for(int i = 0; i < min.length; i++) {
			if(obj.max[i] > min[i] || obj.min[i] > max[i])
				return false;
		}
		return true;
	}
	
	/**
	 * Finds the intersection volume defined by this ∩ obj
	 * @param obj The DOP to intersect with this AABB (must have same k as this)
	 * @return The intersection KDOP of this and obj or null if no volume exists
	 */
	public KDOP getIntersection(KDOP obj) {
		float[] minI = new float[min.length];
		float[] maxI = new float[min.length];
		for(int i = 0; i < min.length; i++) {
			minI[i] = Math.max(min[i], obj.min[i]);
			maxI[i] = Math.min(max[i], obj.max[i]);
			if(minI[i]>maxI[i])
				return null;
		}
		return createKDOP(minI, maxI);
	}
	
	/**
	 * Finds the smallest KDOP that encloses this ∪  obj
	 * @param obj The KDOP to merge with this (must have same k as this)
	 * @return The KDOP that represents the union of this and obj
	 */
	public KDOP merge(KDOP obj) {
		float[] minI = new float[min.length];
		float[] maxI = new float[min.length];
		for(int i = 0; i < min.length; i++) {
			minI[i] = Math.min(min[i], obj.min[i]);
			maxI[i] = Math.max(max[i], obj.max[i]);
		}
		return createKDOP(minI, maxI);
	}
	
	/**
	 * Includes value in the interval on axis
	 * @param value The value to include
	 * @param axis The axis to test for expansion
	 */
	protected void includeValue(float value, int axis) {
		if(value<min[0])
			min[0] = value;
		else if(value>max[0])
			min[0] = value;
	}
	
	/**
	 * Determines whether the interval excludes the value on axis
	 * @param value The value to test for exclusion
	 * @param axis The axis to test on
	 * @return Whether the value is excluded on the axis
	 */
	protected boolean axisExcludes(float value, int axis) {
		return value>max[axis]||value<min[axis];
	}
	
	/**
	 * Determines whether the interval includes value on axis
	 * @param value The value to test for inclusion
	 * @param axis The axis to test on
	 * @return Whether the value is included on the axis
	 */
	protected boolean axisIncludes(float value, int axis) {
		return value<=max[axis]&&value>=min[axis];
	}
	
	protected abstract KDOP createKDOP(float[] min, float[] max);
	
	/**
	 * Checks if this KDOP contains point p
	 * @param p The point to check
	 * @return Whether this KDOP encloses p
	 */
	public abstract boolean contains(Vector3f p);
	
}
