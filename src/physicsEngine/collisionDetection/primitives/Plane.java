package physicsEngine.collisionDetection.primitives;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.utils.Maths;

public class Plane {

	public Vector3f n;
	public Vector3f v0;
	
	public Plane(Vector3f n, Vector3f v0){
		this.n = n;
		this.v0 = v0;
	}
	
	public Plane(Plane plane){
		this.n = plane.n;
		this.v0 = plane.getV0();
	}
	
	public Vector3f getNormal(){
		return n;
	}
	
	public Vector3f getV0(){
		return v0;
	}
	
	public Plane transform(Matrix4f transformation, Vector3f COR, Vector3f iCOR){
		Vector3f n = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(this.n, true), null));
		Vector3f v0 =this.v0;
		if(COR!=null){
			Vector3f.add(COR, v0, v0);
			v0 = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(v0, false), null));
			Vector3f.add(iCOR, v0, v0);
		}else{
			v0 = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(v0, false), null));
		}
		return new Plane(n, v0);
	}
	
	public Plane transform(Matrix4f transformation){
		Vector3f n = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(this.n, true), null));
		Vector3f v0 =this.v0;
		v0 = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(v0, false), null));
		return new Plane(n, v0);
	}
	
	public String toString() {
		return "P[v0 " + v0 + "\nn " + n + "]";
	}
}
