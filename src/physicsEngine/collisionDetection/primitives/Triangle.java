package physicsEngine.collisionDetection.primitives;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.utils.Maths;

public class Triangle {
	
	public Vector3f v0;
	public Vector3f v1;
	public Vector3f v2;
	public Plane p;
	
	public Triangle(Vector3f v0, Vector3f v1, Vector3f v2){
		this.v0 = v0;
		this.v1 = v1;
		this.v2 = v2;
		Vector3f n = Vector3f.cross(Vector3f.sub(v0, v1, null), Vector3f.sub(v0, v2, null), null);
		n.normalise();
		p = new Plane(n, v0);
	}
	
	public Triangle(Vector3f v0, Vector3f v1, Vector3f v2, Vector3f n) {
		this.v0 = v0;
		this.v1 = v1;
		this.v2 = v2;
		if(n.length()==0) {
			System.out.println("Zero length normal");
		}
		n.normalise();
		p = new Plane(n, v0);
	}
	
	public Vector3f getV0(){
		return v0;
	}
	public Vector3f getV1(){
		return v1;
	}
	
	public Vector3f getV2(){
		return v2;
	}
	
	public Plane getP(){
		return p;
	}
	
	public Triangle transform(Matrix4f transformation){
		Vector3f v0 = new Vector3f(this.v0);
		Vector3f v1 = new Vector3f(this.v1);
		Vector3f v2 = new Vector3f(this.v2);
		v0 = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(v0, false), null));
		v1 = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(v1, false), null));
		v2 = Maths.toVector3f(Matrix4f.transform(transformation, Maths.toVector4f(v2, false), null));
		return new Triangle(v0, v1, v2);
	}
	
	@Override
	public String toString() {
		return "T["+v0.toString()+"\n"+v1.toString()+"\n"+v2.toString()+"]";
	}
}
