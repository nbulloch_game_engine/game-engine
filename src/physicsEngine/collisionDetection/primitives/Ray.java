package physicsEngine.collisionDetection.primitives;

import org.lwjgl.util.vector.Vector3f;

public class Ray {
	
	public Vector3f v0;
	public Vector3f v1;
	
	public Ray(Vector3f v0, Vector3f v1){
		this.v0 = v0;
		this.v1 = v1;
	}
	
	public Ray(Vector3f direction){
		this.v0 = new Vector3f(0, 0, 0);
		this.v1 = direction;
	}
	
	public Vector3f getV0(){
		return v0;
	}
	
	public Vector3f getV1(){
		return v1;
	}
	
	public Vector3f getN(){
		return Vector3f.sub(v1, v0, null);
	}
}
