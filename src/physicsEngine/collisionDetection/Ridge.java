package physicsEngine.collisionDetection;

import org.lwjgl.util.vector.Vector3f;

public class Ridge {

	private static int uh_oh = 0;
	
	protected Vector3f a, b;
	private Facet bad;
	private Ridge badRidge;
	
	/**
	 * Generates a ridge pair for both abc and bad
	 * @param a - The first vertex's abc index
	 * @param b - The second vertex's bad index
	 * @param abc - "reference" facet
	 * @param bad - "other" facet sharing vertices a and b
	 */
	public Ridge(int a, int b, Facet abc, Facet bad) {
		this.a = abc.getPoint(a);
		this.b = bad.getPoint(b);
		if(this.a.equals(this.b)) {
			System.out.println("Issues: " + uh_oh++);
		}
		this.bad = bad;
		abc.addFacet(this, a);
		badRidge = new Ridge(this.b, this.a, abc, this);
		bad.addFacet(badRidge, b);
	}
	
	/**
	 * Must send this ridge to abc and badRidge to bad
	 * @param a - First Vertex
	 * @param b - Second Vertex (ccw)
	 * @param bad - "other" facet
	 * @param badRidge - the ridge referencing "this" facet
	 */
	public Ridge(Vector3f a, Vector3f b, Facet bad, Ridge badRidge) {
		this.a = a;
		this.b = b;
		this.bad = bad;
		this.badRidge = badRidge;
	}
	
	public Facet jumpRidge() {
		return bad;
	}
	
	public void updateBackReference(Facet abc) {
		badRidge.setReference(abc);
	}
	
	public void setReference(Facet bad) {
		this.bad = bad;
	}
	
	public Ridge getBackReference() {
		return badRidge;
	}
	
	public Facet createFacet(Facet recycle, Vector3f c) {
		if(recycle==null) {
			Facet facet = new Facet(a, b, c);
			updateBackReference(facet);
			facet.addFacet(this, 0);
			return facet;
		}
		Facet facet = recycle.recycle(a, b, c);
		updateBackReference(facet);
		facet.addFacet(this, 0);
		return facet;
	}
}
