package render.engine.profiler;

import java.util.ArrayList;

public class Profiler {

	private static ArrayList<Long> cost = new ArrayList<Long>();
	
	public static int getNewProfiler(){
		cost.add(0l);
		return cost.size();
	}
	
	public static void startProfiler(int id){
		cost.set(id, System.nanoTime());
	}
	
	public static long endProfiler(int id){
		long dt = System.nanoTime() - cost.get(id);
		cost.set(id, dt);
		return dt;
	}
	
	public static long getDT(int id){
		return cost.get(id);
	}
}
