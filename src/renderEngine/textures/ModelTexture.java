package renderEngine.textures;

public class ModelTexture {
	
	private int textureID;
	private int normalMap = -1;
	private int specularMap;
	private int lightMap;
	private int glowMap;
	
	private float shineDamper = 1;
	private float reflectivity = 0;
	
	private float bloomWidth = 0.5f;
	private float glowMapFactor = 0f;
	private float glowSpeed = 1f;
	
	private boolean hasTransparency = false;
	private boolean useFakeLighting = false;
	private boolean hasSpecularMap = false;
	private boolean hasLightMap = false;
	private boolean useGlowMap = false;
	
	private int numberOfRows = 1;

	public ModelTexture(int id){
		textureID = id;
	}
	
	public ModelTexture(ModelTexture modelTexture) {
		this.textureID = modelTexture.textureID;
		this.normalMap = modelTexture.normalMap;
		this.specularMap = modelTexture.specularMap;
		this.lightMap = modelTexture.lightMap;
		this.glowMap = modelTexture.glowMap;
		this.shineDamper = modelTexture.shineDamper;
		this.reflectivity = modelTexture.reflectivity;
		this.bloomWidth = modelTexture.bloomWidth;
		this.glowMapFactor = modelTexture.glowMapFactor;
		this.glowSpeed = modelTexture.glowSpeed;
		this.hasTransparency = modelTexture.hasTransparency;
		this.useFakeLighting = modelTexture.useFakeLighting;
		this.hasSpecularMap = modelTexture.hasSpecularMap;
		this.hasLightMap = modelTexture.hasLightMap;
		this.useGlowMap = modelTexture.useGlowMap;
		this.numberOfRows = modelTexture.numberOfRows;
	}

	public void setGlowMap(int id){
		glowMap = id;
		useGlowMap = true;
	}
	
	public boolean useGlowMap(){
		return useGlowMap;
	}
	
	public int getGlowMap(){
		return glowMap;
	}
	
	public void setBloomWidth(float width){
		bloomWidth = width;
	}
	
	public float getBloomWidth(){
		return bloomWidth;
	}
	
	public void setGlowMapFactor(float factor){
		glowMapFactor = factor;
	}
	
	public float getGlowMapFactor(){
		return glowMapFactor;
	}
	
	public void setGlowSpeed(float speed){
		glowSpeed = speed;
	}
	
	public float getGlowSpeed(){
		return glowSpeed;
	}
	
	public void setSpecularMap(int id){
		hasSpecularMap = true;
		specularMap = id;
	}
	
	public boolean hasSpecularMap(){
		return hasSpecularMap;
	}
	
	public int getSpecularMap(){
		return specularMap;
	}
	
	public void setLightMap(int id){
		hasLightMap = true;
		lightMap = id;
	} 
	public boolean hasLightMap(){
		return hasLightMap;
	}
	
	public int getLightMap(){
		return lightMap;
	}
	
	public int getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}
	
	public int getNormalMap() {
		return normalMap;
	}

	public void setNormalMap(int normalMap) {
		this.normalMap = normalMap;
	}

	public boolean usingFakeLighting() {
		return useFakeLighting;
	}

	public void setUseFakeLighting(boolean useFakeLighting) {
		this.useFakeLighting = useFakeLighting;
	}

	public boolean hasTransparency() {
		return hasTransparency;
	}

	public void setTransparency(boolean hasTransparency) {
		this.hasTransparency = hasTransparency;
	}

	public int getID() {
		return this.textureID;
	}
	
	public float getShineDamper() {
		return shineDamper;
	}

	public void setShineDamper(float shineDamper) {
		this.shineDamper = shineDamper;
	}

	public float getReflectivity() {
		return reflectivity;
	}

	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
}
