package renderEngine.textures;

public class TerrainTexture {
	
	private int textureID;
	private boolean hasTransparency;
	
	public TerrainTexture(int textureId){
		this.textureID = textureId;
	}

	public void setTransparency(boolean value){
		hasTransparency = value;
	}
	
	public boolean hasTransparency(){
		return hasTransparency;
	}
	
	public int getTextureID() {
		return textureID;
	}
	
}
