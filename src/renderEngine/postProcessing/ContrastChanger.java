package renderEngine.postProcessing;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public class ContrastChanger {

	private ImageRenderer renderer;
	private ContrastShader shader;
	
	public ContrastChanger(){
		shader = new ContrastShader();
		renderer = new ImageRenderer();
	}
	
	public void render(int texture){
		shader.start();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		renderer.renderQuad();
		shader.stop();
	}
	
	public void clean(){
		renderer.clean();
		shader.clean();
	}
}
