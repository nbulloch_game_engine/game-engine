package renderEngine.postProcessing;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import renderEngine.bloom.CombineFilter;
import renderEngine.gaussianBlur.HorizontalBlur;
import renderEngine.gaussianBlur.VerticalBlur;
import renderEngine.models.RawModel;
import renderEngine.renderEngine.Loader;

public class PostProcessing {
	
	private static RawModel quad;
	private static HorizontalBlur hBlur2;
	private static VerticalBlur vBlur2;
	private static HorizontalBlur hBlur4;
	private static VerticalBlur vBlur4;
	private static HorizontalBlur hBlur8;
	private static VerticalBlur vBlur8;
	private static HorizontalBlur hBlur8Final;
	private static VerticalBlur vBlur8Final;
	private static CombineFilter combineFilter;
	private static ImageRenderer combineBlur;
	private static boolean blur = false;
	private static Fbo fbo;
	
	public static void init(){
		quad = Loader.getQuad();
		hBlur2 = new HorizontalBlur(Display.getWidth()/2, Display.getHeight()/2);
		vBlur2 = new VerticalBlur(Display.getWidth()/2, Display.getHeight()/2);
		hBlur4 = new HorizontalBlur(Display.getWidth()/4, Display.getHeight()/4);
		vBlur4 = new VerticalBlur(Display.getWidth()/4, Display.getHeight()/4);
		hBlur8 = new HorizontalBlur(Display.getWidth()/8, Display.getHeight()/8);
		vBlur8 = new VerticalBlur(Display.getWidth()/8, Display.getHeight()/8);
		hBlur8Final = new HorizontalBlur(Display.getWidth()/8, Display.getHeight()/8);
		vBlur8Final = new VerticalBlur(Display.getHeight()/8);
		combineFilter  = new CombineFilter();
		combineBlur = new ImageRenderer(Display.getWidth(), Display.getHeight());
	}
	
	public static void doPostProcessing(int colorTexture, int bloomTexture){
		start();
		hBlur2.render(bloomTexture);
		vBlur2.render(hBlur2.getOutputTexture());
		hBlur4.render(bloomTexture);
		vBlur4.render(hBlur4.getOutputTexture());
		hBlur8.render(bloomTexture);
		vBlur8.render(hBlur8.getOutputTexture());
		if(fbo!=null){
			fbo.bindFrameBuffer();
			combineFilter.render(colorTexture, vBlur2.getOutputTexture(), vBlur4.getOutputTexture(), vBlur8.getOutputTexture());
			fbo.unbindFrameBuffer();
			if(blur){
				hBlur8Final.render(combineFilter.getOutputTexture());
				fbo.bindFrameBuffer();
				vBlur8Final.render(hBlur8Final.getOutputTexture());
				fbo.unbindFrameBuffer();
			}
		}else{
			combineFilter.render(colorTexture, vBlur2.getOutputTexture(), vBlur4.getOutputTexture(), vBlur8.getOutputTexture());
			if(blur){
				hBlur8Final.render(combineFilter.getOutputTexture());
				vBlur8Final.render(hBlur8Final.getOutputTexture());
			}
		}
		end();
	}
	
	public static void blur(){
		if(!blur){
			combineFilter.setImageRenderer(combineBlur);
			blur = true;
		}
	}
	
	public static void unblur(){
		if(blur){
			blur = false;
			combineFilter.setImageRenderer(new ImageRenderer());
		}
	}
	
	public static void renderToFbo(Fbo fbo){
		PostProcessing.fbo = fbo;
	}
	
	public static void clean(){
		hBlur2.clean();
		vBlur2.clean();
		hBlur4.clean();
		vBlur4.clean();
		hBlur8.clean();
		vBlur8.clean();
		hBlur8Final.clean();
		vBlur8Final.clean();
		combineFilter.clean();
		combineBlur.clean();
	}
	
	private static void start(){
		GL30.glBindVertexArray(quad.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}
	
	private static void end(){
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}

}
