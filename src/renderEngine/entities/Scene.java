package renderEngine.entities;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import physicsEngine.collisionDetection.primitives.AABB;
import renderEngine.terrain.Terrain;

public abstract class Scene{
	
	protected ArrayList<Entity> staticNormalMapEntities;
	protected ArrayList<Entity> staticEntities;
	protected ArrayList<Terrain> terrains;
	protected ArrayList<PointLight> lights;
	protected ArrayList<DirectionalLight> DLights;
	protected DirectionalLight sun;
	protected Terrain terrain;
	protected Camera camera;
	protected boolean csmShadows = false;
	protected boolean shadows = true;
	protected boolean skybox = true;
	protected Vector3f skyboxPosition = null;
	
	public abstract Vector4f getClipPlane();
	public abstract Vector3f getSkyColor();
	public abstract float getNearPlane();
	public abstract float getFarPlane();
	public abstract int getNumCascades();
	public abstract float getFogDensity();
	public abstract float getFogGradient();
	public abstract float getFogginess();
	public abstract float getFOV();
	public abstract int getMaxShadowSize();
	public abstract int getMinShadowSize();
	
	public boolean hasCsmShadows() {
		return csmShadows;
	}
	public ArrayList<Entity> getStaticNormalMapEntities() {
		return staticNormalMapEntities;
	}

	public ArrayList<Entity> getStaticEntities() {
		return staticEntities;
	}

	public ArrayList<Terrain> getTerrains() {
		return terrains;
	}

	public ArrayList<PointLight> getLights() {
		return lights;
	}

	public ArrayList<DirectionalLight> getDLights() {
		return DLights;
	}

	public DirectionalLight getSun() {
		return sun;
	}

	public Terrain getTerrain() {
		return terrain;
	}

	public Camera getCamera() {
		return camera;
	}
	
	public boolean hasShadows(){
		return shadows;
	}
	
	public boolean skybox(){
		return skybox;
	}
	
	public Vector3f getSkyboxPosition(){
		return skyboxPosition;
	}
}
