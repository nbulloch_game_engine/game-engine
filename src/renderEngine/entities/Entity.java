package renderEngine.entities;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.models.TexturedModel;
import renderEngine.utils.Maths;
import renderEngine.utils.Quaternion;

public class Entity {

	private static final Vector3f emptyVector = new Vector3f();
	private static final Vector3f defaultScale = new Vector3f(1, 1, 1);
	
	private TexturedModel texturedModel;
	private Vector3f position, CoR, rotation, scale;
	private Quaternion quat;
	private int textureIndex = 0;
	private Matrix4f transformation;
	private float translucency = 1;

	public Entity(Entity entity){
		this.position = entity.position;
		this.rotation = entity.rotation;
		this.scale = entity.scale;
		this.CoR = entity.CoR;
		this.transformation = entity.transformation;
	}
	
	public Entity(Vector3f position) {
		this.position = position;
		this.rotation = emptyVector;
		this.scale = defaultScale;
		transformation = Maths.createTransformationMatrix(position, rotation, scale);
	}
	
	public Entity(TexturedModel texturedModel, Vector3f position, Vector3f rotation, Vector3f CoR, float scale) {
		this.texturedModel = texturedModel;
		this.position = position;
		this.rotation = rotation;
		this.scale = new Vector3f(scale, scale, scale);
		this.CoR = CoR;
		updateMatrix();
	}
	
	public Entity(TexturedModel texturedModel, Vector3f position, Vector3f rotation, Vector3f CoR, Vector3f scale) {
		this.texturedModel = texturedModel;
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		this.CoR = CoR;
		updateMatrix();
	}
	
	public Entity(TexturedModel texturedModel, Vector3f position, Quaternion rotation, Vector3f CoR, float scale) {
		this.texturedModel = texturedModel;
		this.position = position;
		this.quat = rotation;
		this.scale = new Vector3f(scale, scale, scale);
		this.CoR = CoR;
		updateMatrix();
	}
	
	public Entity(TexturedModel texturedModel, Vector3f position, Quaternion rotation, Vector3f CoR, Vector3f scale) {
		this.texturedModel = texturedModel;
		this.position = position;
		this.quat = rotation;
		this.scale = scale;
		this.CoR = CoR;
		updateMatrix();
	}
	
	public void updateMatrix(){
		if(rotation!=null) {
			if(CoR!=null) {
				transformation = Maths.createTransformationMatrix(position, CoR, rotation, scale);
			}else
				transformation = Maths.createTransformationMatrix(position, rotation, scale);
		}else{
			transformation = new Matrix4f();
			transformation.translate(position);
			if(CoR!=null) {
				transformation.translate(CoR);
				Matrix4f.mul(transformation, quat.getMatrix4f(), transformation);
				CoR.negate();
				transformation.translate(CoR);
				CoR.negate();
			}else {
				Matrix4f.mul(transformation, quat.getMatrix4f(), transformation);
			}
			transformation.scale(scale);
		}
	}
	
	public void setTextureIndex(int index){
		textureIndex = index;
	}
	
	public float getTextureXOffset(){
		int column = textureIndex%texturedModel.getTexture().getNumberOfRows();
		return (float)column/(float)texturedModel.getTexture().getNumberOfRows();
	}
	
	public float getTextureYOffset(){
		int row = textureIndex/texturedModel.getTexture().getNumberOfRows();
		return (float)row/(float)texturedModel.getTexture().getNumberOfRows();
	}
	
	public void increasePosition(float dx, float dy, float dz){
		this.position.x+=dx;
		this.position.y+=dy;
		this.position.z+=dz;
		updateMatrix();
	}
	
	public void increaseRotation(Vector3f rotation){
		Vector3f.add(this.rotation, rotation, this.rotation);
		updateMatrix();
	}

	public TexturedModel getTexturedModel() {
		return texturedModel;
	}

	public void setTexturedModel(TexturedModel texturedModel) {
		this.texturedModel = texturedModel;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
		updateMatrix();
	}

	public Vector3f getRotation(){
		return rotation;
	}
	
	public void setRotation(Vector3f rotation){
		this.rotation = rotation;
		quat = null;
		updateMatrix();
	}
	
	public Quaternion getQuat() {
		return quat;
	}
	
	public void setRotation(Quaternion quat) {
		this.quat = quat;
		rotation = null;
		updateMatrix();
	}
	
	public Vector3f getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = new Vector3f(scale, scale, scale);
		updateMatrix();
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
		updateMatrix();
	}

	public Matrix4f getTransformation() {
		return transformation;
	}
	
	public boolean hasCoR(){
		return CoR!=null;
	}
	
	public void setCenterOfRotation(Vector3f CoR){
		this.CoR = CoR;
		updateMatrix();
	}
	
	public Vector3f getCoR() {
		return CoR;
	}

	public float getTranslucency() {
		return translucency;
	}

	public void setTranslucency(float translucency) {
		if(translucency < 1)
			this.translucency = translucency;
		else
			this.translucency = 1;
		if(translucency < 0) {
			this.translucency = 0;
		}
	}
}
