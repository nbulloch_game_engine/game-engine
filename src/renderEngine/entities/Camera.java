package renderEngine.entities;

import java.util.ArrayList;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.renderEngine.DisplayManager;
import renderEngine.utils.LERP;
import renderEngine.utils.Maths;
import renderEngine.utils.MouseManager;
import renderEngine.utils.NLERP;
import renderEngine.utils.Quaternion;
import renderEngine.utils.SLERP;

public class Camera {

	public static final Vector3f FORWARD = new Vector3f(0,0,-1);
	public static final Vector3f UP = new Vector3f(0,1,0);
	public static final Vector3f RIGHT = new Vector3f(1,0,0);
	
	public static final int LERP = 0;
	public static final int NLERP = 1;
	public static final int SLERP = 2;
	
	private static final int[] defaultKeys = new int[] {Keyboard.KEY_W, Keyboard.KEY_S, Keyboard.KEY_D,
			Keyboard.KEY_A, Keyboard.KEY_SPACE, Keyboard.KEY_LSHIFT, Keyboard.KEY_UP, Keyboard.KEY_DOWN,
			Keyboard.KEY_RIGHT, Keyboard.KEY_LEFT, Keyboard.KEY_SPACE, Keyboard.KEY_LSHIFT};
	
	private static ArrayList<Integer>[] keyMap = getDefaultKeys();
	//each array contains the keycodes for
	//[forward, backward, right, left, up, down]
	
	private Vector3f cameraPosition;
	private Quaternion pitch;
	private Quaternion yaw;
	private Quaternion rotation;
	private Quaternion inverseRotation;
	
	private float max_y = 0.64278760969f;//the sin of theta, half of the maximum pitch (default≈80°)
	private float min_x;
	private Vector2f zoomBounds = new Vector2f(0.3f, 8);
	
	private boolean constrainLookAt = true;
	
	private LERP lerp_pitch;
	private LERP lerp_yaw;
	
	private Entity follow;
	private Vector3f followPosition;
	private float followDistance = 1;
	
	private float speed = 1;//units per second (u/s)
	private float angularSpeed = 0.1f;//radians per second per pixel (rad/spx)
	private float zoomSpeed = 0.003f;//units per scroll
	
	private Matrix4f viewMatrix;
	private boolean updateMatrix = false;
	
	/**
	 * Creates a new forward-facing camera at position
	 * @param position The camera's initial position
	 * @param mode The LERP mode of this camera (use SLERP or NLERP, LERP not recommended)
	 */
	public Camera(Vector3f position, int mode) {
		pickLerpMode(mode);
		viewMatrix = new Matrix4f();
		pitch = new Quaternion();
		yaw = new Quaternion();
		rotation = new Quaternion();
		inverseRotation = new Quaternion();
		cameraPosition = position;
		setMaxPitch(max_y);
	}
	
	/**
	 * Creates a new camera at position with rotation
	 * @param position The camera's initial position
	 * @param rotation The camera's initial rotation
	 * @param mode The LERP mode of this camera (use SLERP or NLERP, LERP not recommended)
	 */
	public Camera(Vector3f position, Quaternion rotation, int mode) {
		this(position, mode);
		this.rotation = rotation;
		rotation.conjugate(inverseRotation);
		Maths.calculateYawPitch(rotation, yaw, pitch);
	}
	
	/**
	 * Instantiates the pitch and yaw lerps based on mode
	 * @param mode The type of lerp to perform
	 */
	private void pickLerpMode(int mode) {
		if(mode==LERP) {
			lerp_pitch = new LERP();
			lerp_yaw = new LERP();	
		}else if(mode==NLERP) {
			lerp_pitch = new NLERP();
			lerp_yaw = new NLERP();	
		}else if(mode==SLERP) {
			lerp_pitch = new SLERP();
			lerp_yaw = new SLERP();	
		}
	}
	
	/**
	 * Updates the camera's transformation
	 */
	public void update() {
		if(!updateSlerp()) {
			if(follow!=null&&!followPosition.equals(follow.getPosition())) {
				followPosition = new Vector3f(follow.getPosition());
				updateMatrix = true;
			}if(followPosition!=null) {
				rotateFollowing();
				follow();
			}else{
				rotate();
				move();
			}
		}
	}
	
	/**
	 * Feeds the slerp's output to the camera
	 * @return whether the slerp was updated
	 */
	private boolean updateSlerp(){
		if(lerp_pitch.isDead())
			return false;
		
		pitch = lerp_pitch.getRotation();
		yaw = lerp_yaw.getRotation();
		if(constrainLookAt)
			constrainPitch();
		refreshRotation();
		return true;
	}
	
	/**
	 * Handles mouse input for a following camera
	 */
	private void rotateFollowing() {
		rotate();
		final int dW = Mouse.getDWheel();
		if(dW!=0) {
			followDistance -= (float)dW*zoomSpeed;
			followDistance = Math.max(followDistance, zoomBounds.x);
			followDistance = Math.min(followDistance, zoomBounds.y);
		}
	}
	
	/**
	 * Calculates the new camera position based on it's rotation
	 */
	private void follow() {
		Vector3f localPos = getDirection();
		localPos.negate().scale(followDistance);
		Vector3f.add(localPos, followPosition, cameraPosition);
	}
	
	/**
	 * Handles keyboard input for free-flight camera
	 */
	private void move() {
		boolean[] isPressed = new boolean[keyMap.length];
		for(int i =0; i < keyMap.length; i++) {
			for(Integer key : keyMap[i]) {
				if(Keyboard.isKeyDown(key)) {
					isPressed[i] = true;
					break;
				}
			}
		}
		
		for(int i = 0; i < 3; i++) {
			int index = i*2;
			if(isPressed[index]^isPressed[index+1]) {
				Vector3f direction = getAxis(i);
				if(isPressed[index+1])
					direction.negate();
				direction.scale(speed*DisplayManager.getFrameTimeSeconds());
				Vector3f.add(cameraPosition, direction, cameraPosition);
				updateMatrix = true;
			}
		}
	}
	
	/**
	 * @param axis The axis to get [0 = FORWARD, 1 = RIGHT, 2 = UP]
	 * @return A camera movement axis
	 */
	private Vector3f getAxis(int axis) {
		if(axis!=2){
			if(axis==0)
				return inverseRotation.multiply(FORWARD, null);
			return inverseRotation.multiply(RIGHT, null);
		}
		return new Vector3f(UP);
	}
	
	/**
	 * Handles mouse input
	 */
	private void rotate() {
		boolean update = false;
		float dx = MouseManager.getDX();
		float dy = MouseManager.getDY();
		if(dx!=0) {
			dx*=angularSpeed*DisplayManager.getFrameTimeSeconds();
			Quaternion dYaw = new Quaternion(UP, dx);
			Quaternion.multiply(yaw, dYaw, yaw);
			update = true;
		}if(dy!=0) {
			dy*=-angularSpeed*DisplayManager.getFrameTimeSeconds();
			Quaternion dPitch = new Quaternion(RIGHT, dy);
			Quaternion.multiply(pitch, dPitch, pitch);
			constrainPitch();
			update = true;
		}
		
		if(update) {
			refreshRotation();
		}
	}
	
	/**
	 * Recalculates the camera's rotation based on pitch and yaw, flags a matrix update
	 */
	public void refreshRotation(){
		Quaternion.multiply(pitch, yaw, rotation);
		rotation.conjugate(inverseRotation);
		updateMatrix = true;
	}
	
	/**
	 * Enforces the maximum pitch bound
	 */
	private void constrainPitch() {//pitch = (sin[Θ/2],0,0,cos[Θ/2])
		if(pitch.w<min_x){
			pitch.w=min_x;
			pitch.x=Math.copySign(max_y, pitch.x);
		}
	}
	
	/**
	 * Sets the pitch bounds
	 * @param y The sin of half of the maximum (absolute) pitch
	 */
	public void setMaxPitch(float y) {
		max_y = y;
		min_x = (float)Math.sqrt(1-y*y);
	}
	
	/**
	 * Sets the camera's movement speed while not tracking an entity
	 * @param speed The camera's speed in units per second of key activation
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	/**
	 * Sets the camera's rotaional speed in radians per second per pixel of mouse movement
	 */
	public void setAngularSpeed(float speed) {
		angularSpeed = speed;
	}
	
	/**
	 * Gets the list of keys that trigger movement 0 = FORWARD, 1 = BACKWARD, 2 = RIGHT, 3 = LEFT, 4 = UP, 5 = DOWN
	 * @param key The index of the key list to return
	 */
	public ArrayList<Integer> getKeyList(int key) {
		return keyMap[key];
	}
	
	/**
	 * Sets all 6 keys at once
	 * @param keys The array of keycodes {forward, backward, right, left, up, down}
	 */
	public void addKeySet(int[] keys) {
		for(int i = 0; i < keyMap.length; i++) {
			keyMap[i].add(keys[i]);
		}
	}
	
	/**
	 * Empties the key mappings, be sure to add new keys
	 */
	public void clearKeyMap() {
		for(ArrayList<Integer> keys : keyMap) {
			keys.clear();
		}
	}
	
	/**
	 * @param speed The distance traveled per mouse wheel rotation
	 */
	public void setZoomSpeed(float speed) {
		zoomSpeed = speed;
	}
	
	/**
	 * @param min The minimum distance between the camera and followed object
	 * @param max The maximum distance between the camera and followed object
	 */
	public void setZoomBounds(float min, float max) {
		zoomBounds.x = min;
		zoomBounds.y = max;
	}
	
	/**
	 * Allows the camera to move independently via the mapped keys
	 */
	public void stopFollowing() {
		followPosition = null;
		follow = null;
	}
	
	public void follow(Entity following) {
		follow = following;
		followPosition = following.getPosition();
	}
	
	/**
	 * Whether lookAt() and lookAtSmooth() should be bounded by maxPitch
	 */
	public void constrainLookAt(boolean constrain) {
		constrainLookAt = constrain;
	}
	
	/**
	 * Points the camera at the target
	 * @param target The direction to point the camera
	 */
	public void lookAt(Vector3f target) {
		target.normalise();
		Maths.calculatePitchYaw(target, pitch, yaw);
		yaw.y = -yaw.y;
		if(pitch.w>0)
			pitch.x=-pitch.x;
		else
			pitch.w=-pitch.w;
		if(constrainLookAt)
			constrainPitch();
		refreshRotation();
	}
	
	/**
	 * Smoothly points the camera at the target with a SLERP
	 * @param target the direction to point the camera
	 * @param seconds the length of interpolation in seconds
	 */
	public void lookAtSmooth(Vector3f target, float seconds) {
		target.normalise();
		Quaternion targetPitch = new Quaternion();
		Quaternion targetYaw = new Quaternion();
		Maths.calculatePitchYaw(target, targetPitch, targetYaw);
		
		targetYaw.y = -targetYaw.y;
		if(targetPitch.w>0)
			targetPitch.x=-targetPitch.x;
		else
			targetPitch.w=-targetPitch.w;
		
		lerp_pitch.recycle(pitch, targetPitch, seconds);
		lerp_yaw.recycle(yaw, targetYaw, seconds);
	}
	
	public Matrix4f getViewMatrix() {
		if(updateMatrix) {
			viewMatrix = rotation.getMatrix4f();
			Vector3f viewPos = new Vector3f(cameraPosition);
			viewPos.negate();
			Matrix4f.translate(viewPos, viewMatrix, viewMatrix);
			updateMatrix = false;
		}
		return viewMatrix;
	}
	
	public Vector3f getDirection() {
		return Quaternion.multiply(inverseRotation, FORWARD, null);
	}
	
	public Vector3f getPosition() {
		return cameraPosition;
	}
	
	/**
	 * Reflect the camera for water rendering
	 */
	public void reflect(float waterHeight) {
		float distance = (cameraPosition.z-waterHeight)*2;
		cameraPosition.z -= distance;
		pitch.w *= -1;
	}
	
	/**
	 * @return The default camera movement key mappings [forward, backward, right, left, up, down]
	 */
	public static ArrayList<Integer>[] getDefaultKeys() {
		int numKeys = 6;
		@SuppressWarnings("unchecked")
		ArrayList<Integer>[] keys = new ArrayList[numKeys];
		for(int i = 0; i < numKeys; i++) {
			keys[i] = new ArrayList<Integer>();
			for(int j = i; j < defaultKeys.length; j+=numKeys) {
				keys[i].add(defaultKeys[j]);
			}
		}
		return keys;
	}
	
	
}
