#version 130

in vec2 position;
in vec2 textureCoords;

out vec2 texCoords;
out vec2 pos;

uniform vec2 translation;
uniform vec2 localTranslation;
uniform vec2 parentScale;
uniform vec2 parentPosition;
uniform float scale;
uniform float aspectRatio;
uniform float lineOffset;

void main(){ 
	pos = (position+localTranslation) * 2;
	pos.x += lineOffset;
	pos *= scale;
	pos.x /= aspectRatio;
	pos.xy += translation;
	
	pos.xy *= parentScale;
	pos.xy += parentPosition;
	
	gl_Position = vec4(pos, 0, 1);
	texCoords = textureCoords;
}