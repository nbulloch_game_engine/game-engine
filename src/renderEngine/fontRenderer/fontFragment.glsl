#version 140

in vec2 texCoords;
in vec2 pos;

out vec4 out_color;

uniform sampler2D fontAtlas;
uniform vec4 color;
uniform vec4 outlineColor;
uniform vec2 min;
uniform vec2 max;
uniform float renderWidth;
uniform float fadeWidth;
uniform float outlineWidth;
uniform int distanceField;
uniform int hasOutline;

void main(){
	if(pos.x > max.x || pos.x < min.x || pos.y > max.y || pos.y < min.y){
		discard;
	}
	if(distanceField==1){
		if(hasOutline==1){
			float pixelDistance = 1 - texture(fontAtlas, texCoords).a;
			float alpha = smoothstep(fadeWidth+outlineWidth, renderWidth+outlineWidth, pixelDistance);
			float mixFactor = (pixelDistance - renderWidth) / (fadeWidth - renderWidth);
			vec4 finalColor = mix(color, outlineColor, clamp(mixFactor, 0, 1));
			out_color = vec4(finalColor.xyz, finalColor.a * alpha);
		}else{
			float pixelDistance = 1 - texture(fontAtlas, texCoords).a;
			float alpha = smoothstep(fadeWidth, renderWidth, pixelDistance);
			out_color = vec4(color.xyz, color.a * alpha);
		}
	}else{
		out_color = texture(fontAtlas, texCoords)*color;
	}
}