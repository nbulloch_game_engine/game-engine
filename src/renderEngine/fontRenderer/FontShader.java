package renderEngine.fontRenderer;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.shaders.ShaderProgram;

public class FontShader extends ShaderProgram {

	public static final String vertexFile = "/renderEngine/fontRenderer/fontVertex.glsl";
	public static final String fragmentFile = "/renderEngine/fontRenderer/fontFragment.glsl";
	
	private int
	location_translation,
	location_distanceField,
	location_color,
	location_localTranslation,
	location_fontAtlas,
	location_scale,
	location_renderWidth,
	location_fadeWidth,
	location_outlineColor,
	location_outlineWidth,
	location_aspectRatio,
	location_lineOffset,
	location_hasOutline,
	location_parentScale,
	location_parentPosition,
	location_min,
	location_max;
	
	public FontShader() {
		super(vertexFile, fragmentFile);
	}
	
	@Override
	protected void bindAttributes() {
		super.bindFragOutput(0, "out_color");
		super.bindAttribute(1, "out_bloomColor");
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
	}
	
	@Override
	protected void getAllUniformLocations() {
		location_translation = super.getUniformLocation("translation");
		location_distanceField = super.getUniformLocation("distanceField");
		location_fontAtlas = super.getUniformLocation("fontAtlas");
		location_scale = super.getUniformLocation("scale");
		location_color = super.getUniformLocation("color");
		location_localTranslation = super.getUniformLocation("localTranslation");
		location_renderWidth = super.getUniformLocation("renderWidth");
		location_fadeWidth = super.getUniformLocation("fadeWidth");
		location_outlineColor = super.getUniformLocation("outlineColor");
		location_outlineWidth = super.getUniformLocation("outlineWidth");
		location_aspectRatio = super.getUniformLocation("aspectRatio");
		location_lineOffset = super.getUniformLocation("lineOffset");
		location_hasOutline = super.getUniformLocation("hasOutline");
		location_parentScale = super.getUniformLocation("parentScale");
		location_parentPosition = super.getUniformLocation("parentPosition");
		location_min = super.getUniformLocation("min");
		location_max = super.getUniformLocation("max");
	}
	
	protected void loadHasOutline(boolean hasOutline){
		super.loadBoolean(location_hasOutline, hasOutline);
	}
	
	protected void loadLineOffset(float lineOffset){
		super.loadFloat(location_lineOffset, lineOffset);
	}
	
	protected void loadAspectRatio(float aspectRatio){
		super.loadFloat(location_aspectRatio, aspectRatio);
	}
	
	protected void loadOutlineColor(Vector4f color){
		super.loadVector4f(location_outlineColor, color);
	}
	
	protected void loadOutlineWidth(float width){
		super.loadFloat(location_outlineWidth, width);
	}
	
	protected void loadFadeWidth(float fadeWidth){
		super.loadFloat(location_fadeWidth, fadeWidth);
	}
	
	protected void loadRenderWidth(float width){
		super.loadFloat(location_renderWidth, width);
	}
	
	protected void loadColor(Vector4f color){
		super.loadVector4f(location_color, color);
	}
	
	protected void loadLocalTranslation(Vector2f translation){
		super.loadVector2f(location_localTranslation, translation);
	}
	
	protected void loadScale(float scale){
		super.loadFloat(location_scale, scale);
	}
	
	protected void connectTextureUnits(){
        super.loadInt(location_fontAtlas, 0);
    }
	
	protected void loadTranslation(Vector2f translation){
		super.loadVector2f(location_translation, translation);
	}
	
	protected void loadDistanceField(boolean hasDistanceField){
		super.loadBoolean(location_distanceField, hasDistanceField);
	}
	
	protected void loadParentScale(Vector2f scale){
		super.loadVector2f(location_parentScale, scale);
	}
	
	protected void loadParentPosition(Vector2f position){
		super.loadVector2f(location_parentPosition, position);
	}
	
	protected void loadBounds(Vector2f max, Vector2f min){
		super.loadVector2f(location_max, max);
		super.loadVector2f(location_min, min);
	}
}
