package renderEngine.fontRenderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.font.Character;
import renderEngine.font.DynamicText;
import renderEngine.font.Font;
import renderEngine.font.FontSettings;
import renderEngine.font.Line;
import renderEngine.font.Text;
import renderEngine.font.TextMaster;
import renderEngine.models.RawModel;
import renderEngine.renderEngine.DisplayManager;

public class FontRenderer {

	private static FontShader shader = fontShader();
	
	public static FontShader fontShader(){
		shader = new FontShader();
		shader.start();
		shader.connectTextureUnits();
		shader.stop();
		return shader;
	}
	
	public static void render(Iterator<Font> fonts){
		glCullFace(GL_FRONT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		shader.start();
		shader.loadAspectRatio(DisplayManager.getAspectRatio());
		while(fonts.hasNext()){
			Font font = fonts.next();
			renderText(font.getTexts().iterator(), font.getDynamicTexts().iterator(), font.getCharacters());
		}
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);
		shader.stop();
	}
	
	public static void render(Iterator<Text> texts, Iterator<DynamicText> dynamicTexts, Font font){
		glCullFace(GL_FRONT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		shader.start();
		shader.loadAspectRatio(DisplayManager.getAspectRatio());
		prepareFont(font);
		
		renderText(texts, dynamicTexts, font.getCharacters());
		
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);
		shader.stop();
	}
	
	public static void render(Iterator<Text> texts, Iterator<DynamicText> dynamicTexts){
		render(texts, dynamicTexts, TextMaster.getFont());
	}
	
	private static void renderText(Iterator<Text> texts, Iterator<DynamicText> dynamicTexts, HashMap<Integer, Character> characters){
		while(texts.hasNext()){
			Text text = texts.next();
			text.update();
			if(text.isEnabled()&&text.getParent().enabled()) {
				shader.loadTranslation(text.getPosition());
				shader.loadAspectRatio(DisplayManager.getAspectRatio() * text.getAspectRatio());
				shader.loadScale(text.getScale());
				if(text.getParent()!=null){
					shader.loadParentScale(text.getParent().getWorldSize());
					shader.loadParentPosition(text.getParent().getWorldPosition());
				}else{
					shader.loadParentScale(new Vector2f(1,1));
					shader.loadParentPosition(new Vector2f());
				}
				prepareSettings(text.getSettings());
				shader.loadLocalTranslation(new Vector2f());
				shader.loadBounds(text.getParent().getMax(), text.getParent().getMin());
				for(Line line : text.getLines()){
					prepareModel(line.getModel());
					shader.loadLineOffset(line.getLineOffset());
					glDrawArrays(GL_TRIANGLES, 0, line.getModel().getVertexCount());
					unbindVao();
				}
			}
		}while(dynamicTexts.hasNext()){
			DynamicText text = dynamicTexts.next();
			text.update();
			if(text.isEnabled()&&text.getParent().enabled()) {
				HashMap<Integer, ArrayList<Vector3f>> dynamicCharacters = text.getCharacters();
				prepareSettings(text.getSettings());
				shader.loadTranslation(text.getPosition());
				shader.loadAspectRatio(DisplayManager.getAspectRatio() * text.getAspectRatio());
				shader.loadScale(text.getScale());
				if(text.getParent()!=null){
					shader.loadParentScale(text.getParent().getWorldSize());
					shader.loadParentPosition(text.getParent().getWorldPosition());
				}else{
					shader.loadParentScale(new Vector2f(1,1));
					shader.loadParentPosition(new Vector2f());
				}
				shader.loadBounds(text.getParent().getMax(), text.getParent().getMin());
				for(Integer i : dynamicCharacters.keySet()){
					RawModel model = characters.get(i).getModel();
					prepareModel(model);
					for(Vector3f position : dynamicCharacters.get(i)){
						shader.loadLocalTranslation(new Vector2f(position));
						shader.loadLineOffset(position.z);
						glDrawArrays(GL_TRIANGLES, 0, model.getVertexCount());
					}
					unbindVao();
				}
			}
		}
	}
	
	public static void clean(){
		shader.clean();
	}
	
	private static void prepareFont(Font font){
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, font.getTextureAtlas());
		shader.loadDistanceField(font.hasDistanceField());
	}
	
	private static void prepareModel(RawModel model){
		glBindVertexArray(model.getVaoID());
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
	}
	
	private static void prepareSettings(FontSettings settings){
		shader.loadColor(settings.getColor());
		shader.loadRenderWidth(settings.getRenderWidth());
		shader.loadFadeWidth(settings.getFadeWidth());
		shader.loadHasOutline(settings.hasOutline());
		if(settings.hasOutline()){
			shader.loadOutlineColor(settings.getOutlineColor());
			shader.loadOutlineWidth(settings.getOutlineWidth());
		}
	}
	
	private static void unbindVao(){
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);
	}
}
