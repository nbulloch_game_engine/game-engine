package renderEngine.models;

import renderEngine.normalMappingObjConverter.NormalMappedObjLoader;
import renderEngine.objConverter.OBJLoader;
import renderEngine.renderEngine.Loader;
import renderEngine.textures.ModelTexture;

public class TexturedModel {

	private RawModel model;
	private ModelTexture texture;
	private boolean hasShadows = true;
	private boolean castsShadows = true;
	private int tiles = 1;
	private boolean translucent = false;

	public TexturedModel(TexturedModel model){
		this.model = model.model;
		this.texture = model.texture;
		this.hasShadows = model.hasShadows;
	}
	
	public TexturedModel(String model, String texture, boolean normalMapped){
		this.model = normalMapped?NormalMappedObjLoader.loadOBJ(model):OBJLoader.loadOBJ(model);
		this.texture = new ModelTexture(Loader.loadTexture(texture));
	}
	
	public TexturedModel(RawModel model, ModelTexture texture){
		this.model = model;
		this.texture = texture;
	}
	
	public TexturedModel(String model, int texture, boolean normalMapped){
		this.model = normalMapped?NormalMappedObjLoader.loadOBJ(model):OBJLoader.loadOBJ(model);
		this.texture = new ModelTexture(texture);
	}

	public RawModel getModel() {
		return model;
	}

	public ModelTexture getTexture() {
		return texture;
	}
	
	public void setTexture(ModelTexture texture){
		this.texture = texture;
	}
	
	public void noShadows(){
		hasShadows = false;
	}
	
	public boolean hasShadows(){
		return hasShadows;
	}
	
	public void setTiles(int tiles){
		this.tiles = tiles;
	}
	
	public int getTiles(){
		return tiles;
	}
	
	public void setTranslucent(boolean translucent){
		this.translucent = translucent;
	}
	
	public boolean isTranslucent(){
		return translucent;
	}

	public boolean castsShadows() {
		return castsShadows;
	}

	public void setCastsShadows(boolean castsShadows) {
		this.castsShadows = castsShadows;
	}
}
