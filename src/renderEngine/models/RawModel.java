package renderEngine.models;

public class RawModel {

	private int vaoID, vertexCount;
	int[] vboIDs;
	
	public RawModel(int vaoID, int[] vboIDs, int vertexCount){
		this.vaoID = vaoID;
		this.vboIDs = vboIDs;
		this.vertexCount = vertexCount;
	}
	
	public int getVaoID() {
		return vaoID;
	}

	public int getVertexCount() {
		return vertexCount;
	}
	
	public int[] getVboIDs(){
		return vboIDs;
	}
}
