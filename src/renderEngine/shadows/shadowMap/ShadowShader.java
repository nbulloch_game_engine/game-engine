package renderEngine.shadows.shadowMap;

import org.lwjgl.util.vector.Matrix4f;

import renderEngine.shaders.ShaderProgram;

public class ShadowShader extends ShaderProgram {
	
	private static final String VERTEX_FILE = "/renderEngine/shadows/shadowMap/shadowVertexShader.glsl";
	private static final String FRAGMENT_FILE = "/renderEngine/shadows/shadowMap/shadowFragmentShader.glsl";
	
	private int location_transformationMatrix;
	private int location_projectionViewMatrix;
	
	protected ShadowShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionViewMatrix = super.getUniformLocation("projectionViewMatrix");
	}
	
	protected void loadTransformationMatrix(Matrix4f transformationMatrix){
		super.loadMatrix(location_transformationMatrix, transformationMatrix);
	}
	
	protected void loadProjectionViewMatrix(Matrix4f projectionViewMatrix){
		super.loadMatrix(location_projectionViewMatrix, projectionViewMatrix);
	}
	
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "in_textureCoords");
	}
}
