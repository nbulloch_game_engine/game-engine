#version 130

in vec3 position;
in vec2 in_textureCoords;

out vec2 textureCoords;

uniform mat4 transformationMatrix;
uniform mat4 projectionViewMatrix;

void main(void){
	gl_Position = projectionViewMatrix * transformationMatrix * vec4(position,1.0);
	textureCoords = in_textureCoords;
	
}