package renderEngine.shadows.cascadedShadowMap;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import renderEngine.entities.Entity;
import renderEngine.models.RawModel;
import renderEngine.models.TexturedModel;
import renderEngine.shadows.shadowMap.ShadowFrameBuffer;

public class CsmEntityRenderer {

	private CsmShader shader;
	private CsmPartition[] partitions;
	
	public CsmEntityRenderer(CsmShader shader, CsmPartition[] partitions){
		this.shader = shader;
		this.partitions = partitions;
	}
	
	public void render(HashMap<TexturedModel, ArrayList<Entity>> entities, ShadowFrameBuffer[] fbos){
		shader.start();
		glCullFace(GL_FRONT);
		Matrix4f[] projectionMatrices = new Matrix4f[partitions.length];
		for(int i = 0; i < partitions.length; i++){
			projectionMatrices[i] = partitions[i].getProjectionMatrix();
		}
		Matrix4f[] toShadowSpace = new Matrix4f[partitions.length];
		for(int i = 0; i < partitions.length; i++){
			toShadowSpace[i] = partitions[i].getToShadowSpace();
		}
		shader.loadProjectionMatrices(projectionMatrices);
		shader.loadToShadowSpace(toShadowSpace);
		for(int i = 0; i < fbos.length; i++){
			fbos[i].bindFrameBuffer();
			GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
			shader.loadProjectionMatrices(projectionMatrices);
			shader.loadToShadowSpace(toShadowSpace);
			shader.loadPartition(i);
			for (TexturedModel model : entities.keySet()) {
				shader.loadTiles(model.getTiles());
				RawModel rawModel = model.getModel();
				GL30.glBindVertexArray(rawModel.getVaoID());
				GL20.glEnableVertexAttribArray(0);
				GL20.glEnableVertexAttribArray(1);
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.getTexture().getID());
				if(model.getTexture().hasTransparency()){
					glDisable(GL_CULL_FACE);
				}
				for (Entity entity : entities.get(model)) {
					shader.loadTransformationMatrix(entity.getTransformation());
					GL11.glDrawElements(GL11.GL_TRIANGLES, rawModel.getVertexCount(),
							GL11.GL_UNSIGNED_INT, 0);
				}
				if(model.getTexture().hasTransparency()){
					glEnable(GL_CULL_FACE);
					glCullFace(GL_FRONT);
				}
			}
			fbos[i].unbindFrameBuffer();
		}
		glCullFace(GL_BACK);
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL30.glBindVertexArray(0);
		shader.stop();
	}
}
