#version 130

in vec3 position;
in vec2 in_textureCoords;

out vec2 textureCoords;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrices[5];
uniform mat4 toShadowSpace[5];
uniform int partition;
uniform int tiles;

void main(void){
	gl_Position = projectionMatrices[partition] * toShadowSpace[partition] * transformationMatrix * vec4(position,1.0);
	textureCoords = in_textureCoords * tiles;
	
}