package renderEngine.shadows.cascadedShadowMap;

import java.util.ArrayList;
import java.util.HashMap;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.Entity;
import renderEngine.entities.Scene;
import renderEngine.models.TexturedModel;
import renderEngine.shadows.ShadingModel;
import renderEngine.shadows.shadowMap.ShadowFrameBuffer;
import renderEngine.utils.Maths;
import renderEngine.utils.Quaternion;

public class CsmMasterRenderer implements ShadingModel {

	private static Matrix4f lightViewMatrix = new Matrix4f();
	
	private CsmPartition[] partitions;
	private Matrix4f[] toShadowSpaces;
	private Scene scene;
	
	private CsmEntityRenderer renderer;
	
	private ShadowFrameBuffer[] fbos;
	
	public CsmMasterRenderer(Scene scene) {
		this.scene = scene;
		Vector3f[] frustumDirection = caluculateFrustumDirections(scene);
		float[] splits = generateSplits(scene);
		partitions = new CsmPartition[scene.getNumCascades()];
		toShadowSpaces = new Matrix4f[scene.getNumCascades()];
		for(int i = 0; i < partitions.length; i++){
			partitions[i] = new CsmPartition(scene.getCamera(), splits[i], splits[i+1], frustumDirection, fbos[i].getHeight(), scene);
		}
		renderer = new CsmEntityRenderer(new CsmShader(), partitions);
		updateSun();
		update();
	}
	
	private float[] generateSplits(Scene scene){
		int numSplits = scene.getNumCascades();
		fbos = new ShadowFrameBuffer[numSplits];
		float[] splits = new float[numSplits+1];
		splits[0] = 0;
		for(int i = 1; i < splits.length; i++){
			float near = scene.getNearPlane();
			float far = scene.getFarPlane();
			float uniform = (float)i / numSplits;
			float logarithmic = (float) (near * Math.pow((far / near), (float)i / numSplits))/far;
			splits[i] = (uniform + logarithmic)/2;//practical
		}
		float normalizeOffset = splits[1];
		float normalizeFactor = 1/((splits[numSplits] - splits[numSplits-1]) - normalizeOffset);//to 0.0-1.0
		for(int i = 0; i < numSplits; i++){
			float scalar = ((splits[i+1] - splits[i]) - normalizeOffset) * normalizeFactor;
			int mapSize = (int) ((scene.getMaxShadowSize() - scene.getMinShadowSize()) * (1 - scalar) + scene.getMinShadowSize());//interpolates max->min (near->far)
			int mapSize2 = (int) ((scene.getMaxShadowSize() - scene.getMinShadowSize()) * (1-((float)i/(numSplits-1))) + scene.getMinShadowSize());
			mapSize = (mapSize+mapSize2)/2;
			fbos[i] = new ShadowFrameBuffer(mapSize, mapSize);
		}
		return splits;
	}

	public void render(HashMap<TexturedModel, ArrayList<Entity>> entities, DirectionalLight sun) {
		renderer.render(entities, fbos);
	}
	
	public void update(){
		Matrix4f inverseViewMatrix = new Matrix4f(scene.getCamera().getViewMatrix());
		inverseViewMatrix.invert();
		
		for(int i = 0; i < partitions.length; i++){
			partitions[i].update(scene.getCamera().getPosition(), Maths.toMatrix3f(inverseViewMatrix), lightViewMatrix, scene.getSceneAABB(), scene.getSun().getDirection());
			toShadowSpaces[i] = partitions[i].getToShadowSpaceProj();
		}
	}
	
	public void updateSun(){
		Vector3f sun = scene.getSun().getDirection();
		sun.normalise();
		float pitch = (float) Math.atan2(sun.y, sun.z);
		float yaw = (float) Math.acos(sun.x);
		yaw-=Math.PI/2;
		
		Quaternion pitchq = new Quaternion();
		Quaternion yawq = new Quaternion();
		Maths.calculatePitchYaw(sun, pitchq, yawq);
		
		lightViewMatrix.setIdentity();
		Matrix4f.rotate(pitch, new Vector3f(1, 0, 0), lightViewMatrix, lightViewMatrix);
		Matrix4f matrixY = new Matrix4f();
		matrixY.setIdentity();
		Matrix4f.rotate(yaw, new Vector3f(0, 1, 0), matrixY, matrixY);//cw
		
		Matrix4f.mul(matrixY, lightViewMatrix, lightViewMatrix);
	}
	
	private Vector3f[] caluculateFrustumDirections(Scene scene){
		Vector3f[] frustum = new Vector3f[4];
		double FOV = Math.toRadians(scene.getFOV());
		float far = scene.getFarPlane();
		float near = scene.getNearPlane();
		float dyFar = (float)(far*Math.tan(FOV));
		float dyNear = (float)(near*Math.tan(FOV));
		float aspectRatio = Display.getWidth() / Display.getHeight();
		float dxFar = dyFar * aspectRatio;
		float dxNear = dyNear * aspectRatio;
		frustum[0] = new Vector3f(dxFar, dyFar, -far);
		frustum[1] = new Vector3f(-dxFar, dyFar, -far);
		frustum[2] = new Vector3f(dxNear, -dyNear, -near);
		frustum[3] = new Vector3f(-dxNear, -dyNear, -near);
		return frustum;
	}
	
	public CsmPartition[] getParitions(){
		return partitions;
	}
	
	public Matrix4f[] getToShadowSpaces(){
		return toShadowSpaces;
	}
	
	public int[] getCascadeSizes(){
		int[] sizes = new int[fbos.length];
		for(int i = 0; i < fbos.length; i++){
			sizes[i] = fbos[i].getHeight();
		}
		return sizes;
	}
	
	public float[] getDepths(){
		float[] depths = new float[fbos.length];
		for(int i = 0; i < depths.length; i++){
			depths[i] = partitions[i].getDepth();
		}
		return depths;
	}
	
	public int[] getShadowMaps(){
		int[] shadowMaps = new int[fbos.length];
		for(int i = 0; i < shadowMaps.length; i++){
			shadowMaps[i] = fbos[i].getShadowMap();
		}
		return shadowMaps;
	}
}
