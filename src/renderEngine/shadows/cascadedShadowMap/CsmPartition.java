package renderEngine.shadows.cascadedShadowMap;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import physicsEngine.collisionDetection.CollisionDetection;
import physicsEngine.collisionDetection.primitives.AABB;
import physicsEngine.collisionDetection.primitives.Plane;
import physicsEngine.collisionDetection.primitives.Ray;
import renderEngine.entities.Camera;
import renderEngine.entities.Scene;
import renderEngine.utils.Maths;

public class CsmPartition {
	
	private Vector3f[] localSubFrustum;//0-3:near, 4-7:far; (x,y): (+,+), (-,+), (+,-), (-,-);
	private Vector3f[] worldSubFrustum;//"													 "
	
	private Matrix4f projectionMatrix;
	private Matrix4f toShadowSpace;
	private float depth;
	private boolean render = true;
	private int size;
	
	//start and end are between 0.0 and 1.0 of camera frustum
	public CsmPartition(Camera camera, float partitionStart, float partitionEnd, Vector3f[] frustum, int size, Scene scene){
		this.size = size;
		projectionMatrix = new Matrix4f();
		toShadowSpace = new Matrix4f();
		buildSubFrustum(frustum, partitionStart, partitionEnd, camera.getViewMatrix(), scene);
	}
	
	private void buildSubFrustum(Vector3f[] frustum, float start, float end, Matrix4f viewMatrix, Scene scene){
		float frustumFar = frustum[0].length();
		depth = (scene.getFarPlane()-scene.getNearPlane())*start-scene.getNearPlane();
		float frustumNear = frustum[2].length();
		Vector3f[] directions = new Vector3f[frustum.length];
		localSubFrustum = new Vector3f[8];
		worldSubFrustum = new Vector3f[8];
		for(int i = 0; i < directions.length; i++){
			directions[i] = new Vector3f(frustum[i]);
			directions[i].normalise();
		}
		for(int n = 0; n < 4; n++){
			Vector3f direction = new Vector3f(directions[n]);
			direction.scale((start*(frustumFar-frustumNear)) + frustumNear);
			localSubFrustum[n] = direction;
			worldSubFrustum[n] = new Vector3f();
			direction = directions[n];
			direction.scale((end*frustumFar-frustumNear) + frustumNear);
			localSubFrustum[n+4] = direction;
			worldSubFrustum[n+4] = new Vector3f();
		}
		for(Vector3f point : localSubFrustum){
			Vector4f point4f = new Vector4f(Maths.toVector4f(point, false));
			Matrix4f.transform(viewMatrix, point4f, point4f);
			point = Maths.toVector3f(point4f);
		}
	}
	
	public void update(Vector3f cameraPos, Matrix3f viewMatrix, Matrix4f lightViewMatrix, AABB sceneAABB, Vector3f sun){
		render = true;
		for(int i = 0; i < localSubFrustum.length; i++){
			Matrix3f.transform(viewMatrix, localSubFrustum[i], worldSubFrustum[i]);
			Vector3f.add(cameraPos, worldSubFrustum[i], worldSubFrustum[i]);
		}
		updateShadowBox(lightViewMatrix, sceneAABB, sun);
	}
	
	private AABB getLightViewBox(Matrix4f lightViewMatrix){
		Vector3f[] lightViewSubFrustum = new Vector3f[worldSubFrustum.length];
		for(int i = 0; i < worldSubFrustum.length; i++){
			lightViewSubFrustum[i] = Maths.toVector3f(
					Matrix4f.transform(lightViewMatrix, Maths.toVector4f(worldSubFrustum[i], false), null));
		}
		return new AABB(lightViewSubFrustum);
	}
	
	private Vector3f[] getWorldBox(AABB lightViewShadowBox, Matrix4f iLightViewMatrix){
		Vector3f[] lightViewPoints = lightViewShadowBox.getPoints();//(---),(--+),(-+-),(-++),(+--),(+-+),(++-),(+++)
		Vector3f[] shadowBox = new Vector3f[lightViewPoints.length];
		for(int i = 0; i < lightViewPoints.length; i++){
			shadowBox[i] = Maths.toVector3f(
					Matrix4f.transform(iLightViewMatrix, Maths.toVector4f(lightViewPoints[i], false), null)
					);
		}
		return shadowBox;
	}
	
	private boolean getBoundingPlanes(Plane planeUp, Plane planeDown, AABB sceneAABB, Vector3f boxCenter, Vector3f boxUp, float boxWidth, float boxDepth, Vector3f sun){
		float firstLength = Float.POSITIVE_INFINITY;
		Vector3f boxCenterUp = Vector3f.add(boxCenter, boxUp, null);
		Plane[] scenePlanes = sceneAABB.getPlanes();
		float adjustX = (boxWidth/2)/Math.abs(Vector3f.dot(new Vector3f(sun.x, sun.y, 0).normalise(null), new Vector3f(0,1,0)));
		float adjustZ = (boxDepth/2)/Math.abs(Vector3f.dot(new Vector3f(0, sun.y, sun.z).normalise(null), new Vector3f(0,1,0)));
		Vector3f n = sceneAABB.getMin();
		Vector3f.sub(n, new Vector3f(adjustX, 0, adjustZ), n);
		Vector3f x = sceneAABB.getMax();
		Vector3f.add(x, new Vector3f(adjustX, 0, adjustZ), x);
		boolean intersects = false;
		for(Plane plane : scenePlanes){
			float iScalar = CollisionDetection.linePlaneScalar(plane, new Ray(boxCenter, boxCenterUp));
			Vector3f i = CollisionDetection.linePlaneI(plane, new Ray(boxCenter, boxCenterUp));
			if((plane.n.x!=0||(i.x<=x.x&&i.x>=n.x))&&(plane.n.y!=0||(i.y<=x.y&&i.y>=n.y))&&(plane.n.z!=0||(i.z<=x.z&&i.z>=n.z))){
				if(firstLength==Float.POSITIVE_INFINITY){
					intersects = true;
					firstLength = iScalar;
					planeUp.n = plane.n;
					planeUp.v0 = plane.v0;
				}else{
					if(iScalar>firstLength){
						planeDown.n = planeUp.n;
						planeDown.v0 = planeUp.v0;
						planeUp.n = plane.n;
						planeUp.v0 = plane.v0;
					}else{
						planeDown.n = plane.n;
						planeDown.v0 = plane.v0;
					}
					break;
				}
			}
		}
		return intersects;
	}
	
	private boolean getCorners(Vector3f upCorner, Vector3f downCorner, Plane planeUp, Plane planeDown, Vector3f[] shadowBox){
		float upToPlane = Float.MAX_VALUE;
		float downToPlane = Float.MAX_VALUE;
		Vector3f[] up = {shadowBox[0],shadowBox[2],shadowBox[4],shadowBox[6]};
		Vector3f[] down = {shadowBox[1],shadowBox[3],shadowBox[5],shadowBox[7]};
		for(int i = 0; i < up.length; i++){
			float distance = Maths.projectionFactor(planeUp.n, up[i]);
			if(distance<upToPlane){
				upToPlane = distance;
				upCorner.x = up[i].x;
				upCorner.y = up[i].y;
				upCorner.z = up[i].z;
			}
			distance = Maths.projectionFactor(planeDown.n, down[i]);
			if(distance<downToPlane){
				downToPlane = distance;
				downCorner.x = down[i].x;
				downCorner.y = down[i].y;
				downCorner.z = down[i].z;
			}
		}
		return downToPlane<=0;
	}
	
	private Vector3f getCenter(Matrix4f iLightViewMatrix, AABB lightViewShadowBox, float boxWidth, float boxDepth, float boxHeight, Vector4f topLightView){
		Vector3f adjustedCenter = new Vector3f(lightViewShadowBox.getCenter().x, lightViewShadowBox.getCenter().y, topLightView.z-boxHeight/2);
		adjustedCenter.x-=adjustedCenter.x%(1/(float)size*boxWidth);//discretize
		adjustedCenter.y-=adjustedCenter.y%(1/(float)size*boxDepth);
		adjustedCenter.z-=adjustedCenter.z%(1/(float)size*boxHeight);
		adjustedCenter = Maths.toVector3f(
				Matrix4f.transform(iLightViewMatrix, Maths.toVector4f(adjustedCenter, false), null));
		adjustedCenter.negate();
		return adjustedCenter;
	}
	
	private void updateShadowBox(Matrix4f lightViewMatrix, AABB sceneAABB, Vector3f sun){
		//Light Box (Light Space)
		AABB lightViewShadowBox = getLightViewBox(lightViewMatrix);
		Vector3f max = lightViewShadowBox.getMax();
		Vector3f min = lightViewShadowBox.getMin();
		float boxWidth = max.x-min.x;
		float boxDepth = max.y-min.y;
		Matrix4f iLightViewMatrix = Matrix4f.invert(lightViewMatrix, null);
		//Light Box (World Space)
		Vector3f[] shadowBox = getWorldBox(lightViewShadowBox, iLightViewMatrix);
		Vector3f boxUp = Maths.toVector3f(
				Matrix4f.transform(iLightViewMatrix, Maths.toVector4f(new Vector3f(0,0,-1), true), null));
		Vector3f boxCenter = Maths.toVector3f(
				Matrix4f.transform(iLightViewMatrix, Maths.toVector4f(lightViewShadowBox.getCenter(), false), null));
		//Top and Bottom scene bounds
		Plane planeUp = new Plane(null, null);
		Plane planeDown = new Plane(null, null);
		if(getBoundingPlanes(planeUp, planeDown, sceneAABB, boxCenter, boxUp, boxWidth, boxDepth, sun)){
			//Closest Shadow Box Corners to Scene
			Vector3f upCorner = new Vector3f();
			Vector3f downCorner = new Vector3f();
			Vector3f bottom = shadowBox[1];
			if(getCorners(upCorner, downCorner, planeUp, planeDown, shadowBox)){
				//adjusted Shadow Box Corners to Scene
				bottom = CollisionDetection.linePlaneI(planeDown, new Ray(downCorner, Vector3f.add(downCorner, boxUp, null)));
			}
			Vector3f top = CollisionDetection.linePlaneI(planeUp, new Ray(upCorner, Vector3f.add(upCorner, boxUp, null)));
			//Corners in light space -> boxHeight
			Vector4f topLightView = Matrix4f.transform(lightViewMatrix, Maths.toVector4f(top, false), null);
			Vector4f bottomLightView = Matrix4f.transform(lightViewMatrix, Maths.toVector4f(bottom, false), null);
			float boxHeight = topLightView.z-bottomLightView.z;
			//Translate box to Center
			Vector3f center = getCenter(iLightViewMatrix, lightViewShadowBox, boxWidth, boxDepth, boxHeight, topLightView);
			toShadowSpace = new Matrix4f(lightViewMatrix);
			Matrix4f.translate(center, toShadowSpace, toShadowSpace);
			//Build projectionMatrix
			projectionMatrix.setIdentity();
			projectionMatrix.m00 = 2f / (boxWidth);//x
			projectionMatrix.m11 = 2f / (boxDepth);//y
			projectionMatrix.m22 = -2f / (boxHeight);//z
			projectionMatrix.m33 = 1;
		}
	}
	
	public Matrix4f getProjectionMatrix(){
		return projectionMatrix;
	}
	
	public Vector3f[] getSubfrustum(){
		return worldSubFrustum;
	}
	
	public Matrix4f getToShadowSpace(){
		return toShadowSpace;
	}
	
	public Matrix4f getToShadowSpaceProj(){
		Matrix4f matrix = Matrix4f.mul(projectionMatrix, toShadowSpace, null);
		return matrix;
	}
	
	public float getDepth(){
		return depth;
	}
	
	public boolean renderPartition(){
		return render;
	}
}