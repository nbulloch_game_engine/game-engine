package renderEngine.shadows.cascadedShadowMap;

import org.lwjgl.util.vector.Matrix4f;
import renderEngine.renderEngine.MasterRenderer;
import renderEngine.shaders.ShaderProgram;

public class CsmShader extends ShaderProgram{

	private static final String vertexFile = "/renderEngine/shadows/cascadedShadowMap/CsmVertex.glsl";
	private static final String fragmentFile = "/renderEngine/shadows/cascadedShadowMap/CsmFragment.glsl";
	
	private int
	location_toShadowSpace[],
	location_transformationMatrix,
	location_projectionMatrix[],
    location_partition,
    location_tiles;
	
	public CsmShader() {
		super(vertexFile, fragmentFile);
	}

	@Override
	protected void getAllUniformLocations() {
		location_toShadowSpace = new int[MasterRenderer.MAX_CASCADES];
		location_projectionMatrix = new int[MasterRenderer.MAX_CASCADES];
		for(int i = 0; i < MasterRenderer.MAX_CASCADES; i++){
			location_toShadowSpace[i] = super.getUniformLocation("toShadowSpace[" + i + "]");
			location_projectionMatrix[i] = super.getUniformLocation("projectionMatrices[" + i + "]");
		}
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
	    location_partition = super.getUniformLocation("partition");
	    location_tiles = super.getUniformLocation("tiles");
	}
	
	protected void loadTiles(int tiles){
		super.loadInt(location_tiles, tiles);
	}
	
	@Override
	protected void bindAttributes() {
		super.bindFragOutput(0, "out_color");
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "in_textureCoords");
	}
	
	protected void loadPartition(int partition){
		super.loadInt(location_partition, partition);
	}
	
	protected void loadToShadowSpace(Matrix4f[] toShadowSpace){
		for(int i = 0; i < MasterRenderer.MAX_CASCADES; i++){
			if(i < toShadowSpace.length){
				super.loadMatrix(location_toShadowSpace[i], toShadowSpace[i]);
			}
		}
	}
	
	protected void loadProjectionMatrices(Matrix4f[] projectionMatrix){
		for(int i = 0; i < MasterRenderer.MAX_CASCADES; i++){
			if(i < projectionMatrix.length){
				super.loadMatrix(location_projectionMatrix[i], projectionMatrix[i]);
			}
		}
	}
	
	protected void loadTransformationMatrix(Matrix4f transformationMatrix){
		super.loadMatrix(location_transformationMatrix, transformationMatrix);
	}
}
