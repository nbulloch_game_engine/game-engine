package renderEngine.shadows;

import java.util.ArrayList;
import java.util.HashMap;

import renderEngine.entities.DirectionalLight;
import renderEngine.entities.Entity;
import renderEngine.models.TexturedModel;

public interface ShadingModel {
	
	public void render(HashMap<TexturedModel, ArrayList<Entity>> entities, DirectionalLight sun);
	
}
