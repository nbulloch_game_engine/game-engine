package renderEngine.guis;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

public abstract class DisplayCell extends GuiComponent{

	private static Vector4f defaultBackgroundColor = new Vector4f(0.3f, 0.3f, 0.3f, 1);
	
	protected GuiModel background;
	
	protected int index = -1;
	protected boolean remove;
	
	public DisplayCell(Vector2f position, Vector2f size, GridDisplay parent, int index){
		super(position, size, parent);
		this.index = index;
		parent.setCell(this);
		background = new GuiModel(new Vector2f(), new Vector2f(1, 1), defaultBackgroundColor, this);
	}
	
	public DisplayCell(GridDisplay parent, int index) {
		this(new Vector2f(), new Vector2f(1,1), parent, index);
	}
	
	public DisplayCell(Vector2f position, Vector2f size, GridDisplay parent){
		super(position, size, parent);
		parent.addCell(this);
		background = new GuiModel(new Vector2f(), new Vector2f(1, 1), defaultBackgroundColor, this);
	}
	
	public DisplayCell(GridDisplay parent) {
		this(new Vector2f(), new Vector2f(1,1), parent);
	}
	
	public abstract void update();
	
	public void setBackgroundColor(Vector4f color){
		background.setColor(color);
	}
	
	public void setBackgroundTexture(int texture){
		background.setTexture(texture);
	}
	
	public static void setDefaultBackgroundColor(Vector4f color){
		DisplayCell.defaultBackgroundColor = color;
	}
	
	public DisplayCell setIndex(int index){
		this.index = index;
		return this;
	}
	
	public int getIndex(){
		return index;
	}
	
	public void remove() {
		remove = true;
	}
}
