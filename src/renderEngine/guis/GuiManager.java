package renderEngine.guis;

import java.util.ArrayList;
import java.util.HashMap;

public class GuiManager {

	public static int NUM_GUIS = 0;
	public static int NUM_STATES = 0;
	private static ArrayList<Gui> guis;
	private static HashMap<Integer, GuiState> guiStates;
	
	public static void init(){
		guis = new ArrayList<Gui>();
		guiStates = new HashMap<Integer, GuiState>();
	}
	
	public static void register(Gui gui){
		guis.add(gui);
	}
	
	public static void register(GuiState guiState){
		guiStates.put(guiState.getID(), guiState);
	}
	
	public static Gui getGui(int id){
		return guis.get(id);
	}
	
	public static GuiState getGuiState(int id){
		return guiStates.get(id);
	}
}