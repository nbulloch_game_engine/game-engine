package renderEngine.guis;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

public class ToolTip extends GuiComponent{
	
	private GuiComponent container;
	protected long activationTime = 1000;
	private long start = 0;//one start for all containers -> bugs?
	
	public ToolTip(Vector2f position, Vector2f size, GuiComponent parent) {
		super(position, size, parent);
		super.disable();
	}
	
	public void update() {
		if(container!=null&&!container.focus) {
			disable();
		}
	}

	public void update(GuiComponent container){
		update(container, new Vector4f());
	}
	
	public void update(GuiComponent container, Vector4f hotspot) {
		Vector2f size = container.getWorldSize();
		Vector2f pos = container.getWorldPosition();
		Vector4f h = new Vector4f(hotspot);
		boolean interact = container instanceof Interactable;
		boolean override = !(h.x==h.z&&h.y==h.w);
		if(interact||override) {
			if(!override){
				Interactable i = (Interactable) container;
				h = i.getHotspot();
			}
			h.x*=size.x;
			h.z*=size.x;
			h.y*=size.y;
			h.w*=size.y;
			h.x+=pos.x;
			h.z+=pos.x;
			h.y+=pos.y;
			h.w+=pos.y;
		}else {
			h.x=-size.x;
			h.z=size.x;
			h.y=-size.y;
			h.w=size.y;
			h.x+=pos.x;
			h.z+=pos.x;
			h.y+=pos.y;
			h.w+=pos.y;
		}
		Vector2f m = new Vector2f((float)Mouse.getX()/Display.getWidth()  * 2 - 1, (float)Mouse.getY()/Display.getHeight() * 2 - 1);//mouse
		if(h.x<=m.x&&h.z>=m.x && h.y<=m.y&&h.w>=m.y) {//hovering
			if(start==-1) {
				start = System.currentTimeMillis();
			}else if(System.currentTimeMillis()-start>=activationTime&&!enabled) {
				setPosition(m);
				super.enable();
				this.container = container;
				checkBounds();
			}
		}else if(start!=-1){
			super.disable();
			start = -1;
		}
	}
	
	private void checkBounds() {
		Vector2f min = new Vector2f(Float.MAX_VALUE, Float.MAX_VALUE);
		Vector2f max = new Vector2f(Float.MIN_VALUE, Float.MIN_VALUE);
		for(GuiComponent child : getChildren()) {
			Vector2f childMin = child.getWorldMin();
			Vector2f childMax = child.getWorldMax();
			if(childMax.x>max.x)
				max.x=childMax.x;
			if(childMax.y>max.y)
				max.y = childMax.y;
			if(childMin.x<min.x)
				min.x = childMin.x;
			if(childMin.y<min.y)
				min.y = childMin.y;
		}
		Vector2f delta = new Vector2f();
		if(max.x>1)
			delta.x=-max.x+1;
		else if(min.x<-1)
			delta.x=-min.x-1;
		if(max.y>1)
			delta.y=-max.y+1;
		if(min.y<-1)
			delta.y=-min.y-1;
		Vector2f pos = new Vector2f(getCenter());
		pos.x+=delta.x;
		pos.y+=delta.y;
		setPosition(pos);
	}
	
	@Override
	public void enable() {
	}
}
