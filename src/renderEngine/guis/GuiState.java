package renderEngine.guis;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;

public class GuiState extends GuiComponent{
	
	private final int ID = GuiManager.NUM_STATES++;

	private ArrayList<GuiState> subStates = new ArrayList<GuiState>();
	protected Vector2f animationPosition = new Vector2f();
	protected long animationTime = 0;
	protected long activationTime = 0;
	private boolean active = false;
	protected boolean animate = false;
	
	public GuiState() {
		this(new Vector2f(), new Vector2f(1,1));
	}
	
	public GuiState(Vector2f position, Vector2f size){
		super(position, size, null);
		GuiManager.register(this);
	}
	
	public void update(){
		if(animate) 
			animate();
		for(GuiState subState : subStates) {
			subState.update();
		}
	}
	
	public boolean active(){
		return active;
	}
	
	public void activate(){
		active = true;
		activationTime = System.currentTimeMillis();
		setFocus(true);
		for(GuiState subState : subStates) {
			subState.activate();
		}
	}
	
	public void deactivate(){
		active = false;
		setFocus(false);
		for(GuiState subState : subStates) {
			subState.deactivate();
		}
	}  
	
	private void animate(){
		long delta = System.currentTimeMillis() - activationTime;
		if(delta < animationTime){
			float scale = 1 - ((float)delta / (float)animationTime);
			setPosition(new Vector2f(animationPosition.x*scale, animationPosition.y*scale));
		}else{
			setPosition(new Vector2f());
		}
	}
	
	public void disableText(){
		for(GuiComponent child : getChildren()){
			child.disableText();
		}
	}
	
	public void enableText(){
		for(GuiComponent child : getChildren()){
			child.enableText();
		}
	}
	
	public void addSubState(GuiState subState) {
		subStates.add(subState);
		addChild(subState);
	}

	public int getID() {
		return ID;
	}
}
