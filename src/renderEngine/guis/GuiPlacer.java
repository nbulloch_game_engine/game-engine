package renderEngine.guis;

import org.lwjgl.util.vector.Vector2f;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.utils.Transform2D;

public class GuiPlacer {

	private static GuiComponent parent;
	private static Transform2D transform;
	
	public static void init(Vector2f pos, Vector2f size, GuiComponent parent){
		GuiPlacer.init(pos, size, 0, parent);
	}
	
	public static void init(Vector2f pos, Vector2f size, float rotation, GuiComponent parent) {
		GuiPlacer.parent = parent;
		GuiPlacer.transform = new Transform2D(pos, rotation, size);
	}
	
	public static void initWorldCoords(Vector2f pos, Vector2f size, GuiComponent parent){
		Vector2f localSize = size;
		Vector2f localPos = new Vector2f(pos);
		if(parent!=null) {
			Vector2f parentSize = parent.getWorldSize();
			localSize.x /= parentSize.x;
			localSize.y /= parentSize.y;
			Vector2f pWPos = parent.getWorldPosition();
			localPos.x -= pWPos.x;
			localPos.y -= pWPos.y;
			localPos.x /= parentSize.x;
			localPos.y /= parentSize.y;
		}
		init(localPos, localSize, parent);
	}
	
	/**
	 * defines X in terms of a ratio to Y, including a correction for display aspect ratio
	 * @param ratio - x/y, where y = 1
	 */
	
	public static void setXRatio(float ratio){
		Vector2f worldSize = new Vector2f(transform.getScale());
		Vector2f worldPos = new Vector2f(transform.getPosition());
		if(parent!=null) {
			Vector2f parentSize = parent.getWorldSize();
			Vector2f parentPos = parent.getWorldPosition();
			worldSize.x *= parentSize.x;
			worldSize.y *= parentSize.y;
			worldPos.x *= parentSize.x;
			worldPos.y *= parentSize.y;
			worldPos.x += parentPos.x;
			worldPos.y += parentPos.y;
		}
		worldSize.x = worldSize.y * ratio / DisplayManager.getAspectRatio();
		initWorldCoords(worldPos, worldSize, parent);
	}
	
	/**
	 * defines Y in terms of a ratio to X, including a correction for display aspect ratio
	 * @param ratio - y/x, where x = 1
	 */
	
	public static void setYRatio(float ratio){
		Vector2f worldSize = new Vector2f(transform.getScale());
		Vector2f worldPos = new Vector2f(transform.getPosition());
		if(parent!=null) {
			Vector2f parentSize = parent.getWorldSize();
			Vector2f parentPos = parent.getWorldPosition();
			worldSize.x *= parentSize.x;
			worldSize.y *= parentSize.y;
			worldPos.x *= parentSize.x;
			worldPos.y *= parentSize.y;
			worldPos.x += parentPos.x;
			worldPos.y += parentPos.y;
		}
		worldSize.y = worldSize.x * ratio * DisplayManager.getAspectRatio();
		initWorldCoords(worldPos, worldSize, parent);
	}
	
	public static void setUpperCenter(Vector2f position){
		setPosition(new Vector2f(position.x, position.y-transform.getScale().y));
	}
	
	public static void setLowerCenter(Vector2f position){
		setPosition(new Vector2f(position.x, position.y+transform.getScale().y));
	}
	
	public static void setLeftCenter(Vector2f position){
		setPosition(new Vector2f(position.x+transform.getScale().x, position.y));
	}
	
	public static void setRightCenter(Vector2f position){
		setPosition(new Vector2f(position.x-transform.getScale().x, position.y));
	}
	
	public static void setTopRight(Vector2f position){
		setPosition(new Vector2f(position.x-transform.getScale().x, position.y-transform.getScale().y));
	}
	
	public static void setTopLeft(Vector2f position){
		setPosition(new Vector2f(position.x+transform.getScale().x, position.y-transform.getScale().y));
	}
	
	public static void setLowerRight(Vector2f position){
		setPosition(new Vector2f(position.x-transform.getScale().x, position.y+transform.getScale().y));
	}
	
	public static void setLowerLeft(Vector2f position){
		setPosition(new Vector2f(position.x+transform.getScale().x, position.y+transform.getScale().y));
	}
	
	public static void setPosition(Vector2f position){
		transform.setPosition(position);
	}
	
	public static void scaleSize(float scale) {
		transform.scale(scale);
	}

	public static Vector2f getSize() {
		return new Vector2f(transform.getScale());
	}

	public static Vector2f getPos() {
		return new Vector2f(transform.getPosition());
	}
	
	public static float getRotation() {
		return transform.getRotation();
	}
	
	public static GuiComponent getParent() {
		return parent;
	}
}
