package renderEngine.guis;

import java.util.ArrayList;
import java.util.TreeMap;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.renderEngine.DisplayManager;

public class GridDisplay extends GuiComponent{
	
	private static Vector4f defaultBackgroundColor = new Vector4f(0.2f, 0.2f, 0.2f, 1);
	private static float defaultCellHeight = 1.25f;
	private static float defaultPadding = 0.05f;
	public static boolean hasBackground = true;
	
	private ScrollBar scrollBar;
	private GuiModel background;
	private TreeMap<Integer, DisplayCell> cells;
	private ArrayList<Integer> remove = new ArrayList<Integer>();
	private int numCells = 0;
	private float cellHeight;
	private float padding = defaultPadding;
	private int cols;
	private boolean inLineBar = true;
	private boolean containPage = true;
	
	public GridDisplay(Vector2f position, Vector2f size, int columns, float scrollWidth, GuiComponent parent) {
		super(position, size, parent);
		cells = new TreeMap<Integer, DisplayCell>();
		cols = columns;
		cellHeight = defaultCellHeight;
		scrollBar = new ScrollBar(new Vector2f(), new Vector2f(scrollWidth, 1), 5, this);
		scrollBar.setRightCenter(new Vector2f(1, 0));
		if(hasBackground)
			background = new GuiModel(new Vector2f(), new Vector2f(1, 1), defaultBackgroundColor, this);
		else
			hasBackground = true;
	}
	
	public GridDisplay(Vector2f position, Vector2f size, int columns, float scrollWidth, float cellHeight, GuiComponent parent) {
		this(position, size, columns, scrollWidth, parent);
		this.cellHeight = cellHeight;
	}
	
	public GridDisplay(Vector2f position, Vector2f size, int columns, ScrollBar scrollbar, float cellHeight, GuiComponent parent) {
		super(position, size, parent);
		this.scrollBar = scrollbar;
		inLineBar = false;
		this.addChild(scrollbar);
		cells = new TreeMap<Integer, DisplayCell>();
		cols = columns;
		this.cellHeight = cellHeight;
		if(hasBackground)
			background = new GuiModel(new Vector2f(), new Vector2f(1, 1), defaultBackgroundColor, this);
		else
			hasBackground = true;
		update();
	}
	
	public void addCell(DisplayCell cell){
		int index = numCells++;
		cells.put(index, cell);
		cell.setIndex(index);
		updateCellPositions();
		scrollBar.setToTop();
		if(containPage) {
			cell.setMax(getWorldMax());
			cell.setMin(getWorldMin());
		}
	}
	
	public void setCell(DisplayCell cell){
		cells.put(cell.getIndex(), cell);
		if(cell.getIndex()>=numCells)
			numCells=cell.getIndex()+1;
		updateCellPositions();
		scrollBar.setToTop();
		if(containPage) {
			cell.setMax(getWorldMax());
			cell.setMin(getWorldMin());
		}
	}
	
	public void removeCell(int index) {
		remove.add(index);
	}
	
	public void update() {
		float scroll_i = scrollBar.getOutput();
		scrollBar.update();
		boolean newPos = false;
		for(DisplayCell cell : cells.values()){
			cell.update();
			if(cell.remove) {
				remove.add(cell.getIndex());
				newPos = true;
			}
		}
		for(Integer cell : remove) {
			removeChild(cells.get(cell));
			cells.get(cell).disable();
			cells.remove(cell);
		}remove.clear();
		if(scrollBar.getOutput()!=scroll_i||newPos)
			updateCellPositions();
	}
	
	private void updateCellPositions(){
		float aspectRatio = getAspectRatio() * DisplayManager.getAspectRatio();
		float width = 1;
		if(inLineBar)
			width -= scrollBar.getSize().x;
		float cellWidth = width / cols;
		float padX = width / (cols+1) * padding;
		float cellHeight = this.cellHeight * cellWidth * aspectRatio;
		float padY = padX * aspectRatio;
		float cellPad = cellWidth * padding;
		Vector2f cellSize = new Vector2f(cellWidth - cellPad, cellHeight - cellPad * aspectRatio);
		int i = 0;
		int row = 0;
		for(DisplayCell cell : cells.values()){
			row = i / cols;
			int col = i % cols;
			cell.setSize(cellSize);
			cell.setTopLeft(new Vector2f((cellSize.x * col + padX * (col + 1)) * 2 - 1, -(cellSize.y * row + padY * (row + 1)) * 2 + 1 - scrollBar.getOutput()));
			i++;
		}
		row++;
		float contentLength = (cellSize.y * row + padY * (row + 1)) * 2 - 1;
		scrollBar.setContentLength(contentLength);
	}
	
	private void updateBounds() {
		if(containPage) {
			Vector2f max = getWorldMax();
			Vector2f min = getWorldMin();
			for(DisplayCell cell : cells.values()){
				cell.setMax(max);
				cell.setMin(min);
			}
		}
	}
	
	@Override
	public void setPosition(Vector2f position) {
		super.setPosition(position);
		updateBounds();
	}
	
	@Override
	public void setSize(Vector2f size) {
		super.setSize(size);
		updateBounds();
	}
	
	public void setColumns(int columns){
		cols = columns;
		updateCellPositions();
	}
	
	public void setPadding(float padding){
		this.padding = padding;
		updateCellPositions();
	}
	
	public void setCellHeight(float height){
		cellHeight = height;
		updateCellPositions();
	}
	
	public ScrollBar getScrollBar() {
		return scrollBar;
	}
	
	public void setBackground(Vector4f color){
		background.setColor(color);
	}
	
	public static void setDefaultBackgroundColor(Vector4f color){
		GridDisplay.defaultBackgroundColor = color;
	}
	
	public static void setDefaultPadding(float padding){
		GridDisplay.defaultPadding = padding;
	}
	
	public static void setDefaultCellHeight(float height){
		GridDisplay.defaultCellHeight = height;
	}
	
	public void showAll() {
		containPage = false;
	}

	public void clearCells() {
		remove.addAll(cells.keySet());
	}
}
