package renderEngine.guis;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;
import renderEngine.font.DynamicText;
import renderEngine.font.Text;
import renderEngine.fontRenderer.FontRenderer;
import renderEngine.utils.Transform2D;

public class GuiComponent {
	
	private Vector2f min = new Vector2f(-1,-1), max = new Vector2f(1,1);
	public GuiComponent parent;//private
	private ArrayList<GuiComponent> children = new ArrayList<GuiComponent>();
	private ArrayList<Text> texts = new ArrayList<Text>();
	protected ArrayList<DynamicText> dynamicTexts = new ArrayList<DynamicText>();
	protected boolean enabled = true;
	protected boolean mirrorX = false;
	protected boolean mirrorY = false;	
	protected boolean focus = false;
	
	private Transform2D transform;//local space
	private Transform2D worldTransform;//world space
	private boolean updateTransform = false;
	
	public GuiComponent(){
		this(GuiPlacer.getPos(), GuiPlacer.getSize(), GuiPlacer.getParent());
	}
	
	public GuiComponent(GuiComponent parent){
		this(GuiPlacer.getPos(), GuiPlacer.getSize(), GuiPlacer.getRotation(), parent);
	}
	
	public GuiComponent(Vector2f position, Vector2f size, GuiComponent parent){
		this(position, size, 0, parent);
	}
	
	public GuiComponent(Vector2f position, Vector2f size, float rotation, GuiComponent parent){
		transform = new Transform2D(position, rotation, size);
		worldTransform = new Transform2D(transform);
		this.parent = parent;
		if(parent!=null) {
			parent.addChild(this);
			flagUpdate();
		}
	}
	
	public void setPosition(Vector2f position){
		transform.setPosition(position);
		flagUpdate();
	}
	
	public void changePosition(Vector2f delta){
		transform.changePosition(delta);
		flagUpdate();
	}
	
	public void setSize(Vector2f size){
		transform.setScale(size);
		flagUpdate();
	}
	
	public void setRotation(float rotation) {
		transform.setRotation(rotation);
		flagUpdate();
	}
	
	public void changeRotation(float delta) {
		transform.setRotation(delta);
		flagUpdate();
	}
	
	private void flagUpdate() {
		updateTransform = true;
		for(GuiComponent child : children) {
			child.flagUpdate();
		}
	}
	
	public void addChild(GuiComponent child){
		if(children.contains(child))
			removeChild(child);
		children.add(child);
		child.setMax(max);
		child.setMin(min);
		child.setFocus(focus);
	}
	
	public void removeChild(GuiComponent child){
		children.remove(child);
	}
	
	public ArrayList<GuiComponent> getChildren(){
		return children;
	}
	
	public ArrayList<GuiModel> getModels(){
		ArrayList<GuiModel> models = new ArrayList<GuiModel>();
		for(GuiComponent child : getChildren()){
			models.addAll(child.getModels());
		}
		return models;
	}
	
	public void addText(Text text){
		texts.add(text);
		text.setParent(this);
	}
	
	public void addText(DynamicText text){
		dynamicTexts.add(text);
		text.setParent(this);
	}
	
	public void removeText(Text text){
		texts.remove(text);
	}
	
	public void removeText(DynamicText text){
		dynamicTexts.remove(text);
	}
	
	public void disableText(){
		for(GuiComponent child : children){
			child.disableText();
		}
		for(Text text : texts){
			text.disable();
		}for(DynamicText text : dynamicTexts){
			text.disable();
		}
	}
	
	public void enableText(){
		for(GuiComponent child : children){
			child.enableText();
		}
		for(Text text : texts){
			text.enable();
		}for(DynamicText text : dynamicTexts){
			text.enable();
		}
	}
	
	public void render(){
		if(enabled){
			getWorldTransform();//update if updateTransform
			for(GuiComponent child : getChildren()){
				child.render();
			}
			if(texts.size() + dynamicTexts.size() > 0){
				FontRenderer.render(texts.iterator(), dynamicTexts.iterator());
			}
		}
	}
	
	public float getAspectRatio(){
		Vector2f size = getWorldTransform().getScale();
		return size.x / size.y;
	}
	
	//convenience methods for gui design (pre-rotation)
	
	public Vector2f getCenter(){
		return transform.getPosition();
	}
	
	public Vector2f getTopCenter(){
		return new Vector2f(transform.getPosition().x, transform.getPosition().y+transform.getScale().y);
	}
	
	public Vector2f getLowerCenter(){
		return new Vector2f(transform.getPosition().x, transform.getPosition().y-transform.getScale().y);
	}
	
	public Vector2f getLeftCenter(){
		return new Vector2f(transform.getPosition().x-transform.getScale().x, transform.getPosition().y);
	}
	
	public Vector2f getRightCenter(){
		return new Vector2f(transform.getPosition().x+transform.getScale().x, transform.getPosition().y-transform.getScale().y);
	}
	
	public Vector2f getTopRight(){
		return new Vector2f(transform.getPosition().x+transform.getScale().x, transform.getPosition().y+transform.getScale().y);
	}
	
	public Vector2f getTopLeft(){
		return new Vector2f(transform.getPosition().x-transform.getScale().x, transform.getPosition().y+transform.getScale().y);
	}
	
	public Vector2f getLowerRight(){
		return new Vector2f(transform.getPosition().x+transform.getScale().x, transform.getPosition().y-transform.getScale().y);
	}
	
	public Vector2f getLowerLeft(){
		return new Vector2f(transform.getPosition().x-transform.getScale().x, transform.getPosition().y-transform.getScale().y);
	}
	
	public void setUpperCenter(Vector2f position){
		setPosition(new Vector2f(position.x, position.y-transform.getScale().y));
	}
	
	public void setLowerCenter(Vector2f position){
		setPosition(new Vector2f(position.x, position.y+transform.getScale().y));
	}
	
	public void setLeftCenter(Vector2f position){
		setPosition(new Vector2f(position.x+transform.getScale().x, position.y));
	}
	
	public void setRightCenter(Vector2f position){
		setPosition(new Vector2f(position.x-transform.getScale().x, position.y));
	}
	
	public void setTopRight(Vector2f position){
		setPosition(new Vector2f(position.x-transform.getScale().x, position.y-transform.getScale().y));
	}
	
	public void setTopLeft(Vector2f position){
		setPosition(new Vector2f(position.x+transform.getScale().x, position.y-transform.getScale().y));
	}
	
	public void setLowerRight(Vector2f position){
		setPosition(new Vector2f(position.x-transform.getScale().x, position.y+transform.getScale().y));
	}
	
	public void setLowerLeft(Vector2f position){
		setPosition(new Vector2f(position.x+transform.getScale().x, position.y+transform.getScale().y));
	}
	
	public Vector2f getWorldMax(){
		Vector2f max = getWorldTransform().getScale();
		Vector2f pos = getWorldTransform().getPosition();
		max.x += pos.x;
		max.y += pos.y;
		return max;
	}
	
	public Vector2f getWorldMin(){
		Vector2f min = getWorldTransform().getScale();
		min.x = -min.x;
		min.y = -min.y;
		Vector2f pos = getWorldTransform().getPosition();
		min.x += pos.x;
		min.y += pos.y;
		return min;
	}
	
	public Vector2f getWorldPosition(){
		return getWorldTransform().getPosition();
	}
	
	public Vector2f getWorldSize(){
		return getWorldTransform().getScale();
	}
	
	public Vector2f getSize(){
		return transform.getScale();
	}
	
	public void enable(){
		enabled = true;
		for(GuiComponent component : children){
			component.enable();
		}
	}
	
	public void disable(){
		enabled = false;
		for(GuiComponent component : children){
			component.disable();
		}
	}
	
	public boolean enabled(){
		return enabled;
	}
	
	public void mirrorY(){
		mirrorY = true;
		for(GuiComponent child : children){
			child.mirrorY();
		}
	}
	
	public void mirrorX(){
		mirrorX = true;
		for(GuiComponent child : children){
			child.mirrorX();
		}
	}
	
	public boolean isMirrorX(){
		return mirrorX;
	}
	
	public boolean isMirrorY() {
		return mirrorY;
	}

	public ArrayList<Text> getTexts() {
		return texts;
	}

	public ArrayList<DynamicText> getDynamicTexts() {
		return dynamicTexts;
	}
	
	public Vector2f getMax(){
		return max;
	}
	
	public Vector2f getMin(){
		return min;
	}
	
	public void setMax(Vector2f max){
		this.max = max;
		for(GuiComponent child : getChildren()) {
			Vector2f modelMax = child.getMax();
			boolean x = modelMax.x>max.x;
			boolean y = modelMax.y>max.y;
			if(x)
				modelMax.x = max.x;
			if(y)
				modelMax.y = max.y;
			if(x||y)
				child.setMax(modelMax);
		}
	}
	
	public void setMin(Vector2f min){
		this.min = min;
		for(GuiComponent child : getChildren()) {
			Vector2f modelMin = child.getMin();
			boolean x = modelMin.x<min.x;
			boolean y = modelMin.y<min.y;
			if(x)
				modelMin.x = min.x;
			if(y)
				modelMin.y = min.y;
			if(x||y)
				child.setMin(modelMin);
		}
	}
	
	public void setFocus(boolean focus) {
		if(this.focus!=focus) {
			this.focus = focus;
			for(GuiComponent child : children)
				child.setFocus(focus);
		}
	}
	
	public boolean isFocused() {
		return focus;
	}
	
	public Transform2D getTransform() {
		return transform;
	}
	
	public Transform2D getWorldTransform() {
		if(parent==null)
			return transform;
		if(updateTransform) {
			updateWorldTransform();
		}
		return worldTransform;
	}
	
	private void updateWorldTransform() {
		if(enabled) {
			updateTransform = false;
			Transform2D.multiply(parent.getWorldTransform(), transform, worldTransform);
		}
	}
}
