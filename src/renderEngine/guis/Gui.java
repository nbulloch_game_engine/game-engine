package renderEngine.guis;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;

public abstract class Gui extends GuiComponent {
	
	private final int ID = GuiManager.NUM_GUIS++;
	
	protected ArrayList<GuiState> states;
	protected ArrayList<GuiComponent> bottomChildren = new ArrayList<GuiComponent>();
	protected ArrayList<GuiComponent> topChildren = new ArrayList<GuiComponent>();
	
	public Gui(Vector2f position, Vector2f size) {
		super(position, size, null);
		states = new ArrayList<GuiState>();
		GuiManager.register(this);
	}
	
	public void update(){
		for(GuiState state : states){
			if(state.active())
				state.update();
		}
	}
	
	@Override
	public ArrayList<GuiComponent> getChildren(){
		ArrayList<GuiComponent> children = new ArrayList<GuiComponent>(bottomChildren);
		for(GuiState state : states){
			if(state.active()){
				children.addAll(state.getChildren());
			}
		}
		children.addAll(topChildren);
		return children;
	}
	
	public ArrayList<GuiState> getStates(){
		return states;
	}
	
	protected void addState(GuiState state){
		states.add(state);
	}
	
	public int getID(){
		return ID;
	}
}
