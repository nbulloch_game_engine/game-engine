package renderEngine.guis;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

public abstract class Interactable extends GuiComponent{
	
	private static boolean interacting = false;
	private Vector2f[] hotspot;//(min, max)
	private boolean selected = false;
	private boolean init = true;
	
	public Interactable(Vector2f position, Vector2f size, Vector2f minHotspot, Vector2f maxHotspot, GuiComponent parent){
		super(position, size, parent);
		hotspot = new Vector2f[2];
		hotspot[0] = new Vector2f(minHotspot.x, minHotspot.y);
		hotspot[1] = new Vector2f(maxHotspot.x, maxHotspot.y);
	}
	
	public Interactable(Vector2f position, Vector2f size, GuiComponent parent) {
		this(position, size, new Vector2f(-1, -1), new Vector2f(1, 1), parent);
	}

	public void update(){
		if(focus) {
			if(!interacting || selected || init){
				Vector2f[] p = new Vector2f[2];
				Vector2f size = getWorldSize();
				Vector2f position = getWorldPosition();
				for(int i = 0; i < 2; i++){
					p[i] = new Vector2f(hotspot[i].x, hotspot[i].y);
					p[i].x *= size.x;
					p[i].y *= size.y;
					p[i].x += position.x;
					p[i].y += position.y;
				}
				
				Vector2f pos = new Vector2f((float)Mouse.getX()/Display.getWidth()  * 2 - 1, (float)Mouse.getY()/Display.getHeight() * 2 - 1);
				if((pos.x > p[0].x && pos.x < p[1].x && pos.y > p[0].y && pos.y < p[1].y) || selected){
					if(Mouse.isButtonDown(0)){
						interact();
						selected = true;
						interacting = true;
					}else{
						if(selected){
							selected = false;
							interacting = false;
						}
						hover();
					}
				}else{
					if(selected && !Mouse.isButtonDown(0)){
						selected = false;
						interacting = false;
					}
					uninteract();
				}
				init = false;
			}
		}
	}
	
	@Override
	public void disable(){
		super.disable();
		selected = false;
		interacting = false;
		uninteract();
	}
	
	protected abstract void uninteract();
	
	protected abstract void hover();
	
	protected abstract void interact();
	
	public void setMinHotspot(Vector2f minHotspot){
		hotspot[0].x = minHotspot.x;
		hotspot[0].y = minHotspot.y;
	}
	
	public void setMaxHotspot(Vector2f maxHotspot){
		hotspot[1].x = maxHotspot.x;
		hotspot[1].y = maxHotspot.y;
	}
	
	@Override
	public void setFocus(boolean focus) {
		super.setFocus(focus);
		if(!focus&&selected) {
			selected = false;
			interacting = false;
			uninteract();
		}
	}
	
	public Vector4f getHotspot() {
		return new Vector4f(hotspot[0].x, hotspot[0].y, hotspot[1].x, hotspot[1].y);
	}
}
