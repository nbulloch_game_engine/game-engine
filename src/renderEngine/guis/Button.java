package renderEngine.guis;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.font.DynamicText;
import renderEngine.font.Text;
import renderEngine.renderEngine.Loader;

public class Button extends Interactable {

	private static Vector4f defaultColor = new Vector4f(1, 1, 1, 0);
	private static Vector4f defaultHoverColor = new Vector4f(0.2f, 0.8f, 0.2f, 1);
	private static Vector4f defaultPressColor = new Vector4f(0.3f, 0.7f, 0.3f, 1);
	private static Vector4f defaultTextHoverColor;
	private static int defaultRepeatDelay = 100;
	private static int defaultInitialDelay = 400;
	
	private static boolean pressedButton = false;
	
	private GuiModel button;
	
	private Vector4f disabledColor = null;
	private Vector4f color;
	private Vector4f hoverColor;
	private Vector4f pressColor;
	
	private boolean useTextures = false;
	
	private int disabledTexture = -1;
	private int texture;
	private int hoverTexture;
	private int pressTexture;
	
	private int repeatDelay;
	private int initialDelay;
	private boolean releaseActivated = true;
	private boolean activated = false;
	private boolean pressable = true;
	
	private boolean repeat = false;
	private boolean initialPress = false;
	private boolean pressed = false;
	
	private long lastPress = 0;
	
	private boolean selected = false;
	
	private Vector4f textHoverColor;
	//private Vector4f textPressColor;
	
	private Vector2f size = null;
	private Vector2f hoverSize = null;
	
	public Button(Vector2f position, Vector2f size, GuiComponent parent) {
		super(position, size, parent);
		color = defaultColor;
		hoverColor = defaultHoverColor;
		pressColor = defaultPressColor;
		repeatDelay = defaultRepeatDelay;
		initialDelay = defaultInitialDelay;
		textHoverColor = defaultTextHoverColor;
		button = new GuiModel(new Vector2f(), new Vector2f(1, 1), color, this);
	}
	
	public Button(Vector2f position, Vector2f size, int texture, int hover, GuiComponent parent){
		this(position, size, parent);
		setTextures(texture, hover);
		update();
	}
	
	public Button() {
		this(GuiPlacer.getPos(), GuiPlacer.getSize(), GuiPlacer.getParent());
	}
	
	public void interact() {
		if(selected) {
			hovered();
		}else if(pressable) {
			if(!releaseActivated){
				if(repeat){
					boolean delayed = System.currentTimeMillis() - lastPress >= repeatDelay;
					if(initialPress){
						delayed = System.currentTimeMillis() - lastPress >= initialDelay;
					}
					if(delayed){
						activated = true;
						if(lastPress == 0){
							initialPress = true;
						}else{
							initialPress = false;
						}
						lastPress = System.currentTimeMillis();
					}else{
						activated = false;
					}
					pressed();
				}else{
					if(!pressed){
						pressed = true;
						activated = true;
					}else{
						activated = false;
					}
				}
			}else{
				pressed();
				pressed = true;
			}
		}
	}
	
	@Override
	protected void uninteract() {
		if(pressable)
			unhovered();
	}
	
	@Override
	protected void hover() {
		if(!releaseActivated){
			if(repeat){
				activated = false;
				lastPress = 0;
			}else{
				pressed = false;
				activated = false;
			}
		}else{
			if(pressed){
				pressed = false;
				activated = true;
			}else{
				activated = false;
			}
		}
		hovered();
	}
	
	private void pressed(){
		if(useTextures){
			button.switchTexture(pressTexture);
		}else{
			button.setColor(pressColor);
		}if(hoverSize!=null){
			setSize(size);
		}
	}
	
	private void hovered(){
		if(pressable){
			if(useTextures){
				button.switchTexture(hoverTexture);
			}else{
				button.setColor(hoverColor);
			}
			for(Text text : getTexts()){
				text.getSettings().setOverrideColor(textHoverColor);
			}for(DynamicText text : dynamicTexts){
				text.getSettings().setOverrideColor(textHoverColor);
			}
			if(hoverSize!=null){
				Vector2f size = new Vector2f(this.size);
				size.x*=hoverSize.x;
				size.y*=hoverSize.y;
				setSize(size);
			}
		}
	}
	
	private void unhovered(){
		if(selected){
			hovered();
		}else{
			if(useTextures){
				button.switchTexture(texture);
			}else{
				button.setColor(color);
			}
			for(Text text : getTexts()){
				text.getSettings().setOverrideColor(null);
			}for(DynamicText text : dynamicTexts){
				text.getSettings().setOverrideColor(null);
			}
		}if(hoverSize!=null){
			setSize(size);
		}
	}
	
	public void setTextures(int texture, int hoverTexture, int pressTexture){
		useTextures = true;
		releaseActivated = true;
		this.texture = texture;
		this.hoverTexture = hoverTexture;
		this.pressTexture = pressTexture;
	}
	
	public void setTextures(String texture, String hoverTexture, String pressTexture){
		setTextures(Loader.loadTexture(texture), Loader.loadTexture(hoverTexture), Loader.loadTexture(pressTexture));
	}
	
	public void setTextures(int texture, int hoverTexture){
		useTextures = true;
		releaseActivated = false;
		this.texture = texture;
		this.hoverTexture = hoverTexture;
	}
	
	public void setTextures(String texture, String hoverTexture){
		setTextures(Loader.loadTexture(texture), Loader.loadTexture(hoverTexture));
	}
	
	public void setColor(Vector4f color){
		useTextures = false;
		this.color = color;
	}
	
	public void setHoverColor(Vector4f hoverColor){
		useTextures = false;
		this.hoverColor = hoverColor;
	}
	
	public void setPressColor(Vector4f pressColor){
		useTextures = false;
		this.pressColor = pressColor;
	}
	
	/**
	 * @param delay - delay in miliseconds until a pressed button will be repeated
	 */
	
	public void setRepeatDelay(int delay){
		repeatDelay = delay;
		repeat = true;
	}
	
	/**
	 * @param delay - delay in miliseconds from the firts press until repitition starts
	 */
	
	public void setInitialDelay(int delay){
		initialDelay = delay;
		repeat = true;
	}
	
	public void setReleaseActivated(){
		if(!useTextures){
			releaseActivated = true;
		}
	}
	
	public void setPressActivated(){
		releaseActivated = false;
	}
	
	public void repeats(boolean repeat){
		this.repeat = repeat;
	}
	
	public void setTextHoverColor(Vector4f color){
		textHoverColor = color;
	}
	
	public void setDisabledTexture(int texture) {
		disabledTexture = texture;
	}
	
	public void setDisabledColor(Vector4f color) {
		disabledColor = color;
	}
	
	public void disableButton() {
		if(pressable) {
			if(useTextures) {
				if(disabledTexture!=-1) {
					button.switchTexture(disabledTexture);
				}else
					button.switchTexture(texture);
			}else {
				if(disabledColor!=null)
					button.setColor(disabledColor);
				else
					button.setColor(color);
			}
		}
		pressable = false;
	}
	
	public void enableButton() {
		pressable = true;
	}
	
	public void setSelected(boolean selected){
		this.selected = selected;
		if(selected) {
			hovered();
		}else {
			unhovered();
		}
	}
	
	public void setHoverSize(Vector2f size){
		this.size = getSize();
		hoverSize = size;
	}
	
	public static void setDefaultColor(Vector4f color){
		Button.defaultColor = color;
	}
	
	public static void setDefaultHoverColor(Vector4f color){
		Button.defaultHoverColor = color;
	}
	
	public static void setDefaultPressColor(Vector4f color){
		Button.defaultPressColor = color;
	}
	
	public static void setDefaultRepeatDelay(int delay){
		Button.defaultRepeatDelay = delay;
	}
	
	public static void setDefaultInitialDelay(int delay){
		Button.defaultInitialDelay = delay;
	}
	
	public static void setDefaultTextHoverColor(Vector4f color){
		Button.defaultTextHoverColor = color;
	}
	
	public static void updatePressedButton(){
		if(!Mouse.isButtonDown(0)){
			pressedButton = false;
		}
	}
	
	public boolean isActivated(){
		boolean value = activated;
		activated = false;
		if(value && !repeat){
			if(pressedButton){
				value = false;
			}else{
				pressedButton = true;
			}
		}
		return value;
	}
	
	public GuiModel getButtonModel() {
		return button;
	}
}
