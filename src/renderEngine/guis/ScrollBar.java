package renderEngine.guis;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.renderEngine.Loader;

public class ScrollBar extends Interactable{

	private static Vector4f defaultTrackColor = new Vector4f(0.7f, 0.7f, 0.7f, 1f);
	private static Vector4f defaultInputColor = new Vector4f(0.5f, 0.5f, 0.5f, 1f);
	private static Vector4f defaultInputHoverColor = new Vector4f(0.4f, 0.4f, 0.4f, 1f);
	
	private GuiModel track;
	private GuiModel input;
	
	private Vector4f inputColor;
	private Vector4f inputHoverColor;
	
	private boolean selected = false;
	private float position = 0;
	private float offset = 0;
	
	private boolean texturedInput = false;
	
	private int inputTexture;
	private int inputHoverTexture;
	
	private boolean hoverIndication = true;
	
	private float contentLength;
	private float output;
	
	private boolean update = true;
	
	public ScrollBar(Vector2f position, Vector2f size, float contentLength, GuiComponent parent) {
		super(position, size, parent);
		inputColor = defaultInputColor;
		inputHoverColor = defaultInputHoverColor;
		track = new GuiModel(new Vector2f(), new Vector2f(1,1), defaultTrackColor, this);
		input = new GuiModel(new Vector2f(), new Vector2f(), defaultInputColor, this);
		setContentLength(contentLength);
		setInputPosition(1);
	}
	
	@Override
	protected void interact(){
		if(update){
			float mouse = (float)Mouse.getY()/(float)Display.getHeight() * 2 - 1;//(-1) - 1;
			float localMouse = mouse/getWorldSize().y;
			if(!selected){
				float max = input.getWorldMax().y;
				float min = input.getWorldMin().y;
				
				if(min <= mouse && mouse <= max){
					offset = localMouse - this.position;
				}
			}
			selected = true;
			setInputPosition(localMouse - offset);
		}else{
			uninteract();
		}
	}
	
	@Override
	protected void uninteract(){
		if(texturedInput){
			input.switchTexture(inputTexture);
		}else {
			input.setColor(inputColor);
		}
		if(!Mouse.isButtonDown(0)){
			offset = 0;
			selected = false;
		}
	}
	
	@Override
	protected void hover(){
		if(update){
			if(hoverIndication){
				if(texturedInput){
					input.switchTexture(inputHoverTexture);
				}else {
					input.setColor(inputHoverColor);
				}
			}
		}else{
			uninteract();
		}
	}
	
	public void setInputPosition(float position){
		float halfInput = (1 / contentLength);
		if(position < -1 + halfInput){
			position = -1 + halfInput;
		}else if(position > 1 - halfInput){
			position = 1 - halfInput;
		}
		this.position = position;
		input.setPosition(new Vector2f(0, position));
		output = position / (1 - halfInput);//(-1) - 1
		output = (output - 1) / 2;//(-1) - 0
		output *= (contentLength - 1);//(-hidden pages) - 0
	}
	
	public void setToTop(){
		float halfInput = (1 / contentLength);
		position = 1 - halfInput;
		input.setPosition(new Vector2f(0, position));
		output = position / (1 - halfInput);//(-1) - 1
		output = (output - 1) / 2;//(-1) - 0
		output *= (contentLength - 1);//(-hidden pages) - 0
	}
	
	public void setToBottom(){
		float halfInput = (1 / contentLength);
		position = -1 + halfInput;
		input.setPosition(new Vector2f(0, position));
		output = position / (1 - halfInput);//(-1) - 1
		output = (output - 1) / 2;//(-1) - 0
		output *= (contentLength - 1);//(-hidden pages) - 0
	}
	
	public void setContentLength(float length){
		contentLength = length;
		if(contentLength<=1){
			update();
			update = false;
			output = 0;
			input.disable();
		}else{
			update = true;
			float inputLength = 1 / length;
			input.setSize(new Vector2f(1, inputLength));
			setInputPosition(position);
			input.enable();
		}
		
	}
	
	public void setHoverIndication(boolean hoverIndication){
		this.hoverIndication = hoverIndication;
	}
	
	public void setTrackColor(Vector4f color){
		track.setColor(color);
	}
	
	public void setInputColor(Vector4f color){
		inputColor = color;
		texturedInput = false;
	}
	
	public void setInputHoverColor(Vector4f color){
		inputHoverColor = color;
		texturedInput = false;
		hoverIndication = true;
	}
	
	public void setTrackTexture(int trackTexture){
		track.setTexture(trackTexture);
	}
	
	public void setInputTextures(int inputTexture, int inputHoverTexture){
		if(this.inputTexture != inputTexture){
			Loader.cleanTexture(this.inputTexture);
		}if(this.inputHoverTexture != inputHoverTexture){
			Loader.cleanTexture(this.inputHoverTexture);
		}
		this.inputTexture = inputTexture;
		this.inputHoverTexture = inputHoverTexture;
		hoverIndication = true;
		texturedInput = true;
	}
	
	public void setInputTexture(int inputTexture){
		if(this.inputTexture != inputTexture){
			Loader.cleanTexture(this.inputTexture);
		}
		this.inputTexture = inputTexture;
		hoverIndication = false;
		texturedInput = true;
	}
	
	public static void setDefaultTrackColor(Vector4f color){
		ScrollBar.defaultTrackColor = color;
	}
	
	public static void setDefaultInputColor(Vector4f color){
		ScrollBar.defaultInputColor = color;
	}
	
	public static void setDefaultInputHoverColor(Vector4f color){
		ScrollBar.defaultInputHoverColor = color;
	}
	
	public float getOutput(){
		return output;
	}
}
