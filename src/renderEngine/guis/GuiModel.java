package renderEngine.guis;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.guiRenderer.GuiRenderer;
import renderEngine.renderEngine.Loader;

public class GuiModel extends GuiComponent {

	private Vector4f color;
	private int texture;
	private boolean textured;
	
	private float xTextureMax = 1;
	private float xTextureMin = 0;
	
	public GuiModel(Vector2f position, Vector2f size, Vector4f color, GuiComponent parent){
		super(position, size, parent);
		this.color = color;
		textured = false;
	}
	
	public GuiModel(Vector2f position, Vector2f size, int texture, GuiComponent parent){
		super(position, size, parent);
		this.texture = texture;
		textured = true;
	}
	
	public GuiModel(Vector4f color, GuiComponent parent){
		super(parent);
		this.color = color;
		textured = false;
	}
	
	public GuiModel(int texture, GuiComponent parent){
		super(parent);
		this.texture = texture;
		textured = true;
	}
	
	public GuiModel(Vector4f color){
		super();
		this.color = color;
		textured = false;
	}
	
	public GuiModel(int texture){
		super();
		this.texture = texture;
		textured = true;
	}
	
	@Override
	public ArrayList<GuiModel> getModels(){
		ArrayList<GuiModel> models = new ArrayList<GuiModel>();
		if(enabled){
			models.add(this);
		}for(GuiComponent child : getChildren()){
			models.addAll(child.getModels());
		}
		return models;
	}
	
	public float getXTextureMax(){
		return xTextureMax;
	}
	
	protected void setXTextureMax(float xTextureMax){
		this.xTextureMax = xTextureMax;
	}
	
	public float getXTextureMin(){
		return xTextureMin;
	}
	
	protected void setXTextureMin(float xTextureMin){
		this.xTextureMin = xTextureMin;
	}
	
	public Vector4f getColor() {
		return color;
	}
	
	public void setColor(Vector4f color) {
		textured = false;
		this.color = color;
	}
	
	public int getTexture() {
		return texture;
	}
	
	public void switchTexture(int texture){
		textured = true;
		this.texture = texture;
	}
	
	
	/**
	 * Deletes old texture from memory and replaces
	 */
	public void setTexture(int texture) {
		if(this.texture != texture && textured){
			Loader.cleanTexture(this.texture);
		}
		textured = true;
		this.texture = texture;
	}
	
	public boolean isTextured() {
		return textured;
	}
	
	@Override
	public void render(){
		if(enabled){
			GuiRenderer.render(this);
			super.render();
		}
	}
}
