package renderEngine.guis;

import org.lwjgl.util.vector.Vector2f;

public class TexturedBar extends GuiComponent{
	
	private GuiModel bar;
	private GuiModel empty;
	
	private float value;
	private float maxValue;
	
	private float barEdge = 0;
	
	public TexturedBar(Vector2f position, Vector2f size, int barTexture, int emptyTexture, float maxValue, float value, GuiComponent parent) {
		super(position, size, parent);
		bar = new GuiModel(new Vector2f(), new Vector2f(1, 1), barTexture, this);
		empty = new GuiModel(new Vector2f(), new Vector2f(1, 1), emptyTexture, this);
		this.maxValue = maxValue;
		this.value = value;
		updateBar();
	}
	
	public void setBarTexture(int texture){
		bar.setTexture(texture);
	}
	
	public void setEmptyTexture(int texture){
		bar.setTexture(texture);
	}
	
	private void updateBar(){
		float bound = value / maxValue * (1 - barEdge * 2);
		bound += barEdge;
		bar.setXTextureMax(bound);
		empty.setXTextureMin(bound);
	}
	
	public void setBarEdge(float edge){
		barEdge = edge;
		updateBar();
	}
	
	public void setMaxValue(float maxValue){
		this.maxValue = maxValue;
		updateBar();
	}
	
	public void setValue(float value){
		this.value = value;
		updateBar();
	}

}
