package renderEngine.guis;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

public class Bar extends GuiComponent{

	private static Vector4f barColor = new Vector4f(0,1,0,1);
	private static Vector4f outlineColor = new Vector4f(1,1,1,1);
	private static Vector4f emptyColor = new Vector4f(.75f,.75f,.75f,1);
	private static float defaultOutlineWidth = 0.05f;
	private static boolean defaultOutline = false;
	
	private float outlineWidth = defaultOutlineWidth;
	private float maxValue;
	private float currentValue;
	
	private GuiModel barOutline;
	private GuiModel emptyBar;
	private GuiModel bar;
	
	private boolean outline;
	
	public Bar(Vector2f position, Vector2f size, float maxValue, float value, GuiComponent parent) {
		super(position, size, parent);
		this.maxValue = maxValue;
		currentValue = value;
		outline = defaultOutline;
		barOutline = new GuiModel(new Vector2f(), new Vector2f(1, 1), outlineColor, this);
		emptyBar = new GuiModel(new Vector2f(), new Vector2f(1, 1), emptyColor, this);
		bar = new GuiModel(new Vector2f(), new Vector2f(1, 1), barColor, this);
		if(!outline){
			barOutline.disable();
		}
		updateBarModels();
	}
	
	private void updateBarModels(){
		if(outline){
			float aspectRatio = (float)Display.getWidth() / (float)Display.getHeight();
			float barHeight = 1 - outlineWidth * aspectRatio;
			float barAspectRatio = getSize().y / getSize().x;
			float xOutline = outlineWidth * barAspectRatio;
			float totalWidth = 1 - xOutline;
			float barScalar = currentValue / maxValue;
			float barWidth = totalWidth * barScalar;
			float emptyWidth = totalWidth * (1 - barScalar);
			bar.setSize(new Vector2f(barWidth, barHeight));
			emptyBar.setSize(new Vector2f(emptyWidth, barHeight));
			bar.setLeftCenter(new Vector2f(-1 + xOutline, 0));
			emptyBar.setRightCenter(new Vector2f(1 - xOutline, 0));
		}else{
			float barWidth = currentValue / maxValue;
			float emptyWidth = 1 - barWidth;
			bar.setSize(new Vector2f(barWidth, 1));
			emptyBar.setSize(new Vector2f(emptyWidth, 1));
			bar.setLeftCenter(new Vector2f(-1, 0));
			emptyBar.setRightCenter(new Vector2f(1, 0));
		}
	}
	
	public void setValue(float value){
		currentValue = value;
		updateBarModels();
	}
	
	public void setMaxValue(float value){
		maxValue = value;
		updateBarModels();
	}
	
	public void setOutlineWidth(float outlineWidth){
		if(!outline){
			enableOutline();
		}
		this.outlineWidth = outlineWidth;
		updateBarModels();
	}
	
	public void disableOutline(){
		outline = false;
		barOutline.disable();
		updateBarModels();
	}
	
	public void enableOutline(){
		outline = true;
		barOutline.enable();
		updateBarModels();
	}
	
	public void setBarColor(Vector4f barColor){
		bar.setColor(barColor);
	}
	
	public void setOutlineColor(Vector4f outlineColor){
		barOutline.setColor(outlineColor);
		enableOutline();
	}
	
	public void setEmptyColor(Vector4f emptyColor){
		emptyBar.setColor(emptyColor);
	}
	
	public static void setDefaultOutlineWidth(float outlineWidth){
		Bar.defaultOutlineWidth = outlineWidth;
	}
	
	public static void setDefaultBarColor(Vector4f barColor){
		Bar.barColor = barColor;
	}
	
	public static void setDefaultOutlineColor(Vector4f outlineColor){
		Bar.outlineColor = outlineColor;
	}
	
	public static void setDefaultEmptyColor(Vector4f emptyColor){
		Bar.emptyColor = emptyColor;
	}
	
	public static void setDefaultOutline(boolean outline){
		Bar.defaultOutline = outline;
	}
}
