package renderEngine.font;

import java.util.ArrayList;
import java.util.HashMap;

import renderEngine.renderEngine.Loader;

public class Font {

	private HashMap<Integer, Character> characters;
	private int textureAtlas;
	private float base, lineHeight;
	private boolean distanceField = false;
	private FontSettings settings;
	
	private ArrayList<Text> texts;
	private ArrayList<DynamicText> dynamicTexts;
	
	public Font(HashMap<Integer, Character> characters, int textureAtlas, float base, float lineHeight){
		this.characters = characters;
		this.textureAtlas = textureAtlas;
		this.base = base;
		this.lineHeight = lineHeight;
		settings = new FontSettings();
		texts = new ArrayList<Text>();
		dynamicTexts = new ArrayList<DynamicText>();
	}
	
	public void clean(){
		for(Integer i : characters.keySet()){
			characters.get(i).clean();
		}
		Loader.cleanTexture(textureAtlas);
	}
	
	public ArrayList<Text> getTexts(){
		return texts;
	}
	
	public ArrayList<DynamicText> getDynamicTexts(){
		return dynamicTexts;
	}
	
	public HashMap<Integer, Character> getCharacters() {
		return characters;
	}

	public int getTextureAtlas() {
		return textureAtlas;
	}

	public float getBase() {
		return base;
	}

	public float getLineHeight() {
		return lineHeight;
	}

	protected void setDistanceField(boolean distanceField){
		this.distanceField = distanceField;
	}
	
	public boolean hasDistanceField(){
		return distanceField;
	}
	
	public FontSettings getSettings(){
		return new FontSettings(settings);
	}
	
	public void setSettings(FontSettings settings){
		this.settings = settings;
	}
}
