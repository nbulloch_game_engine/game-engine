package renderEngine.font;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.guis.GuiComponent;
import renderEngine.renderEngine.DisplayManager;

public class DynamicText {

	private static final int SPACE = 32;
	
	private GuiComponent parent;
	
	private String text;
	private Font font;
	private Vector2f position;
	private HashMap<Integer, ArrayList<Vector3f>> characters;
	private FontSettings settings;
	private float line;
	private boolean centered = false;
	private boolean right = false;
	private float aspectRatio = 1;
	private float scale = 1;
	private boolean enabled = true;
	private boolean updated = true;
	
	public DynamicText(String text, String fontName, Vector2f position, float line){
		this(text, TextMaster.getFont(fontName), position, TextMaster.getFont(fontName).getSettings(), line);
	}
	
	public DynamicText(String text, String fontName, Vector2f position){
		this(text, TextMaster.getFont(fontName), position, TextMaster.getFont(fontName).getSettings(), 1);
	}
	
	public DynamicText(String text, Vector2f position, float line){
		this(text, TextMaster.getFont(), position, TextMaster.getFont().getSettings(), line);
	}
	
	public DynamicText(String text, Vector2f position){
		this(text, TextMaster.getFont(), position, TextMaster.getFont().getSettings(), 1);
	}
	
	public DynamicText(String text, Font font, Vector2f position, FontSettings settings, float line){
		this.font = font;
		this.position = position;
		this.settings = settings;
		this.line = line;
		this.text = text;
		characters = new HashMap<Integer, ArrayList<Vector3f>>();
	}
	
	public void updateText(){
		this.characters.clear();
		HashMap<Integer, Character> characters = font.getCharacters();
		float wordLength = 0;
		char[] letters = text.toCharArray();
		Vector2f cursor = new Vector2f();
		int wordIndex = 0;
		HashMap<Integer, ArrayList<Vector3f>> linePositions = new HashMap<Integer, ArrayList<Vector3f>>();
		ArrayList<Vector2f> wordPositions = new ArrayList<Vector2f>();
		float aspectRatio = DisplayManager.getAspectRatio();
		if(parent!=null)
			aspectRatio *= parent.getAspectRatio();
		float maxLineLength = line/scale*aspectRatio;
		for(int i = 0; i < letters.length; i++){
			int letter = (int)letters[i];
			Character character = characters.get(letter);
			if(letter!=SPACE){
				wordPositions.add(new Vector2f(wordLength, 0));
				wordLength+=character.getXadvance();
			}if(letter==SPACE || i == letters.length - 1){
				if(i-1 >= 0 && letter == SPACE){
					Character lastChar = characters.get((int)letters[i-1]);
					wordLength -= lastChar.getXadvance() - lastChar.getWidth();
				}else{
					wordLength -= character.getXadvance() - character.getWidth();
				}
				if(cursor.x+wordLength+characters.get(SPACE).getXadvance()>maxLineLength){
					if(centered || right){
						float toCenter = (maxLineLength - cursor.x);
						if(right)
							toCenter *= 2;
						for(Integer k : linePositions.keySet()){
							for(Vector3f pos : linePositions.get(k)){
								pos.z = toCenter;
							}
						}
					}if(cursor.x != 0){
						cursor.x = 0;
						cursor.y -= font.getLineHeight();
					}
					for(Integer k : linePositions.keySet()){
						for(Vector3f pos : linePositions.get(k)){
							addDynamicCharacter(this.characters, k, pos);
						}
					}
					linePositions.clear();
				}if(cursor.x != 0){
					cursor.x += characters.get(SPACE).getXadvance();
				}for(int j = 0; j < wordPositions.size(); j++){
					Vector2f pos = wordPositions.get(j);
					Vector2f.add(pos, cursor, pos);
					addDynamicCharacter(linePositions, letters[wordIndex+j], new Vector3f(pos.x, pos.y, 0));
				}
				cursor.x += wordLength;
				wordLength = 0;
				wordIndex = i+1;
				wordPositions.clear();
			}if(i == letters.length - 1){
				if(centered || right){
					float toCenter = (maxLineLength - cursor.x);
					if(right)
						toCenter *= 2;
					for(Integer k : linePositions.keySet()){
						for(Vector3f pos : linePositions.get(k)){
							pos.z = toCenter;
							addDynamicCharacter(this.characters, k, pos);
						}
					}
				}else{
					for(Integer k : linePositions.keySet()){
						for(Vector3f pos : linePositions.get(k)){
							addDynamicCharacter(this.characters, k, pos);
						}
					}
				}
				linePositions.clear();
			}
		}
		updated = false;
	}
	
	public void update(){
		if(updated){
			updateText();
		}
	}
	
	public void setText(String text){
		if(!text.equals(this.text)){
			this.text = text;
			updated = true;
		}
	}
	
	private void addDynamicCharacter(HashMap<Integer, ArrayList<Vector3f>> map,int character, Vector3f position){
		ArrayList<Vector3f> batch = map.get(character);
		if(batch==null){
			ArrayList<Vector3f> newBatch = new ArrayList<Vector3f>();
			newBatch.add(position);
			map.put(character, newBatch);
		}else{
			batch.add(position);
		}
	}
	
	public void center(){
		this.centered = true;
		this.right = false;
		updated = true;
	}
	
	public void alignRight(){
		this.centered = false;
		this.right = true;
		updated = true;
	}
	
	public void alignLeft(){
		this.centered = false;
		this.right = false;
		updated = true;
	}
	
	public void setLine(float line){
		this.line = line;
		if(parent!=null)
			this.line *= parent.getWorldSize().x * 2;
		updated = true;
	}
	
	public void setScale(float scale){
		this.scale = scale;
		updated = true;
	}
	
	public void setPosition(Vector2f position){
		this.position = position;
	}
	
	public Vector2f getPosition(){
		Vector2f output = new Vector2f(this.position);
		if(centered){
			output.y += font.getBase() * scale;
			output.x -= line;
		}else if(settings.useBasePosition())
			output.y += font.getBase() * scale * 2;
		else
			output.y += font.getBase() * scale;
		if(right)
			output.x -= line * 2;
		return output;
	}
	
	public HashMap<Integer, ArrayList<Vector3f>> getCharacters(){
		return characters;
	}
	
	public FontSettings getSettings(){
		return settings;
	}
	
	public void setParent(GuiComponent parent){
		this.parent = parent;
		aspectRatio = parent.getWorldSize().x / parent.getWorldSize().y;
		updated = true;
	}
	
	public GuiComponent getParent(){
		return parent;
	}
	
	public float getAspectRatio(){
		return aspectRatio;
	}
	
	public float getScale(){
		return scale;
	}
	
	public void enable(){
		enabled = true;
	}
	
	public void disable(){
		enabled = false;
	}
	
	public boolean isEnabled(){
		return enabled;
	}
	
	public void useSettings(FontSettings settings){
		this.settings = settings;
	}
}
