package renderEngine.font;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.lwjgl.util.vector.Vector2f;

import renderEngine.renderEngine.Loader;

public class FontLoader {

	private static HashMap<String, String> values;
	private static BufferedReader reader;
	
	public static Font loadFont(String fontfile){
		values = new HashMap<String, String>();
		Font font = null;
		HashMap<Integer, Character> characters = new HashMap<Integer, Character>();
		File file = new File(fontfile);
		FileReader fileReader;
		try {
			fileReader = new FileReader(file);
			reader = new BufferedReader(fileReader);
			
			nextLine();
			boolean distanceField = getValue("smooth")==1;
			float[] pad = getValues("padding");
			
			nextLine();
			float scaleW = getValue("scaleW");
			float scaleH = getValue("scaleH");
			float lineHeight = getValue("lineHeight")/scaleH;
			float base = getValue("base")/scaleH;
			pad[0]/=scaleH;
			pad[1]/=scaleW;
			pad[2]/=scaleH;
			pad[3]/=scaleW;
			
			nextLine();
			String textureFile = values.get("file");
			textureFile = textureFile.substring(1,textureFile.length()-1);
			int textureAtlas = Loader.loadTexture(textureFile);
			
			nextLine();
			
			while(nextLine()){
				int id = getValue("id");
				float x = getValue("x")/scaleW;
				float y = getValue("y")/scaleH;
				Vector2f position = new Vector2f(x, y);
				float width = getValue("width")/scaleW;
				float height = getValue("height")/scaleH;
				Vector2f size = new Vector2f(width, height);
				float xoffset = getValue("xoffset")/scaleW;
				float yoffset = getValue("yoffset")/scaleH;
				Vector2f offset = new Vector2f(xoffset, yoffset);
				float xadvance = getValue("xadvance")/scaleW;
				Character character = new Character(position, size, offset, xadvance, pad);
				characters.put(id, character);
			}
			font = new Font(characters, textureAtlas, base, lineHeight);
			font.setDistanceField(distanceField);
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return font;
	}
	
	private static boolean nextLine() throws IOException{
		values.clear();
		String line = reader.readLine();
		if(line==null)
			return false;
		for(String pair : line.split(" ")){
			String[] value = pair.split("=");
			if(value.length==2){
				values.put(value[0], value[1]);
			}
		}
		return true;
	}
	
	private static float[] getValues(String value){
		String[] stringNums = values.get(value).split(",");
		float[] intNums = new float[stringNums.length];
		for(int i = 0; i < intNums.length; i++){
			intNums[i] = Integer.parseInt(stringNums[i]);
		}
		return intNums;
	}
	
	private static int getValue(String value){
		return Integer.parseInt(values.get(value));
	}
}
