package renderEngine.font;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.util.vector.Vector2f;
import renderEngine.guis.GuiComponent;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;

public class Text {

	private static final int SPACE = 32;
	
	private GuiComponent parent;
	private Font font;
	private Vector2f position;
	private String text;
	private FontSettings settings;
	private float line;
	private float aspectRatio = 1;
	private ArrayList<Line> lines;
	private boolean centered;
	private boolean right;
	private float scale = 1;
	private boolean enabled = true;
	private boolean init = true;
	
	public Text(String text, String fontName, Vector2f position){
		this(text, TextMaster.getFont(fontName), position, TextMaster.getFont(fontName).getSettings());
	}
	
	public Text(String text, Vector2f position){
		this(text, TextMaster.getFont(), position, TextMaster.getFont().getSettings());
	}
	
	public Text(String text, Font font, Vector2f position, FontSettings settings){
		this.font = font;
		this.position = position;
		this.text = text;
		this.settings = settings;
		this.line = 1;
		lines = new ArrayList<Line>();
	}
	
	public void buildMesh(){
		float aspectRatio = DisplayManager.getAspectRatio();
		if(parent!=null)
			aspectRatio *= parent.getAspectRatio();
		Line.centered = centered;
		Line.right = right;
		HashMap<Integer, Character> characters = font.getCharacters();
		Line.SPACE = characters.get(SPACE).getXadvance();
		Word word = new Word();
		float cursor = 0;
		lines.clear();
		lines.add(new Line(cursor));
		char[] letters = text.toCharArray();
		float maxLineLength = line/scale*aspectRatio;
		for(int i = 0; i < letters.length; i++){
			int letter = (int)letters[i];
			Character character = characters.get(letter);
			if(i!=letters.length&&text.substring(i, i+1).equals("\n")){
				lines.get(0).addWord(word);
				word.clear();
				lines.get(0).endLine(maxLineLength);
				cursor -= font.getLineHeight();
				lines.add(0, new Line(cursor));
			}else{
				if(letter!=SPACE){
					word.addLetter(character);
				}if(letter == SPACE || i == letters.length - 1){
					if(lines.get(0).getLength()+Line.SPACE+word.getWidth()>maxLineLength){
						if(lines.get(0).getLength() != 0){
							lines.get(0).endLine(maxLineLength);
							cursor -= font.getLineHeight();
							lines.add(0, new Line(cursor));
						}
					}lines.get(0).addWord(word);
					word.clear();
				}
			}
		}
		lines.get(0).endLine(maxLineLength);
	}
	
	public Vector2f getPosition(){
		Vector2f output = new Vector2f(this.position);
		if(centered){
			output.y += font.getBase() * scale;
			output.x -= line;
		}else if(settings.useBasePosition())
			output.y += font.getBase() * scale * 2;
		else
			output.y += font.getBase() * scale;
		if(right){
			output.x -= line * 2;
		}
		return output;
	}
	
	public void setPosition(Vector2f position){
		this.position = position;
	}
	
	public void setLine(float line){
		this.line = line;
		if(parent!=null)
			this.line *= parent.getWorldSize().x * 2;
	}
	
	public void center(){
		centered = true;
		right = false;
	}
	
	public void alignRight(){
		right = true;
		centered = false;
	}
	
	public String getText(){
		return text;
	}
	
	public FontSettings getSettings(){
		return settings;
	}
	
	protected void clean(){
		for(Line line : lines)
			Loader.cleanModel(line.getModel());
	}
	
	public ArrayList<Line> getLines(){
		return lines;
	}
	
	public void setScale(float scale){
		this.scale = scale;
	}
	
	public float getScale(){
		return scale;
	}
	
	public void enable(){
		enabled = true;
	}
	
	public void disable(){
		enabled = false;
	}
	
	public boolean isEnabled(){
		return enabled;
	}
	
	public void useSettings(FontSettings settings){
		this.settings = settings;
	}
	
	public void setParent(GuiComponent parent){
		this.parent = parent;
		aspectRatio = parent.getWorldSize().x / parent.getWorldSize().y;
		if(!init){
			buildMesh();
		}
	}
	
	public float getAspectRatio(){
		return aspectRatio;
	}
	
	public GuiComponent getParent(){
		return parent;
	}
	
	public void update(){
		if(init){
			buildMesh();
			init = false;
		}
	}
}
