package renderEngine.font;

import java.util.HashMap;
import java.util.Iterator;

import renderEngine.fontRenderer.FontRenderer;

public class TextMaster {

	private static HashMap<String, Font> fonts;
	
	public static void init(Font font){
		fonts = new HashMap<String, Font>();
		fonts.put("default", font);
	}
	
	
	/**
	 * @param name - use this key to access the font
	 * @param font
	 */
	public static void addFont(String fontName, Font font){
		if(fonts.get(fontName)!=null)
			removeFont(fontName);
		fonts.put(fontName, font);
	}
	
	public static Font getFont(String fontName){
		return fonts.get(fontName);
	}
	
	public static Font getFont(){
		return fonts.get("default");
	}
	
	public static void removeFont(String fontName){
		Font font = fonts.get(fontName);
		if(font!=null){
			font.clean();
			for(Text text : font.getTexts()){
				text.clean();
			}
		}
		fonts.remove(fontName);
	}
	
	public static void addText(Text text, String fontName){
		Font font = fonts.get(fontName);
		if(font!=null){
			font.getTexts().add(text);
		}else{
			System.err.println("ERROR: " + fontName + " font not loaded!");
		}
	}
	
	public static void removeText(Text text, String fontName){
		Font font = fonts.get(fontName);
		if(font!=null){
			font.getTexts().remove(text);
		}else{
			System.err.println("ERROR: " + fontName + " font not loaded!");
		}
	}
	
	public static void addText(Text text){
		String fontName = "default";
		addText(text, fontName);
	}
	
	public static void removeText(Text text){
		String fontName = "default";
		removeText(text, fontName);
	}
	
	public static void addText(DynamicText text, String fontName){
		Font font = fonts.get(fontName);
		if(font!=null){
			font.getDynamicTexts().add(text);
		}else{
			System.err.println("ERROR: " + fontName + " font not loaded!");
		}
	}
	
	public static void removeText(DynamicText text, String fontName){
		Font font = fonts.get(fontName);
		if(font!=null){
			font.getDynamicTexts().remove(text);
		}else{
			System.err.println("ERROR: " + fontName + " font not loaded!");
		}
	}
	
	public static void addText(DynamicText text){
		String fontName = "default";
		addText(text, fontName);
	}
	
	public static void removeText(DynamicText text){
		String fontName = "default";
		removeText(text, fontName);
	}
	
	public static void render(){
		Iterator<Font> iterator = fonts.values().iterator();
		FontRenderer.render(iterator);
	}
	
	public static void clean(){
		FontRenderer.clean();
	}
}
