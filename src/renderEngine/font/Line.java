package renderEngine.font;

import java.util.ArrayList;
import org.lwjgl.util.vector.Vector2f;

import renderEngine.models.RawModel;
import renderEngine.renderEngine.Loader;

public class Line {

	public static float SPACE;
	public static boolean centered = false;
	public static boolean right = false;
	
	private ArrayList<Vector2f> pos;
	private ArrayList<Vector2f> textureCoords;
	private float cursor;
	private float position;
	private float lineOffset;
	private RawModel model;
	private float toWidth;
	
	public Line(float position){
		this.position = position;
		cursor = 0;
		pos = new ArrayList<Vector2f>();
		textureCoords = new ArrayList<Vector2f>();
	}
	
	public void addWord(Word word){
		if(cursor != 0){
			cursor += SPACE;
		}for(Vector2f p : word.getPositions()){
			p.x += cursor;
			p.y += position;
			pos.add(p);
		}for(Vector2f t : word.getTextureCoords())
			textureCoords.add(t);
		cursor += word.getLength();
		toWidth = word.getLength() - word.getWidth();
	}
	
	public void endLine(float maxLength){
		model = Loader.loadToVAO(toArray(pos), toArray(textureCoords));
		lineOffset = 0;
		if(centered){
			lineOffset = (maxLength - (cursor - toWidth));
		}else if(right){
			lineOffset = (maxLength - (cursor - toWidth)) * 2;
		}
		pos = null;
		textureCoords = null;
	}
	
	private float[] toArray(ArrayList<Vector2f> list){
		float[] array = new float[list.size() * 2];
		for(int i = 0; i < list.size(); i++){
			array[i * 2] = list.get(i).x;
			array[i * 2 + 1] = list.get(i).y;
		}
		return array;
	}
	
	public float getLineOffset(){
		return lineOffset;
	}
	
	public RawModel getModel(){
		return model;
	}
	
	public float getLength(){
		return cursor;
	}
}
