package renderEngine.font;

import org.lwjgl.util.vector.Vector4f;

public class FontSettings {
	
	private Vector4f color = new Vector4f(1,1,1,1);
	private boolean basePosition = true;
	private boolean centered = false;
	private float renderWidth = .5f;
	private Vector4f outlineColor = new Vector4f(0,0,0,1);
	private float fadeWidth = 0.2f;
	private float outlineWidth;
	private boolean hasOutline = false;
	private Vector4f overrideColor;
	
	public FontSettings(){}
	
	public FontSettings(FontSettings settings){
		this.color = settings.color;
		this.basePosition = settings.basePosition;
		this.centered = settings.centered;
		this.renderWidth = settings.renderWidth;
		this.outlineWidth = settings.outlineWidth;
		this.fadeWidth = settings.fadeWidth;
		this.hasOutline = settings.hasOutline;
	}
	
	public boolean hasOutline(){
		return hasOutline;
	}
	
	public float getRenderWidth(){
		return renderWidth;
	}
	
	public void setRenderWidth(float width){
		renderWidth = width;
	}
	
	public float getFadeWidth(){
		return (float) (renderWidth+fadeWidth);
	}
	
	public void setFadeWidth(float fadeWidth){
		this.fadeWidth = fadeWidth;
	}
	
	public boolean isCentered(){
		return centered;
	}
	
	public void isCentered(boolean centered){
		this.centered = centered;
	}
	
	public boolean useBasePosition(){
		return basePosition;
	}
	
	public void useBasePosition(boolean use){
		basePosition = use;
	}

	public Vector4f getColor() {
		return overrideColor != null ? overrideColor : color;
	}

	public void setColor(Vector4f color) {
		this.color = color;
	}
	
	public Vector4f getOutlineColor() {
		return outlineColor;
	}

	public void setOutlineColor(Vector4f outlineColor) {
		this.outlineColor = outlineColor;
	}
	
	public float getOutlineWidth(){
		return outlineWidth;
	}
	
	public void setOutlineWidth(float width){
		hasOutline = true;
		outlineWidth = width;
	}
	
	public void disableOutline(){
		hasOutline = false;
	}
	
	public void setOverrideColor(Vector4f color){
		overrideColor = color;
	}
}
