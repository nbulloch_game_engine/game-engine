package renderEngine.font;

import org.lwjgl.util.vector.Vector2f;
import renderEngine.models.RawModel;
import renderEngine.renderEngine.Loader;

public class Character {
	
	private Vector2f position, size, offset;
	private float xadvance;
	private Vector2f[] pos;
	private Vector2f[] texcoords;
	private RawModel model = null;
	
	protected Character(Vector2f position, Vector2f size, Vector2f offset, float xadvance, float[] pad){
		this.position = position;
		this.size = size;
		this.offset = offset;
		this.xadvance = xadvance;
		createMesh(pad);
	}
	
	private void createMesh(float[] pad){//pad: up, right, down, left
		pos = new Vector2f[6];
		texcoords = new Vector2f[6];
		float xmin = -pad[3];
		float ymin = -pad[2];
		float x = size.x + pad[1];
		float y = size.y + pad[0];
		pos[0] = new Vector2f(xmin, ymin);
		pos[1] = new Vector2f(x, y);
		pos[2] = new Vector2f(xmin, y);
		pos[3] = new Vector2f(pos[0]);
		pos[4] = new Vector2f(x, ymin);
		pos[5] = new Vector2f(pos[1]);
		for(Vector2f position : pos){
			position.y = size.y - position.y - (size.y+offset.y);
			position.x += offset.x;
		}
		x = size.x;
		y = size.y;
		texcoords[0] = new Vector2f(0, 0);
		texcoords[1] = new Vector2f(x, y);
		texcoords[2] = new Vector2f(0, y);
		texcoords[3] = new Vector2f(texcoords[0]);
		texcoords[4] = new Vector2f(x, 0);
		texcoords[5] = new Vector2f(texcoords[1]);
		for(Vector2f coord : texcoords){
			Vector2f.add(coord, position, coord);
		}
		model = Loader.loadToVAO(toFloatArray(pos), toFloatArray(texcoords));
	}
	
	public RawModel getModel(){
		return model;
	}
	
	protected void clean(){
		Loader.cleanModel(model);
	}
	
	private float[] toFloatArray(Vector2f[] in){
		float[] array = new float[in.length * 2];
		for(int i = 0; i < in.length; i++){
			array[i*2] = in[i].x;
			array[i*2+1] = in[i].y;
		}
		return array;
	} 
	
	public Vector2f getPosition() {
		return position;
	}

	public Vector2f getSize() {
		return size;
	}

	public Vector2f getOffset() {
		return offset;
	}

	public float getXadvance() {
		return xadvance;
	}
	
	public float getWidth(){
		return size.x;
	}
	
	public Vector2f[] getPositions(){
		return pos;
	}
	
	public Vector2f[] getTextureCoords(){
		return texcoords;
	}
}
