package renderEngine.font;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;

public class Word {

	private ArrayList<Vector2f> positions;
	private ArrayList<Vector2f> textureCoords;
	private float wordLength;
	private float wordWidth;
	
	public Word(){
		positions = new ArrayList<Vector2f>();
		textureCoords = new ArrayList<Vector2f>();
		wordLength = 0;
	}
	
	public void addLetter(Character character){
		for(Vector2f pos : character.getPositions())
			positions.add(new Vector2f(pos.x + wordLength, pos.y));
		for(Vector2f texCoords : character.getTextureCoords())
			textureCoords.add(texCoords);
		wordWidth = wordLength + character.getWidth();
		wordLength += character.getXadvance();
	}
	
	public void clear(){
		positions.clear();
		textureCoords.clear();
		wordLength = 0;
	}
	
	public ArrayList<Vector2f> getPositions(){
		return positions;
	}
	
	public ArrayList<Vector2f> getTextureCoords(){
		return textureCoords;
	}
	
	public float getLength(){
		return wordLength;
	}
	
	public float getWidth(){
		return wordWidth;
	}
}
