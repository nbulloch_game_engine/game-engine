package renderEngine.utils;

public class SLERP extends NLERP{
	
	private static final float DOT_THRESHOLD = 0.999994f;
	
	private boolean nlerp = false;
	private float theta;
	private Quaternion v2;

	public SLERP(Quaternion initial, Quaternion target, float duration) {
		super(initial, target, duration);
		v2 = new Quaternion();
	}

	public SLERP(){
		super();
		v2 = new Quaternion();
	}

	@Override
	protected Quaternion interpolate(Quaternion initial, Quaternion target, float blend){
		if(nlerp)
			return super.interpolate(initial, target, blend);
		final float blendTheta = theta * blend;
		return result.setValues(initial.multiply((float) Math.cos(blendTheta)).add(v2.multiply((float) Math.sin(blendTheta))));
	}
	
	@Override
	public boolean recycle(Quaternion initial, Quaternion target, float seconds) {
		nlerp = false;
		return super.recycle(initial, target, seconds);
	}
	
	@Override
	protected void initiate() {
		super.initiate();
		if(dot > DOT_THRESHOLD) {
			nlerp = true;
			return;
		}else if(dot < 0.0f) {
			dot = -dot;
			target.negate();
		}
		dot = Math.max(dot, -1); dot = Math.min(dot, 1); //clamp dot to [-1,1]
		theta = (float) Math.acos(dot);
		v2 = target.sub(initial.multiply(dot)).normalize();
	}
}
