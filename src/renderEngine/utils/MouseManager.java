package renderEngine.utils;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class MouseManager {
	
	private static float DX;
	private static float DY;
	
	private static boolean isLocked = true;
	private static Cursor emptyCursor = getEmptyCursor();
	
	public static void hideMouse(){
		isLocked = true;
		try {
			Mouse.setGrabbed(true);
			Mouse.setNativeCursor(emptyCursor);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		clearDelta();
	}
	
	public static void clearDelta(){
		updateCursor();
		DX = 0;
		DY = 0;
	}
	
	public static void showMouse(){
		isLocked = false;
		try {
			Mouse.setNativeCursor(null);
			Mouse.setGrabbed(false);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	private static Cursor getEmptyCursor(){
		Cursor emptyCursor = null;
		try {
			emptyCursor = new Cursor(1, 1, 0, 0, 1, BufferUtils.createIntBuffer(16), null);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		return emptyCursor;
	}
	
	public static void updateCursor(){
		if(isLocked){
			DX = -(Display.getWidth()/2 - Mouse.getX());
			DY = -(Display.getHeight()/2 - Mouse.getY());
			Mouse.setCursorPosition(Display.getWidth()/2, Display.getHeight()/2);
		}else{
			DX = Mouse.getDX();
			DY = Mouse.getDY();
		}
		//System.out.println("Mouse: " + ((float)Mouse.getX()/Display.getWidth() * 2 - 1) + ", " + ((float)Mouse.getY()/Display.getHeight() * 2 - 1));
	}
	
	public static boolean isLocked() {
		return isLocked;
	}

	public static float getDX() {
		return DX;
	}

	public static float getDY() {
		return DY;
	}
	
}
