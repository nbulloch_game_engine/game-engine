package renderEngine.utils;

import org.lwjgl.util.vector.Vector3f;

public class LERP {

	protected Quaternion initial, target, result;
	private float dt;//delta time in ms
	private long begin;
	private boolean dead = true;
	
	//per-LERP precalculations
	protected float dot;
	private Vector3f axis;
	boolean parallel = false;
	private Quaternion delta;
	
	/**
	 * Constructs a dead LERP for later use
	 */
	public LERP() {
		result = new Quaternion();
	}
	
	/**
	 * Construct a new LERP and begins interpolation
	 * @param current an object's initial rotation
	 * @param target the object's final rotation
	 * @param duration the duration of the interpolation in seconds
	 */
	public LERP(Quaternion initial, Quaternion target, float duration){
		result = new Quaternion();
		recycle(initial, target, duration);
	}
	
	/**
	 * Gets the current rotation of returns the target quaternion if dead
	 * @return The interpolated rotation according to the current time
	 */
	public Quaternion getRotation(){
		float blend = getBlend();
		if(blend==0)
			return initial;
		if(dead)
			return target;
		return interpolate(initial, target, blend);
	}
	
	protected Quaternion interpolate(Quaternion initial, Quaternion target, float blend){
		if(parallel)
			result.setValues(axis, blend * (float) Math.PI);
		else {
			result.setValues(initial);
			Quaternion.add(result, delta.multiply(blend), result);
		}return result;
	}
	
	/**
	 * @return The normalized progress of the interpolation [0-1]
	 */
	public float getBlend(){
		float blend = (float)(System.currentTimeMillis()-begin)/dt;
		if(blend>1){
			dead = true;
			blend = 1;
		}
		return blend;
	}
	
	/**
	 * returns true if the interpolation has finished
	 */
	public boolean isDead(){
		return dead;
	}
	
	/**
	 * Sets the current time to t<sub>0</sub>
	 */
	public void begin(){
		begin = System.currentTimeMillis();
		dead = false;
	}
	
	/**
	 * Sets all the values and begins the interpolation. 
	 * Use on an expired LERP to avoid initializing a new LERP object.
	 * Returns true and sets values if the current LERP is dead.
	 * @param duration length in seconds
	 */
	public boolean recycle(Quaternion initial, Quaternion target, float duration){
		if(dead){
			parallel = false;
			this.initial = initial;
			this.target = target;
			dt = duration*1000;
			begin();
			initiate();
		}return dead;
	}
	
	/**
	 * Pre-calculates the axis of rotation and determines approximate parallelism
	 */
	protected void initiate() {
		dot = initial.dot(target);
		if(dot < 0) {
			target.negate();
			if(dot < Maths.EPSILON-1) {
				Vector3f v = initial.getVector();
				Vector3f nonparallel = new Vector3f(v.y,-v.z,v.x);
				Vector3f.cross(v, nonparallel, axis);
				axis.normalise();
				parallel = true;
				return;
			}
		}
		delta = target.sub(initial);
	}
}
