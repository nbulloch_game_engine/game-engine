package renderEngine.utils;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.entities.Camera;

public class Maths {
	
	public static final float SQRT_HALF = (float) Math.sqrt(0.5f);
	public static final float EPSILON = 0.000001f;
	
	public static float barryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
		float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
		float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
		float l3 = 1.0f - l1 - l2;
		return l1 * p1.y + l2 * p2.y + l3 * p3.y;
	}
	
	public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f centerOfRotation, Vector3f rotation, Vector3f scale){
		Matrix4f matrix = new Matrix4f();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.translate(centerOfRotation, matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1,0,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0,1,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0,0,1), matrix, matrix);
		Matrix4f.translate(centerOfRotation.negate(null), matrix, matrix);
		Matrix4f.scale(scale, matrix, matrix);
		return matrix;
	}
	
	public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f rotation, Vector3f scale){
		Vector3f r = new Vector3f(rotation);
		r.x = (float)Math.toRadians(rotation.x);
		r.y = (float)Math.toRadians(rotation.y);
		r.z = (float)Math.toRadians(rotation.z);
		return createTransformationMatrixRadians(translation, r, scale);
	}
	
	public static Matrix4f createTransformationMatrixRadians(Vector3f translation, Vector3f rotation, Vector3f scale){
		Matrix4f matrix = new Matrix4f();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.rotate((float) rotation.x, new Vector3f(1,0,0), matrix, matrix);
		Matrix4f.rotate((float) rotation.y, new Vector3f(0,1,0), matrix, matrix);
		Matrix4f.rotate((float) rotation.z, new Vector3f(0,0,1), matrix, matrix);
		Matrix4f.scale(scale, matrix, matrix);
		return matrix;
	}
	
	public static Matrix4f createTranslationMatrix(Vector3f translation){
		Matrix4f matrix = new Matrix4f();
		Matrix4f.translate(translation, matrix, matrix);
		return matrix;
	}
		
	public static Matrix4f create2DTransformationMatrix(Vector2f translation, Vector2f scale) {
		Matrix4f matrix = new Matrix4f();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.scale(new Vector3f(scale.x, scale.y, 1f), matrix, matrix);
		return matrix;
	}
	
	public static Matrix4f create2DTransformationMatrix(Vector2f translation, Vector2f scale, float rotation) {//rotation does not work as expected
		Matrix4f matrix = new Matrix4f();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation), new Vector3f(0,0,1), matrix, matrix);
		Matrix4f.scale(new Vector3f(scale.x, scale.y, 1f), matrix, matrix);
		return matrix;
	}
	
	/**
	 * Creates an XYZ rotation 3x3 matrix
	 * @param rotation The Euler angles of rotation about (x, y, z) axis in degrees
	 * @return A rotation matrix
	 */
	public static Matrix3f createRotationMatrix(Vector3f rotation){
		Matrix4f matrix4f = new Matrix4f();
		Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1,0,0), matrix4f, matrix4f);
		Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0,1,0), matrix4f, matrix4f);
		Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0,0,1), matrix4f, matrix4f);
		return toMatrix3f(matrix4f);
	}
	
	/**
	 * Creates an XYZ rotation 3x3 matrix
	 * @param rotation The angles of rotation (x, y, z) in radians
	 * @return A rotation matrix
	 */
	public static Matrix3f createRotationMatrixRadians(Vector3f rotation){
		Matrix4f matrix4f = new Matrix4f();
		Matrix4f.rotate(rotation.x, new Vector3f(1,0,0), matrix4f, matrix4f);
		Matrix4f.rotate(rotation.y, new Vector3f(0,1,0), matrix4f, matrix4f);
		Matrix4f.rotate(rotation.z, new Vector3f(0,0,1), matrix4f, matrix4f);
		return toMatrix3f(matrix4f);
	}
	
	public static Matrix3f toMatrix3f(Matrix4f matrix4f){
		Matrix3f matrix = new Matrix3f();
		matrix.m00 = matrix4f.m00;
		matrix.m01 = matrix4f.m01;
		matrix.m02 = matrix4f.m02;
		matrix.m10 = matrix4f.m10;
		matrix.m11 = matrix4f.m11;
		matrix.m12 = matrix4f.m12;
		matrix.m20 = matrix4f.m20;
		matrix.m21 = matrix4f.m21;
		matrix.m22 = matrix4f.m22;
		return matrix;
	}
	
	public static Vector3f projectVector(Vector3f projectTo, Vector3f input){
		float scalarComponentA = Vector3f.dot(projectTo, input);
		float scalarComponentB = Vector3f.dot(projectTo, projectTo);
		float scalar = scalarComponentA/scalarComponentB;
		return (Vector3f) new Vector3f(projectTo).scale(scalar);
	}
	
	public static float projectionFactor(Vector3f projectTo, Vector3f input){
		float scalarComponentA = Vector3f.dot(projectTo, input);
		float scalarComponentB = Vector3f.dot(projectTo, projectTo);
		return scalarComponentA/scalarComponentB;
	}
	
	public static Matrix3f roundMatrix(Matrix3f matrix, Double amount){
		matrix.m00 = (float)(Math.round(matrix.m00 * amount) / amount);
		matrix.m01 = (float)(Math.round(matrix.m01 * amount) / amount);
		matrix.m02 = (float)(Math.round(matrix.m02 * amount) / amount);
		matrix.m10 = (float)(Math.round(matrix.m10 * amount) / amount);
		matrix.m11 = (float)(Math.round(matrix.m11 * amount) / amount);
		matrix.m12 = (float)(Math.round(matrix.m12 * amount) / amount);
		matrix.m20 = (float)(Math.round(matrix.m20 * amount) / amount);
		matrix.m21 = (float)(Math.round(matrix.m21 * amount) / amount);
		matrix.m22 = (float)(Math.round(matrix.m22 * amount) / amount);
		return matrix;
	}
	
	public static Vector3f divideVector3f(Vector3f vector, float scalar){
		Vector3f vec = new Vector3f(vector);
		vec.x/=scalar;
		vec.y/=scalar;
		vec.z/=scalar;
		return vec;
	}
	
	public static Vector3f multVector3f(Vector3f vec1, Vector3f vec2, Vector3f storage){
		Vector3f vec;
		if(storage==null){
			vec = new Vector3f();
		}else{
			vec = storage;
		}
		vec.x = vec1.x * vec2.x;
		vec.y = vec1.y * vec2.y;
		vec.z = vec1.z * vec2.z;
		return vec;
	}
	
	public static Vector3f addVector3f(Vector3f vector, float scalar){
		Vector3f vec = new Vector3f(vector);
		vec.x+=scalar;
		vec.y+=scalar;
		vec.z+=scalar;
		return vec;
	}
	
	public static Vector3f subVector3f(Vector3f vector, float scalar){
		Vector3f vec = new Vector3f(vector);
		vec.x-=scalar;
		vec.y-=scalar;
		vec.z-=scalar;
		return vec;
	}
	
	public static Vector4f toVector4f(Vector3f vector, boolean direction){
		return new Vector4f(vector.x, vector.y, vector.z, direction ? 0 : 1);
	}
	
	public static Vector3f toVector3f(Vector4f vector){
		return new Vector3f(vector.x, vector.y, vector.z);
	}
	
	public static Vector3f getMinComponents(Vector3f a, Vector3f b, Vector3f out){
		if(out==null)
			return (out = new Vector3f(Math.min(a.x, b.x),Math.min(a.y, b.y),Math.min(a.z, b.z)));
		out.set(Math.min(a.x, b.x),Math.min(a.y, b.y),Math.min(a.z, b.z));
		return out;
	}
	
	public static Vector3f getMaxComponents(Vector3f a, Vector3f b, Vector3f out){
		if(out==null)
			return (out = new Vector3f(Math.max(a.x, b.x),Math.max(a.y, b.y),Math.max(a.z, b.z)));
		out.set(Math.max(a.x, b.x),Math.max(a.y, b.y),Math.max(a.z, b.z));
		return out;
	}
	
	/**
	 * Calculates any vector's pitch and yaw in the global frame of reference defined by FORWARD (0,0,-1), UP (0,1,0), RIGHT (1,0,0)
	 * @param vector The <b>unit vector</b> to convert to pitch and yaw
	 * @param picth The quaternion that stores pitch output
	 * @param yaw The quaternion that stores yaw output 
	 */
	public static void calculatePitchYaw(Vector3f vector, Quaternion pitch, Quaternion yaw) {
		float len_xz = (float) Math.sqrt(vector.x*vector.x+vector.z*vector.z);
		len_xz = Math.min(len_xz, 1);//clamp to exactly 1 to avoid imaginary results
		float cos_xz = 0;
		if(len_xz!=0)//avoid division by zero
			cos_xz = -vector.z/len_xz;
		float hThetaSin_xz = (float) Math.sqrt(1-cos_xz)*SQRT_HALF;
		float hThetaCos_xz = (float) Math.sqrt(1+cos_xz)*SQRT_HALF;
		hThetaCos_xz = Math.copySign(hThetaSin_xz, -vector.x);
		yaw.setValues(0,hThetaSin_xz,0,hThetaCos_xz);
		
		//cos_y = len_xz;
		float hThetaSin_y = (float) Math.sqrt(1-len_xz)*SQRT_HALF;
		float hThetaCos_y = (float) Math.sqrt(1+len_xz)*SQRT_HALF;
		hThetaCos_xz = Math.copySign(hThetaSin_y, vector.y);
		pitch.setValues(hThetaSin_y,0,0,hThetaCos_y);
	}
	
	/**
	 * Calculates any vector's yaw and pitch in the global frame of reference defined by FORWARD (0,0,-1), UP (0,1,0), RIGHT (1,0,0)
	 * @param yaw The quaternion that stores yaw output 
	 * @param vector The <b>unit vector</b> to convert to yaw and pitch
	 * @param picth The quaternion that stores pitch output
	 */
	public static void calculateYawPitch(Vector3f vector, Quaternion yaw, Quaternion pitch){
		float len_yz = (float) Math.sqrt(vector.y*vector.y+vector.z*vector.z);
		len_yz = Math.min(len_yz, 1);//clamp to exactly 1 to avoid imaginary results
		float cos_xz = 0;
		if(len_yz!=0)//avoid division by zero
			cos_xz = -vector.z/len_yz;
		float hThetaSin_yz = (float) Math.sqrt(1-cos_xz)*SQRT_HALF;
		float hThetaCos_yz = (float) Math.sqrt(1+cos_xz)*SQRT_HALF;
		hThetaCos_yz = Math.copySign(hThetaSin_yz, vector.y);
		pitch.setValues(hThetaSin_yz,0,0,hThetaCos_yz);
		
		//cos_x = len_yz;
		float hThetaSin_y = (float) Math.sqrt(1-len_yz)*SQRT_HALF;
		float hThetaCos_y = (float) Math.sqrt(1+len_yz)*SQRT_HALF;
		hThetaCos_y = Math.copySign(hThetaSin_y, -vector.x);
		yaw.setValues(0,hThetaSin_y,0,hThetaCos_y);
	}
	
	/**
	 * Calculates a Quaternion's equivalent pitch and yaw Quaternions (minus roll)
	 * @param rotation The rotation to deconstruct
	 * @param pitch The quaternion that stores pitch output
	 * @param yaw The quaternion that stores yaw output 
	 */
	public static void calculatePitchYaw(Quaternion rotation, Quaternion pitch, Quaternion yaw) {
		calculatePitchYaw(rotation.multiply(Camera.FORWARD, null), pitch, yaw);
	}
	
	/**
	 * Calculates a Quaternion's equivalent yaw and pitch Quaternions (minus roll)
	 * @param rotation The rotation to deconstruct
	 * @param yaw The quaternion that stores yaw output
	 * @param pitch The quaternion that stores pitch output 
	 */
	public static void calculateYawPitch(Quaternion rotation, Quaternion yaw, Quaternion pitch) {
		calculateYawPitch(rotation.multiply(Camera.FORWARD, null), yaw, pitch);
	}
	
	public static Matrix3f covarianceMatrix(Vector3f[] points) {
		Matrix3f cov = new Matrix3f();
		cov.m00 = cov.m11 = cov.m22 = 0;
		
		float iLen = 1.0f / (float) points.length;
		Vector3f mean = new Vector3f();
		
		for(Vector3f point : points) {
			Vector3f.add(point, mean, mean);
		}
		mean.scale(iLen);
		
		for(Vector3f point : points) {
			Vector3f p = Vector3f.sub(point, mean, null);
			cov.m00 += p.x*p.x;
			cov.m11 += p.y*p.y;
			cov.m22 += p.z*p.z;
			cov.m01 += p.x*p.y;
			cov.m02 += p.x*p.z;
			cov.m12 += p.y*p.z;
		}
		cov.m00 *= iLen;
		cov.m11 *= iLen;
		cov.m22 *= iLen;
		cov.m01 = cov.m10 = cov.m01 * iLen;
		cov.m02 = cov.m20 = cov.m02 * iLen;
		cov.m12 = cov.m21 = cov.m12 * iLen;
		
		return cov;
	}
	
	/**
	 * A simplified inverse matrix calculation based on the assumption that the matrix is symmetric
	 * @param sym The symmetric matrix
	 * @return The inverse matrix of sym
	 */
	public static Matrix3f symmetricInverse(Matrix3f sym) {
		//sym = [a,b,c
		//		b,e,f
		//		c,f,q]
		Matrix3f inv = new Matrix3f();
		float bf = sym.m01*sym.m12;
		float ce = sym.m02*sym.m11;
		float fa = sym.m12*sym.m00;
		float bq = sym.m01*sym.m22;
		
		inv.m00 = sym.m11*sym.m22 - sym.m12*sym.m12;
		inv.m11 = sym.m00*sym.m22 - sym.m02*sym.m02;
		inv.m22 = sym.m00*sym.m11 - sym.m01*sym.m01;
		
		inv.m01 = inv.m10 = sym.m12*sym.m02 - bq;
		inv.m02 = inv.m20 = bf - ce;
		inv.m12 = inv.m21 = sym.m02*sym.m01 - fa;
		
		float det = sym.m00*sym.m11*sym.m22 + 2*bf*sym.m02 - sym.m02*ce
				- sym.m12*fa - sym.m01*bq;
		
		scale(inv, 1f / det, inv);
		
		return inv;
	}
	
	public static Matrix3f scale(Matrix3f mat, float scale, Matrix3f out) {
		if(out==null) {
			out = new Matrix3f();
		}else if(!out.equals(mat)){
			out.load(mat);
		}
		
		out.m00 *= scale;
		out.m01 *= scale;
		out.m02 *= scale;
		out.m10 *= scale;
		out.m11 *= scale;
		out.m12 *= scale;
		out.m20 *= scale;
		out.m21 *= scale;
		out.m22 *= scale;
		
		return out;
	}
}
