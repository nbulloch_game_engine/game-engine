package renderEngine.utils;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Quaternion {

	//Useful math site: http://www.j3d.org/matrix_faq/matrfaq_latest.html
	public float x,y,z,w;
	
	/**
	 * Creates the identity quaternion (0,0,0,1)
	 */
	public Quaternion() {
		setIdentity();
	}
	
	/**
	 * Creates quaternion in (xi, yj, zk, w) form
	 */
	public Quaternion(float x, float y, float z, float w) {
		setValues(x,y,z,w);
	}
	
	/**
	 * Creates a Quaternion representing the rotation from origin to destination
	 * @param origin The starting position of rotation
	 * @param destination The ending position of rotation
	 */
	public Quaternion(Vector3f origin, Vector3f destination) {
		this(Quaternion.rotationAsQuaternion(origin, destination));
	}
	
	/**
	 * Creates a quaternion representing a rotation of theta radians about the axis
	 * @param theta Angle of rotation in Radians
	 * @param axis The UNIT VECTOR axis of rotation
	 */
	public Quaternion(Vector3f axis, float theta) {
		final float sin = (float)Math.sin(theta/2f);
		setValues(axis.x*sin, axis.y*sin, axis.z*sin, (float)Math.cos(theta/2f));
		normalize();
	}
	
	/**
	 * Copies a quaternion
	 */
	public Quaternion(Quaternion quat) {
		this(quat.x, quat.y, quat.z, quat.w);
	}
	
	/**
	 * Creates a Quaternion from a ROTATION ONLY matrix
	 * @param m matrix to be converted to a Quaternion
	 */
	public Quaternion(Matrix3f m) {
		setValues(m);
	}
	
	/**
	 * Creates a Quaternion from a ROTATION ONLY matrix
	 * @param m matrix to be converted to a Quaternion
	 */
	public Quaternion(Matrix4f m) {
		setValues(m);
	}
	
	/**
	 * Converts a matrix to a quaternion and stores the values
	 * @param m The rotation matrix to convert to a quaternion
	 */
	public Quaternion setValues(Matrix3f m) { //http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
		final float trace = m.m00 + m.m11 + m.m22;
		final float s;
		if(trace > 0) {// may return -q
			s = 0.5f / (float) Math.sqrt(trace+1.0f);
			x = (m.m12 - m.m21) * s;
			y = (m.m20 - m.m02) * s;
			z = (m.m01 - m.m10) * s;
			w = 0.25f / s;
		}else if(m.m00 > m.m11 && m.m00 > m.m22) {// may return -q
			s = (float) Math.sqrt(1f + m.m00 - m.m11 - m.m22) * 2f;
			y = (m.m01 + m.m10) / s;
	        z = (m.m02 + m.m20) / s;
	        w = (m.m12 - m.m21) / s;
	        x = 0.25f * s;
		}else if(m.m11 > m.m22) {// may return -q
			s  = (float) Math.sqrt(1f - m.m00 + m.m11 - m.m22) * 2f;
			z = (m.m12 + m.m21) / s;
	        w = (m.m20 - m.m02) / s;
	        x = (m.m01 + m.m10) / s;
	        y = 0.25f * s;
		}else {// may return -q
			s  = (float) Math.sqrt(1f - m.m00 - m.m11 + m.m22) * 2f;
			w = (m.m01 - m.m10) / s;
	        x = (m.m02 + m.m20) / s;
	        y = (m.m12 + m.m21) / s;
	        z = 0.25f * s;
		}
		normalize();
		negate();
		return this;
	}
	
	/**
	 * Converts a matrix to a quaternion and stores the values
	 * @param m The rotation matrix to convert to a quaternion
	 */
	public Quaternion setValues(Matrix4f m) { //http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
		final float trace = m.m00 + m.m11 + m.m22;
		final float s;
		if(trace > 0) {// may return -q
			s = 0.5f / (float) Math.sqrt(trace+1.0f);
			x = (m.m12 - m.m21) * s;
			y = (m.m20 - m.m02) * s;
			z = (m.m01 - m.m10) * s;
			w = 0.25f / s;
		}else if(m.m00 > m.m11 && m.m00 > m.m22) {// may return -q
			s = (float) Math.sqrt(1f + m.m00 - m.m11 - m.m22) * 2f;
			y = (m.m01 + m.m10) / s;
	        z = (m.m02 + m.m20) / s;
	        w = (m.m12 - m.m21) / s;
	        x = 0.25f * s;
		}else if(m.m11 > m.m22) {// may return -q
			s  = (float) Math.sqrt(1f - m.m00 + m.m11 - m.m22) * 2f;
			z = (m.m12 + m.m21) / s;
	        w = (m.m20 - m.m02) / s;
	        x = (m.m01 + m.m10) / s;
	        y = 0.25f * s;
		}else {// may return -q
			s  = (float) Math.sqrt(1f - m.m00 - m.m11 + m.m22) * 2f;
			w = (m.m01 - m.m10) / s;
	        x = (m.m02 + m.m20) / s;
	        y = (m.m12 + m.m21) / s;
	        z = 0.25f * s;
		}
		normalize();
		negate();
		return this;
	}
	
	/**
	 * Sets the values without normalizing
	 * @return This Quaternion
	 */
	public Quaternion setValues(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		return this;
	}
	
	public Quaternion setValues(Quaternion quat){
		x = quat.x;
		y = quat.y;
		z = quat.z;
		w = quat.w;
		return this;
	}
	
	public Quaternion setValues(Vector3f axis, float w){
		x = axis.x;
		y = axis.y;
		z = axis.z;
		this.w = w;
		return this;
	}
	
	/**
	 * @return The x, y, z components as a new Vector3f
	 */
	public Vector3f getVector() {
		return new Vector3f(x,y,z);
	}
	
	/**
	 * Sets the Quaternion to (0,0,0,1)
	 */
	public Quaternion setIdentity() {
		setValues(0,0,0,1);
		return this;
	}
	
	/**
	 * Sets the Quaternion to (0,0,0,0)
	 */
	public Quaternion setZero() {
		setValues(0,0,0,0);
		return this;
	}
	
	/**
	 * @return (-x, -y, -z, -w)
	 */
	public Quaternion negate() {
		x = -x;
		y = -y;
		z = -z;
		w = -w;
		return this;
	}
	
	/**
	 * @return (-x, -y, -z, -w) and store it in dest
	 */
	public Quaternion negate(Quaternion dest) {
		if(dest==null)
			return dest = new Quaternion(-x, -y, -z, -w);
		return dest.setValues(-x, -y, -z, -w);
	}
	
	/**
	 * Returns A new Quaternion (x+quat.x, y+quat.y, z+quat.z, w+quat.w)
	 * 
	 * @param quat The Quaternion to add
	 */
	public Quaternion add(Quaternion quat) {
		return new Quaternion(x+quat.x, y+quat.y, z+quat.z, w+quat.w);
	}
	
	/**
	 * Returns A new Quaternion (x-quat.x, y-quat.y, z-quat.z, w-quat.w)
	 * 
	 * @param quat The Quaternion to subtract
	 */
	public Quaternion sub(Quaternion quat) {
		return new Quaternion(x-quat.x, y-quat.y, z-quat.z, w-quat.w);
	}
	
	/**
	 * Rotates v with this Quaternion 
	 * @param v the Vector to rotate
	 * @return the rotated Vector
	 */
	public Vector3f multiply(Vector3f v, Vector3f dest) {
		return Quaternion.multiply(this, v, dest);
	}
	
	/**
	 * Returns the length of this Quaternion
	 */
	public float length() {
		return (float)Math.sqrt(x*x+y*y+z*z+w*w);
	}
	
	/**
	 * Faster than length()
	 * 
	 * @return The length of this Quaternion squared
	 */
	public float lengthSquared() {
		return x*x+y*y+z*z+w*w;
	}
	
	/**
	 * Conjugates this quaternion (-x, -y, -z, w)
	 * @return the conjugate
	 */
	public Quaternion conjugate() {
		x = -x;
		y = -y;
		z = -z;
		w = -w;
		return this;
	}
	
	/**
	 * Calculates this Quaternion's conjugate (-x, -y, -z, w) and stores the result in dest
	 * @return the conjugate, dest
	 */
	public Quaternion conjugate(Quaternion dest) {
		if(dest==null)
			return dest = new Quaternion(-x, -y, -z, w);
		return dest.setValues(-x, -y, -z, w);
	}
	
	/**
	 * Inverts this Quaternion. Does not create a new Quaternion
	 */
	public Quaternion invert() {
		final float ls = lengthSquared();
		if(ls == 0) {
			throw new IllegalStateException("Cannont invert a Quaternion with a length of 0");
		}
		
		setValues(-x/ls, -y/ls, -z/ls, w/ls);
		return this;
	}
	
	/**
	 * @return This Quaternion divided by its length (a unit Quaternion)
	 */
	public Quaternion normalize() {
		final float l = length();
		if(l==0) {
			throw new IllegalStateException("Cannot normalize the zero Quaternion");
		}
		setValues(x/l, y/l, z/l, w/l);
		return this;
	}
	
	/**
	 * Normalizes this Quaternion and returns it as a new Quaternion
	 * @return A new Quaternion representing the Unit Quaternion of this Quaternion
	 */
	public Quaternion getUnit() {
		final float l = length();
		if(l==0) {
			throw new IllegalStateException("Cannot normalize the zero Quaternion");
		}
		return new Quaternion(x/l, y/l, z/l, w/l);
	}
	
	/**
	 * @param quat
	 * @return The dot product of this Quaternion and quat
	 */
	public float dot(Quaternion quat) {
		return x*quat.x + y*quat.y + z*quat.z + w*quat.w;
	}
	
	/**
	 * Gets the angle and axis of rotation
	 * 
	 * @param axis Stores resulting axis of rotation
	 * @return The angle of rotation in radians
	 */
	public float getAxisRotation(Vector3f axis) {
		final float l = length();
		if(l==0) {
			throw new IllegalStateException("Cannot get axis and rotation of the zero Quaternion");
		}
		
		axis.set(x, y, z);
		axis.normalise();
		return (float) Math.acos(w) * 2;
	}
	
	/**
	 * Calculates an equivalent rotation matrix as described by http://work.thaslwanter.at/Kinematics/html/04_Quaternions.html
	 * @return A rotation matrix of the same effect as this Quaternion
	 */
	public Matrix3f getMatrix3f() {
		final float xx = x*x;
		final float yy = y*y;
		final float zz = z*z;
		
		final float xy = x*y;
		final float xz = x*z;
		final float xw = x*w;
		final float yz = y*z;
		final float yw = y*w;
		final float zw = z*w;
		
		Matrix3f m = new Matrix3f();
		m.m00  = 1 - 2 * ( yy + zz );
		m.m10  =     2 * ( xy - zw );
		m.m20  =     2 * ( xz + yw );
		m.m01  =     2 * ( xy + zw );
		m.m11  = 1 - 2 * ( xx + zz );
		m.m21  =     2 * ( yz - xw );
		m.m02  =     2 * ( xz - yw );
		m.m12  =     2 * ( yz + xw );
		m.m22  = 1 - 2 * ( xx + yy );
		
		return m;
	}
	
	/**
	 * Calculates an equivalent rotation matrix
	 * @return A rotation matrix of the same effect as this Quaternion
	 */
	public Matrix4f getMatrix4f() {
		final float xx = x*x;
		final float yy = y*y;
		final float zz = z*z;
		
		final float xy = x*y;
		final float xz = x*z;
		final float xw = x*w;
		final float yz = y*z;
		final float yw = y*w;
		final float zw = z*w;
		
		Matrix4f m = new Matrix4f();
		m.setIdentity();
		m.m00  = 1 - 2 * ( yy + zz );
		m.m10  =     2 * ( xy - zw );
		m.m20  =     2 * ( xz + yw );
		m.m01  =     2 * ( xy + zw );
		m.m11  = 1 - 2 * ( xx + zz );
		m.m21  =     2 * ( yz - xw );
		m.m02  =     2 * ( xz - yw );
		m.m12  =     2 * ( yz + xw );
		m.m22  = 1 - 2 * ( xx + yy );
		
		return m;
	}
	
	/**
	 * Multiplies this Quaternion's components with value. Creates a new Quaternion.
	 * 
	 * @return The product of multiplication
	 */
	public Quaternion multiply(float value) {
		return new Quaternion(x*value, y*value, z*value, w*value);
	}
	
	/*
	 * v = (vx,vy,vz,0)
	 * q = (ux,uy,uz,s) = (u,s)
	 * q' = (-ux,-uy,-uz,s) = (-u,s) = Congugate of normalized Quaternion q
	 * qv = (v*qw + q x v, -v1·v2)
	 * qvq' = (2[u(u·v) + s(u x v)] + v[s^2 - u·u], 0)
	 */
	
	/**
	 * Multiplies a Quaternion with a Vector3f.
	 * @param q The normalized Quaternion to multiply
	 * @param vector The Vector3f to multiply
	 * @param dest Stores the result (returns a new vector if null)
	 * @return qvq' - a rotated vector
	 */
	public static Vector3f multiply(Quaternion q, Vector3f vector, Vector3f dest) {
		if(dest==null)
			dest = new Vector3f();
		final Vector3f u = q.getVector();
		final Vector3f v = new Vector3f(vector);
		
		final float s = q.w;
		final float u_dot_v = Vector3f.dot(u, v);
		final float u_dot_u = Vector3f.dot(u, u);
		final Vector3f u_cross_v = Vector3f.cross(u, v, null);
		
		u.scale(u_dot_v);
		u_cross_v.scale(s);
		Vector3f.add(u, u_cross_v, dest);
		dest.scale(2);
		
		v.scale(s*s-u_dot_u);
		Vector3f.add(v, dest, dest);
		return dest;//2[u(u·v) + s(u x v)] + v[s^2 - u·u]
	}
	
	/**
	 * q1 = (v1, w1), q2 = (v2, w2). Stores the result in dest. Multiplies in matrix order.
	 * @param dest The storage Quaternion. Creates a one if null.
	 * @param q1 Left Quaternion (second rotation)
	 * @param q2 Right Quaternion (first rotation)
	 * @return q1q2 = (v1w2 + v2w1 + v1 x v2, w1w2 - v1·v2)
	 */
	public static Quaternion multiply(Quaternion q1, Quaternion q2, Quaternion dest) {
		if(dest==null)
			dest = new Quaternion();
		Vector3f v1 = q1.getVector();
		Vector3f v2 = q2.getVector();
		final float dot = Vector3f.dot(v1, v2);
		final float w = q1.w*q2.w - dot;
		final Vector3f cross = Vector3f.cross(v1, v2, null);
		v1.scale(q2.w);
		v2.scale(q1.w);
		Vector3f.add(v1, v2, v1);
		Vector3f.add(v1, cross, v1);
		return dest.setValues(v1.x, v1.y, v1.z, w);
	}
	
	/**
	 * Adds Quaternion left to Quaternion right and stores it in dest
	 * if dest is null, a new Quaternion is created
	 */
	public static void add(Quaternion left, Quaternion right, Quaternion dest) {
		if(dest==null)
			dest = new Quaternion();
		dest.setValues(left.x+right.x, left.y+right.y, left.z+right.z, left.w+right.w);
	}
	
	/**
	 * Subtracts left from right and stores it in dest
	 * if dest is null, a new Quaternion is created
	 */
	public static void sub(Quaternion left, Quaternion right, Quaternion dest) {
		if(dest==null) {
			dest = new Quaternion();
		}
		dest.setValues(left.x-right.x, left.y-right.y, left.z-right.z, left.w-right.w);
	}
	
	/**
	 * Spherical Linear intERPolation. Produces a constant velocity and minimal torque, but is not commutative. Slow. Takes the shortest path. Creates a new Quaternion.
	 * @param i The initial rotation (normalized)
	 * @param f The final rotation (normalized)
	 * @param blend A factor representing the amount of rotation from i to f in range [0, 1]
	 * @return A Quaternion blended smoothly between i to f
	 */
	public static Quaternion slerp(Quaternion i, Quaternion f, float blend) { //http://number-none.com/product/Understanding%20Slerp,%20Then%20Not%20Using%20It/
		if (blend <= 0 || blend > 1) {
            throw new IllegalArgumentException("\"blend\" must be greater than zero and smaller than one");
        }
		
		Quaternion end = new Quaternion(f);
		final float DOT_THRESHOLD = 1-Maths.EPSILON;
		float dot = i.dot(f);
		if(dot > DOT_THRESHOLD) //avoid singularity at dot ≈ 1
			return nlerp(i, end, blend);
		if(dot < 0.0f) {//take shorter path
			dot = -dot;
			end.negate();
		}
		dot = Math.max(dot, -1); dot = Math.min(dot, 1); //clamp dot to [-1,1]
		final float theta = blend * (float) Math.acos(dot);
		Quaternion v2 = f.sub(i.multiply(dot)).normalize();
		return i.multiply((float) Math.cos(theta)).add(v2.multiply((float) Math.sin(theta)));
	}
	
	/**
	 * Normalized LERP. Fast quaternion interpolation method that is commutative and torque-minimal. Takes the shortest path. Creates a new Quaternion.
	 * @param i The initial rotation (normalized)
	 * @param f The final rotation (normalized)
	 * @param blend A factor representing the amount of rotation from i to f in range [0, 1]
	 * @return A blended quaternion between i and f
	 */
	public static Quaternion nlerp(Quaternion i, Quaternion f, float blend) {
		return Quaternion.lerp(i, f, blend).normalize();
	}
	
	/**
	 * Basic Linear intERPolation of two quaternions. Takes the shortest path. Creates a new Quaternion.
	 * @param i The initial rotation (normalized)
	 * @param f The final rotation (normalized)
	 * @param blend A factor representing the amount of rotation from i to f in range [0, 1]
	 * @return A linear interpolation of i and f of blend
	 */
	public static Quaternion lerp(Quaternion i, Quaternion f, float blend) {
		if (blend <= 0 || blend > 1) {
            throw new IllegalArgumentException("\"blend\" must be greater than zero and smaller than one");
        }
		final float dot = i.dot(f);
		Quaternion end = f;
		Quaternion out = new Quaternion();
		if(dot < 0) {//takes the shorter path
			end.negate();
			if(dot < Maths.EPSILON-1) {
				Vector3f v = i.getVector();
				Vector3f nonparallel = new Vector3f(v.y,-v.z,v.x);
				Vector3f axis = Vector3f.cross(v, nonparallel, null);
				axis.normalise();
				return out.setValues(axis, blend * (float) Math.PI);
			}
		}
		Quaternion delta = end.sub(i);
		delta = delta.multiply(blend);
		out = i.add(delta);
		return out;
	}
	
	/**
	 * Calculates a Quaternion representation of the shortest rotation from v1 to v2
	 * @return A Quaternion representing the shortest rotation from v1 to v2
	 */
	public static Quaternion rotationAsQuaternion(Vector3f v1, Vector3f v2) {
		float dot = Vector3f.dot(v1, v2);
		Vector3f cross = Vector3f.cross(v1, v2, null);
		Quaternion q = new Quaternion().setValues(cross.x, cross.y, cross.z, dot);
		float len = q.length();
		
		final float bound = (1-Maths.EPSILON) * len;
		if(dot > bound) {                //protect against (nearly) parallel vectors
			return new Quaternion();
		}else if(dot < -bound) {
			final float v2len = v2.length();
			Vector3f nonparallel = new Vector3f(v2len,0,0);
			float d = Vector3f.dot(nonparallel, v1);
			if(Math.abs(d) > bound) {
				nonparallel.x = 0;
				nonparallel.y = v2len;
			}
			Vector3f axis = Vector3f.cross(v1, nonparallel, null);
			axis.normalise();
			return new Quaternion(axis.x, axis.y, axis.z, 0); //Θ = π sin(π/2) = 1, cos(π/2) = 0
		}
		
		q.w += len;
		return q.normalize();
	}
	
	/**
	 * @return The multiplicative identity Quaternion (0,0,0,1)
	 */
	public static Quaternion identity() {
		return new Quaternion();
	}
	
	@Override
	public String toString() {
		return "(" + x + ", " + y + ", " + z + ", " + w + ")";
	}
}
