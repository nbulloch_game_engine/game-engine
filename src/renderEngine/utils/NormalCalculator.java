package renderEngine.utils;

import org.lwjgl.util.vector.Vector3f;

public class NormalCalculator {

	static Vector3f input = new Vector3f(-1,0,1);//set non-normalized input (tangent, bitangent, normal)
	
	public static void main(String[] args){
		//calculate input
		input = new Vector3f(0,0,1);
		Vector3f add = new Vector3f(0,-1,1);
		add.normalise();
		Vector3f.add(input, add, input);
		input.normalise();
		Vector3f.add(input, new Vector3f(1,1,1), input);
		
		float max = Math.max(input.x, input.y);
		max = Math.max(max, input.z);
		
		System.out.println(input + ":" + max);
		
		input.scale(255/2);
		
		System.out.println(input);
	}
	
}
