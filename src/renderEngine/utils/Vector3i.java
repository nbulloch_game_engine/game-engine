package renderEngine.utils;

import org.lwjgl.util.vector.Vector3f;

public class Vector3i {
	
	private static final Vector3f empty = new Vector3f();
	
	public int x, y, z;
	private boolean created = true;
	
	public Vector3i() {
		this(0,0,0);
		created = false;
	}
	
	public Vector3i(int x, int y, int z) {
		set(x,y,z);
	}
	
	public Vector3i(Vector3i copy) {
		set(copy);
	}
	
	public Vector3i(float x, float y, float z, float error) {
		this(fillEmpty(x,y,z), error);
	}
	
	public Vector3i(Vector3f vector, float error) {
		int xi = Math.round(vector.x);
		int yi = Math.round(vector.y);
		int zi = Math.round(vector.z);
		if(Math.abs(xi - vector.x)>error) {
			created = false;
			return;
		}if(Math.abs(yi - vector.y)>error) {
			created = false;
			return;
		}if(Math.abs(zi - vector.z)>error) {
			created = false;
			return;
		}
		set(xi, yi, zi);
	}
	
	public static Vector3i add(Vector3i left, Vector3i right, Vector3i dest) {
		if(dest==null) {
			dest = new Vector3i(left);
		}else
			dest.set(left);
		dest.x+=right.x;
		dest.y+=right.y;
		dest.z+=right.z;
		return dest;
	}
	
	public static Vector3i sub(Vector3i left, Vector3i right, Vector3i dest) {
		if(dest==null) {
			dest = new Vector3i(left);
		}else
			dest.set(left);
		dest.x-=right.x;
		dest.y-=right.y;
		dest.z-=right.z;
		return dest;
	}
	
	public static Vector3f add(Vector3f left, Vector3i right, Vector3f dest) {
		if(dest==null) {
			dest = new Vector3f(left);
		}else
			dest.set(left);
		dest.x+=right.x;
		dest.y+=right.y;
		dest.z+=right.z;
		return dest;
	}
	
	public static Vector3f sub(Vector3f left, Vector3i right, Vector3f dest) {
		if(dest==null) {
			dest = new Vector3f(left);
		}else
			dest.set(left);
		dest.x-=right.x;
		dest.y-=right.y;
		dest.z-=right.z;
		return dest;
	}
	
	public static Vector3f sub(Vector3i left, Vector3f right, Vector3f dest) {
		if(dest==null) {
			dest = new Vector3f(left.x, left.y, left.z);
		}else
			dest.set(left.x, left.y, left.z);
		dest.x-=right.x;
		dest.y-=right.y;
		dest.z-=right.z;
		return dest;
	}
	
	public static Vector3i cross(Vector3i left, Vector3i right, Vector3i dest) {
		if(dest==null) {
			dest = new Vector3i();
		}
		dest.x = left.y*right.z - left.z*right.y;
		dest.y = left.z*right.x - left.x*right.z;
		dest.z = left.x*right.y - left.y*right.x;
		return dest;
	}
	
	public int lengthSquared() {
		return x*x+y*y+z*z;
	}
	
	public float length() {
		return (float) Math.sqrt(lengthSquared());
	}
	
	public void set(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void set(Vector3i vector) {
		this.x = vector.x;
		this.y = vector.y;
		this.z = vector.z;
	}
	
	public boolean created() {
		return created;
	}
	
	private static Vector3f fillEmpty(float x, float y, float z) {
		empty.set(x, y, z);
		return empty;
	}
	
	@Override
	public String toString() {
		return "Vector3i: [" + x + ", " + y + ", " + z + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		Vector3i o = (Vector3i) obj;
		return o.x==x&&o.y==y&o.z==z;
	}
	
}
