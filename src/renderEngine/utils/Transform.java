package renderEngine.utils;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Transform {

	private Quaternion rotation;
	private Vector3f position;
	private Vector3f scale;
	
	/**
	 * Sets identity transformation
	 */
	public Transform() {
		setIdentity();
	}
	
	public Transform(Quaternion rotation, Vector3f position, Vector3f scale) {
		setValues(rotation, position, scale);
	}
	
	public Transform(Matrix4f transformation) {
		setValues(transformation);
	}
	
	public Transform(Transform copy) {
		this(copy.rotation, copy.position, copy.scale);
	}
	
	public Transform setValues(Quaternion rotation, Vector3f position, Vector3f scale) {
		this.rotation = rotation;
		this.position = position;
		this.scale = scale;
		return this;
	}
	
	public Matrix4f getMatrix() {
		Matrix4f m = new Matrix4f();
		//translate
		m.m30 = position.x;
		m.m31 = position.y;
		m.m32 = position.z;
		//rotate
		if(rotation!=null) {
			Matrix4f rotate = rotation.getMatrix4f();
			Matrix4f.mul(m, rotate, m);
		}
		//scale
		m.scale(scale);
		return m;
	}
	
	public Quaternion getQuaternion() {
		if(rotation == null)
			return Quaternion.identity();
		return rotation;
	}
	
	public Vector3f getPosition() {
		return position;
	}
	
	public Vector3f getScale() {
		return scale;
	}
	
	public void setIdentity() {
		rotation = null;
		position = new Vector3f();
		scale = new Vector3f(1,1,1);
	}
	
	/**
	 * Sets the Transform's values based on a transformation matrix <b>Super Slow</b>
	 * @param t The transformation matrix to decompose
	 */
	public void setValues(Matrix4f t) {
		position = new Vector3f(t.m30, t.m31, t.m32);
		Vector3f s_x = new Vector3f(t.m00,t.m01,t.m02);
		Vector3f s_y = new Vector3f(t.m10,t.m11,t.m12);
		Vector3f s_z = new Vector3f(t.m20,t.m21,t.m22);
		scale = new Vector3f(s_x.length(), s_y.length(), s_z.length());
		final float cosTheta = (t.m00/scale.x+t.m11/scale.y+t.m22/scale.z-1)/2;
		final float sin = (float)Math.sqrt(1-cosTheta)*Maths.SQRT_HALF;
		final float cos = (float)Math.sqrt(1+cosTheta)*Maths.SQRT_HALF;
		Vector3f axis = new Vector3f(t.m21-t.m12, t.m02-t.m20, t.m10-t.m01);
		axis.normalise();
		if(rotation==null)
			rotation = new Quaternion(axis.x*sin, axis.y*sin, axis.z*sin, cos).normalize();
		else
			rotation.setValues(axis.x*sin, axis.y*sin, axis.z*sin, cos).normalize();
	}
	
	/**
	 * Sets the quaternion without recalculating the matrix
	 * @param quaternion the new quaternion
	 */
	public void setQuaternion(Quaternion quaternion) {
		rotation = quaternion;
	}
	
	/**
	 * Sets the position without recalculating the matrix
	 * @param position the new position
	 */
	public void setPosition(Vector3f position) {
		this.position = position;
	}
	
	/**
	 * Adds delta to the position without recalculating the matrix
	 * @param delta the change in position
	 */
	public void deltaPosition(Vector3f delta) {
		Vector3f.add(delta, position, position);
	}
	
	/**
	 * Sets the scale without recalculating the matrix
	 * @param scale the new scale
	 */
	public void setScale(float scale) {
		this.scale = new Vector3f(scale, scale, scale);
	}
	
	public void multiply(Transform transform) {
		scale.x*=transform.scale.x;
		scale.y*=transform.scale.y;
		scale.z*=transform.scale.z;
		
		if(rotation == null) {
			rotation = transform.rotation;
		}else if(transform.rotation!=null) {
			Quaternion.multiply(rotation, transform.rotation, rotation);
		}
		
		position = applyTo(new Vector3f(transform.position));
	}
	
	public void rotate(Quaternion rotation) {
		if(this.rotation!=null)
			Quaternion.multiply(this.rotation, rotation, this.rotation);
		else
			this.rotation = rotation;
	}
	
	/**
	 * Transforms the vector without creating a new one
	 * @param vector the vector to transform
	 * @return the transformed vector
	 */
	public Vector3f applyTo(Vector3f vector) {
		vector.x*=scale.x;
		vector.y*=scale.y;
		vector.z*=scale.z;
		if(rotation!=null)
			rotation.multiply(vector, vector);
		Vector3f.add(vector, position, vector);
		return vector;
	}
	
	/**
	 * Multiplies two transformations in matrix order. (A then B = BA)
	 * @param left The second transform applied by the result
	 * @param right The first transform applied by the result
	 * @param dest The result of multiplication
	 * @return dest, or a new Transform if null
	 */
	public static Transform multiply(Transform left, Transform right, Transform dest) {
		if(dest==null) {
			dest = new Transform();
		}
		
		Vector3f scale = new Vector3f(left.scale);
		scale.x*=right.scale.x;
		scale.y*=right.scale.y;
		scale.z*=right.scale.z;
		
		Quaternion rotation = null;
		if(left.rotation == null) {
			rotation = right.rotation;
		}else if(right.rotation == null) {
			rotation = left.rotation;
		}else {	
			Quaternion.multiply(left.rotation, right.rotation, rotation);
		}
		
		Vector3f position = left.applyTo(new Vector3f(right.position));
		return dest.setValues(rotation, position, scale);
	}
	
	public String toString() {
		String rot = "No Rotation";
		if(rotation!=null)
			rot = rotation.toString();
		return "Position: " + position + "\nScale: " + scale + "\n" + rot;
	}
}
