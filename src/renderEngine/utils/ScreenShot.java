package renderEngine.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import renderEngine.postProcessing.Fbo;

public class ScreenShot {

	private static final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
	
	public static void render(String outputFile){
		GL11.glReadBuffer(GL11.GL_FRONT);
		int width = Display.getDisplayMode().getWidth();
		int height = Display.getDisplayMode().getHeight();
		int bpp = Display.getDisplayMode().getBitsPerPixel()/8;
		ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * bpp).order(ByteOrder.nativeOrder());
		GL11.glReadPixels(0, 0, width, height, GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, buffer);
		
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				int i = (x + (width * y)) * bpp;
		        int b = buffer.get(i) & 0xFF;
		        int g = buffer.get(i + 1) & 0xFF;
		        int r = buffer.get(i + 2) & 0xFF;
		        int a = buffer.get(i + 3) & 0xFF;
		        image.setRGB(x, height - (y + 1), (a << 24) | (r << 16) | (g << 8) | b);
			}
		}
		File file = new File(outputFile);
		try{
			File parent = file.getParentFile();
			if(!parent.exists())
				 if(!parent.mkdirs())
					 System.err.println("Failed to make screenshot parent directory.");
			ImageIO.write(image, outputFile.substring(outputFile.indexOf(".")+1), file);
			System.out.println(outputFile);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void render(){
		Date date = new Date();
		render("screenshots/screenshot" + dateFormat.format(date) + ".png");
	}
	
	public static void render(Fbo fbo, String outputFile){
		fbo.bindToRead();
		int width = fbo.getWidth();
		int height = fbo.getHeight();
		int bpp = fbo.getBytesPerPixel();
		ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * bpp).order(ByteOrder.nativeOrder());
		GL11.glReadPixels(0, 0, width, height, GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, buffer);
		fbo.unbindFrameBuffer();
		
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				int i = (x + (width * y)) * bpp;
		        int b = buffer.get(i) & 0xFF;
		        int g = buffer.get(i + 1) & 0xFF;
		        int r = buffer.get(i + 2) & 0xFF;
		        int a = buffer.get(i + 3) & 0xFF;
		        image.setRGB(x, height - (y + 1), (a << 24) | (r << 16) | (g << 8) | b);
			}
		}
		try{
			ImageIO.write(image, outputFile.substring(outputFile.indexOf(".")+1), new File(outputFile));
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void render(Fbo fbo, String outputFile, int targetWidth, int targetHeight, int xCenter, int yCenter){
		fbo.bindToRead();
		int width = fbo.getWidth();
		int height = fbo.getHeight();
		int bpp = fbo.getBytesPerPixel();
		ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * bpp).order(ByteOrder.nativeOrder());
		GL11.glReadPixels(0, 0, width, height, GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, buffer);
		fbo.unbindFrameBuffer();
		
		BufferedImage image = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
		int xOffset = (width - targetWidth)/2;
		int yOffset = (height - targetHeight)/2;
		for(int x = xOffset; x < width - xOffset; x++){
			for(int y = yOffset; y < height - yOffset; y++){
				int i = (x + xCenter + (width * (y + yCenter))) * bpp;
		        int b = buffer.get(i) & 0xFF;
		        int g = buffer.get(i + 1) & 0xFF;
		        int r = buffer.get(i + 2) & 0xFF;
		        int a = buffer.get(i + 3) & 0xFF;
		        image.setRGB(x - xOffset, height - (y + 1) - yOffset, (a << 24) | (r << 16) | (g << 8) | b);
			}
		}
		try{
			ImageIO.write(image, outputFile.substring(outputFile.indexOf(".")+1), new File(outputFile));
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
