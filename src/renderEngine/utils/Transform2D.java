package renderEngine.utils;

import org.lwjgl.util.vector.Matrix2f;
import org.lwjgl.util.vector.Vector2f;

public class Transform2D {

	private Vector2f position;
	private Vector2f scale;
	private float rotation;
	private Matrix2f transform;
	
	/**
	 * Creates a new identity Transform2D
	 */
	public Transform2D() {
		setIdentity();
	}
	
	/**
	 * Creates a new Transform2D with identical values
	 * @param copy The Transform2D to duplicate
	 */
	public Transform2D(Transform2D copy) {
		setValues(copy.transform, copy.position, copy.rotation, copy.scale);
	}
	
	/**
	 * Creates a new Transform2D to represent position, rotation and scale
	 * @param position The traslation this Transformation applies
	 * @param rotation The rotation this Transformation applies
	 * @param scale The scale this Transformation applies
	 */
	public Transform2D(Vector2f position, float rotation, Vector2f scale) {
		setValues(position, rotation, scale);
	}
	
	/**
	 * Sets all of the Transform2D values
	 * @param position The new translation
	 * @param rotation The new rotation
	 * @param scale The new scale
	 * @return This Transform2D
	 */
	public Transform2D setValues(Vector2f position, float rotation, Vector2f scale) {
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		this.transform = createMatrix(rotation, scale);
		return this;
	}
	
	/**
	 * Sets all of the Transform2D values without recalculating the matrix
	 * @param matrix The matrix of the transformation described by rotation and scale
	 * @param position The new translation
	 * @param rotation The new rotation
	 * @param scale The new scale
	 * @return This Transform2D
	 */
	public Transform2D setValues(Matrix2f matrix, Vector2f position, float rotation, Vector2f scale) {
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		this.transform = matrix;
		return this;
	}
	
	/**
	 * Set the position
	 * @param position The new position
	 */
	public void setPosition(Vector2f position) {
		this.position = position;
	}
	
	/**
	 * Adds delta to the current position
	 * @param delta The change in position
	 */
	public void changePosition(Vector2f delta) {
		Vector2f.add(position, delta, position);
	}
	
	/**
	 * Sets the rotation in radians
	 * @param rotation The rotation in radians
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
		transform = createMatrix(rotation, scale);
	}
	
	/**
	 * Adds delta to the current rotation
	 * @param delta The radian change in rotations
	 */
	public void changeRotation(float delta) {
		rotation += delta;
		transform = createMatrix(rotation, scale);
	}
	
	/**
	 * Scales the matrix by a scalar value
	 * @param scale The value that both components of {@link #scale} are multiplied by
	 */
	public void scale(float scale){
		transform.m00*=scale;
		transform.m10*=scale;
		transform.m01*=scale;
		transform.m11*=scale;
		this.scale.scale(scale);
	}
	
	/**
	 * Sets the scale
	 * @param scale The new scale
	 */
	public void setScale(Vector2f scale) {
		if(this.scale.x!=0&&this.scale.y!=0) {
			final float matrixScaleX = scale.x / this.scale.x; //cancels old scale and multiplies new scale
			final float matrixScaleY = scale.y / this.scale.y;
			transform.m00 *= matrixScaleX;
			transform.m10 *= matrixScaleX;
			transform.m01 *= matrixScaleY;
			transform.m11 *= matrixScaleY;
		} else {
			transform = createMatrix(rotation, scale);
		}
		this.scale = scale;
	}
	
	/**
	 * Convinience method for setting {@linkplain scale} to a scalar
	 * @param scale The scalar value of scale
	 */
	public void setScale(float scale) {
		setScale(new Vector2f(scale, scale));
	}
	
	/**
	 * Sets this Transform2D to the identity Transform2D: rotation = 0, scale = 1, position = (0,0)
	 */
	public void setIdentity() {
		transform = new Matrix2f();
		position = new Vector2f();
		scale = new Vector2f(1,1);
		rotation = 0;
	}
	
	/**
	 * Builds a matrix for applying rotation and scale
	 * @param rotation The rotation of the matrix
	 * @param scale The scale of the matrix
	 * @return A matrix representing the rotation and scale
	 */
	private Matrix2f createMatrix(float rotation, Vector2f scale) {
		Matrix2f m = new Matrix2f();
		if(rotation != 0) {
			final float s = (float) Math.sin(rotation);
			final float c = (float) Math.cos(rotation);
			m.m00 = c;
			m.m01 = s;
			m.m10 = -s;
			m.m11 = c;
		}
		m.m00 *= scale.x;
		m.m10 *= scale.x;
		m.m01 *= scale.y;
		m.m11 *= scale.y;
		return m;
	}
	
	public Vector2f getPosition() {
		return position;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	public Vector2f getScale() {
		return new Vector2f(scale);
	}
	
	public Matrix2f getMatrix() {
		return transform;
	}
	
	/**
	 * Transforms this Transformation by right as defined by the matrix math (this x right).
	 * @param right The Tranform2D to apply to this tranformation
	 */
	public void multiply(Transform2D right) {
		Vector2f transformedPos = new Vector2f(right.position);
		if(rotation!=0) {
			Matrix2f.transform(transform, transformedPos, transformedPos);
		}else {
			transformedPos.x*=scale.x;
			transformedPos.y*=scale.y;
		}
		Vector2f.add(transformedPos, position, position);
		Matrix2f.mul(transform, right.transform, transform);
		rotation += right.rotation;
		scale.x*=right.scale.x;
		scale.y*=right.scale.y;
	}
	
	/**
	 * Rotates, translates, and scales vector
	 * @param vector The vector to apply this tranformation to
	 */
	public void applyTo(Vector2f vector) {
		if(rotation!=0) {
			Matrix2f.transform(transform, vector, vector);
		}else {
			vector.x*=scale.x;
			vector.y*=scale.y;
		}
		Vector2f.add(vector, position, vector);
	}
	
	/**
	 * Multiplies left by right as defined by matrix multiplication (left X right) and stores the result in dest. If dest is null, a new Tranform2D is created
	 * @param left The base transform
	 * @param right The transform applied to left
	 * @param dest The result of multiplication
	 * @return dest
	 */
	public static Transform2D multiply(Transform2D left, Transform2D right, Transform2D dest) {
		if(dest == null)
			dest = new Transform2D();
		
		Vector2f position = new Vector2f(right.position);
		left.applyTo(position);
		float rotation = left.rotation + right.rotation;
		Vector2f scale = new Vector2f(left.scale);
		scale.x *=right.scale.x;
		scale.y *=right.scale.y;
		Matrix2f matrix = Matrix2f.mul(left.transform, right.transform, null);
		return dest.setValues(matrix, position, rotation, scale);
	}
	
	@Override
	public String toString() {
		return "Position: " + position + "\n" + 
				"Rotation: " + rotation + "\n" + 
				"Scale: " + scale;
	}
}
