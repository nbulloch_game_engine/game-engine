package renderEngine.utils;

public class NLERP extends LERP {

	public NLERP(Quaternion initial, Quaternion target, float duration) {
		super(initial, target, duration);
	}
	
	public NLERP() {
		super();
	}
	
	@Override
	protected Quaternion interpolate(Quaternion initial, Quaternion target, float blend){
		return super.interpolate(initial, target, blend).normalize();
	}
}
