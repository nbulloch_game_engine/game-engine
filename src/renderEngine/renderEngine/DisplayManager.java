package renderEngine.renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.PixelFormat;
import org.newdawn.slick.opengl.ImageIOImageData;
import org.lwjgl.opengl.ContextAttribs;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

public class DisplayManager {
	
	//private static final int FPS_CAP = 60;
	private static DisplayMode mode;
	
	private static long lastFrameTime;
	private static float delta;
	
	private static boolean fullscreen = false;
	
	public static void createDisplay(){
		
		ContextAttribs attribs = new ContextAttribs(3, 3)
				.withForwardCompatible(true)
				.withProfileCore(true);
		
		try {
			DisplayMode[] modes = Display.getAvailableDisplayModes();
			 
			for (int i=0;i<modes.length;i++) {
				DisplayMode current = modes[i];
			    System.out.println(current.getWidth() + "x" + current.getHeight() + "x" +
			                        current.getBitsPerPixel() + " " + current.getFrequency() + "Hz");
			}
			//mode = modes[fullscreen?17:1];
			mode = modes[2];
			System.out.println(mode);
			Display.setDisplayMode(mode);
			if(fullscreen)
				Display.setFullscreen(mode.isFullscreenCapable());
			Display.create(new PixelFormat().withDepthBits(24), attribs);
			Display.setTitle("RoboTech");
			Mouse.setNativeCursor(null);
			Mouse.setGrabbed(true);
			glEnable(GL_MULTISAMPLE);
		} catch (LWJGLException  e) {
			e.printStackTrace();
		}
		
		lastFrameTime = System.nanoTime();
		
		if(!GLContext.getCapabilities().OpenGL32)
			System.err.println("OpenGL 3.2 Not Supported");
	}
	
	public static void updateDisplay(){
		//Display.sync(60);
		Display.setVSyncEnabled(true);
		Display.update();
		long currentFrameTime = System.nanoTime();
		delta = (currentFrameTime - lastFrameTime);
		lastFrameTime = System.nanoTime();
	}
	
	public static float getFrameTimeNanos(){
		return delta;
	}
	
	public static float getFrameTimeSeconds(){
		return delta/1000000000f;
	}
	
	public static void closeDisplay(){
		Display.destroy();
	}
	
	public static float getAspectRatio(){
		return (float)Display.getWidth() / (float)Display.getHeight();
	}

	public static void setIcon(String[] icons) {;
		try {
			ByteBuffer[] images = new ByteBuffer[icons.length];
			for(int i = 0; i < icons.length; i++){
				images[i] = new ImageIOImageData().imageToByteBuffer(ImageIO.read(new File("res/" + icons[i])), false, false, null);
			}
			Display.setIcon(images);
		}catch(IOException e){
			System.out.println("Coundn't load icon");
		}
	}
}
