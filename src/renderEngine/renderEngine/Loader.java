package renderEngine.renderEngine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.GL_TEXTURE_LOD_BIAS;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL33.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GLContext;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import renderEngine.models.RawModel;
import renderEngine.textures.TextureData;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class Loader {
	
	private static ArrayList<Integer> vaos = new ArrayList<Integer>();
	private static ArrayList<Integer> vbos = new ArrayList<Integer>();
	private static ArrayList<Integer> textures = new ArrayList<Integer>();
	private static RawModel quad;
	
	public static void init(){
		float[] positions = {1,-1, 1,1, -1,-1, -1,1};
		float[] uv = {1,0, 1,1, 0,0, 0,1};
		Loader.quad = loadToVAO(positions, uv);
	}

	public static RawModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, int[] indices){
		int vaoID = createVAO();
		int[] vboIDs = new int[3];
		bindIndicesBuffer(indices);
		vboIDs[0] = storeDataInAttributeList(0, 3, positions);
		vboIDs[1] = storeDataInAttributeList(1, 2, textureCoords);
		vboIDs[2] = storeDataInAttributeList(2, 3, normals);
		unbindVAO();
		return new RawModel(vaoID, vboIDs, indices.length);
	}
	
	public static RawModel loadToVAO(float[] positions){
		int vaoID = createVAO();
		int[] vboIDs = new int[1];
		vboIDs[0] = storeDataInAttributeList(0, 3, positions);
		unbindVAO();
		return new RawModel(vaoID, vboIDs, positions.length);
	}
	
	public static RawModel loadToVAO(float[] positions, float[] textureCoords){
		int vaoID = createVAO();
		int[] vboIDs = new int[2];
		vboIDs[0] = storeDataInAttributeList(0, 2, positions);
		vboIDs[1] = storeDataInAttributeList(1, 2, textureCoords);
		unbindVAO();
		return new RawModel(vaoID, vboIDs, positions.length);
	}
	
	public static RawModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, float[] tangents, int[] indices){
		int vaoID = createVAO();
		int[] vboIDs = new int[4];
		bindIndicesBuffer(indices);
		vboIDs[0] = storeDataInAttributeList(0, 3, positions);
		vboIDs[1] = storeDataInAttributeList(1, 2, textureCoords);
		vboIDs[2] = storeDataInAttributeList(2, 3, normals);
		vboIDs[3] = storeDataInAttributeList(3, 3, tangents);
		unbindVAO();
		return new RawModel(vaoID, vboIDs, indices.length);
	}
	
	public static RawModel loadToVAO(float[] positions, int dimensions){
		int vaoId = createVAO();
		int[] vboIDs = new int[1];
		vboIDs[0] = storeDataInAttributeList(0, dimensions, positions);
		unbindVAO();
		return new RawModel(vaoId, vboIDs, positions.length);
	}
	
	public static void loadToVAO(float[] positions, int dimensions, int[] id) {
		int vaoId = createVAO();
		id[0] = vaoId;
		int[] vboIDs = new int[1];
		vboIDs[0] = storeDataInAttributeList(0, dimensions, positions);
		id[1] = vboIDs[0];
		unbindVAO();
	}
	
	public static int loadToDebugVAO(float[] positions, float[] direction1, float[] direction2, float[] direction3, float[] color1, float[] color2, float[] color3){
		int vaoID = createVAO();
		storeDataInAttributeList(0, 3, positions);
		storeDataInAttributeList(1, 3, direction1);
		storeDataInAttributeList(2, 3, direction2);
		storeDataInAttributeList(3, 3, direction3);
		storeDataInAttributeList(4, 3, color1);
		storeDataInAttributeList(5, 3, color2);
		storeDataInAttributeList(6, 3, color3);
		unbindVAO();
		return vaoID;
	}
	
	public static int createEmptyVbo(int floatCount){
		int vbo = glGenBuffers();
		vbos.add(vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, floatCount * 4, GL_STREAM_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		return vbo;
	}
	
	public static void addInstancedAttribute(int vao, int vbo, int attribute, int dataSize, int instancedDataLength, int offset){
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindVertexArray(vao);
		glVertexAttribPointer(attribute, dataSize, GL_FLOAT, false, instancedDataLength * 4, offset * 4);
		glVertexAttribDivisor(attribute, 1);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	
	public static void updateVbo(int vbo, float[] data, FloatBuffer buffer){
		buffer.clear();
		buffer.put(data);
		buffer.flip();
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, buffer.capacity() * 4, GL_STREAM_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0 , buffer);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public static int loadTexture(String fileName){
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture(fileName.substring(fileName.length()-3).toUpperCase(), new FileInputStream("res/"+fileName));
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0f);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			if(GLContext.getCapabilities().GL_EXT_texture_filter_anisotropic){
				float amount = Math.min(4f, glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT));
				glTexParameterf(GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
			}else{
				System.out.println("Anisotropic Filtering Not Supported");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		int textureID = texture.getTextureID();
		textures.add(textureID);
		return textureID;
	}
	
	public static int loadTextureUnclamped(String fileName){
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture(fileName.substring(fileName.length()-3).toUpperCase(), new FileInputStream("res/"+fileName));
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0f);
			if(GLContext.getCapabilities().GL_EXT_texture_filter_anisotropic){
				float amount = Math.min(4f, glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT));
				glTexParameterf(GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
			}else{
				System.out.println("Anisotropic Filtering Not Supported");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		int textureID = texture.getTextureID();
		textures.add(textureID);
		return textureID;
	}
	
	public static int loadFontTextureAtlas(String fileName){
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("res/"+fileName+".png"));
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int textureID = texture.getTextureID();
		textures.add(textureID);
		return textureID;
	}
	
	public static void cleanModel(RawModel model){
		glDeleteVertexArrays(model.getVaoID());
		for(int id : model.getVboIDs()){
			glDeleteBuffers(id);
		}
	}
	
	public static void cleanTexture(int id){
		glDeleteTextures(id);
	}
	
	public static void clean(){
		for(int vao : vaos){
			glDeleteVertexArrays(vao);
		}
		for(int vbo : vbos){
			glDeleteBuffers(vbo);
		}
		for(int texture : textures){
			glDeleteTextures(texture);
		}
	}
	
	public static int loadCubeMap(String[] textureFiles){
		int texID = glGenTextures();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texID);
		for(int i = 0;i<6;i++){
			TextureData data = decodeTextureFile("res/" + textureFiles[i] + ".png");
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, data.getWidth(), data.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data.getBuffer());
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		textures.add(texID);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		return texID;
	}
	
	private static TextureData decodeTextureFile(String fileName) {
		int width = 0;
		int height = 0;
		ByteBuffer buffer = null;
		try {
			FileInputStream in = new FileInputStream(fileName);
			PNGDecoder decoder = new PNGDecoder(in);
			width = decoder.getWidth();
			height = decoder.getHeight();
			buffer = ByteBuffer.allocateDirect(4 * width * height);
			decoder.decode(buffer, width * 4, Format.RGBA);
			buffer.flip();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Tried to load texture " + fileName + ", didn't work");
			System.exit(-1);
		}
		return new TextureData(buffer, width, height);
	}
	
	private static int createVAO(){
		int vaoID = glGenVertexArrays();
		vaos.add(vaoID);
		glBindVertexArray(vaoID);
		return vaoID;
	}
	
	private static int storeDataInAttributeList(int attributeNumber, int coordinateSize, float[] data){
		int vboID = glGenBuffers();
		vbos.add(vboID);
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		return vboID;
	}
	
	private static void bindIndicesBuffer(int[] indices){
		int vboID = glGenBuffers();
		vbos.add(vboID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
	}
	
	private static IntBuffer storeDataInIntBuffer(int[] data){
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	private static void unbindVAO(){
		glBindVertexArray(0);
	}
	
	private static FloatBuffer storeDataInFloatBuffer(float[] data){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static RawModel getQuad(){
		return quad;
	}
}
