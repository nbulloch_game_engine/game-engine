package renderEngine.renderEngine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.entities.Scene;
import renderEngine.shaders.TerrainShader;
import renderEngine.shadows.cascadedShadowMap.CsmMasterRenderer;
import renderEngine.shadows.shadowMap.ShadowBox;
import renderEngine.terrain.Terrain;
import renderEngine.textures.TerrainTexturePack;
import renderEngine.utils.Maths;

public class TerrainRenderer {
	
	private TerrainShader shader;
	
	public TerrainRenderer(Matrix4f projectionMatrix){
		shader = new TerrainShader();
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.connectTextureUnits();
		shader.stop();
	}
	
	public void render(ArrayList<Terrain> terrains, Matrix4f toShadowSpace, Scene scene, CsmMasterRenderer shadowRenderer){
		shader.start();
		shader.loadClipPlane(scene.getClipPlane());
		shader.loadSkyColor(scene.getSkyColor());
		shader.loadLights(scene.getLights(), scene.getDLights());
		shader.loadViewMatrix(scene.getCamera());
		shader.loadToShadowSpace(toShadowSpace);
		shader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
		shader.loadFogValues(scene.getFogDensity(), scene.getFogGradient());
		shader.loadHasShadows(scene.hasShadows());
		if(scene.hasShadows()){
			if(scene.hasCsmShadows()){
				shader.loadToCSMSpaces(shadowRenderer.getToShadowSpaces());
				shader.loadCSMDepths(shadowRenderer.getDepths());
				shader.loadCSMSizes(shadowRenderer.getCascadeSizes());
			}else{
				shader.loadToShadowSpace(toShadowSpace);
				shader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
			}
		}
		for(Terrain terrain: terrains){
			shader.loadTiles(terrain.getTiles());
			prepareTerrain(terrain);
			loadModelMatrix(terrain);
			glDrawElements(GL_TRIANGLES, terrain.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
			unbindTexturedModel();
		}
		shader.stop();
	}
	
	private void prepareTerrain(Terrain terrain){
		glBindVertexArray(terrain.getModel().getVaoID());
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		bindTextures(terrain);
		shader.loadShineVariables(1, 0);
	}
	
	private void bindTextures(Terrain terrain){
		TerrainTexturePack texturePack = terrain.getTexturePack();
		boolean texPack = terrain.usesTexturePack();
		shader.loadBooleans(texPack);
		if(terrain.getTexture().hasTransparency()) MasterRenderer.disableCulling();
		if(texPack){
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texturePack.getBackgroundTexture().getTextureID());
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, texturePack.getrTexture().getTextureID());
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, texturePack.getgTexture().getTextureID());
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, texturePack.getbTexture().getTextureID());
			glActiveTexture(GL_TEXTURE4);
			glBindTexture(GL_TEXTURE_2D, terrain.getBlendMap().getTextureID());
		}else{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, terrain.getTexture().getTextureID());
		}
	}
	
	private void unbindTexturedModel(){
		MasterRenderer.enableCulling();
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glBindVertexArray(0);
	}
	
	private void loadModelMatrix(Terrain terrain){
		Matrix4f transformationMatrix = Maths.createTransformationMatrix(new Vector3f(terrain.getX(), 0, terrain.getZ()),new Vector3f(0,0,0), new Vector3f(1, 1, 1));
		shader.loadTransformationMatrix(transformationMatrix);
	}
	
	public void clean(){
		shader.clean();
	}
}
