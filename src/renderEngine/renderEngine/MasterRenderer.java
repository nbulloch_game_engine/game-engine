package renderEngine.renderEngine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;

import renderEngine.debug.DebugLineRenderer;
import renderEngine.debug.DebugLineShader;
import renderEngine.debug.DebugVectorRenderer;
import renderEngine.debug.DebugVectorShader;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.Entity;
import renderEngine.entities.Scene;
import renderEngine.models.TexturedModel;
import renderEngine.normalMappingRenderer.NormalMappingRenderer;
import renderEngine.shadows.cascadedShadowMap.CsmMasterRenderer;
import renderEngine.shadows.shadowMap.ShadowMapMasterRenderer;
import renderEngine.skybox.SkyboxRenderer;
import renderEngine.terrain.Terrain;

public class MasterRenderer {

	public static final int MAX_CASCADES = 5;
	
	private Matrix4f projectionMatrix;
	
	private EntityRenderer renderer;
	private TerrainRenderer terrainRenderer;
	private NormalMappingRenderer normalMapRenderer;
	private SkyboxRenderer skyboxRenderer;
	private ShadowMapMasterRenderer shadowMapRenderer;
	public CsmMasterRenderer csmRenderer;
	private DebugVectorRenderer debugVectorRenderer;
	private DebugLineRenderer debugLineRenderer;
	
	private DebugVectorShader debugVectorShader = new DebugVectorShader();
	private DebugLineShader debugLineShader = new DebugLineShader();
	private boolean debugMode = true;
	
	private HashMap<TexturedModel, ArrayList<Entity>> entities = new HashMap<TexturedModel, ArrayList<Entity>>();
	private HashMap<TexturedModel, ArrayList<Entity>> translucentEntities = new HashMap<TexturedModel, ArrayList<Entity>>();
	private HashMap<TexturedModel, ArrayList<Entity>> normalMapEntities = new HashMap<TexturedModel, ArrayList<Entity>>();
	private HashMap<TexturedModel, ArrayList<Entity>> translucentNormalMapEntities = new HashMap<TexturedModel, ArrayList<Entity>>();
	private ArrayList<Terrain> terrains = new ArrayList<Terrain>();
	
	private Scene scene;
	
	public MasterRenderer(Scene scene){
		this.scene = scene;
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		createProjectionMatrix();
		renderer = new EntityRenderer(projectionMatrix);
		terrainRenderer = new TerrainRenderer(projectionMatrix);
		normalMapRenderer = new NormalMappingRenderer(projectionMatrix);
		shadowMapRenderer = new ShadowMapMasterRenderer(scene.getCamera(), scene);
		csmRenderer = new CsmMasterRenderer(scene);
		debugVectorRenderer = new DebugVectorRenderer(debugVectorShader);
		debugLineRenderer = new DebugLineRenderer(debugLineShader);
		if(scene.skybox()){
			if(scene.getSkyboxPosition()==null){
				skyboxRenderer = new SkyboxRenderer(projectionMatrix);
			}else{
				skyboxRenderer = new SkyboxRenderer(projectionMatrix, scene.getSkyboxPosition());
			}
		}
	}
	
	public void updateCsm(){
		csmRenderer.update();
	}
	
	public Matrix4f getProjectionMatrix(){
		return projectionMatrix;
	}
	
	public static void enableCulling(){
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	
	public static void disableCulling(){
		glDisable(GL_CULL_FACE);
	}
	
	public void renderScene(ArrayList<Entity> entities, ArrayList<Entity> normalEntities, ArrayList<Terrain> terrains){
		for(Terrain terrain: terrains){
			processTerrain(terrain);
		}for(Entity entity: entities){
			processEntity(entity);
		}for(Entity entity: normalEntities){
			processNormalMapEntity(entity);
		}
		render();
	}
	
	public void render(){
		prepare();
		if(scene.skybox())
			skyboxRenderer.render(scene);
		renderer.render(entities, scene, shadowMapRenderer.getToShadowMapSpaceMatrix(), csmRenderer);
		normalMapRenderer.render(normalMapEntities, scene, shadowMapRenderer.getToShadowMapSpaceMatrix(), csmRenderer);
		terrainRenderer.render(terrains, shadowMapRenderer.getToShadowMapSpaceMatrix(), scene, csmRenderer);
		if(debugMode) {
			debugVectorRenderer.render(scene.getCamera(), projectionMatrix);
			debugLineRenderer.render(scene.getCamera(), projectionMatrix);
		}
		renderer.renderTranslucent(translucentEntities, scene, shadowMapRenderer.getToShadowMapSpaceMatrix(), csmRenderer);
		normalMapRenderer.renderTranslucent(translucentNormalMapEntities, scene, shadowMapRenderer.getToShadowMapSpaceMatrix(), csmRenderer);
		
		terrains.clear();
		entities.clear();
		translucentEntities.clear();
		normalMapEntities.clear();
		translucentNormalMapEntities.clear();
	}
	
	public void processTerrain(Terrain terrain){
		terrains.add(terrain);
	}
	
	public void processEntity(Entity entity){
		TexturedModel entityModel = entity.getTexturedModel();
		if(entityModel.isTranslucent() || entity.getTranslucency() != 1){
			ArrayList<Entity> batch = translucentEntities.get(entityModel);
			if(batch!=null){
				batch.add(entity);
			}else{
				ArrayList<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				translucentEntities.put(entityModel, newBatch);
			}
		}else{
			ArrayList<Entity> batch = entities.get(entityModel);
			if(batch!=null){
				batch.add(entity);
			}else{
				ArrayList<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				entities.put(entityModel, newBatch);
			}
		}
	}
	
	public void processNormalMapEntity(Entity entity){
		TexturedModel entityModel = entity.getTexturedModel();
		if(entityModel.isTranslucent() || entity.getTranslucency() != 1){
			ArrayList<Entity> batch = translucentNormalMapEntities.get(entityModel);
			if(batch!=null){
				batch.add(entity);
			}else{
				ArrayList<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				translucentNormalMapEntities.put(entityModel, newBatch);
			}
		}else{
			ArrayList<Entity> batch = normalMapEntities.get(entityModel);
			if(batch!=null){
				batch.add(entity);
			}else{
				ArrayList<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				normalMapEntities.put(entityModel, newBatch);
			}
		}
	}
	
	public void processShadowEntity(Entity entity){
		TexturedModel entityModel = entity.getTexturedModel();
		if(entity.getTranslucency() == 1){
			ArrayList<Entity> batch = entities.get(entityModel);
			if(batch!=null){
				batch.add(entity);
			}else{
				ArrayList<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				entities.put(entityModel, newBatch);
			}
		}
	}
	
	public void renderShadowMap(ArrayList<Entity> entityList, ArrayList<Entity> normalEntities, DirectionalLight sun){
		if(scene.hasShadows()){
			for(Entity entity : entityList){
				if(entity.getTexturedModel().castsShadows())
					processShadowEntity(entity);
			}
			for(Entity entity : normalEntities){
				if(entity.getTexturedModel().castsShadows())
					processShadowEntity(entity);
			}
			if(scene.hasCsmShadows()){
				csmRenderer.render(entities, sun);
			}else{
				shadowMapRenderer.render(entities, sun);
			}
			entities.clear();
		}
	}
	
	public int getShadowMapTexture(){
		return shadowMapRenderer.getShadowMap();
	}
	
	public int[] getCsmTextures(){
		if(scene.hasCsmShadows()){
			return csmRenderer.getShadowMaps();
		}return null;
	}
	
	public void clean(){
		renderer.clean();
		terrainRenderer.clean();
		normalMapRenderer.clean();
		shadowMapRenderer.clean();
	}
	
	public void prepare() {
		glEnable(GL_DEPTH_TEST);
		glClearColor(0f, 0f, 0f, 0);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, getShadowMapTexture());
		if(scene.hasCsmShadows()){
			for(int i = 0; i < getCsmTextures().length; i++){
				glActiveTexture(GL_TEXTURE6+i);
				glBindTexture(GL_TEXTURE_2D, getCsmTextures()[i]);
			}
		}
	}
	
	private void createProjectionMatrix(){
    	projectionMatrix = new Matrix4f();
    	float near = scene.getNearPlane();
    	float far = scene.getFarPlane();
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(scene.getFOV() / 2f))));
		float x_scale = y_scale / aspectRatio;
		float frustum_length = far - near;

		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((far + near) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * near * far) / frustum_length);
		projectionMatrix.m33 = 0;
    }
	
	public void toggleDebugMode(){
		debugMode = !debugMode;
	}
}
