package renderEngine.renderEngine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.entities.Entity;
import renderEngine.entities.Scene;
import renderEngine.models.RawModel;
import renderEngine.models.TexturedModel;
import renderEngine.shaders.StaticShader;
import renderEngine.shadows.cascadedShadowMap.CsmMasterRenderer;
import renderEngine.shadows.shadowMap.ShadowBox;
import renderEngine.textures.ModelTexture;

public class EntityRenderer {
	
	private StaticShader shader;
	private float offset = 0;
	
	public EntityRenderer(Matrix4f projectionMatrix){
		shader = new StaticShader();
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.connectTextureUnits();
		shader.stop();
	}

	public void render(HashMap<TexturedModel, ArrayList<Entity>> entities, Scene scene, Matrix4f toShadowSpace, CsmMasterRenderer shadowRenderer){
		shader.start();
		shader.loadClipPlane(scene.getClipPlane());
		shader.loadSkyColor(scene.getSkyColor());
		shader.loadLights(scene.getLights(), scene.getDLights());
		shader.loadViewMatrix(scene.getCamera());
		shader.loadNumLights(scene.getLights().size());
		shader.loadNumDLights(scene.getDLights().size());
		offset++;
		offset%=5000;
		shader.setOffset(offset/5000);
		shader.loadFogValues(scene.getFogDensity(), scene.getFogGradient());
		if(scene.hasShadows()){
			if(scene.hasCsmShadows()){
				shader.loadToCSMSpaces(shadowRenderer.getToShadowSpaces());
				shader.loadCSMDepths(shadowRenderer.getDepths());
				shader.loadCSMSizes(shadowRenderer.getCascadeSizes());
			}else{
				shader.loadToShadowSpace(toShadowSpace);
				shader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
			}
		}
		for(TexturedModel model:entities.keySet()){
			prepareTexturedModel(model, scene.hasShadows());
			ArrayList<Entity> batch = entities.get(model);
			for(Entity entity:batch){
				prepareInstance(entity);
				glDrawElements(GL_TRIANGLES, model.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
			}
			unbindTexturedModel();
		}
		shader.stop();
	}
	
	public void renderTranslucent(HashMap<TexturedModel, ArrayList<Entity>> translucentEntities, Scene scene, Matrix4f toShadowSpace, CsmMasterRenderer shadowRenderer){
		shader.start();
		shader.loadClipPlane(scene.getClipPlane());
		shader.loadSkyColor(scene.getSkyColor());
		shader.loadLights(scene.getLights(), scene.getDLights());
		shader.loadViewMatrix(scene.getCamera());
		shader.loadNumLights(scene.getLights().size());
		shader.loadNumDLights(scene.getDLights().size());
		offset++;
		offset%=5000;
		shader.setOffset(offset/5000);
		shader.loadFogValues(scene.getFogDensity(), scene.getFogGradient());
		if(scene.hasShadows()){
			if(scene.hasCsmShadows()){
				shader.loadToCSMSpaces(shadowRenderer.getToShadowSpaces());
				shader.loadCSMDepths(shadowRenderer.getDepths());
				shader.loadCSMSizes(shadowRenderer.getCascadeSizes());
			}else{
				shader.loadToShadowSpace(toShadowSpace);
				shader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
			}
		}
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		ArrayList<TexturedModel> orderedModels = new ArrayList<TexturedModel>();//assumes point models @ position
		ArrayList<Entity> orderedEntities = new ArrayList<Entity>();
		for(TexturedModel model : translucentEntities.keySet()){
			ArrayList<Entity> entities = translucentEntities.get(model);
			for(Entity entity : entities){
				int index = 0;
				float dPos = Vector3f.sub(entity.getPosition(), scene.getCamera().getPosition(), null).lengthSquared();
				for(int i = 0; i < orderedEntities.size(); i++){
					Entity entityAdded = orderedEntities.get(i);
					float dPosAdded = Vector3f.sub(entityAdded.getPosition(), scene.getCamera().getPosition(), null).lengthSquared();
					if(dPos>dPosAdded){
						break;
					}
					index = i + 1;
				}
				orderedModels.add(index, model);
				orderedEntities.add(index, entity);
			}
			
		}
		TexturedModel last = null;
		for(int i = 0; i < orderedModels.size(); i++){
			TexturedModel model = orderedModels.get(i);
			if(!model.equals(last)){
				prepareTexturedModel(model, scene.hasShadows());
			}
			Entity entity = orderedEntities.get(i);
			prepareInstance(entity);
			glDrawElements(GL_TRIANGLES, model.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
			if(!(i+1 < orderedModels.size()) || !model.equals(orderedModels.get(i+1)))
				unbindTexturedModel();
			last = model;
		}
		glDisable(GL_BLEND);
		shader.stop();
	}
	
	private void prepareTexturedModel(TexturedModel model, boolean shadows){
		RawModel rawModel = model.getModel();
		glBindVertexArray(rawModel.getVaoID());
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		ModelTexture texture = model.getTexture();
		shader.loadNumberOfRows(texture.getNumberOfRows());
		if(texture.hasTransparency()) MasterRenderer.disableCulling();
		shader.loadFakeLightingVariable(texture.usingFakeLighting());
		shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, model.getTexture().getID());
		shader.loadUsesSpecularMap(texture.hasSpecularMap());
		if(texture.hasSpecularMap()){
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, texture.getSpecularMap());
		}
		shader.loadUsesLightMap(texture.hasLightMap());
		if(texture.hasLightMap()){
			
			shader.loadBloomWidth(texture.getBloomWidth());
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, texture.getLightMap());
			shader.loadUseGlowMap(texture.useGlowMap());
			if(texture.useGlowMap()){
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, texture.getGlowMap());
				shader.loadGlowMapFactor(texture.getGlowMapFactor());
				shader.loadGlowSpeed(texture.getGlowSpeed());
			}
		}
		shader.loadHasShadows(model.hasShadows()&&shadows);
		shader.loadTiles(model.getTiles());
		shader.loadIsTranslucent(model.isTranslucent());
	}
	
	private void unbindTexturedModel(){
		MasterRenderer.enableCulling();
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glBindVertexArray(0);
	}
	
	private void prepareInstance(Entity entity){
		GL11.glDisable(GL11.GL_CULL_FACE);
		shader.loadTransformationMatrix(entity.getTransformation());
		shader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset());
		shader.loadTranslucency(entity.getTranslucency());
	}
	
	public void clean(){
		shader.clean();
	}
}