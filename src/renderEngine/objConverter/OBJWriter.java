package renderEngine.objConverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.lwjgl.util.vector.Vector3f;

import physicsEngine.collisionDetection.primitives.Triangle;

public class OBJWriter {

	/**
	 * Writes the triangles to an .obj file with all uv's at (.5,.5)
	 * @param obj - triangles to write
	 * @param file - location to write to
	 */
	public static void write(Triangle[] obj, String file) {
		ArrayList<Vector3f> v = new ArrayList<Vector3f>();
		ArrayList<Integer> i_v = new ArrayList<Integer>();
		ArrayList<Vector3f> n = new ArrayList<Vector3f>();
		ArrayList<Integer> i_n = new ArrayList<Integer>();
		for(int i = 0; i < obj.length; i++) {
			Triangle t = obj[i];
			processVertex(t.v0, v, i_v);
			processVertex(t.v1, v, i_v);
			processVertex(t.v2, v, i_v);
			if(n.contains(t.p.n)) {
				i_n.add(n.indexOf(t.p.n)+1);
			}else {
				n.add(t.p.n);
				i_n.add(n.size());
			}
		}
		try {
			File f = new File(OBJLoader.RES_LOC + file + ".obj");
			if(!f.exists())
			f.createNewFile();
			FileWriter fw;
			fw = new FileWriter(f);
			BufferedWriter writer = new BufferedWriter(fw);
			for(Vector3f vec : v) {
				writer.append("v " + vec.x + " " + vec.y + " " + vec.z + "\n");
			}
			writer.append("vt 0.5 0.5 0.5\n");
			for(Vector3f norm : n) {
				writer.append("vn " + norm.x + " " + norm.y + " " + norm.z + "\n");
			}for(int i = 0; i < i_n.size(); i++) {
				int nInd = i_n.get(i);
				writer.append("f " + i_v.get(i*3) + "/1/" + nInd + " "
						+ i_v.get(i*3+1) + "/1/" + nInd + " "
						+ i_v.get(i*3+2) + "/1/" + nInd + "\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void processVertex(Vector3f p, ArrayList<Vector3f> v, ArrayList<Integer> i_v) {
		if(!v.contains(p)) {
			v.add(p);
			i_v.add(v.size());
		}else {
			i_v.add(v.indexOf(p)+1);
		}
	}
	
}
