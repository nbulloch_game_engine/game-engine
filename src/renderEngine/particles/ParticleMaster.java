package renderEngine.particles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.lwjgl.util.vector.Matrix4f;

import renderEngine.entities.Camera;

public class ParticleMaster {
	
	private static HashMap<ParticleTexture, ArrayList<Particle>> particles = new HashMap<ParticleTexture, ArrayList<Particle>>();
	private static ParticleRenderer renderer;
	
	public static void init(Matrix4f projectionMatrix){
		renderer = new ParticleRenderer(projectionMatrix);
	}
	
	public static void update(Camera camera){
		Iterator<Entry<ParticleTexture, ArrayList<Particle>>> mapIterator = particles.entrySet().iterator();
		while(mapIterator.hasNext()){
			Entry<ParticleTexture, ArrayList<Particle>> enrty = mapIterator.next();
			ArrayList<Particle> list = enrty.getValue();
			Iterator<Particle> iterator = list.iterator();
			while(iterator.hasNext()){
				Particle p = iterator.next();
				boolean stillAlive = p.update(camera);
				if(!stillAlive){
					iterator.remove();
					if(list.isEmpty()){
						mapIterator.remove();
					}
				}
			}
			if(!enrty.getKey().usesAdditiveBlending()){
				InsertionSort.sortHighToLow(list);
			}
		}
	}
	
	public static void renderParticles(Camera camera){
		renderer.render(particles, camera);
	}
	
	public static void clean(){
		renderer.clean();
	}
	
	public static void addParticle(Particle particle){
		ArrayList<Particle> list = particles.get(particle.getTexture());
		if(list==null){
			list = new ArrayList<Particle>();
			particles.put(particle.getTexture(), list);
		}
		list.add(particle);
	}
	
}
