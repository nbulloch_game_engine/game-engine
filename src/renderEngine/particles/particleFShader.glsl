#version 130//check version

in vec2 textureCoords1;
in vec2 textureCoords2;
in float blend;

out vec4 out_color;
out vec4 out_bloomColor;

uniform sampler2D particleTexture;

void main(void){
	
	vec4 color1 = texture(particleTexture, textureCoords1);
	vec4 color2 = texture(particleTexture, textureCoords2);
	
	out_color = mix(color1, color2, blend);
	out_bloomColor = vec4(0);
}