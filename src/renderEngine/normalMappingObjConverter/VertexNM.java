package renderEngine.normalMappingObjConverter;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class VertexNM {
	
	private Vector3f position;
	private Vector2f texture;
	private Vector3f normal;
	private Vector3f tangent;
	
	protected VertexNM(Vector3f position, Vector2f texture, Vector3f normal){
		this.position = position;
		this.texture = texture;
		this.normal = normal;
	}
	
	protected VertexNM(VertexNM vert){
		this.position = vert.getPosition();
		this.texture = vert.getTexture();
		this.normal = vert.getNormal();
		if(vert.getTangent()!=null){
			this.tangent = vert.getTangent();
		}
	}
	
	protected void setTangent(Vector3f tangent){
		this.tangent = tangent;
	}

	protected Vector3f getPosition() {
		return position;
	}

	protected Vector2f getTexture() {
		return texture;
	}

	protected Vector3f getNormal() {
		return normal;
	}
	
	protected Vector3f getTangent() {
		return tangent;
	}
	
	public boolean isUsed(){
		return tangent!=null;
	}
	
	public String toString(){
		String output = "position: " + position + "\n";
		output += "uv: " + texture + "\n";
		output += "normal: " + normal + "\n";
		output += "tangent: " + tangent + "\n";
		if(tangent!=null)
			output += "bitangent: " + Vector3f.cross(normal, tangent, null);
		return output;
	}
	
	@Override
	public boolean equals(Object other){
		if((other == null) || (getClass() != other.getClass())){
			return false;
	    }
		VertexNM vert = (VertexNM) other;
		if(position.equals(vert.getPosition()) && texture.equals(vert.getTexture()) && normal.equals(vert.getNormal()) && tangent.equals(vert.getTangent())){
			return true;
		}
		return false;
	}
}
