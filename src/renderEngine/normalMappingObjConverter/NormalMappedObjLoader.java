package renderEngine.normalMappingObjConverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.models.RawModel;
import renderEngine.renderEngine.Loader;

public class NormalMappedObjLoader {

	private static final String RES_LOC = "res/";
	private static final String VERTEX_POSITION = "v ";
	private static final String VERTEX_TEXTURE = "vt ";
	private static final String VERTEX_NORMAL = "vn ";
	private static final String FACE = "f ";

	public static RawModel loadOBJ(String fileName) {
		long start = System.currentTimeMillis();
		String filePath = RES_LOC + fileName + ".obj";//fileName
		try {
			File file = new File(filePath);
			FileReader fileReader = new FileReader(file);
			BufferedReader reader = new BufferedReader(fileReader);
			String line = reader.readLine();
			ArrayList<Vector3f> positions = new ArrayList<Vector3f>();
			ArrayList<Vector2f> textures = new ArrayList<Vector2f>();
			ArrayList<Vector3f> normals = new ArrayList<Vector3f>();
			ArrayList<VertexNM> allVertices = new ArrayList<VertexNM>();
			while(line!=null){
				if(line.startsWith(VERTEX_POSITION)){
					String[] linePositions = line.substring(VERTEX_POSITION.length()).split(" ");
					positions.add(new Vector3f(Float.parseFloat(linePositions[0]), Float.parseFloat(linePositions[1]), Float.parseFloat(linePositions[2])));
				}else if(line.startsWith(VERTEX_TEXTURE)){
					String[] uvs = line.substring(VERTEX_TEXTURE.length()).split(" ");
					textures.add(new Vector2f(Float.parseFloat(uvs[0]), Float.parseFloat(uvs[1])));
				}else if(line.startsWith(VERTEX_NORMAL)){
					String[] lineNormals = line.substring(VERTEX_NORMAL.length()).split(" ");
					normals.add(new Vector3f(Float.parseFloat(lineNormals[0]), Float.parseFloat(lineNormals[1]), Float.parseFloat(lineNormals[2])));
				}else if(line.startsWith(FACE)){
					line = line.substring(FACE.length()).replace("/", " ");
					String[] face = line.split(" ");
					for(int i = 0; i < 9; i+=3){
						Vector3f position = positions.get(Integer.parseInt(face[i]) - 1);
						Vector2f texture = textures.get(Integer.parseInt(face[i+1]) - 1);
						Vector3f normal = normals.get(Integer.parseInt(face[i+2]) - 1);
						VertexNM vertex = new VertexNM(position, texture, normal);
						allVertices.add(vertex);
					}
					VertexNM v0 = allVertices.get(allVertices.size()-3);
					VertexNM v1 = allVertices.get(allVertices.size()-2);
					VertexNM v2 = allVertices.get(allVertices.size()-1);
					Vector3f tangent = calculateTangent(v0, v1, v2);
					v0.setTangent(tangent);
					v1.setTangent(tangent);
					v2.setTangent(tangent);
				}
				line = reader.readLine();
			}
			reader.close();
			//model
			int[] indices = generateIndices(allVertices);
			float[] positionsArray = new float[allVertices.size() * 3];
			float[] texturesArray = new float[allVertices.size() * 2];
			float[] normalsArray = new float[allVertices.size() * 3];
			float[] tangentsArray = new float[allVertices.size() * 3];
			modelDataToArrays(allVertices, positionsArray, texturesArray, normalsArray, tangentsArray);
			RawModel model = Loader.loadToVAO(positionsArray, texturesArray, normalsArray, tangentsArray, indices);
			System.out.println(fileName + ".obj loaded with normals in " + (float)(System.currentTimeMillis() - start) / 1000f + " seconds.");
			return model;
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Could not read file at " + filePath + ".");
		}
		return null;
	}
	
	public static Vector3f calculateTangent(VertexNM v0, VertexNM v1, VertexNM v2){
		Vector3f edge1 = Vector3f.sub(v1.getPosition(), v0.getPosition(), null);
		Vector3f edge2 = Vector3f.sub(v2.getPosition(), v0.getPosition(), null);
		Vector2f deltaUV1 = Vector2f.sub(v1.getTexture(), v0.getTexture(), null);
		Vector2f deltaUV2 = Vector2f.sub(v2.getTexture(), v0.getTexture(), null);
		
		Vector3f tangent = new Vector3f();
		
		tangent.x = deltaUV2.y * edge1.x - deltaUV1.y * edge2.x;
		tangent.y = deltaUV2.y * edge1.y - deltaUV1.y * edge2.y;
		tangent.z = deltaUV2.y * edge1.z - deltaUV1.y * edge2.z;
		
		tangent.normalise();
		
		return tangent;
	}
	
	public static int[] generateIndices(ArrayList<VertexNM> allVertices){
		int[] indices = new int[allVertices.size()];
		int indicesIndex = 0;
		for(VertexNM vertex : allVertices){
			indices[indicesIndex++] = allVertices.indexOf(vertex);
		}
		return indices;
	}
	
	public static void modelDataToArrays(ArrayList<VertexNM> vertices, float[] positionsOut, float[] texturesOut, float[] normalsOut, float[] tangentsOut){
		int index = 0;
		for(VertexNM vertex : vertices){
			Vector3f position = vertex.getPosition();
			positionsOut[index * 3] = position.x;
			positionsOut[(index * 3) + 1] = position.y;
			positionsOut[(index * 3) + 2] = position.z;
			Vector2f texture = vertex.getTexture();
			texturesOut[index * 2] = texture.x;
			texturesOut[(index * 2) + 1] = 1 - texture.y;
			Vector3f normal = vertex.getNormal();
			normalsOut[index * 3] = normal.x;
			normalsOut[(index * 3) + 1] = normal.y;
			normalsOut[(index * 3) + 2] = normal.z;
			Vector3f tangent = vertex.getTangent();
			tangentsOut[index * 3] = tangent.x;
			tangentsOut[(index * 3) + 1] = tangent.y;
			tangentsOut[(index * 3) + 2] = tangent.z;
			index++;
		}
	}
}