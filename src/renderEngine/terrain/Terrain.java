package renderEngine.terrain;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.entities.Camera;
import renderEngine.entities.Entity;
import renderEngine.models.RawModel;
import renderEngine.renderEngine.Loader;
import renderEngine.textures.TerrainTexture;
import renderEngine.textures.TerrainTexturePack;
import renderEngine.utils.Maths;

public class Terrain {

	private static final float MAX_HEIGHT = 40;
	private static final float MAX_PIXEL_COLOR = 256 * 256 * 256;

	private float size = 800;
	private float x,z;
	private RawModel model;
	private TerrainTexture texture;
	private TerrainTexturePack texturePack;
	private TerrainTexture blendMap;
	private boolean usesTexturePack = true;
	private int tiles = 40;
	
	private float[][] heights;
	
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack, TerrainTexture blendMap, String heightMap){
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(heightMap);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack, TerrainTexture blendMap, HeightsGenerator generator, int vertexCount){
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(generator, vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack, TerrainTexture blendMap, int vertexCount){
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexture texture, String heightMap){
		this.usesTexturePack = false;
		this.texture = texture;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(heightMap);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexture texture, HeightsGenerator generator, int vertexCount){
		this.usesTexturePack = false;
		this.texture = texture;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(generator, vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexture texture, int vertexCount){
		this.usesTexturePack = false;
		this.texture = texture;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack, TerrainTexture blendMap, String heightMap, int size){
		this.size = size;
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(heightMap);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack, TerrainTexture blendMap, HeightsGenerator generator, int vertexCount, int size){
		this.size = size;
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(generator, vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack, TerrainTexture blendMap, int vertexCount, int size){
		this.size = size;
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexture texture, String heightMap, int size){
		this.size = size;
		this.usesTexturePack = false;
		this.texture = texture;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(heightMap);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexture texture, HeightsGenerator generator, int vertexCount, int size){
		this.size = size;
		this.usesTexturePack = false;
		this.texture = texture;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(generator, vertexCount);
	}
	
	public Terrain(float gridX, float gridZ, TerrainTexture texture, int vertexCount, int size){
		this.size = size;
		this.usesTexturePack = false;
		this.texture = texture;
		this.x = gridX * size;
		this.z = gridZ * size;
		this.model = generateTerrain(vertexCount);
	}
	
	public float getX() {
		return x;
	}

	public float getZ() {
		return z;
	}

	public RawModel getModel() {
		return model;
	}

	public TerrainTexturePack getTexturePack() {
		return texturePack;
	}

	public TerrainTexture getBlendMap() {
		return blendMap;
	}
	
	public TerrainTexture getTexture(){
		return texture;
	}
	
	public boolean containsEntity(Entity entity){
		float entityX = entity.getPosition().x;
		float entityZ = entity.getPosition().z;
		return entityX>=this.x&&entityX<=this.x+size&entityZ>=this.z&&entityZ<=this.z+size;
	}
	
	public boolean containsCamera(Camera camera){
		float cameraX = camera.getPosition().x;
		float cameraZ = camera.getPosition().z;
		return cameraX>=this.x&&cameraX<=this.x+size&cameraZ>=this.z&&cameraZ<=this.z+size;
	}
	
	public boolean containsPoint(float x, float z){
		return x>this.x&&x<=this.x+size&z>this.z&&z<=this.z+size;
	}
	
	public float getHeightOfTerrain(float worldX, float worldZ){
		float terrainX = worldX - this.x;
		float terrainZ = worldZ - this.z;
		float gridSquaresize = size / ((float)heights.length-1);
		int gridX = (int) Math.floor(terrainX / gridSquaresize);
		int gridZ = (int) Math.floor(terrainZ / gridSquaresize);
		if (gridX+1>=heights.length||gridZ+1>=heights.length||gridX<0||gridZ<0) {
			return 0;
		}
		float xCoord = (terrainX%gridSquaresize)/gridSquaresize;
		float zCoord = (terrainZ%gridSquaresize)/gridSquaresize;
		float answer;
		if (xCoord <= (1-zCoord)) {
			answer = Maths
					.barryCentric(new Vector3f(0, heights[gridX][gridZ], 0), new Vector3f(1,
							heights[gridX + 1][gridZ], 0), new Vector3f(0,
							heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
		} else {
			answer = Maths
					.barryCentric(new Vector3f(1, heights[gridX + 1][gridZ], 0), new Vector3f(1,
							heights[gridX + 1][gridZ + 1], 1), new Vector3f(0,
							heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
		}
		return answer;
	}

	private RawModel generateTerrain(String heightMap){
		
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File("res/" + heightMap + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		int vertexCount = image.getHeight();
		
		heights = new float[vertexCount][vertexCount];
		int count = vertexCount * vertexCount;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(vertexCount-1)*(vertexCount-1)];
		int vertexPointer = 0;
		for(int i=0;i<vertexCount;i++){
			for(int j=0;j<vertexCount;j++){
				vertices[vertexPointer*3] = (float)j/((float)vertexCount - 1) * size;
				float height = getHeight(j, i, image);
				heights[j][i] = height;
				vertices[vertexPointer*3+1] = height;
				vertices[vertexPointer*3+2] = (float)i/((float)vertexCount - 1) * size;
				Vector3f normal = calculateNormal(j, i, image);
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] = normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)vertexCount - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)vertexCount - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<vertexCount-1;gz++){
			for(int gx=0;gx<vertexCount-1;gx++){
				int topLeft = (gz*vertexCount)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*vertexCount)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return Loader.loadToVAO(vertices, textureCoords, normals, indices);
	}
	
	private RawModel generateTerrain(int vertexCount){
		heights = new float[vertexCount][vertexCount];
		int count = vertexCount * vertexCount;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(vertexCount-1)*(vertexCount-1)];
		int vertexPointer = 0;
		for(int i=0;i<vertexCount;i++){
			for(int j=0;j<vertexCount;j++){
				vertices[vertexPointer*3] = (float)j/((float)vertexCount - 1) * size;
				float height = getHeight(j, i);
				heights[j][i] = height;
				vertices[vertexPointer*3+1] = height;
				vertices[vertexPointer*3+2] = (float)i/((float)vertexCount - 1) * size;
				Vector3f normal = calculateNormal(j, i);
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] = normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)vertexCount - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)vertexCount - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<vertexCount-1;gz++){
			for(int gx=0;gx<vertexCount-1;gx++){
				int topLeft = (gz*vertexCount)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*vertexCount)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return Loader.loadToVAO(vertices, textureCoords, normals, indices);
	}
	
	private RawModel generateTerrain(HeightsGenerator generator, int vertexCount){
		heights = new float[vertexCount][vertexCount];
		int count = vertexCount * vertexCount;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(vertexCount-1)*(vertexCount-1)];
		int vertexPointer = 0;
		for(int i=0;i<vertexCount;i++){
			for(int j=0;j<vertexCount;j++){
				vertices[vertexPointer*3] = (float)j/((float)vertexCount - 1) * size;
				float height = getHeight(j, i, generator);
				heights[j][i] = height;
				vertices[vertexPointer*3+1] = height;
				vertices[vertexPointer*3+2] = (float)i/((float)vertexCount - 1) * size;
				Vector3f normal = calculateNormal(j, i, generator);
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] = normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)vertexCount - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)vertexCount - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<vertexCount-1;gz++){
			for(int gx=0;gx<vertexCount-1;gx++){
				int topLeft = (gz*vertexCount)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*vertexCount)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return Loader.loadToVAO(vertices, textureCoords, normals, indices);
	}
	
	private Vector3f calculateNormal(int x, int y, BufferedImage image){
		float heightL = getHeight(x-1, y, image);
		float heightR = getHeight(x+1, y, image);
		float heightD = getHeight(x, y-1, image);
		float heightU = getHeight(x, y+1, image);
		Vector3f normal = new Vector3f(heightL-heightR, 2f, heightD-heightU);
		normal.normalise();
		return normal;
	}
	
	private Vector3f calculateNormal(int x, int y, HeightsGenerator generator){
		float heightL = getHeight(x-1, y, generator);
		float heightR = getHeight(x+1, y, generator);
		float heightD = getHeight(x, y-1, generator);
		float heightU = getHeight(x, y+1, generator);
		Vector3f normal = new Vector3f(heightL-heightR, 2f, heightD-heightU);
		normal.normalise();
		return normal;
	}
	
	private Vector3f calculateNormal(int x, int y){
		return new Vector3f(0,1,0);
	}
	
	private float getHeight(int x, int y, BufferedImage image){
		if(x<0 || x>=image.getWidth() || y<0 || y>=image.getHeight()){
			return 0;
		}
		float height = image.getRGB(x, y);
		height += MAX_PIXEL_COLOR/2;
		height /= MAX_PIXEL_COLOR/2;
		height *= MAX_HEIGHT;
		return height;
	}
	
	private float getHeight(int x, int z, HeightsGenerator generator){
		return generator.generateHeight(x, z);
	}
	
	private float getHeight(int x, int z){
		return 0;
	}

	public boolean usesTexturePack() {
		return usesTexturePack;
	}

	public int getTiles() {
		return tiles;
	}

	public void setTiles(int tiles) {
		this.tiles = tiles;
	}
	
	public float getSize(){
		return size;
	}
}
