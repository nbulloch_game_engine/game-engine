package renderEngine.skybox;
 
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.entities.Camera;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.shaders.ShaderProgram;
import renderEngine.utils.Maths;
 
public class SkyboxShader extends ShaderProgram{
 
    private static final String VERTEX_FILE = "/renderEngine/skybox/skyboxVertexShader.glsl";
    private static final String FRAGMENT_FILE = "/renderEngine/skybox/skyboxFragmentShader.glsl";
     
    private static final float ROTATE_SPEED = 0f;
    
    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_fogColor;
    private int location_cubeMap;
    private int location_cubeMap2;
    private int location_blendFactor;
    private int location_fogginess;
     
    private float rotation = 0;
    
    public SkyboxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
     
    public void loadProjectionMatrix(Matrix4f matrix){
        super.loadMatrix(location_projectionMatrix, matrix);
    }
    
    public void loadViewMatrix(Camera camera){
        Matrix4f matrix = new Matrix4f(camera.getViewMatrix());
        matrix.m30 = 0;
        matrix.m31 = 0;
        matrix.m32 = 0;
        rotation += ROTATE_SPEED * DisplayManager.getFrameTimeSeconds();
        Matrix4f.rotate((float) Math.toRadians(rotation), new Vector3f(0,1,0), matrix, matrix);
        super.loadMatrix(location_viewMatrix, matrix);
    }
 
    public void loadViewMatrix(Camera camera, Vector3f position){
    	Matrix4f matrix = new Matrix4f(camera.getViewMatrix());
        rotation += ROTATE_SPEED * DisplayManager.getFrameTimeSeconds();
        Matrix4f transformation = Maths.createTransformationMatrix(position, new Vector3f(0,rotation,0), new Vector3f(1,1,1));
        Matrix4f.mul(matrix, transformation, matrix);
        //Matrix4f.translate(position.negate(null), matrix, matrix);
        //Matrix4f.rotate((float) Math.toRadians(rotation), new Vector3f(0,1,0), matrix, matrix);
        super.loadMatrix(location_viewMatrix, matrix);
    }
     
    public void loadFogColor(Vector3f fogColor){
    	super.loadVector3f(location_fogColor, fogColor);
    }
    
    public void loadFogginess(float fogginess){
    	super.loadFloat(location_fogginess, fogginess);
    }
    
    public void connectTextureUnits(){
    	super.loadInt(location_cubeMap, 0);
    	super.loadInt(location_cubeMap2, 1);
    }
    
    public void loadBlendFactor(float blend){
    	super.loadFloat(location_blendFactor, blend);
    }
    
    @Override
    protected void getAllUniformLocations() {
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        location_fogColor = super.getUniformLocation("fogColor");
        location_cubeMap = super.getUniformLocation("cubeMap");
        location_cubeMap2 = super.getUniformLocation("cubeMap2");
        location_blendFactor = super.getUniformLocation("blendFactor");
        location_fogginess = super.getUniformLocation("fogginess");
    }
 
    @Override
    protected void bindAttributes() {
    	super.bindFragOutput(0, "out_color");
    	super.bindFragOutput(1, "out_bloomColor");
        super.bindAttribute(0, "position");
    }
 
}
