package renderEngine.bloom;

import renderEngine.shaders.ShaderProgram;

public class CombineShader extends ShaderProgram {

	private static final String VERTEX_FILE = "/renderEngine/bloom/simpleVertex.glsl";
	private static final String FRAGMENT_FILE = "/renderEngine/bloom/combineFragment.glsl";
	
	private int location_colorTexture,
	location_highlightTexture2,
	location_highlightTexture4,
	location_highlightTexture8;
	
	protected CombineShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}
	
	@Override
	protected void getAllUniformLocations() {
		location_colorTexture = super.getUniformLocation("colorTexture");
		location_highlightTexture2 = super.getUniformLocation("highlightTexture2");
		location_highlightTexture4 = super.getUniformLocation("highlightTexture4");
		location_highlightTexture8 = super.getUniformLocation("highlightTexture8");
	}
	
	protected void connectTextureUnits(){
		super.loadInt(location_colorTexture, 0);
		super.loadInt(location_highlightTexture2, 1);
		super.loadInt(location_highlightTexture4, 2);
		super.loadInt(location_highlightTexture8, 3);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
}
