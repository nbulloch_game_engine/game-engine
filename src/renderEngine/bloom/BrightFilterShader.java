package renderEngine.bloom;

import renderEngine.shaders.ShaderProgram;

public class BrightFilterShader extends ShaderProgram{
	
	private static final String VERTEX_FILE = "/renderEngine/bloom/simpleVertex.glsl";
	private static final String FRAGMENT_FILE = "/renderEngine/bloom/brightFilterFragment.glsl";
	
	private int textureColor_location;
	private int glowMap_location;
	private int offset_location;
	
	public BrightFilterShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() {	
		textureColor_location = super.getUniformLocation("colorTexture");
		glowMap_location = super.getUniformLocation("glowMap");
		offset_location = super.getUniformLocation("offset");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
	
	public void setOffset(float value){
		super.loadFloat(offset_location, value);
	}
	
	public void connectTextureUnits(){
		super.loadInt(textureColor_location, 0);
		super.loadInt(glowMap_location, 1);
	}
}
