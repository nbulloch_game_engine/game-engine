#version 130//check version

in vec2 textureCoords;

out vec4 out_color;

uniform sampler2D colorTexture;
uniform sampler2D glowMap;
uniform float offset;

void main(void){
	vec4 color =  texture(colorTexture, textureCoords);
	float glowColor = texture(glowMap, vec2((textureCoords.x - offset*43.4), (textureCoords.y - offset*22.6))).r;
	float glowColor2 = texture(glowMap, vec2((textureCoords.x - offset*17.9), (textureCoords.y + offset*42.8))).g;
	float glowColor3 = texture(glowMap, vec2((textureCoords.x + offset*58.1), (textureCoords.y + offset*25.4))).b;
	float glowFactor = (glowColor + glowColor2 + glowColor3)/3;
	//float lumaBrightness = (color.r * 0.2126) + (color.g * 0.7152) + (color.b * 0.0722);
	float brightness = - (color.r * 0.2122) - (color.g * 0.2126) + (color.b * 0.9752);
 	//gradient
	if(brightness > 0.7){
		out_color = color * glowFactor;
	}else{
		out_color = vec4(0.0);
	}
}