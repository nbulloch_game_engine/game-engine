package renderEngine.bloom;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import renderEngine.postProcessing.ImageRenderer;

public class BrightFilter {

	private ImageRenderer renderer;
	private BrightFilterShader shader;
	private float offset = 0;
	
	public BrightFilter(int width, int height){
		shader = new BrightFilterShader();
		shader.start();
		shader.connectTextureUnits();
		shader.stop();
		renderer = new ImageRenderer(width, height);
	}
	
	public void render(int texture, int glowMap){
		shader.start();
		offset++;
		shader.setOffset(offset/60000);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, glowMap);
		renderer.renderQuad();
		shader.stop();
	}
	
	public int getOutputTexture(){
		return renderer.getOutputTexture();
	}
	
	public void clean(){
		renderer.clean();
		shader.clean();
	}
	
}
