#version 130

in vec2 textureCoords;

out vec4 out_color;

uniform sampler2D colorTexture;
uniform sampler2D highlightTexture2;
uniform sampler2D highlightTexture4;
uniform sampler2D highlightTexture8;

void main(void){
	
	vec4 sceneColor = texture(colorTexture, textureCoords);
	vec4 highlightColor2 = texture(highlightTexture2, textureCoords);
	vec4 highlightColor4 = texture(highlightTexture4, textureCoords);
	vec4 highlightColor8 = texture(highlightTexture8, textureCoords);
	out_color = sceneColor + (highlightColor2*(1-sceneColor.a) + highlightColor4*2 + highlightColor8*(sceneColor.a)*4);
	
}