#version 130

in vec2 position;

out vec2 textureCoords;
out vec2 pos;

uniform mat2 transform;
uniform vec2 translation;
uniform int mirrorX;
uniform int mirrorY;

void main(){
	pos = position * transform + translation;
	gl_Position = vec4(pos, 0.0, 1.0);
	textureCoords = vec2((position.x+1.0)/2.0, 1 - (position.y+1.0)/2.0);
	if(mirrorX == 1)
		textureCoords.x = 1 - textureCoords.x;
	if(mirrorY == 1)
		textureCoords.y = 1 - textureCoords.y;
}