package renderEngine.guiRenderer;
 
import org.lwjgl.util.vector.Matrix2f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.shaders.ShaderProgram;
 
public class GuiShader extends ShaderProgram{
     
    private static final String VERTEX_FILE = "/renderEngine/guiRenderer/guiVertexShader.glsl";
    private static final String FRAGMENT_FILE = "/renderEngine/guiRenderer/guiFragmentShader.glsl";
     
    private int location_translation,
    location_transform,
    location_hasTexture,
    location_color,
    location_textureBounds,
    location_min,
    location_max,
    location_mirrorX,
    location_mirrorY;
 
    public GuiShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
 
    @Override
    protected void getAllUniformLocations() {
    	location_translation = super.getUniformLocation("translation");
    	location_transform = super.getUniformLocation("transform");
        location_hasTexture = super.getUniformLocation("hasTexture");
        location_color = super.getUniformLocation("color");
        location_textureBounds = super.getUniformLocation("textureBounds");
        location_min = super.getUniformLocation("min");
        location_max = super.getUniformLocation("max");
        location_mirrorX = super.getUniformLocation("mirrorX");
        location_mirrorY = super.getUniformLocation("mirrorY");
    }
 
    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
    
    protected void loadMirror(boolean mirrorX, boolean mirrorY){
    	super.loadBoolean(location_mirrorX, mirrorX);
    	super.loadBoolean(location_mirrorY, mirrorY);
    }
    
    protected void loadMin(Vector2f min){
    	super.loadVector2f(location_min, min);
    }
    
    protected void loadMax(Vector2f max){
    	super.loadVector2f(location_max, max);
    }
    
    protected void loadTextureBounds(float min, float max){
    	super.loadVector2f(location_textureBounds, new Vector2f(min, max));
    }
    
    protected void loadHasTexture(boolean hasTexture){
    	super.loadBoolean(location_hasTexture, hasTexture);
    }
    
    protected void loadColor(Vector4f color){
    	super.loadVector4f(location_color, color);
    }

	public void loadTranlation(Vector2f translation) {
		super.loadVector2f(location_translation, translation);
	}
	
	public void loadTransform(Matrix2f transform){
		super.loadMatrix(location_transform, transform);
	}
}
