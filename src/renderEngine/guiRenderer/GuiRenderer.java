package renderEngine.guiRenderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import renderEngine.guis.GuiModel;
import renderEngine.models.RawModel;
import renderEngine.renderEngine.Loader;

public class GuiRenderer {

	private static final RawModel QUAD = Loader.getQuad();
	private static GuiShader shader = init();
	
	public static GuiShader init(){
		shader = new GuiShader();
		return shader;
	}
	
	public static void render(GuiModel model){
		shader.start();
		glBindVertexArray(QUAD.getVaoID());
		glEnableVertexAttribArray(0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		shader.loadTranlation(model.getWorldPosition());
		shader.loadTransform(model.getWorldTransform().getMatrix());
		shader.loadHasTexture(model.isTextured());
		shader.loadTextureBounds(model.getXTextureMin(), model.getXTextureMax());
		shader.loadMax(model.getMax());
		shader.loadMin(model.getMin());
		if(model.isTextured()){
			if(model.getTexture()==-1)
				return;
			shader.loadMirror(model.isMirrorX(), model.isMirrorY());
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, model.getTexture());
		}else{
			shader.loadMirror(false, false);
			shader.loadColor(model.getColor());
		}
		glDrawArrays(GL_TRIANGLE_STRIP, 0, QUAD.getVertexCount());
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
		shader.stop();
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
	}
	
	public static void clean(){
		shader.clean();
	}
}
