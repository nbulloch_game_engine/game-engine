#version 130

in vec2 textureCoords;
in vec2 pos;

out vec4 out_Color;

uniform sampler2D guiTexture;
uniform vec4 color;
uniform vec2 textureBounds;//x - min, y - max
uniform vec2 min;
uniform vec2 max;
uniform int hasTexture;

void main(){
	if(textureCoords.x < textureBounds.x || textureCoords.x > textureBounds.y){
		discard;
	}if(pos.x > max.x || pos.x < min.x || pos.y > max.y || pos.y < min.y){
		discard;
	}if(hasTexture==1){
		out_Color = texture(guiTexture,textureCoords);
	}else{
		out_Color = color;
	}
}