#version 150

out vec2 textureCoords;
out float depth;
in vec2 textureCoordinates;
uniform mat4 projectionMatrix;
uniform vec2 offset;
uniform float numberOfRows;
uniform int tiles;

vec4 setPosition(vec4 worldPosition, mat4 viewMatrix){
	vec4 viewPosition = viewMatrix * worldPosition;
	gl_Position = projectionMatrix * viewPosition;
	depth = gl_Position.z;
	textureCoords = ((textureCoordinates/numberOfRows) + offset) * tiles;
	return viewPosition;
}


out float visibility;
uniform float density;
uniform float gradient;

float calculateFog(vec4 viewPosition){
	float distance = length(viewPosition.xyz);
	visibility = exp(-pow((distance*density),gradient));
	visibility = clamp(visibility,0.0,1.0);
	return distance;
}

const int maxLights = 4;
const int maxDLights = 6;
out vec3 viewDLDirection[maxDLights];
out vec3 toLightVector[maxLights];
out vec3 toCameraVector;
uniform vec3 DLDirection[maxDLights];
uniform vec3 lightPosition[maxLights];
uniform int numLights;
uniform int numDLights;

void transformLights(vec4 worldPosition, mat4 transformationMatrix, mat4 viewMatrix){
	for(int i=0;i<numLights;i++){
		toLightVector[i] = lightPosition[i] - worldPosition.xyz;
	}
	for(int i=0;i<numDLights;i++){
		viewDLDirection[i] = DLDirection[i];
	}
	toCameraVector = (inverse(viewMatrix) * vec4(0.0,0.0,0.0,1.0)).xyz - worldPosition.xyz;
}


out vec3 surfaceNormal;
uniform mat4 transformationMatrix;
uniform int useFakeLighting;

void transformSurfaceNormal(vec3 normal){
	if(useFakeLighting==1){
		normal = vec3(0.0,1.0,0.0);
	}
	surfaceNormal = (transformationMatrix * vec4(normal,0.0)).xyz;
}


const float transitionDistance = 10.0;
uniform float shadowDistance;

float getShadowDistance(float distance){
	distance = distance - (shadowDistance - transitionDistance);
	distance = distance / transitionDistance;
	return clamp(1.0-distance, 0.0, 1.0);
}


const int maxCascades = 5;
out vec4 csmCoords[maxCascades];
uniform mat4 toCSMSpaces[maxCascades];
uniform int cascades;

void getCSMPositions(vec4 worldPosition){
	for(int i = 0; i < cascades; i++){
		vec4 csmPosition = toCSMSpaces[i] * worldPosition;
		csmCoords[i] = csmPosition/2+0.5;
	}
}


out vec4 shadowCoords;
in vec3 position;
in vec3 normal;
uniform mat4 viewMatrix;
uniform mat4 toShadowMapSpace;
uniform vec4 plane;

void main(){
	//World Space
	vec4 worldPosition = transformationMatrix * vec4(position,1.0);
	
	//Clip
	gl_ClipDistance[0] = dot(worldPosition, plane);
	
	//View Space
	vec4 viewPosition = setPosition(worldPosition, viewMatrix);
	float distance = calculateFog(viewPosition);
	
	//Lighting
	transformLights(worldPosition, transformationMatrix, viewMatrix);
	transformSurfaceNormal(normal);
	
	//Shadow Space
	shadowCoords = toShadowMapSpace * worldPosition;
	shadowCoords.w = getShadowDistance(distance);
	getCSMPositions(worldPosition);
}