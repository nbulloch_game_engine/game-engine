#version 130

in vec2 textureCoords;
uniform sampler2D backgroundTexture;
uniform sampler2D rTexture;
uniform sampler2D gTexture;
uniform sampler2D bTexture;
uniform sampler2D blendMap;
uniform sampler2D shadowMap;
uniform float texturePack;
uniform int tiles;

vec4 getColor(){
	vec2 tiledCoords = textureCoords * tiles;
	vec4 totalColor;
	if(texturePack==1){
		vec4 blendMapColor = texture(blendMap, textureCoords);
		float backTextureAmount = 1 - (blendMapColor.r + blendMapColor.g + blendMapColor.b);
		vec4 backgroundTextureColor = texture(backgroundTexture, tiledCoords) * backTextureAmount;
		vec4 rTextureColor = texture(rTexture, tiledCoords) * blendMapColor.r;
		vec4 gTextureColor = texture(gTexture, tiledCoords) * blendMapColor.g;
		vec4 bTextureColor = texture(bTexture, tiledCoords) * blendMapColor.b;
		totalColor = backgroundTextureColor + rTextureColor + gTextureColor + bTextureColor;
	}else{
		totalColor = texture(backgroundTexture, tiledCoords);
	}
	return totalColor;
}

void checkTransparency(vec4 color){
	if(color.a<0.5){
		discard;
	}
}


const int pcfCount = 2;
const float totalTexels = (pcfCount * pcfCount + 1.0) * (pcfCount * pcfCount + 1.0);
const int maxCascades = 5;
in vec4 csmCoords[maxCascades];
in vec4 shadowCoords;
in float depth;
uniform sampler2D CSMs[maxCascades];
uniform float CSMDepths[maxCascades];
uniform int CSMSizes[maxCascades]; 
uniform int cascades;
uniform int hasShadows;

float calculateShadows(vec3 unitNormal){
	float total = 0;
	float lightFactor = 1.0;
	
	int cascade;
	if(hasShadows==1){
		for(int i = 0; i < cascades; i++){
			if(depth>CSMDepths[i]){
				cascade = i;
			}else{
				break;
			}
		}
		float mapSize = 16384.0*pcfCount;
		if(cascades!=0){
			mapSize = CSMSizes[cascade]*pcfCount;
		}
		float texelSize = 1.0 / mapSize;
		float objectNearestLight = 0;
		for(int x = -pcfCount; x <= pcfCount; x++){
			for(int y = -pcfCount; y <= pcfCount; y++){
				if(cascades==0){
					objectNearestLight = texture(shadowMap, shadowCoords.xy + vec2(x, y) * texelSize).r;
					if(shadowCoords.z > objectNearestLight + 0.00005){
						total++;
					}
				}else{
					objectNearestLight = texture(CSMs[cascade], csmCoords[cascade].xy + vec2(x, y) * texelSize).r;
					if(csmCoords[cascade].z >= objectNearestLight){
						total++;
					}
				}
			}
		}
		total /= totalTexels * 2;
	}	
	
	if(cascade==-1){
		lightFactor = 1.0 - (total * shadowCoords.w);
	}else{
		lightFactor = 1.0 - total;
	}
	
	return lightFactor;
}


uniform float shineDamper;
uniform float reflectivity;

void doLighting(inout vec3 totalDiffuse, inout vec3 totalSpecular, vec3 unitNormal, vec3 unitToCamera, float attFactor, vec3 toLightVector, vec3 color){
	vec3 unitLightVector = normalize(toLightVector);
	float nDotl = dot(unitNormal,unitLightVector);
	float brightness = max(nDotl,0.0);
	vec3 lightDirection = -unitLightVector;
	vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);
	float specularFactor = dot(reflectedLightDirection, unitToCamera);
	specularFactor = max(specularFactor,0.0);
	float dampedFactor = pow(specularFactor, shineDamper);
	totalDiffuse += (brightness * color)/attFactor;
	totalSpecular += (dampedFactor * reflectivity * color)/attFactor;
}


const int maxLights = 4;
const int maxDLights = 6;
in vec3 surfaceNormal;
in vec3 toLightVector[maxLights];
in vec3 viewDLDirection[maxDLights];
in vec3 toCameraVector;
in float visibility;
out vec4 out_color;
out vec4 out_bloomColor;
uniform vec3 lightColor[maxLights];
uniform vec3 attenuation[maxLights];
uniform vec3 DLColor[maxDLights];
uniform vec3 skyColor;
uniform int numLights;
uniform int numDLights;

void main(){
	//Transparency
	vec4 textureColor = getColor();
	checkTransparency(textureColor);
	
	//Shadows
	vec3 unitNormal = normalize(surfaceNormal);
	float lightFactor = calculateShadows(unitNormal);
	
	vec3 unitToCamera = normalize(toCameraVector);
	vec3 totalDiffuse = vec3(0.0);
	vec3 totalSpecular = vec3(0.0);
	for(int i=0;i<numLights;i++){
		float distance = length(toLightVector[i]);
		float attFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
		doLighting(totalDiffuse, totalSpecular, unitNormal, unitToCamera, attFactor, toLightVector[i], lightColor[i]);
	}
	for(int i=0;i<numDLights;i++){
		doLighting(totalDiffuse, totalSpecular, unitNormal, unitToCamera, 1.0, -viewDLDirection[i], DLColor[i]);
	}
	totalDiffuse = max(totalDiffuse * lightFactor, 0.4);
	
	//Final Lighting
	out_color =  vec4(totalDiffuse,1.0) * textureColor + vec4(totalSpecular,1.0);
	out_color = mix(vec4(skyColor,10.0),out_color,visibility);
	out_bloomColor = vec4(0.0);
}