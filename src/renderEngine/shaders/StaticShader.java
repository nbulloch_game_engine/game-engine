package renderEngine.shaders;
 
import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.entities.Camera;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.PointLight;
import renderEngine.renderEngine.MasterRenderer;
 
public class StaticShader extends ShaderProgram{
	
    private static final int MAX_POINT_LIGHTS = 4;
    private static final int MAX_DIRECTIONAL_LIGHTS = 6;
     
    private static final String VERTEX_FILE = "/renderEngine/shaders/vertexShader.glsl";
    private static final String FRAGMENT_FILE = "/renderEngine/shaders/fragmentShader.glsl";
     
    private int
    //Clip Plane
    location_plane,
    
    //Location
    location_transformationMatrix,
    location_projectionMatrix,
    location_viewMatrix,
    
    //Model Assets
    location_modelTexture,
    	//Tiled Texture
    	location_tiles,
    	//Texture Atlas
    	location_numberOfRows,
    	location_offset,
    	//Material Properties
    	location_shineDamper,
    	location_reflectivity,
    
    //Lighting
    location_useFakeLighting,
    	//PointLights
	    location_lightPosition[],
	    location_lightColor[],
	    location_attenuation[],
	    location_numLights,
	    //Directional Lights
	    location_DLDirection[],
	    location_DLColor[],
	    location_numDLights,
	    
	//Shadows
	location_hasShadows,
	    //Cascaded Shadow Maps
	    location_toCSMSpaces[],
	    location_CSMs[],
	    location_CSMDepths[],
	    location_CSMSizes[],
	    location_cascades,
	    //Shadow Map
	    location_toShadowMapSpace,
	    location_shadowMap,
	    location_shadowDistance,
	    
	//Special Effects
	    //Variable Specularity
	    location_specularMap,
	    location_usesSpecularMap,
	    //Glow
	    location_useGlowMap,
	    location_lightMap,
	    location_usesLightMap,
	    location_glowMap,
	    location_glowOffset,
	    location_bloomWidth,
	    location_glowMapFactor,
	    location_glowSpeed,
	    //Fog
	    location_skyColor,
	    location_density,
	    location_gradient,
	    //Translucency
	    location_translucent,
    	location_translucency;
 
    public StaticShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
 
    @Override
    protected void bindAttributes() {
    	super.bindFragOutput(0, "out_color");
    	super.bindFragOutput(1, "out_bloomColor");
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoordinates");
        super.bindAttribute(2, "normal");
    }
 
    @Override
    protected void getAllUniformLocations() {
    //Clip Plane
    	location_plane = super.getUniformLocation("plane");
    //Location
        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
    //Model Assets
        location_modelTexture = super.getUniformLocation("modelTexture");
        //Tiled Texture
        location_tiles = super.getUniformLocation("tiles");
        //Texture Atlas
        location_numberOfRows = super.getUniformLocation("numberOfRows");
        location_offset = super.getUniformLocation("offset");
        //Material Properties
        location_shineDamper = super.getUniformLocation("shineDamper");
        location_reflectivity = super.getUniformLocation("reflectivity");
    //Lighting
        location_useFakeLighting = super.getUniformLocation("useFakeLighting");
        //Point Lights
        location_numLights = super.getUniformLocation("numLights");
        location_lightPosition = new int[MAX_POINT_LIGHTS];
        location_lightColor = new int[MAX_POINT_LIGHTS];
        location_attenuation = new int[MAX_POINT_LIGHTS];
        for(int i=0;i<MAX_POINT_LIGHTS;i++){
        	location_lightPosition[i] = super.getUniformLocation("lightPosition[" + i + "]");
            location_lightColor[i] = super.getUniformLocation("lightColor[" + i + "]");
            location_attenuation[i] = super.getUniformLocation("attenuation[" + i + "]");
        }
        //Directional Lights
        location_numDLights = super.getUniformLocation("numDLights");
        location_DLDirection = new int[MAX_DIRECTIONAL_LIGHTS];
        location_DLColor = new int[MAX_DIRECTIONAL_LIGHTS];
        for(int i=0;i<MAX_DIRECTIONAL_LIGHTS;i++){
            location_DLDirection[i] = super.getUniformLocation("DLDirection[" + i + "]");
            location_DLColor[i] = super.getUniformLocation("DLColor[" + i + "]");
        }
    //Shadows
        location_hasShadows = super.getUniformLocation("hasShadows");
        //Cascaded Shadow Maps
        location_CSMs = new int[MasterRenderer.MAX_CASCADES];
        location_toCSMSpaces = new int[MasterRenderer.MAX_CASCADES];
        location_CSMDepths = new int[MasterRenderer.MAX_CASCADES];
        location_CSMSizes = new int[MasterRenderer.MAX_CASCADES];
        for(int i=0;i<MasterRenderer.MAX_CASCADES;i++){
        	location_CSMs[i] = super.getUniformLocation("CSMs["+ i +"]");
        	location_toCSMSpaces[i] = super.getUniformLocation("toCSMSpaces["+ i +"]");
        	location_CSMDepths[i] = super.getUniformLocation("CSMDepths["+ i +"]");
        	location_CSMSizes[i] = super.getUniformLocation("CSMSizes[" + i + "]");
        }
        location_cascades = super.getUniformLocation("cascades");
        //Shadow Map
        location_toShadowMapSpace = super.getUniformLocation("toShadowMapSpace");
        location_shadowMap = super.getUniformLocation("shadowMap");
        location_shadowDistance = super.getUniformLocation("shadowDistance");
    //Special Effects
        //Variable Specularity
        location_specularMap = super.getUniformLocation("specularMap");
        location_usesSpecularMap = super.getUniformLocation("usesSpecularMap");
        //Glow
        location_useGlowMap = super.getUniformLocation("useGlowMap");
        location_lightMap = super.getUniformLocation("lightMap");
        location_usesLightMap = super.getUniformLocation("usesLightMap");
        location_glowMap = super.getUniformLocation("glowMap");
        location_glowOffset = super.getUniformLocation("glowOffset");
        location_bloomWidth = super.getUniformLocation("bloomWidth");
        location_glowMapFactor = super.getUniformLocation("glowMapFactor");
        location_glowSpeed = super.getUniformLocation("glowSpeed");
        //Fog
        location_skyColor = super.getUniformLocation("skyColor");
        location_density = super.getUniformLocation("density");
        location_gradient = super.getUniformLocation("gradient");
        //Translucency
        location_translucent = super.getUniformLocation("translucent");
        location_translucency = super.getUniformLocation("translucency");
    }
    
    public void loadTranslucency(float translucency){
    	super.loadFloat(location_translucency, translucency);
    }
    
    public void loadCSMDepths(float[] depths){
    	for(int i = 0; i < depths.length; i++){
    		super.loadFloat(location_CSMDepths[i], depths[i]);
    	}super.loadInt(location_cascades, depths.length);
    }
    
    public void loadToCSMSpaces(Matrix4f[] toCSMSpaces){
    	for(int i = 0; i < toCSMSpaces.length; i++){
    		super.loadMatrix(location_toCSMSpaces[i], toCSMSpaces[i]);
    	}
    }
    
    public void loadCSMSizes(int[] sizes){
    	for(int i = 0; i < sizes.length; i++){
    		super.loadInt(location_CSMSizes[i], sizes[i]);
    	}
    }
    
    public void loadUseGlowMap(boolean useGlowMap){
    	super.loadBoolean(location_useGlowMap, useGlowMap);
    }
    
    public void loadBloomWidth(float bloomWidth){
    	super.loadFloat(location_bloomWidth, bloomWidth);
    }
    
    public void loadGlowMapFactor(float glowMapFactor){
    	super.loadFloat(location_glowMapFactor, glowMapFactor);
    }
    
    public void loadGlowSpeed(float speed){
    	super.loadFloat(location_glowSpeed, speed);
    }
    
    public void loadIsTranslucent(boolean translucent){
    	super.loadBoolean(location_translucent, translucent);
    }
    
    public void loadTiles(int tiles){
    	super.loadInt(location_tiles, tiles);
    }
    
    public void loadHasShadows(boolean hasShadows){
    	super.loadBoolean(location_hasShadows, hasShadows);
    }
    
    public void loadNumLights(int numLights){
    	super.loadInt(location_numLights, numLights);
    }
    
    public void loadNumDLights(int numDLights){
    	super.loadInt(location_numDLights, numDLights);
    }
    
    public void loadFogValues(float density, float gradient){
    	super.loadFloat(location_density, density);
    	super.loadFloat(location_gradient, gradient);
    }
    
    public void connectTextureUnits(){
    	super.loadInt(location_modelTexture, 0);
    	super.loadInt(location_specularMap, 1);
    	super.loadInt(location_lightMap, 2);
    	super.loadInt(location_glowMap, 3);
    	//4
    	super.loadInt(location_shadowMap, 5);
    	for(int i = 0; i < MasterRenderer.MAX_CASCADES; i++){
    		super.loadInt(location_CSMs[i], 6+i);
    	}
    }
    
    public void setOffset(float value){
    	super.loadFloat(location_glowOffset, value);
    }
    
    public void loadUsesLightMap(boolean value){
    	super.loadBoolean(location_usesLightMap, value);
    }
    
    public void loadUsesSpecularMap(boolean value){
    	super.loadBoolean(location_usesSpecularMap, value);
    }
    
    public void loadShadowDistance(float distance){
    	super.loadFloat(location_shadowDistance, distance);
    }
    
    public void loadToShadowSpace(Matrix4f matrix){
    	super.loadMatrix(location_toShadowMapSpace, matrix);
    }
    
    public void loadClipPlane(Vector4f plane){
    	super.loadVector4f(location_plane, plane);
    }
    
    public void loadNumberOfRows(float value){
    	super.loadFloat(location_numberOfRows, value);
    }
    
    public void loadOffset(float x, float y){
    	super.loadVector2f(location_offset, new Vector2f(x,y));
    }
    
    public void loadSkyColor(Vector3f skyColor){
    	super.loadVector3f(location_skyColor, skyColor);
    }
    
    public void loadFakeLightingVariable(boolean useFakeLighting){
        super.loadBoolean(location_useFakeLighting, useFakeLighting);
    }
    
    public void loadShineVariables(float damper,float reflectivity){
        super.loadFloat(location_shineDamper, damper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
     
    public void loadTransformationMatrix(Matrix4f matrix){
        super.loadMatrix(location_transformationMatrix, matrix);
    }
     
    public void loadLights(List<PointLight> lights, List<DirectionalLight> DLights){
        for(int i=0;i<MAX_POINT_LIGHTS;i++){
            if(i<lights.size()){
                super.loadVector3f(location_lightPosition[i], lights.get(i).getPosition());
                super.loadVector3f(location_lightColor[i], lights.get(i).getColor());
                super.loadVector3f(location_attenuation[i], lights.get(i).getAttenuation());
            }else{
                super.loadVector3f(location_lightPosition[i], new Vector3f(0, 0, 0));
                super.loadVector3f(location_lightColor[i], new Vector3f(0, 0, 0));
                super.loadVector3f(location_attenuation[i], new Vector3f(1, 0, 0));
            }
        }
        for(int i=0;i<MAX_DIRECTIONAL_LIGHTS;i++){
        	if(i<DLights.size()){
            	super.loadVector3f(location_DLDirection[i], DLights.get(i).getDirection());
            	super.loadVector3f(location_DLColor[i], DLights.get(i).getColor());
            }else{
            	super.loadVector3f(location_DLDirection[i], new Vector3f(0, 0, 0));
            	super.loadVector3f(location_DLColor[i], new Vector3f(0, 0, 0));
            }
        }
    }
     
    public void loadViewMatrix(Camera camera){
        super.loadMatrix(location_viewMatrix, camera.getViewMatrix());
    }
     
    public void loadProjectionMatrix(Matrix4f projection){
        super.loadMatrix(location_projectionMatrix, projection);
    }
 
}