package renderEngine.shaders;
 
import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.entities.Camera;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.PointLight;
import renderEngine.renderEngine.MasterRenderer;
 
public class TerrainShader extends ShaderProgram {

	private static final int MAX_POINT_LIGHTS = 4;
    private static final int MAX_DIRECTIONAL_LIGHTS = 6; 
	
    private static final String VERTEX_FILE = "/renderEngine/shaders/terrainVertexShader.glsl";
    private static final String FRAGMENT_FILE = "/renderEngine/shaders/terrainFragmentShader.glsl";
     
    private int
    //Location
    location_transformationMatrix,
    location_projectionMatrix,
    location_viewMatrix,
    //Lighing
    location_lightPosition[],
    location_lightColor[],
    location_attenuation[],
    location_DLDirection[],
    location_DLColor[],
    location_shineDamper,
    location_reflectivity,
    location_numLights,
    location_numDLights,
    //Fog
    location_skyColor,
    location_density,
    location_gradient,
    //Assets
    location_texturePack,
    location_backgroundTexture,
    location_rTexture,
    location_gTexture,
    location_bTexture,
    location_blendMap,
    location_tiles,
    //Clip Plane
    location_plane,
    //Shadows
    	location_hasShadows,
	    //Shadow Map
	    location_toShadowMapSpace,
	    location_shadowMap,
	    location_shadowDistance,
	    //Cascaded Shadow Maps
	  	location_toCSMSpaces[],
	    location_CSMs[],
	  	location_CSMDepths[],
	  	location_CSMSizes[],
	  	location_cascades;
    
    public TerrainShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
 
    @Override
    protected void bindAttributes() {
    	super.bindFragOutput(0, "out_color");
    	super.bindFragOutput(1, "out_bloomColor");
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoordinates");
        super.bindAttribute(2, "normal");
    }
 
    @Override
    protected void getAllUniformLocations() {
    	//Location
        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        //Lighting
        location_shineDamper = super.getUniformLocation("shineDamper");
        location_reflectivity = super.getUniformLocation("reflectivity");
        //Fog
        location_skyColor = super.getUniformLocation("skyColor");
        location_density = super.getUniformLocation("density");
        location_gradient = super.getUniformLocation("gradient");
        //Assets
        location_texturePack = super.getUniformLocation("texturePack");
        location_backgroundTexture = super.getUniformLocation("backgroundTexture");
        location_rTexture = super.getUniformLocation("rTexture");
        location_gTexture = super.getUniformLocation("gTexture");
        location_bTexture = super.getUniformLocation("bTexture");
        location_blendMap = super.getUniformLocation("blendMap");
        location_tiles = super.getUniformLocation("tiles");
        //Clip Plane
        location_plane = super.getUniformLocation("plane");
        //Shadows
        location_hasShadows = super.getUniformLocation("hasShadows");
        //Shadow Map
        location_toShadowMapSpace = super.getUniformLocation("toShadowMapSpace");
        location_shadowMap = super.getUniformLocation("shadowMap");
        location_shadowDistance = super.getUniformLocation("shadowDistance");
        //Cascaded Shadow Maps
        location_CSMs = new int[MasterRenderer.MAX_CASCADES];
        location_toCSMSpaces = new int[MasterRenderer.MAX_CASCADES];
        location_CSMDepths = new int[MasterRenderer.MAX_CASCADES];
        location_CSMSizes = new int[MasterRenderer.MAX_CASCADES];
        for(int i=0;i<MasterRenderer.MAX_CASCADES;i++){
        	location_CSMs[i] = super.getUniformLocation("CSMs["+ i +"]");
        	location_toCSMSpaces[i] = super.getUniformLocation("toCSMSpaces["+ i +"]");
        	location_CSMDepths[i] = super.getUniformLocation("CSMDepths["+ i +"]");
        	location_CSMSizes[i] = super.getUniformLocation("CSMSizes[" + i + "]");
        }
        location_cascades = super.getUniformLocation("cascades");
        //Lighting
        location_numLights = super.getUniformLocation("numLights");
        location_lightPosition = new int[MAX_POINT_LIGHTS];
        location_lightColor = new int[MAX_POINT_LIGHTS];
        location_attenuation = new int[MAX_POINT_LIGHTS];
        for(int i=0;i<MAX_POINT_LIGHTS;i++){
        	location_lightPosition[i] = super.getUniformLocation("lightPosition[" + i + "]");
            location_lightColor[i] = super.getUniformLocation("lightColor[" + i + "]");
            location_attenuation[i] = super.getUniformLocation("attenuation[" + i + "]");
        }
        location_numDLights = super.getUniformLocation("numDLights");
        location_DLDirection = new int[MAX_DIRECTIONAL_LIGHTS];
        location_DLColor = new int[MAX_DIRECTIONAL_LIGHTS];
        for(int i=0;i<MAX_DIRECTIONAL_LIGHTS;i++){
            location_DLDirection[i] = super.getUniformLocation("DLDirection[" + i + "]");
            location_DLColor[i] = super.getUniformLocation("DLColor[" + i + "]");
        }
    }
    
    public void loadHasShadows(boolean hasShadows){
    	super.loadBoolean(location_hasShadows, hasShadows);
    }
    
    public void loadCSMDepths(float[] depths){
    	for(int i = 0; i < depths.length; i++){
    		super.loadFloat(location_CSMDepths[i], depths[i]);
    	}super.loadInt(location_cascades, depths.length);
    }
    
    public void loadToCSMSpaces(Matrix4f[] toCSMSpaces){
    	for(int i = 0; i < toCSMSpaces.length; i++){
    		super.loadMatrix(location_toCSMSpaces[i], toCSMSpaces[i]);
    	}
    }
    
    public void loadCSMSizes(int[] sizes){
    	for(int i = 0; i < sizes.length; i++){
    		super.loadInt(location_CSMSizes[i], sizes[i]);
    	}
    }
    
    public void loadTiles(int tiles){
    	super.loadInt(location_tiles, tiles);
    }
    
    public void loadFogValues(float density, float gradient){
    	super.loadFloat(location_density, density);
    	super.loadFloat(location_gradient, gradient);
    }
    
    public void loadBooleans(boolean texturePack){
    	super.loadBoolean(location_texturePack, texturePack);
    }
    
    public void connectTextureUnits(){
    	super.loadInt(location_backgroundTexture, 0);
    	super.loadInt(location_rTexture, 1);
    	super.loadInt(location_gTexture, 2);
    	super.loadInt(location_bTexture, 3);
    	super.loadInt(location_blendMap, 4);
    	super.loadInt(location_shadowMap, 5);
    	for(int i = 0; i < MasterRenderer.MAX_CASCADES; i++){
    		super.loadInt(location_CSMs[i], 6+i);
    	}
    }
    
    public void loadShadowDistance(float distance){
    	super.loadFloat(location_shadowDistance, distance);
    }
    
    public void loadToShadowSpace(Matrix4f matrix){
    	super.loadMatrix(location_toShadowMapSpace, matrix);
    }
    
    public void loadClipPlane(Vector4f plane){
    	super.loadVector4f(location_plane, plane);
    }
    
    public void loadSkyColor(Vector3f skycolor){
    	super.loadVector3f(location_skyColor, skycolor);
    }
    
    public void loadShineVariables(float damper,float reflectivity){
        super.loadFloat(location_shineDamper, damper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
     
    public void loadTransformationMatrix(Matrix4f matrix){
        super.loadMatrix(location_transformationMatrix, matrix);
    }
     
    public void loadLights(ArrayList<PointLight> lights, ArrayList<DirectionalLight> DLights){
        for(int i=0;i<lights.size();i++){
        	super.loadVector3f(location_lightPosition[i], lights.get(i).getPosition());
        	super.loadVector3f(location_lightColor[i], lights.get(i).getColor());
        	super.loadVector3f(location_attenuation[i], lights.get(i).getAttenuation());
        }
        for(int i=0;i<DLights.size();i++){
            super.loadVector3f(location_DLDirection[i], DLights.get(i).getDirection());
            super.loadVector3f(location_DLColor[i], DLights.get(i).getColor());
        }
        super.loadInt(location_numLights, lights.size());
        super.loadInt(location_numDLights, DLights.size());
    }
     
    public void loadViewMatrix(Camera camera){
        super.loadMatrix(location_viewMatrix, camera.getViewMatrix());
    }
    
    public void loadProjectionMatrix(Matrix4f projection){
        super.loadMatrix(location_projectionMatrix, projection);
    }
}