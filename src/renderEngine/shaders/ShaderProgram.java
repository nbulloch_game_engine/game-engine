package renderEngine.shaders;
 
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix2f;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
 
public abstract class ShaderProgram {
     
    private int programID;
    private int vertexShaderID;
    private int geometryShaderID;
    private int fragmentShaderID;
     
    private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
    private static FloatBuffer matrixBuffer3f = BufferUtils.createFloatBuffer(9);
    private static FloatBuffer matrixBuffer2f = BufferUtils.createFloatBuffer(4);
     
    public ShaderProgram(String vertexFile, String fragmentFile){
        vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
        fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
        programID = glCreateProgram();
        glAttachShader(programID, vertexShaderID);
        glAttachShader(programID, fragmentShaderID);
        bindAttributes();
        glLinkProgram(programID);
        glValidateProgram(programID);
        getAllUniformLocations();
    }
    
    public ShaderProgram(String vertexFile, String geometryFile, String fragmentFile){
        vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
        geometryShaderID = loadShader(geometryFile, GL_GEOMETRY_SHADER);
        fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
        programID = glCreateProgram();
        glAttachShader(programID, vertexShaderID);
        glAttachShader(programID, geometryShaderID);
        glAttachShader(programID, fragmentShaderID);
        bindAttributes();
        glLinkProgram(programID);
        glValidateProgram(programID);
        getAllUniformLocations();
    }
     
    protected abstract void getAllUniformLocations();
     
    protected int getUniformLocation(String uniformName){
        return glGetUniformLocation(programID, uniformName);
    }
     
    public void start(){
        glUseProgram(programID);
    }
     
    public void stop(){
        glUseProgram(0);
    }
     
    public void clean(){
        stop();
        glDetachShader(programID, vertexShaderID);
        glDetachShader(programID, geometryShaderID);
        glDetachShader(programID, fragmentShaderID);
        glDeleteShader(vertexShaderID);
        glDeleteShader(geometryShaderID);
        glDeleteShader(fragmentShaderID);
        glDeleteProgram(programID);
    }
     
    protected abstract void bindAttributes();
     
    protected void bindAttribute(int attribute, String variableName){
        glBindAttribLocation(programID, attribute, variableName);
    }
    
    protected void bindFragOutput(int attachment, String name){
    	glBindFragDataLocation(programID, attachment, name);
    }
     
    protected void loadFloat(int location, float value){
        glUniform1f(location, value);
    }
    
    protected void loadInt(int location, int value){
    	glUniform1i(location, value);
    }
     
    protected void loadVector4f(int location, Vector4f vector){
        glUniform4f(location,vector.x,vector.y,vector.z,vector.w);
    }
    
    protected void loadVector3f(int location, Vector3f vector){
        glUniform3f(location,vector.x,vector.y,vector.z);
    }
    
    protected void loadVector2f(int location, Vector2f vector){
        glUniform2f(location,vector.x,vector.y);
    }
     
    protected void loadBoolean(int location, boolean value){
        int toLoad = 0;
        if(value){
            toLoad = 1;
        }
        glUniform1i(location, toLoad);
    }
     
    protected void loadMatrix(int location, Matrix4f matrix){
        matrix.store(matrixBuffer);
        matrixBuffer.flip();
        glUniformMatrix4(location, false, matrixBuffer);
    }
    
    protected void loadMatrix(int location, Matrix3f matrix){
        matrix.store(matrixBuffer3f);
        matrixBuffer3f.flip();
        glUniformMatrix3(location, false, matrixBuffer3f);
    }
    
    protected void loadMatrix(int location, Matrix2f matrix){
        matrix.store(matrixBuffer2f);
        matrixBuffer2f.flip();
        glUniformMatrix2(location, false, matrixBuffer2f);
    }
     
    private static int loadShader(String file, int type){
        StringBuilder shaderSource = new StringBuilder();
        try{
        	InputStream in = Class.class.getResourceAsStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = reader.readLine())!=null){
                shaderSource.append(line).append("\n");
            }
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
            System.exit(-1);
        }
        int shaderID = glCreateShader(type);
        glShaderSource(shaderID, shaderSource);
        glCompileShader(shaderID);
        if(glGetShaderi(shaderID, GL_COMPILE_STATUS )== GL_FALSE){
        	System.out.println(file+"!!");
            System.out.println(glGetShaderInfoLog(shaderID, 500));
            System.err.println("Could not compile shader!");
            System.exit(-1);
        }
        return shaderID;
    }
    
    public int getProgram(){
    	return programID;
    }
}