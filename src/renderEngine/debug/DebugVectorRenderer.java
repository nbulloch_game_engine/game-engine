package renderEngine.debug;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix2f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.entities.Camera;
import renderEngine.models.RawModel;
import renderEngine.objConverter.OBJLoader;
import renderEngine.utils.Quaternion;
import renderEngine.utils.Transform;
import renderEngine.utils.Transform2D;

public class DebugVectorRenderer {

	private static final RawModel VECTOR_MODEL = OBJLoader.loadOBJ("vector");
	private static final Quaternion VECTOR_DIRECTION = new Quaternion(0,0,-1, 0);
	private static final ArrayList<Matrix4f> lines = new ArrayList<Matrix4f>();
	private DebugVectorShader shader;
	
	public DebugVectorRenderer(DebugVectorShader shader){
		this.shader = shader;
	}
	
	public void render(Camera camera, Matrix4f projectionMatrix){
		Matrix4f viewProjectionMatrix = Matrix4f.mul(projectionMatrix, camera.getViewMatrix(), null);
		shader.start();
		shader.loadViewProjectionMatrix(viewProjectionMatrix);
		glBindVertexArray(VECTOR_MODEL.getVaoID());
		glEnableVertexAttribArray(0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		for(Matrix4f line : lines){
			shader.loadTransformationMatrix(line);
			glDrawElements(GL11.GL_TRIANGLES, VECTOR_MODEL.getVertexCount(), GL_UNSIGNED_INT, 0);
		}
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
		glDisable(GL_BLEND);
		shader.stop();
	}
	
	public static ArrayList<Matrix4f> getLines(){
		return lines;
	}
	
	public static void addLine(Transform2D transform) {
		Matrix2f m = transform.getMatrix();
		Matrix4f matrix = new Matrix4f();
		matrix.m00 = m.m00;
		matrix.m01 = m.m01;
		matrix.m10 = m.m10;
		matrix.m11 = m.m11;
		matrix.translate(transform.getPosition());
		
		lines.add(matrix);
	}
	
	public static void addLine(Vector3f direction, Vector3f position) {
		Quaternion quat = Quaternion.rotationAsQuaternion(VECTOR_DIRECTION.getVector(), direction);
		Matrix4f transformation = new Matrix4f();
		transformation.translate(position);
		
		lines.add(Matrix4f.mul(transformation, quat.getMatrix4f(), null));
	}
	
	public static void addLine(Quaternion rotation, Vector3f position) {
		Quaternion quat = Quaternion.multiply(VECTOR_DIRECTION, rotation, null);
		Matrix4f transformation = new Matrix4f();
		transformation.translate(position);
		
		lines.add(Matrix4f.mul(transformation, quat.normalize().getMatrix4f(), null));
	}
	
	public static void addLine(Transform tranform) {
		Quaternion quat = Quaternion.multiply(VECTOR_DIRECTION, tranform.getQuaternion(), null);
		Matrix4f transformation = new Matrix4f();
		transformation.scale(tranform.getScale());
		transformation.translate(tranform.getPosition());
		
		lines.add(Matrix4f.mul(transformation, quat.normalize().getMatrix4f(), null));
	}
	
	public static void addLine(Matrix4f line) {
		lines.add(line);
	}
	
	public static void removeLine(Matrix4f line) {
		lines.remove(line);
	}
	
	public static void clearLines() {
		lines.clear();
	}
}
