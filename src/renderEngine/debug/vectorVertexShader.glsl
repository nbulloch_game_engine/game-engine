#version 150

in vec3 position;

out vec3 pos;

uniform mat4 modelMatrix;
uniform mat4 viewProjectionMatrix;

void main(void){

	gl_Position = viewProjectionMatrix * modelMatrix * vec4(position, 1.0);
	pos = vec3((modelMatrix * vec4(position, 1.0)).xyz);
	
}