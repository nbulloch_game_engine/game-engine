package renderEngine.debug;

import org.lwjgl.util.vector.Matrix4f;
import renderEngine.shaders.ShaderProgram;

public class DebugLineShader extends ShaderProgram {
	
	private static final String VERTEX_FILE = "/renderEngine/debug/lineVertexShader.glsl";
	private static final String FRAGMENT_FILE = "/renderEngine/debug/lineFragmentShader.glsl";
	
	private int location_transformationMatrix;
	private int location_viewProjectionMatrix;
	private int location_id;
	 
	public DebugLineShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("modelMatrix");
		location_viewProjectionMatrix = super.getUniformLocation("viewProjectionMatrix");
		location_id = super.getUniformLocation("id");
	}
	
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindFragOutput(0, "out_color");
		super.bindFragOutput(1, "out_bloom");
	}
	
	public void loadId(int id) {
		super.loadInt(location_id, id);
	}
	
	public void loadViewProjectionMatrix(Matrix4f viewProjectionMatrix){
		super.loadMatrix(location_viewProjectionMatrix, viewProjectionMatrix);
	}
	
	public void loadTransformationMatrix(Matrix4f matrix){
        super.loadMatrix(location_transformationMatrix, matrix);
    }
}
