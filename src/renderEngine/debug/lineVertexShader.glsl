#version 150

in vec3 position;

out vec3 pos;
out vec3 color;

uniform mat4 modelMatrix;
uniform mat4 viewProjectionMatrix;
uniform int id;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void){

	vec2 idv = vec2(id);
	color = vec3(rand(idv), rand(idv*2), rand(idv*3));
	gl_Position = viewProjectionMatrix * modelMatrix * vec4(position, 1.0);
	pos = vec3((modelMatrix * vec4(position, 1.0)).xyz);
	
}