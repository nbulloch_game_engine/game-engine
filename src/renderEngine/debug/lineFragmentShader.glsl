#version 150

in vec3 pos;
in vec3 color;

out vec4 out_color;
out vec4 out_bloom;

void main(void){
	
	out_color = vec4(color.rgb, 1.0);
	out_bloom = vec4(0);
}