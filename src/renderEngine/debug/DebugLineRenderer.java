package renderEngine.debug;

import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.entities.Camera;
import renderEngine.models.RawModel;
import renderEngine.objConverter.OBJLoader;
import renderEngine.utils.Quaternion;

public class DebugLineRenderer {

	private static final RawModel VECTOR_MODEL = OBJLoader.loadOBJ("line");
	private static final Quaternion VECTOR_DIRECTION = new Quaternion(1,0,0,0);
	private static final ArrayList<Matrix4f> lines = new ArrayList<Matrix4f>();
	private DebugLineShader shader;
	
	public DebugLineRenderer(DebugLineShader shader){
		this.shader = shader;
	}
	
	public void render(Camera camera, Matrix4f projectionMatrix){
		Matrix4f viewProjectionMatrix = Matrix4f.mul(projectionMatrix, camera.getViewMatrix(), null);
		shader.start();
		shader.loadViewProjectionMatrix(viewProjectionMatrix);
		glBindVertexArray(VECTOR_MODEL.getVaoID());
		glEnableVertexAttribArray(0);
		for(int i = 0; i < lines.size(); i++){
			shader.loadTransformationMatrix(lines.get(i));
			shader.loadId(i);
			glDrawElements(GL11.GL_TRIANGLES, VECTOR_MODEL.getVertexCount(), GL_UNSIGNED_INT, 0);
		}
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
		shader.stop();
	}
	
	public static ArrayList<Matrix4f> getLines(){
		return lines;
	}
	
	public static void addLine(Vector3f a, Vector3f b) {
		Vector3f direction = Vector3f.sub(b, a, null);
		Quaternion quat = Quaternion.rotationAsQuaternion(VECTOR_DIRECTION.getVector(), direction);
		Matrix4f transformation = new Matrix4f();
		transformation.translate(a);
		Matrix4f.mul(transformation, quat.getMatrix4f(), transformation);
		transformation.scale(new Vector3f(direction.length(),1,1));
		for(Matrix4f mat : lines) {
			if(mat.equals(transformation))
				return;
		}
		lines.add(transformation);
	}
	
	public static void clearLines() {
		lines.clear();
	}
}
