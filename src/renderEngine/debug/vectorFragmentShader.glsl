#version 150

in vec3 pos;

out vec4 out_color;
out vec4 out_bloom;

void main(void){
	
	out_color = vec4(sin(pos.x)/2+0.5, sin(pos.y)/2+0.5, sin(pos.z)/2+0.5, 0.5);
	out_bloom = vec4(0);
}