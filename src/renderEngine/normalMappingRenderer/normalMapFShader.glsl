#version 130

const int pcfCount = 2;
const int totalTexels = (pcfCount * pcfCount + 1) * (pcfCount * pcfCount + 1);
const int maxCascades = 5;
in vec4 csmCoords[maxCascades];
in vec4 shadowCoords;
in float depth;
uniform sampler2D CSMs[maxCascades];
uniform sampler2D shadowMap;
uniform float CSMDepths[maxCascades];
uniform int CSMSizes[maxCascades]; 
uniform int cascades;
uniform int hasShadows;

float calculateShadows(vec3 unitNormal, vec3 sunDirection){
	float total = 0;
	float lightFactor = 1.0;
	
	float slope = dot(sunDirection, unitNormal);
	float bias = 1 - slope*slope;
	if(slope > 0){
		bias = -bias;
	}
	
	int cascade;
	if(hasShadows==1){
		for(int i = 0; i < cascades; i++){
			if(depth>CSMDepths[i]){
				cascade = i;
			}else{
				break;
			}
		}
		bias = 0.0045 * bias;
		float mapSize = 8192.0*pcfCount;
		if(cascades!=0){
			mapSize = CSMSizes[cascade]*pcfCount;
		}
		float texelSize = 1.0 / mapSize;
		float objectNearestLight = 0;
		for(int x = -pcfCount; x <= pcfCount; x++){
			for(int y = -pcfCount; y <= pcfCount; y++){
				if(cascades==0){
					objectNearestLight = texture(shadowMap, shadowCoords.xy + vec2(x, y) * texelSize).r;
					if(shadowCoords.z > objectNearestLight){
						total++;
					}
				}else{
					objectNearestLight = texture(CSMs[cascade], csmCoords[cascade].xy + vec2(x, y) * texelSize).r;
					if(csmCoords[cascade].z >= objectNearestLight + bias){
						total++;
					}
				}
			}
		}
		
		total /= totalTexels * 2;
		
		if(cascade==-1){
			lightFactor = 1.0 - (total * shadowCoords.w);
		}else{
			lightFactor = 1.0 - total;
		}
	}
	return lightFactor;
}


uniform float shineDamper;
uniform float reflectivity;

void doLighting(inout vec3 totalDiffuse, inout vec3 totalSpecular, vec3 unitNormal, vec3 unitToCamera, float attFactor, vec3 toLightVector, vec3 color){
	vec3 unitLightVector = normalize(toLightVector);	
	float nDotl = dot(unitNormal,unitLightVector);
	float brightness = max(nDotl,0.0);
	vec3 lightDirection = -unitLightVector;
	vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);
	float specularFactor = dot(reflectedLightDirection, unitToCamera);
	specularFactor = max(specularFactor,0.0);
	float dampedFactor = pow(specularFactor, shineDamper);
	totalDiffuse += (brightness * color)/attFactor;
	totalSpecular += (dampedFactor * reflectivity * color)/attFactor;
}


out vec4 out_bloomColor;
uniform sampler2D lightMap;
uniform sampler2D glowMap;
uniform float bloomWidth;
uniform float glowMapFactor;
uniform float glowOffset;
uniform float glowSpeed;
uniform int usesLightMap;
uniform int useGlowMap;
uniform int tiles;

void glow(float lumaBrightness, inout vec3 totalDiffuse, inout vec3 totalSpecular, vec4 textureColor, vec2 textureCoords){
	out_bloomColor = vec4(0.0);
	if(usesLightMap==1) {
		vec2 glowCoords = textureCoords/tiles;
		vec4 lightMapInfo = texture(lightMap, textureCoords);
		if(lightMapInfo.g > 0.5){
			if(useGlowMap==1){
				totalDiffuse = vec3(1.0);
				totalSpecular = vec3(0.0);
				
				vec3 glowColor = vec3(
					texture(glowMap, vec2(glowCoords.x + glowOffset*25.4*glowSpeed, glowCoords.y - glowOffset*29.6*glowSpeed)).r,
					texture(glowMap, vec2(glowCoords.x - glowOffset*27.6*glowSpeed, glowCoords.y - glowOffset*22.5*glowSpeed)).g,
					texture(glowMap, vec2(glowCoords.x + glowOffset*23.3*glowSpeed, glowCoords.y + glowOffset*26.7*glowSpeed)).b
					);
				
				float glowFactor = (glowColor.r + glowColor.g + glowColor.b)/3f;
				
				out_bloomColor = textureColor * (glowFactor - glowMapFactor) / (1 - glowMapFactor);
			}else{
				out_bloomColor = textureColor * lumaBrightness / 2;
			}
			out_bloomColor.a = bloomWidth;
		}
	}
}


uniform sampler2D specularMap;
uniform int usesSpecularMap;

void useSpecularMap(inout vec3 totalSpecular, vec2 textureCoords){
	if(usesSpecularMap==1){
		vec4 mapInfo = texture(specularMap, textureCoords);
		totalSpecular.rgb *= mapInfo.rgb;
	}
}

void checkTransparency(vec4 textureColor){
	if(textureColor.a<0.5){
		discard;
	}
}


const int maxLights = 4;
const int maxDLights = 6;
in vec3 toLightVector[maxLights];
in vec3 viewDLDirection[maxDLights];
in vec3 toCameraVector;
in vec2 textureCoords;
in vec2 coords;
in float visibility;
out vec4 out_color;
uniform sampler2D modelTexture;
uniform sampler2D normalMap;
uniform vec3 lightColor[maxLights];
uniform vec3 attenuation[maxLights];
uniform vec3 DLColor[maxDLights];
uniform vec3 skyColor;
uniform float translucency;
uniform int numLights;
uniform int numDLights;

void main(){
	//Transparency
	vec4 textureColor = texture(modelTexture, textureCoords);
	checkTransparency(textureColor);
	
	//Shadows
	vec4 normalMapValue = texture(normalMap, coords, -1.0);
	vec3 unitNormal = normalize(2.0 * normalMapValue.rgb - 1.0);
	//unitNormal = normalMapValue.rgb;
	float lightFactor = calculateShadows(unitNormal, viewDLDirection[0]);
	
	//Lighting
	vec3 unitToCamera = normalize(toCameraVector);
	vec3 totalDiffuse = vec3(0.0);
	vec3 totalSpecular = vec3(0.0);
	for(int i=0;i<numLights;i++){
		float distance = length(toLightVector[i]);
		float attFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
		doLighting(totalDiffuse, totalSpecular, unitNormal, unitToCamera, attFactor, toLightVector[i], lightColor[i]);
	}
	for(int i=0;i<numDLights;i++){
		doLighting(totalDiffuse, totalSpecular, unitNormal, unitToCamera, 1.0, -viewDLDirection[i], DLColor[i]);
	}
	totalDiffuse = max(totalDiffuse * lightFactor, 0.3);
	
	//Special Effects
	useSpecularMap(totalSpecular, coords);
	float lumaBrightness = (textureColor.r * 0.2126) + (textureColor.g * 0.7152) + (textureColor.b * 0.0722);
	glow(lumaBrightness, totalDiffuse, totalSpecular, textureColor, coords);
	
	//Final Lighing
	vec3 secondaryDiffuse = vec3((1 - lumaBrightness)/20) * totalDiffuse;
	out_color = vec4(totalDiffuse,1.0) * textureColor + vec4(secondaryDiffuse, 1.0) + vec4(totalSpecular, 1.0);
	out_color.a = translucency;
	out_color = mix(vec4(skyColor,1.0), out_color, visibility);
}

