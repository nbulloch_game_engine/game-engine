package renderEngine.normalMappingRenderer;
 
import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.entities.Camera;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.PointLight;
import renderEngine.renderEngine.MasterRenderer;
import renderEngine.shaders.ShaderProgram;
 
public class NormalMappingShader extends ShaderProgram{
     
    private static final int MAX_POINT_LIGHTS = 4;
    private static final int MAX_DIRECTIONAL_LIGHTS = 6;
     
    private static final String VERTEX_FILE = "/renderEngine/normalMappingRenderer/normalMapVShader.glsl";
    private static final String FRAGMENT_FILE = "/renderEngine/normalMappingRenderer/normalMapFShader.glsl";
    
    private int
    //Clip Plane
	location_plane,//unknown effect
	
	//Location
    location_transformationMatrix,
    location_projectionMatrix,
    location_viewMatrix,
    location_camPos,
	
	//Model Assets
    location_modelTexture,
    location_normalMap,
    	//Texture Atlas
	    location_numberOfRows,
	    location_offset,
	    //Material Properties
	    location_shineDamper,
	    location_reflectivity,
	
	//Lighting
		//Point Lights
	    location_lightPosition[],
	    location_lightColor[],
	    location_attenuation[],
	    location_numLights,
	    //Directional Lights
	    location_DLDirection[],
	    location_DLColor[],
	    location_numDLights,
	    
	    
	//Shadows
	location_hasShadows,
		//Cascaded Shadow Maps
		location_toCSMSpaces[],
  	    location_CSMs[],
	    location_CSMDepths[],
	    location_CSMSizes[],
	    location_cascades,
	    //Shadow Map
	    location_toShadowMapSpace,
	    location_shadowMap,
	    location_shadowDistance,
	    
	
	//Special Effects
	    //Variable Specularity
	    location_specularMap,
	    location_usesSpecularMap,
	    //Glow
	    location_usesLightMap,
	    location_lightMap,//green = glow
	    location_useGlowMap,
	    location_glowMap,//glow pattern
	    location_glowOffset,//glow animation
	    location_bloomWidth,
	    location_glowMapFactor,//amount the glow pattern shows through
	    location_glowSpeed,
	    //Fog
	    location_skyColor,
	    location_density,
	    location_gradient,
    	//Translucency
    	location_translucency;
    
 
    public NormalMappingShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
 
    @Override
    protected void bindAttributes() {
    	super.bindFragOutput(0, "out_color");
    	super.bindFragOutput(1, "out_bloomColor");
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoordinates");
        super.bindAttribute(2, "normal");
        super.bindAttribute(3, "tangent");
    }
 
    @Override
    protected void getAllUniformLocations() {
    //Clip Plane
        location_plane = super.getUniformLocation("plane");
    //Location
        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        //Tangent Space Lighting
        location_camPos = super.getUniformLocation("camPos");
        
   //Model Assets
        location_modelTexture = super.getUniformLocation("modelTexture");
        location_normalMap = super.getUniformLocation("normalMap");
        //Texture Atlas
        location_numberOfRows = super.getUniformLocation("numberOfRows");
        location_offset = super.getUniformLocation("offset");
        //Material Properties
        location_shineDamper = super.getUniformLocation("shineDamper");
        location_reflectivity = super.getUniformLocation("reflectivity");
   //Lighting
        //Point Lights
        location_lightPosition = new int[MAX_POINT_LIGHTS];
        location_lightColor = new int[MAX_POINT_LIGHTS];
        location_attenuation = new int[MAX_POINT_LIGHTS];
        for(int i=0;i<MAX_POINT_LIGHTS;i++){
        	location_lightPosition[i] = super.getUniformLocation("lightPosition[" + i + "]");
            location_lightColor[i] = super.getUniformLocation("lightColor[" + i + "]");
            location_attenuation[i] = super.getUniformLocation("attenuation[" + i + "]");
        }
        location_numLights = super.getUniformLocation("numLights");
        //Directional Lights
        location_DLDirection = new int[MAX_DIRECTIONAL_LIGHTS];
        location_DLColor = new int[MAX_DIRECTIONAL_LIGHTS];
        for(int i=0;i<MAX_DIRECTIONAL_LIGHTS;i++){
            location_DLDirection[i] = super.getUniformLocation("DLDirection[" + i + "]");
            location_DLColor[i] = super.getUniformLocation("DLColor[" + i + "]");
        }
        location_numDLights = super.getUniformLocation("numDLights");
    //Shadows
        location_hasShadows = super.getUniformLocation("hasShadows");
        //Cascaded Shadow Maps
        location_CSMs = new int[MasterRenderer.MAX_CASCADES];
        location_toCSMSpaces = new int[MasterRenderer.MAX_CASCADES];
        location_CSMDepths = new int[MasterRenderer.MAX_CASCADES];
        location_CSMSizes = new int[MasterRenderer.MAX_CASCADES];
        for(int i=0;i<MasterRenderer.MAX_CASCADES;i++){
        	location_CSMs[i] = super.getUniformLocation("CSMs["+ i +"]");
        	location_toCSMSpaces[i] = super.getUniformLocation("toCSMSpaces["+ i +"]");
        	location_CSMDepths[i] = super.getUniformLocation("CSMDepths["+ i +"]");
        	location_CSMSizes[i] = super.getUniformLocation("CSMSizes[" + i + "]");
        }
        location_cascades = super.getUniformLocation("cascades");
        //Shadow Map
        location_toShadowMapSpace = super.getUniformLocation("toShadowMapSpace");
        location_shadowMap = super.getUniformLocation("shadowMap");
        location_shadowDistance = super.getUniformLocation("shadowDistance");
    //Special Effects
        //Variable Specularity
        location_specularMap = super.getUniformLocation("specularMap");
        location_usesSpecularMap = super.getUniformLocation("usesSpecularMap");
        //Glow
        location_lightMap = super.getUniformLocation("lightMap");
        location_usesLightMap = super.getUniformLocation("usesLightMap");
        location_glowMap = super.getUniformLocation("glowMap");
        location_glowOffset = super.getUniformLocation("glowOffset");
        location_useGlowMap = super.getUniformLocation("useGlowMap");
        location_bloomWidth = super.getUniformLocation("bloomWidth");
        location_glowMapFactor = super.getUniformLocation("glowMapFactor");
        location_glowSpeed = super.getUniformLocation("glowSpeed");
        //Fog
        location_skyColor = super.getUniformLocation("skyColor");
        location_density = super.getUniformLocation("density");
        location_gradient = super.getUniformLocation("gradient");
        //Translucency
        location_translucency = super.getUniformLocation("translucency");
    }
    
    protected void loadTranslucency(float translucency){
    	super.loadFloat(location_translucency, translucency);
    }
    
    protected void loadCSMDepths(float[] depths){
    	for(int i = 0; i < depths.length; i++){
    		super.loadFloat(location_CSMDepths[i], depths[i]);
    	}super.loadInt(location_cascades, depths.length);
    }
    
    protected void loadToCSMSpaces(Matrix4f[] toCSMSpaces){
    	for(int i = 0; i < toCSMSpaces.length; i++){
    		super.loadMatrix(location_toCSMSpaces[i], toCSMSpaces[i]);
    	}
    }
    
    protected void loadCSMSizes(int[] sizes){
    	for(int i = 0; i < sizes.length; i++){
    		super.loadInt(location_CSMSizes[i], sizes[i]);
    	}
    }
    
    protected void loadBloomWidth(float bloomWidth){
    	super.loadFloat(location_bloomWidth, bloomWidth);
    }
    
    protected void loadGlowMapFactor(float factor){
    	super.loadFloat(location_glowMapFactor, factor);
    }
    
    protected void loadGlowSpeed(float speed){
    	super.loadFloat(location_glowSpeed, speed);
    }
    
    protected void loadUseGlowMap(boolean useGlowMap){
    	super.loadBoolean(location_useGlowMap, useGlowMap);
    }
    
    protected void loadHasShadows(boolean hasShadows){
    	super.loadBoolean(location_hasShadows, hasShadows);
    }
    
    protected void loadFogValues(float density, float gradient){
    	super.loadFloat(location_density, density);
    	super.loadFloat(location_gradient, gradient);
    }
    
    protected void connectTextureUnits(){
        super.loadInt(location_modelTexture, 0);
        super.loadInt(location_normalMap, 1);
        super.loadInt(location_specularMap, 2);
        super.loadInt(location_lightMap, 3);
        super.loadInt(location_glowMap,4);
        super.loadInt(location_shadowMap, 5);
        for(int i = 0; i < MasterRenderer.MAX_CASCADES; i++){
    		super.loadInt(location_CSMs[i], 6+i);
    	}
    }
    
    protected void loadCamPos(Vector3f camPos){
    	super.loadVector3f(location_camPos, camPos);
    }
    
    protected void loadUsesLightMap(boolean value){
    	super.loadBoolean(location_usesLightMap, value);
    }
    
    protected void loadGlowOffset(float value){
		super.loadFloat(location_glowOffset, value);
	}
    
    protected void loadUsesSpecularMap(boolean value){
    	super.loadBoolean(location_usesSpecularMap, value);
    }
    
    protected void loadShadowDistance(float distance){
    	super.loadFloat(location_shadowDistance, distance);
    }
    
    protected void loadToShadowMapSpace(Matrix4f matrix){
    	super.loadMatrix(location_toShadowMapSpace, matrix);
    }
    
    protected void loadClipPlane(Vector4f plane){
        super.loadVector4f(location_plane, plane);
    }
     
    protected void loadNumberOfRows(int numberOfRows){
        super.loadFloat(location_numberOfRows, numberOfRows);
    }
     
    protected void loadOffset(float x, float y){
        super.loadVector2f(location_offset, new Vector2f(x,y));
    }
     
    protected void loadSkyColor(Vector3f skyColor){
        super.loadVector3f(location_skyColor, skyColor);
    }
     
    protected void loadShineVariables(float damper,float reflectivity){
        super.loadFloat(location_shineDamper, damper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
     
    protected void loadTransformationMatrix(Matrix4f matrix){
        super.loadMatrix(location_transformationMatrix, matrix);
    }
     
    protected void loadLights(List<PointLight> lights, List<DirectionalLight> DLights){
        for(int i=0;i<lights.size();i++){
        	super.loadVector3f(location_lightPosition[i], lights.get(i).getPosition());
            super.loadVector3f(location_lightColor[i], lights.get(i).getColor());
            super.loadVector3f(location_attenuation[i], lights.get(i).getAttenuation());
        }
        for(int i=0;i<DLights.size();i++){
            super.loadVector3f(location_DLDirection[i], DLights.get(i).getDirection());
            super.loadVector3f(location_DLColor[i], DLights.get(i).getColor());
        }
        super.loadInt(location_numLights, lights.size());
        super.loadInt(location_numDLights, DLights.size());
    }
     
    protected void loadViewMatrix(Camera camera){
        super.loadMatrix(location_viewMatrix, camera.getViewMatrix());
    }
     
    protected void loadProjectionMatrix(Matrix4f projection){
        super.loadMatrix(location_projectionMatrix, projection);
    }
}