#version 140

in vec3 position;
in vec3 normal;
in vec3 tangent;
uniform mat4 transformationMatrix;

vec4 transformPoint(out vec3 surfaceNormal, out vec3 tangentNormal){
	vec4 worldPosition;
	worldPosition = transformationMatrix * vec4(position,1.0);
	surfaceNormal = (transformationMatrix * vec4(normal,0.0)).xyz; 
	tangentNormal = (transformationMatrix * vec4(tangent, 0.0)).xyz;
	return worldPosition;
}


in vec2 textureCoordinates;
out vec2 textureCoords;
out vec2 coords;//no tile
out float depth;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec2 offset;
uniform float numberOfRows;

vec4 setPosition(vec4 worldPosition){
	vec4 viewPosition = viewMatrix * worldPosition;
	gl_Position = projectionMatrix * viewPosition;
	depth = gl_Position.z;
	textureCoords = (textureCoordinates/numberOfRows) + offset;
	coords = textureCoordinates;
	return viewPosition;
}

mat3 buildTangentMatrix(vec3 surfaceNormal, vec3 surfaceTangent){
	vec3 norm = normalize(surfaceNormal);
	vec3 tang = normalize(surfaceTangent);
	vec3 bitang = normalize(cross(norm, tang));
	
	mat3 toTangentSpace = transpose(mat3(tang, bitang, norm));
	return toTangentSpace;
}


out float visibility;
uniform float density;
uniform float gradient;

float calculateFog(in vec4 viewPosition){
	float distance = length(viewPosition.xyz);
	visibility = exp(-pow(distance*density,gradient));
	visibility = clamp(visibility,0.0,1.0);
	return distance;
}


uniform float shadowDistance;
const float transitionDistance = 10.0;

float getShadowDistance(in float distance){
	distance = distance - (shadowDistance - transitionDistance);
	distance = distance / transitionDistance;
	return clamp(1.0-distance, 0.0, 1.0);
}


const int maxLights = 4;
const int maxDLights = 6;
out vec3 toCameraVector;
out vec3 toLightVector[maxLights];
out vec3 viewDLDirection[maxDLights];
uniform vec3 lightPosition[maxLights];
uniform vec3 DLDirection[maxDLights];
uniform vec3 camPos;
uniform int numLights;
uniform int numDLights;

void tangentSpaceLighting(mat3 tangentSpace, vec4 worldPosition){
	for(int i=0; i<numLights; i++){
		toLightVector[i] = tangentSpace * (lightPosition[i] - worldPosition.xyz);
	}for(int i=0; i<numDLights; i++){
		viewDLDirection[i] = tangentSpace * DLDirection[i];
	}
	toCameraVector = tangentSpace * (camPos - worldPosition.xyz);
}


const int maxCascades = 5;
out vec4 csmCoords[maxCascades];
uniform mat4 toCSMSpaces[maxCascades];
uniform int cascades;

void getCSMPositions(vec4 worldPosition){
	for(int i = 0; i < cascades; i++){
		vec4 csmPosition = toCSMSpaces[i] * worldPosition;
		csmCoords[i] = csmPosition/2+0.5;
	}
}


out vec4 shadowCoords;
uniform mat4 toShadowMapSpace;
uniform vec4 plane;

void main(){
	//World Space
	vec3 surfaceNormal, tangentNormal;
	vec4 worldPosition = transformPoint(surfaceNormal, tangentNormal);
	
	//Clip
	gl_ClipDistance[0] = dot(worldPosition, plane);
	
	//View Space
	vec4 viewPosition = setPosition(worldPosition);
	float distance = calculateFog(viewPosition);
	
	//Tangent Space
	mat3 tangentSpace = buildTangentMatrix(surfaceNormal, tangentNormal);
	tangentSpaceLighting(tangentSpace, worldPosition);
	
	//Shadow Space
	shadowCoords = toShadowMapSpace * worldPosition;
	shadowCoords.w = getShadowDistance(distance);
	getCSMPositions(worldPosition);
}