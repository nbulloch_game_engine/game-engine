package renderEngine.normalMappingRenderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import java.util.ArrayList;
import java.util.HashMap;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Matrix4f;
import renderEngine.entities.Entity;
import renderEngine.entities.Scene;
import renderEngine.models.RawModel;
import renderEngine.models.TexturedModel;
import renderEngine.renderEngine.MasterRenderer;
import renderEngine.shadows.cascadedShadowMap.CsmMasterRenderer;
import renderEngine.shadows.shadowMap.ShadowBox;
import renderEngine.textures.ModelTexture;

public class NormalMappingRenderer {

	private NormalMappingShader shader;
	private float offset = 0;

	public NormalMappingRenderer(Matrix4f projectionMatrix) {
		this.shader = new NormalMappingShader();
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.connectTextureUnits();
		shader.stop();
	}

	public void render(HashMap<TexturedModel, ArrayList<Entity>> entities, Scene scene, Matrix4f toShadowSpace, CsmMasterRenderer shadowRenderer) {
		offset++;
		offset%=5000;
		shader.start();
		shader.loadGlowOffset(offset/5000);
		prepare(scene);
		if(scene.hasShadows()){
			if(scene.hasCsmShadows()){
				shader.loadToCSMSpaces(shadowRenderer.getToShadowSpaces());
				shader.loadCSMDepths(shadowRenderer.getDepths());
				shader.loadCSMSizes(shadowRenderer.getCascadeSizes());
			}else{
				shader.loadToShadowMapSpace(toShadowSpace);
				shader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
			}
		}
		shader.loadFogValues(scene.getFogDensity(), scene.getFogGradient());
		shader.loadCamPos(scene.getCamera().getPosition());
		for(TexturedModel model : entities.keySet()){
			prepareTexturedModel(model, scene.hasShadows());
			ArrayList<Entity> batch = entities.get(model);
			for (Entity entity : batch) {
				prepareInstance(entity);
				glDrawElements(GL11.GL_TRIANGLES, model.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
			}
			unbindTexturedModel();
		}
		shader.stop();
	}
	
	public void renderTranslucent(HashMap<TexturedModel, ArrayList<Entity>> entities, Scene scene, Matrix4f toShadowMapSpace, CsmMasterRenderer shadowRenderer) {
		shader.start();
		shader.loadGlowOffset(offset/20000);
		prepare(scene);
		if(scene.hasShadows()){
			if(scene.hasCsmShadows()){
				shader.loadToCSMSpaces(shadowRenderer.getToShadowSpaces());
				shader.loadCSMDepths(shadowRenderer.getDepths());
				shader.loadCSMSizes(shadowRenderer.getCascadeSizes());
			}else{
				shader.loadToShadowMapSpace(toShadowMapSpace);
				shader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
			}
		}
		shader.loadFogValues(scene.getFogDensity(), scene.getFogGradient());
		shader.loadCamPos(scene.getCamera().getPosition());
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		for(TexturedModel model : entities.keySet()){
			prepareTexturedModel(model, scene.hasShadows());
			ArrayList<Entity> batch = entities.get(model);
			for (Entity entity : batch) {
				prepareInstance(entity);
				glDrawElements(GL11.GL_TRIANGLES, model.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
			}
			unbindTexturedModel();
		}
		glDisable(GL_BLEND);
		shader.stop();
	}
	
	public void clean(){
		shader.clean();
	}

	private void prepareTexturedModel(TexturedModel model, boolean shadows) {
		RawModel rawModel = model.getModel();
		glBindVertexArray(rawModel.getVaoID());
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		ModelTexture texture = model.getTexture();
		shader.loadNumberOfRows(texture.getNumberOfRows());
		if (texture.hasTransparency()) MasterRenderer.disableCulling();
		shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, model.getTexture().getID());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, model.getTexture().getNormalMap());
		shader.loadUsesSpecularMap(texture.hasSpecularMap());
		if(texture.hasSpecularMap()){
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, texture.getSpecularMap());
		}
		shader.loadUsesLightMap(texture.hasLightMap());
		if(texture.hasLightMap()){
			shader.loadBloomWidth(texture.getBloomWidth());
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, texture.getLightMap());
			shader.loadUseGlowMap(texture.useGlowMap());
			if(texture.useGlowMap()){
				glActiveTexture(GL13.GL_TEXTURE4);
				glBindTexture(GL11.GL_TEXTURE_2D, texture.getGlowMap());
				shader.loadGlowMapFactor(texture.getGlowMapFactor());
				shader.loadGlowSpeed(texture.getGlowSpeed());
			}
		}
		shader.loadHasShadows(model.hasShadows()&&shadows);
	}

	private void unbindTexturedModel() {
		MasterRenderer.enableCulling();
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
		glBindVertexArray(0);
	}

	private void prepareInstance(Entity entity) {
		shader.loadTransformationMatrix(entity.getTransformation());
		shader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset());
		shader.loadTranslucency(entity.getTranslucency());
	}

	private void prepare(Scene scene) {
		shader.loadClipPlane(scene.getClipPlane());
		shader.loadSkyColor(scene.getSkyColor());
		shader.loadLights(scene.getLights(), scene.getDLights());
		shader.loadViewMatrix(scene.getCamera());
	}
}
